#!lua

-- Projet ----------------------------------------------------------------------

function DefineGameProject(_projectName)

    local localProjectDir = GamesName.."/".._projectName.."/"

    project (_projectName)
        location (BuildDir..localProjectDir)

        filter {}
            kind        "WindowedApp"
            SetCppLanguage()

            targetdir   (BinDir)
            debugdir    (RootDir)

            SetCharacterSet()

            CommonCppFileSettings(SourcesDir..localProjectDir)

            includedirs {SourcesDir..EngineName, SourcesDir..localProjectDir}

            libdirs     {BinDir}
            flags       {"NoMinimalRebuild", "MultiProcessorCompile"}

            LinkEngineLib()

            SetTargetSuffixes()

            entrypoint  "mainCRTStartup"

        FilterConfiguration("Release", function()
            warnings    "Extra"
            optimize    "Full"
            flags       {"FatalWarnings"}
        end )
        FilterConfiguration("Debug", function()
            symbols     "On"
        end )
        filter {}
end
