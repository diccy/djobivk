#!lua

-- Projet ----------------------------------------------------------------------

function DefineTestsProject(_projectName, _projectToTestName)

    local localProjectDir = TestsName.."/".._projectName.."/"

    project (_projectName)
        location (BuildDir..localProjectDir)

        filter {}
            kind        "WindowedApp"
            SetCppLanguage()

            targetdir   (BinDir)
            debugdir    (RootDir)

            SetCharacterSet()

            CommonCppFileSettings(SourcesDir..localProjectDir)

            includedirs {SourcesDir..EngineName, SourcesDir..localProjectDir}

            libdirs     {BinDir}
            flags       {"NoMinimalRebuild", "MultiProcessorCompile"}

            LinkEngineLib()

            SetTargetSuffixes()

            entrypoint  "mainCRTStartup"

        FilterConfiguration("Release", function()
            warnings    "Extra"
            optimize    "Full"
            flags       {"FatalWarnings"}
        end )
        FilterConfiguration("Debug", function()
            symbols     "On"
        end )
        filter {}

        ForEachSupportedPlatformAndConfig( function(_platform, _config)
            links (_projectToTestName..GetSuffix(_platform, _config))
        end )
        filter {}
end
