#!lua

-- Projet Engine ---------------------------------------------------------------

local engineLibList = {"vulkan", "glfw", "glm", "stb", "imgui"}

function DefineEngineProject()

    project (EngineName)
        location (BuildDir..EngineName)

        filter {}
            kind        "StaticLib"
            SetCppLanguage()

            targetdir   (BinDir)
            debugdir    (RootDir)
            targetname  (EngineName)

            SetCharacterSet()

            CommonCppFileSettings(SourcesDir..EngineName.."/")

            libdirs     {}
            flags       {"NoMinimalRebuild", "MultiProcessorCompile"}

            IncludeExternalLibs(engineLibList)
            ImplSourcesExternalLibs(engineLibList)

            SetTargetSuffixes()

        FilterConfiguration("Release", function()
            warnings    "Extra"
            optimize    "Full"
            flags       {"FatalWarnings"}
        end )
        FilterConfiguration("Debug", function()
            warnings    "Extra"
            symbols     "On"
        end )
        filter {}
end


-- Link ------------------------------------------------------------------------

function LinkEngineLib()

    LinkExternalLibs(engineLibList)

    ForEachSupportedPlatformAndConfig( function(_p, _c)
        dependson   { EngineName }
        links       { EngineName..GetSuffix(_p, _c) }
    end )
    filter {}
end
