#!lua

table_insert = table.insert
os_getenv = os.getenv


-- Common ----------------------------------------------------------------------

local function IncludeExternalLib(_names)

    includes = {}
    for _, name in ipairs(_names) do
        table_insert(includes, ExternalDir..name.."/include")
    end
    includedirs (includes)
end

local function LinkExternalLib(_names)

    dirs = {}
    for _, name in ipairs(_names) do
        table_insert(dirs, ExternalDir..name.."/lib")
    end
    libdirs (dirs)

    ForEachSupportedPlatformAndConfig(function(_p, _c)
        names = {}
        for _, name in ipairs(_names) do
            table_insert(names, name..GetSuffix(_p, _c))
        end
        links (names)
    end )
    filter {}
end


-- GLFW ------------------------------------------------------------------------

local function IncludeExternalLib_GLFW()

    defines {"_GLFW_WIN32=1"}
    IncludeExternalLib({"glfw"})
end

local function LinkExternalLib_GLFW()

    IncludeExternalLib_GLFW()
    LinkExternalLib({"glfw"})
end


-- GLM -------------------------------------------------------------------------

local function IncludeExternalLib_GLM()

    defines {"GLM_ENABLE_EXPERIMENTAL=1"}
    IncludeExternalLib({"glm"})
end

local function LinkExternalLib_GLM()

    IncludeExternalLib_GLM()
end


-- Vulkan ----------------------------------------------------------------------

local vkSdkPathDir = os_getenv("VK_SDK_PATH")
local vulkanSdkDir = os_getenv("VULKAN_SDK")
local vulkanDir = vkSdkPathDir or vulkanSdkDir

local function IncludeExternalLib_Vulkan()

    filter {}
        includedirs {vulkanDir.."/Include"}
    IncludeExternalLib({"vma"})
end

local function LinkExternalLib_Vulkan()

    IncludeExternalLib_Vulkan()

    filter {}
        links   {"vulkan-1"}
        libdirs {vulkanDir.."/Lib"}
end


-- Stb -------------------------------------------------------------------------

local function IncludeExternalLib_Stb()

    IncludeExternalLib({"stb"})
end

local function LinkExternalLib_Stb()

    IncludeExternalLib_Stb()
    LinkExternalLib({"stb"})
end


-- DearImGui -------------------------------------------------------------------

local function IncludeExternalLib_DearImGui()

    IncludeExternalLib({"imgui"})
end

local function ImplSourcesExternalLib_DearImGui()

    files  {ExternalDir.."imgui/src/imgui_impl_glfw.cpp",
            ExternalDir.."imgui/src/imgui_impl_vulkan.cpp"}
end

local function LinkExternalLib_DearImGui()

    IncludeExternalLib_DearImGui()
    LinkExternalLib({"imgui"})
end


-- Link ------------------------------------------------------------------------

local IncludeExternalLibFunctions =
{
    ["glfw"]   = IncludeExternalLib_GLFW,
    ["glm"]    = IncludeExternalLib_GLM,
    ["vulkan"] = IncludeExternalLib_Vulkan,
    ["stb"]    = IncludeExternalLib_Stb,
    ["imgui"]  = IncludeExternalLib_DearImGui,
}

local ImplSourcesExternalLibFunctions =
{
    ["glfw"]   = function() end,
    ["glm"]    = function() end,
    ["vulkan"] = function() end,
    ["stb"]    = function() end,
    ["imgui"]  = ImplSourcesExternalLib_DearImGui,
}

local LinkExternalLibFunctions =
{
    ["glfw"]   = LinkExternalLib_GLFW,
    ["glm"]    = LinkExternalLib_GLM,
    ["vulkan"] = LinkExternalLib_Vulkan,
    ["stb"]    = LinkExternalLib_Stb,
    ["imgui"]  = LinkExternalLib_DearImGui,
}

function IncludeExternalLibs(_externalLibraries)

    for _, lib in pairs(_externalLibraries) do
        IncludeExternalLibFunctions[lib]()
    end
end

function ImplSourcesExternalLibs(_externalLibraries)

    for _, lib in pairs(_externalLibraries) do
        ImplSourcesExternalLibFunctions[lib]()
    end
end

function LinkExternalLibs(_externalLibraries)

    for _, lib in pairs(_externalLibraries) do
        LinkExternalLibFunctions[lib]()
    end
end
