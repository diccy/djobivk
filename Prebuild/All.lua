#!lua

require("Directories")
require("Utils")
require("External")
require("Engine")
require("Tests")
require("Games")


-- Definition workspace --------------------------------------------------------

workspace ("DjobiVk")
    location (BuildDir)
    SetWorkspacePlatforms()
    SetWorkspaceConfigurations()
    SetArchitectures()

    FilterPlatform("x64", function()
        defines {"__DJO_x64__=1"}
    end )
    filter {}
    FilterConfiguration("Release", function()
        defines {"__DJO_RELEASE__=1"}
    end )
    FilterConfiguration("Debug", function()
        defines {"__DJO_DEBUG__=1"}
    end )
    filter {}

    -- Engine
    group "00_Engine"
    DefineEngineProject()

    -- Tests
    group "01_Tests"
    DefineTestsProject("TestsEngine", EngineName)

    -- Games
    group "02_Games"
    DefineGameProject("VulkanTuto")
