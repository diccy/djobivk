#!lua

-- Arborecence de la solution --------------------------------------------------

BinName = "Bin"
BuildName = "Build"
SourcesName = "Sources"
ExternalName = "External"
EngineName = "Engine"
TestsName = "Tests"
GamesName = "Games"

PremakeDir = _PREMAKE_DIR.."/"

RootDir = PremakeDir.."../"
    BinDir = RootDir..BinName.."/"
    BuildDir = RootDir..BuildName.."/"
    SourcesDir = RootDir..SourcesName.."/"
    ExternalDir = RootDir..ExternalName.."/"
