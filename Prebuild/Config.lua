#!lua

SupportedPlatforms =
{
    ["x64"] = true,
}

SupportedConfigurations =
{
    ["Release"] = true,
    ["Debug"]   = true,
}

function SetCharacterSet()

    -- if Windows
    characterset "MBCS"
    -- else let default (Unicode) value
end
