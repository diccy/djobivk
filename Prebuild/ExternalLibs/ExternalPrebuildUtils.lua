#!lua

require("Utils")


function SourceFiles(_sourcesDir, _sourceFiles)

    table_insert = table.insert
    sourceFiles = {}
    for _, f in ipairs(_sourceFiles) do
        table_insert(sourceFiles, _sourcesDir..f)
    end
    files (sourceFiles)
end

function BuildExternalLib(_baseName, _includeDir, _sourcesDir, _sourceFiles)
    local solutionName = _baseName
    local projectName = _baseName

    newoption { trigger = "rootDir", value = "path", description = _baseName.." root directory path" }

    local rootDir = _OPTIONS["rootDir"]
    local buildDir = rootDir..'build/'

    workspace (solutionName)
        location (buildDir)
        SetWorkspacePlatforms()
        SetWorkspaceConfigurations()
        SetArchitectures()

        project (projectName)
            location (buildDir)

            filter {}
                kind        ("StaticLib")
                SetCppLanguage()

                targetdir   (buildDir.."lib/")
                targetname  (projectName)

                SetCharacterSet()

                includedirs {rootDir.._includeDir}
                SourceFiles(rootDir.._sourcesDir, _sourceFiles)

                libdirs     {}
                flags       {"NoMinimalRebuild", "MultiProcessorCompile"}

                SetTargetSuffixes()

            FilterConfiguration("Release", function()
                warnings    "Extra"
                optimize    "Full"
            end )
            FilterConfiguration("Debug", function()
                warnings    "Extra"
                symbols     "On"
            end )
            filter {}
end
