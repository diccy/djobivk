
#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_PSD
#define STBI_NO_GIF
#define STBI_NO_PIC
#define STBI_NO_PNM

#include "stb_image.h"

#undef STB_IMAGE_IMPLEMENTATION
#undef STBI_NO_PSD
#undef STBI_NO_GIF
#undef STBI_NO_PIC
#undef STBI_NO_PNM
