#!lua

require("ExternalPrebuildUtils")

BuildExternalLib("imgui", "", "",
    { "imgui.cpp",
      "imgui_draw.cpp",
      "imgui_tables.cpp",
      "imgui_widgets.cpp" })
