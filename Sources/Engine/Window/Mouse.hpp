#pragma once

#include <Core/Types.hpp>
#include <Core/TypeTraits.hpp>


namespace Djo
{
    enum class EMouseButton : u32
    {
        Left = 0,
        Right,
        Middle,

        DJO_ENUM_COUNT_INVALID
    };
}
