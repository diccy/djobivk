#pragma once

#include <Window/WindowInitDesc.hpp>
#include <Window/Keyboard.hpp>
#include <Window/Mouse.hpp>
#include <Core/Pointers.hpp>
#include <Core/ResultEnum.hpp>

// fw
struct GLFWwindow;

namespace Djo
{
    class Window
    {
        DJO_NO_COPY_NO_MOVE(Window);

    public:

        Window();
        ~Window();

        DJO_INLINE const GLFWwindow* GetGLFWWindow() const  { return m_glfwWindow; }
        DJO_INLINE       GLFWwindow* GetGLFWWindow()        { return m_glfwWindow; }

        EResult Init(const WindowInitDesc& _initDesc);
        void Close();

        Vec2u GetSize() const;
        void SetSize(const Vec2u& _size);

        Vec2u GetFrameBufferSize() const;
        f32 GetFrameBufferRatio() const;

        bool IsKeyPressed(EKeyboardKey _key) const;
        bool IsMouseButtonPressed(EMouseButton _button) const;
        Vec2f GetMousePosition() const;
        bool HasFocus() const;

        void PollEvents();
        void Display();

    private:

        GLFWwindow* m_glfwWindow;
        TIEventListener<WindowEvent>* m_windowEventListener;
    };
}
