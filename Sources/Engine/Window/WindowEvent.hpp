#pragma once

#include <Window/Keyboard.hpp>
#include <Window/Mouse.hpp>


namespace Djo
{
    // Based on SFML

    struct WindowEvent
    {
        enum class EType
        {
            Closed,               // The window requested to be closed (no data)
            WindowResized,        // The window was resized (data in event.size)
            FrameBufferResized,   // The window's frame buffer was resized (data in event.size)
            KeyPressed,           // A key was pressed (data in event.key)
            KeyReleased,          // A key was released (data in event.key)
            MouseWheelScrolled,   // The mouse wheel was scrolled (data in event.mouseWheelScroll)
            MouseButtonPressed,   // A mouse button was pressed (data in event.mouseButton)
            MouseButtonReleased,  // A mouse button was released (data in event.mouseButton)
            MouseMoved,           // The mouse cursor moved (data in event.mouseMove)

            DJO_ENUM_COUNT_INVALID
        };

        struct SizeData
        {
            u32 width;   // New width, in pixels
            u32 height;  // New height, in pixels
        };

        struct KeyData
        {
            EKeyboardKey code;  // Code of the key that has been pressed
            u32 mods;           // Is the Ctrl, Alt, Shift and/or System key pressed?
        };

        struct MousePosData
        {
            f32 x;  // X position of the mouse pointer, relative to the left of the owner window
            f32 y;  // Y position of the mouse pointer, relative to the top of the owner window
        };

        struct MouseButtonData 
        {
            EMouseButton button;  // Code of the button that has been pressed
            f32 x;                // X position of the mouse pointer, relative to the left of the owner window
            f32 y;                // Y position of the mouse pointer, relative to the top of the owner window
        };

        struct MouseWheelData
        {
            f32 delta;  // Wheel offset (positive is up/left, negative is down/right). High-precision mice may use non-integral offsets.
            f32 x;      // X position of the mouse pointer, relative to the left of the owner window
            f32 y;      // Y position of the mouse pointer, relative to the top of the owner window
        };

        EType type{ EType::Invalid };
        union
        {
            SizeData size;                // Size event parameters (Event::Resized)
            KeyData key;                  // Key event parameters (Event::KeyPressed, Event::KeyReleased)
            MousePosData mousePos;        // Mouse move event parameters (Event::MouseMoved)
            MouseButtonData mouseButton;  // Mouse button event parameters (Event::MouseButtonPressed, Event::MouseButtonReleased)
            MouseWheelData mouseWheel;    // Mouse wheel event parameters (Event::MouseWheelScrolled)
        };
    };
}
