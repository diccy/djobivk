DJO_MATCH(GLFW_KEY_SPACE,           EKeyboardKey::Space);
DJO_MATCH(GLFW_KEY_APOSTROPHE,      EKeyboardKey::Apostrophe);
DJO_MATCH(GLFW_KEY_COMMA,           EKeyboardKey::Comma);
DJO_MATCH(GLFW_KEY_MINUS,           EKeyboardKey::Minus);
DJO_MATCH(GLFW_KEY_PERIOD,          EKeyboardKey::Period);
DJO_MATCH(GLFW_KEY_SLASH,           EKeyboardKey::Slash);
DJO_MATCH(GLFW_KEY_0,               EKeyboardKey::Num0);
DJO_MATCH(GLFW_KEY_1,               EKeyboardKey::Num1);
DJO_MATCH(GLFW_KEY_2,               EKeyboardKey::Num2);
DJO_MATCH(GLFW_KEY_3,               EKeyboardKey::Num3);
DJO_MATCH(GLFW_KEY_4,               EKeyboardKey::Num4);
DJO_MATCH(GLFW_KEY_5,               EKeyboardKey::Num5);
DJO_MATCH(GLFW_KEY_6,               EKeyboardKey::Num6);
DJO_MATCH(GLFW_KEY_7,               EKeyboardKey::Num7);
DJO_MATCH(GLFW_KEY_8,               EKeyboardKey::Num8);
DJO_MATCH(GLFW_KEY_9,               EKeyboardKey::Num9);
DJO_MATCH(GLFW_KEY_SEMICOLON,       EKeyboardKey::Semicolon);
DJO_MATCH(GLFW_KEY_EQUAL,           EKeyboardKey::Equal);
DJO_MATCH(GLFW_KEY_A,               EKeyboardKey::A);
DJO_MATCH(GLFW_KEY_B,               EKeyboardKey::B);
DJO_MATCH(GLFW_KEY_C,               EKeyboardKey::C);
DJO_MATCH(GLFW_KEY_D,               EKeyboardKey::D);
DJO_MATCH(GLFW_KEY_E,               EKeyboardKey::E);
DJO_MATCH(GLFW_KEY_F,               EKeyboardKey::F);
DJO_MATCH(GLFW_KEY_G,               EKeyboardKey::G);
DJO_MATCH(GLFW_KEY_H,               EKeyboardKey::H);
DJO_MATCH(GLFW_KEY_I,               EKeyboardKey::I);
DJO_MATCH(GLFW_KEY_J,               EKeyboardKey::J);
DJO_MATCH(GLFW_KEY_K,               EKeyboardKey::K);
DJO_MATCH(GLFW_KEY_L,               EKeyboardKey::L);
DJO_MATCH(GLFW_KEY_M,               EKeyboardKey::M);
DJO_MATCH(GLFW_KEY_N,               EKeyboardKey::N);
DJO_MATCH(GLFW_KEY_O,               EKeyboardKey::O);
DJO_MATCH(GLFW_KEY_P,               EKeyboardKey::P);
DJO_MATCH(GLFW_KEY_Q,               EKeyboardKey::Q);
DJO_MATCH(GLFW_KEY_R,               EKeyboardKey::R);
DJO_MATCH(GLFW_KEY_S,               EKeyboardKey::S);
DJO_MATCH(GLFW_KEY_T,               EKeyboardKey::T);
DJO_MATCH(GLFW_KEY_U,               EKeyboardKey::U);
DJO_MATCH(GLFW_KEY_V,               EKeyboardKey::V);
DJO_MATCH(GLFW_KEY_W,               EKeyboardKey::W);
DJO_MATCH(GLFW_KEY_X,               EKeyboardKey::X);
DJO_MATCH(GLFW_KEY_Y,               EKeyboardKey::Y);
DJO_MATCH(GLFW_KEY_Z,               EKeyboardKey::Z);
DJO_MATCH(GLFW_KEY_LEFT_BRACKET,    EKeyboardKey::LBracket);
DJO_MATCH(GLFW_KEY_BACKSLASH,       EKeyboardKey::Backslash);
DJO_MATCH(GLFW_KEY_RIGHT_BRACKET,   EKeyboardKey::RBracket);
DJO_MATCH(GLFW_KEY_GRAVE_ACCENT,    EKeyboardKey::Square2);
//DJO_MATCH(GLFW_KEY_WORLD_1,         EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_WORLD_2,         EKeyboardKey::Invalid);
DJO_MATCH(GLFW_KEY_ESCAPE,          EKeyboardKey::Escape);
DJO_MATCH(GLFW_KEY_ENTER,           EKeyboardKey::Enter);
DJO_MATCH(GLFW_KEY_TAB,             EKeyboardKey::Tab);
DJO_MATCH(GLFW_KEY_BACKSPACE,       EKeyboardKey::Backspace);
DJO_MATCH(GLFW_KEY_INSERT,          EKeyboardKey::Insert);
DJO_MATCH(GLFW_KEY_DELETE,          EKeyboardKey::Delete);
DJO_MATCH(GLFW_KEY_RIGHT,           EKeyboardKey::Right);
DJO_MATCH(GLFW_KEY_LEFT,            EKeyboardKey::Left);
DJO_MATCH(GLFW_KEY_DOWN,            EKeyboardKey::Down);
DJO_MATCH(GLFW_KEY_UP,              EKeyboardKey::Up);
DJO_MATCH(GLFW_KEY_PAGE_UP,         EKeyboardKey::PageUp);
DJO_MATCH(GLFW_KEY_PAGE_DOWN,       EKeyboardKey::PageDown);
DJO_MATCH(GLFW_KEY_HOME,            EKeyboardKey::Home);
DJO_MATCH(GLFW_KEY_END,             EKeyboardKey::End);
DJO_MATCH(GLFW_KEY_CAPS_LOCK,       EKeyboardKey::CapsLock);
DJO_MATCH(GLFW_KEY_SCROLL_LOCK,     EKeyboardKey::ScrollLock);
DJO_MATCH(GLFW_KEY_NUM_LOCK,        EKeyboardKey::NumLock);
DJO_MATCH(GLFW_KEY_PRINT_SCREEN,    EKeyboardKey::PrintScreen);
DJO_MATCH(GLFW_KEY_PAUSE,           EKeyboardKey::Pause);
DJO_MATCH(GLFW_KEY_F1,              EKeyboardKey::F1);
DJO_MATCH(GLFW_KEY_F2,              EKeyboardKey::F2);
DJO_MATCH(GLFW_KEY_F3,              EKeyboardKey::F3);
DJO_MATCH(GLFW_KEY_F4,              EKeyboardKey::F4);
DJO_MATCH(GLFW_KEY_F5,              EKeyboardKey::F5);
DJO_MATCH(GLFW_KEY_F6,              EKeyboardKey::F6);
DJO_MATCH(GLFW_KEY_F7,              EKeyboardKey::F7);
DJO_MATCH(GLFW_KEY_F8,              EKeyboardKey::F8);
DJO_MATCH(GLFW_KEY_F9,              EKeyboardKey::F9);
DJO_MATCH(GLFW_KEY_F10,             EKeyboardKey::F10);
DJO_MATCH(GLFW_KEY_F11,             EKeyboardKey::F11);
DJO_MATCH(GLFW_KEY_F12,             EKeyboardKey::F12);
//DJO_MATCH(GLFW_KEY_F13,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F14,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F15,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F16,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F17,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F18,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F19,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F20,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F21,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F22,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F23,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F24,             EKeyboardKey::Invalid);
//DJO_MATCH(GLFW_KEY_F25,             EKeyboardKey::Invalid);
DJO_MATCH(GLFW_KEY_KP_0,            EKeyboardKey::Numpad0);
DJO_MATCH(GLFW_KEY_KP_1,            EKeyboardKey::Numpad1);
DJO_MATCH(GLFW_KEY_KP_2,            EKeyboardKey::Numpad2);
DJO_MATCH(GLFW_KEY_KP_3,            EKeyboardKey::Numpad3);
DJO_MATCH(GLFW_KEY_KP_4,            EKeyboardKey::Numpad4);
DJO_MATCH(GLFW_KEY_KP_5,            EKeyboardKey::Numpad5);
DJO_MATCH(GLFW_KEY_KP_6,            EKeyboardKey::Numpad6);
DJO_MATCH(GLFW_KEY_KP_7,            EKeyboardKey::Numpad7);
DJO_MATCH(GLFW_KEY_KP_8,            EKeyboardKey::Numpad8);
DJO_MATCH(GLFW_KEY_KP_9,            EKeyboardKey::Numpad9);
DJO_MATCH(GLFW_KEY_KP_DECIMAL,      EKeyboardKey::NumpadDot);
DJO_MATCH(GLFW_KEY_KP_DIVIDE,       EKeyboardKey::NumpadDivide);
DJO_MATCH(GLFW_KEY_KP_MULTIPLY,     EKeyboardKey::NumpadMult);
DJO_MATCH(GLFW_KEY_KP_SUBTRACT,     EKeyboardKey::NumpadMinus);
DJO_MATCH(GLFW_KEY_KP_ADD,          EKeyboardKey::NumpadPlus);
DJO_MATCH(GLFW_KEY_KP_ENTER,        EKeyboardKey::NumpadEnter);
//DJO_MATCH(GLFW_KEY_KP_EQUAL,        EKeyboardKey::Invalid);
DJO_MATCH(GLFW_KEY_LEFT_SHIFT,      EKeyboardKey::LShift);
DJO_MATCH(GLFW_KEY_LEFT_CONTROL,    EKeyboardKey::LCtrl);
DJO_MATCH(GLFW_KEY_LEFT_ALT,        EKeyboardKey::LAlt);
DJO_MATCH(GLFW_KEY_LEFT_SUPER,      EKeyboardKey::LSystem);
DJO_MATCH(GLFW_KEY_RIGHT_SHIFT,     EKeyboardKey::RShift);
DJO_MATCH(GLFW_KEY_RIGHT_CONTROL,   EKeyboardKey::RCtrl);
DJO_MATCH(GLFW_KEY_RIGHT_ALT,       EKeyboardKey::RAlt);
DJO_MATCH(GLFW_KEY_RIGHT_SUPER,     EKeyboardKey::RSystem);
DJO_MATCH(GLFW_KEY_MENU,            EKeyboardKey::Menu);
