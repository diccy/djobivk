#pragma once

#include <Window/Keyboard.hpp>
#include <Window/Mouse.hpp>
#include <Core/StructUtils.hpp>

#define GLFW_INCLUDE_NONE
#include <glfw/glfw3.h>


namespace Djo
{
    // GLFW keyboard keys

    DJO_LUT_START(c_lutFromGLFWKey, (GLFW_KEY_LAST + 1), int, EKeyboardKey, EKeyboardKey::Invalid)
        #define DJO_MATCH(glfw_, kbenum_)  DJO_LUT_MATCH(glfw_, kbenum_)
        #include "Match_GLFWKeyboardKeys.hpp"
        #undef DJO_MATCH
    DJO_LUT_END();

    constexpr EKeyboardKey FromGLFWKey(const int _key)  { return c_lutFromGLFWKey.a[_key]; }


    DJO_LUT_START(c_lutToGLFWKey, ECount<EKeyboardKey>, EKeyboardKey, int, GLFW_KEY_UNKNOWN)
        #define DJO_MATCH(glfw_, kbenum_)  DJO_LUT_MATCH(EValue(kbenum_), glfw_)
        #include "Match_GLFWKeyboardKeys.hpp"
        #undef DJO_MATCH
    DJO_LUT_END();

    constexpr int ToGLFWKey(const EKeyboardKey _key)  { return c_lutToGLFWKey.a[EValue(_key)]; }


    // GLFW mouse buttons

    DJO_LUT_START(c_lutFromGLFWMouseButton, (GLFW_MOUSE_BUTTON_LAST + 1), int, EMouseButton, EMouseButton::Invalid)
        #define DJO_MATCH(glfw_, menum_)  DJO_LUT_MATCH(glfw_, menum_)
        #include "Match_GLFWMouseButtons.hpp"
        #undef DJO_MATCH
    DJO_LUT_END();

    constexpr EMouseButton FromGLFWMouseButton(const int _button)  { return c_lutFromGLFWMouseButton.a[_button]; }


    DJO_LUT_START(c_lutToGLFWMouseButton, ECount<EMouseButton>, EMouseButton, int, GLFW_KEY_UNKNOWN)
        #define DJO_MATCH(glfw_, menum_)  DJO_LUT_MATCH(EValue(menum_), glfw_)
        #include "Match_GLFWMouseButtons.hpp"
        #undef DJO_MATCH
    DJO_LUT_END();

    constexpr int ToGLFWMouseButton(const EMouseButton _button)  { return c_lutToGLFWMouseButton.a[EValue(_button)]; }


    // GLFW mods

    // They share same values
    DJO_STATIC_ASSERT(GLFW_MOD_SHIFT ==      EValue(EKeyboardModFlag::Shift));
    DJO_STATIC_ASSERT(GLFW_MOD_CONTROL ==    EValue(EKeyboardModFlag::Ctrl));
    DJO_STATIC_ASSERT(GLFW_MOD_ALT ==        EValue(EKeyboardModFlag::Alt));
    DJO_STATIC_ASSERT(GLFW_MOD_SUPER ==      EValue(EKeyboardModFlag::System));
    DJO_STATIC_ASSERT(GLFW_MOD_CAPS_LOCK ==  EValue(EKeyboardModFlag::CapsLock));
    DJO_STATIC_ASSERT(GLFW_MOD_NUM_LOCK ==   EValue(EKeyboardModFlag::NumLock));

    constexpr u32 FromGLFWMods(const int _mods)  { return (u32)_mods; }
    constexpr int ToGLFWMods(const u32 _mods)    { return (int)_mods; }


    // GLFW enums to str, for debug mainly

    constexpr
    const char*
    GLFWKeyToStr(const int _key)
    {
        switch (_key)
        {
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_UNKNOWN);

            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_SPACE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_APOSTROPHE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_COMMA);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_MINUS);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_PERIOD);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_SLASH);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_0);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_1);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_2);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_3);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_4);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_5);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_6);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_7);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_8);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_9);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_SEMICOLON);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_EQUAL);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_A);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_B);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_C);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_D);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_E);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_G);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_H);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_I);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_J);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_K);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_L);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_M);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_N);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_O);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_P);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_Q);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_R);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_S);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_T);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_U);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_V);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_W);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_X);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_Y);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_Z);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_LEFT_BRACKET);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_BACKSLASH);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_RIGHT_BRACKET);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_GRAVE_ACCENT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_WORLD_1);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_WORLD_2);

            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_ESCAPE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_ENTER);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_TAB);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_BACKSPACE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_INSERT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_DELETE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_RIGHT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_LEFT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_DOWN);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_UP);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_PAGE_UP);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_PAGE_DOWN);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_HOME);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_END);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_CAPS_LOCK);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_SCROLL_LOCK);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_NUM_LOCK);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_PRINT_SCREEN);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_PAUSE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F1);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F2);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F3);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F4);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F5);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F6);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F7);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F8);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F9);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F10);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F11);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F12);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F13);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F14);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F15);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F16);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F17);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F18);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F19);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F20);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F21);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F22);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F23);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F24);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_F25);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_0);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_1);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_2);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_3);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_4);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_5);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_6);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_7);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_8);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_9);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_DECIMAL);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_DIVIDE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_MULTIPLY);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_SUBTRACT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_ADD);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_ENTER);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_KP_EQUAL);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_LEFT_SHIFT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_LEFT_CONTROL);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_LEFT_ALT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_LEFT_SUPER);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_RIGHT_SHIFT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_RIGHT_CONTROL);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_RIGHT_ALT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_RIGHT_SUPER);
            DJO_CASE_VALUE_RETURN_STR(GLFW_KEY_MENU);

            default:
            {
                return "<Error : No GLFW Key>";
            }
        }
    }

    constexpr
    const char*
    GLFWMouseButtonToStr(const int _button)
    {
        switch (_button)
        {
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_LEFT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_RIGHT);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_MIDDLE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_4);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_5);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_6);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_7);
            DJO_CASE_VALUE_RETURN_STR(GLFW_MOUSE_BUTTON_8);

            default:
            {
                return "<Error : No GLFW Mouse Button>";
            }
        }
    }

    constexpr
    const char*
    GLFWActionToStr(const int _action)
    {
        switch (_action)
        {
            DJO_CASE_VALUE_RETURN_STR(GLFW_RELEASE);
            DJO_CASE_VALUE_RETURN_STR(GLFW_PRESS);
            DJO_CASE_VALUE_RETURN_STR(GLFW_REPEAT);

            default:
            {
                return "<Error : No GLFW Action>";
            }
        }
    }
}
