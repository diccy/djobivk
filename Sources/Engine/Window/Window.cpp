
#include <Window/Window.hpp>

#include <Graphics/Vulkan/Vulkan.hpp> // Keep before including GLFW
#include <Window/GLFW/GLFW.hpp>
#include <Window/WindowEvent.hpp>
#include <Math/Math.hpp>
#include <System/GlobalProfiler.hpp>
#include <Core/EventListener.hpp>
#include <Core/GlobalLog.hpp>


namespace Djo
{
    // fw
    //void CallbackGLFWWindowPos();
    void CallbackGLFWWindowSize(GLFWwindow* _glfwWindow, int _width, int _height);
    void CallbackGLFWWindowClose(GLFWwindow* _glfwWindow);
    //void CallbackGLFWWindowRefresh();
    //void CallbackGLFWWindowFocus();
    //void CallbackGLFWWindowIconify();
    //void CallbackGLFWWindowMaximize();
    void CallbackGLFWFrameBufferSize(GLFWwindow* _glfwWindow, int _width, int _height);
    //void CallbackGLFWWindowContentScale();
    void CallbackGLFWKey(GLFWwindow* _glfwWindow, int _key, int _scancode, int _action, int _mods);
    //void CallbackGLFWChar(GLFWwindow* _glfwWindow, unsigned int _key);
    //void CallbackGLFWCharMods();
    void CallbackGLFWMouseButton(GLFWwindow* _glfwWindow, int _button, int _action, int _mods);
    void CallbackGLFWMousePosition(GLFWwindow* _glfwWindow, double _x, double _y);
    //void CallbackGLFWCursorEnter();
    void CallbackGLFWScroll(GLFWwindow* _glfwWindow, double _xOffset, double _yOffset);
    //void CallbackGLFWDrop();
    //void CallbackGLFWJoystick();

    static bool s_isGLFWInitialized = false;
    static u32 s_glfwWindowCount = 0;

    static
    void
    GLFWErrorCallback(const int _errorCode, const char* const _message)
    {
        DJO_LOG_CORE_ERROR << "GLFW error: (" << _errorCode << "): " << _message << std::endl;
        DJO_FAILED_ASSERTION(false);
    }

    Window::Window()
        : m_glfwWindow{ nullptr }
        , m_windowEventListener{ nullptr }
    {
    }

    Window::~Window()
    {
        Close();
    }

    EResult
    Window::Init(const WindowInitDesc& _initDesc)
    {
        DJO_PROFILE_FUNCTION();

        DJO_ASSERT(m_glfwWindow == nullptr);

        DJO_ASSERT_MSG(s_isGLFWInitialized == false, "Multiple windows not supported");
        DJO_ASSERT_MSG(s_glfwWindowCount == 0, "Multiple windows not supported");

        DJO_LOG_CORE_INFO << "Creating window : " << _initDesc.name << " [" << _initDesc.initSize.x << ',' << _initDesc.initSize.y << ']' << std::endl;

        // Init GLFW

        DJO_TODO("Remove GLFW init from Window::Init code?");
        if (!s_isGLFWInitialized)
        {
            DJO_PROFILE_SCOPE("glfwInit");
            if (::glfwInit() != GLFW_TRUE)
            {
                DJO_LOG_CORE_ERROR << "GLFW initialization failed" << std::endl;
                return MakeResult(EResult::FAIL, "glfwInit failed");
            }
            ::glfwSetErrorCallback(GLFWErrorCallback);
            s_isGLFWInitialized = true;
        }

        // Init window properties

        // https://www.glfw.org/docs/latest/window_guide.html#window_hints
        ::glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        //::glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        // Create window

        GLFWwindow* const glfwWindow = ::glfwCreateWindow((int)(_initDesc.initSize.x), (int)(_initDesc.initSize.y), _initDesc.name.c_str(), nullptr, nullptr);
        if (glfwWindow == nullptr)
            return MakeResult(EResult::FAIL, "glfwCreateWindow failed");

        ++s_glfwWindowCount;

        m_glfwWindow = glfwWindow;
        m_windowEventListener = _initDesc.windowEventListener;

        // GLFW callbacks

        ::glfwSetWindowUserPointer(m_glfwWindow, m_windowEventListener);

        // Window callbacks
        //::glfwSetWindowPosCallback(m_glfwWindow, CallbackGLFWWindowPos);
        ::glfwSetWindowSizeCallback(m_glfwWindow, CallbackGLFWWindowSize);
        ::glfwSetWindowCloseCallback(m_glfwWindow, CallbackGLFWWindowClose);
        //::glfwSetWindowRefreshCallback(m_glfwWindow, CallbackGLFWWindowRefresh);
        //::glfwSetWindowFocusCallback(m_glfwWindow, CallbackGLFWWindowFocus);
        //::glfwSetWindowIconifyCallback(m_glfwWindow, CallbackGLFWWindowIconify);
        //::glfwSetWindowMaximizeCallback(m_glfwWindow, CallbackGLFWWindowMaximize);
        ::glfwSetFramebufferSizeCallback(m_glfwWindow, CallbackGLFWFrameBufferSize);
        //::glfwSetWindowContentScaleCallback(m_glfwWindow, CallbackGLFWWindowContentScale);
        // Input callbacks
        ::glfwSetKeyCallback(m_glfwWindow, CallbackGLFWKey);
        //::glfwSetCharCallback(m_glfwWindow, CallbackGLFWChar);
        //::glfwSetCharModsCallback(m_glfwWindow, CallbackGLFWCharMods);
        ::glfwSetMouseButtonCallback(m_glfwWindow, CallbackGLFWMouseButton);
        ::glfwSetCursorPosCallback(m_glfwWindow, CallbackGLFWMousePosition);
        //::glfwSetCursorEnterCallback(m_glfwWindow, CallbackGLFWCursorEnter);
        ::glfwSetScrollCallback(m_glfwWindow, CallbackGLFWScroll);
        //::glfwSetDropCallback(m_glfwWindow, CallbackGLFWDrop);
        // Joystick callbacks
        //::glfwSetJoystickCallback(CallbackGLFWJoystick);

        return EResult::OK;
    }

    Vec2u
    Window::GetSize() const
    {
        Vec2i size;
        ::glfwGetWindowSize(m_glfwWindow, &size.x, &size.y);
        return Vec2u{ size };
    }

    void
    Window::SetSize(const Vec2u& _size)
    {
        ::glfwSetWindowSize(m_glfwWindow, (int)_size.x, (int)_size.y);
    }

    Vec2u
    Window::GetFrameBufferSize() const
    {
        Vec2i size;
        ::glfwGetFramebufferSize(m_glfwWindow, &size.x, &size.y);
        return Vec2u{ size };
    }

    f32
    Window::GetFrameBufferRatio() const
    {
        const Vec2u viewportSize = GetFrameBufferSize();
        return (f32)viewportSize.x / (f32)viewportSize.y;
    }

    bool
    Window::IsKeyPressed(const EKeyboardKey _key) const
    {
        return HasFocus() && ::glfwGetKey(m_glfwWindow, ToGLFWKey(_key)) == GLFW_PRESS;
    }

    bool
    Window::IsMouseButtonPressed(const EMouseButton _button) const
    {
        return HasFocus() && ::glfwGetMouseButton(m_glfwWindow, ToGLFWMouseButton(_button)) == GLFW_PRESS;
    }

    Vec2f
    Window::GetMousePosition() const
    {
        double x, y;
        ::glfwGetCursorPos(m_glfwWindow, &x, &y);
        return Vec2f{ (f32)(x), (f32)(y) };
    }

    bool
    Window::HasFocus() const
    {
        return ::glfwGetWindowAttrib(m_glfwWindow, GLFW_FOCUSED);
    }

    void
    Window::PollEvents()
    {
        ::glfwPollEvents();
    }

    void
    Window::Display()
    {
    }

    void
    Window::Close()
    {
        if (m_glfwWindow != nullptr)
        {
            DJO_PROFILE_SCOPE("glfwDestroyWindow");

            ::glfwDestroyWindow(m_glfwWindow);
            m_windowEventListener = nullptr;
            m_glfwWindow = nullptr;
            --s_glfwWindowCount;
            if (s_glfwWindowCount == 0 && s_isGLFWInitialized)
            {
                DJO_PROFILE_SCOPE("glfwTerminate");

                ::glfwTerminate();
                s_isGLFWInitialized = false;
            }
        }
    }


    // Window callback functions

    /*
    void
    CallbackGLFWWindowPos()
    {
    }
    */

    void
    CallbackGLFWWindowSize(GLFWwindow* const _glfwWindow, const int _width, const int _height)
    {
        // Redundant with FrameBufferResized ?

        DJO_TODO("Have a particular event when user finishes to resize?");

        WindowEvent event{ WindowEvent::EType::WindowResized };
        event.size.width = _width;
        event.size.height = _height;

        DJO_LOG_CORE_INFO << "CallbackGLFWWindowSize : " << '[' << _width << ", " << _height << ']' << std::endl;

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    void
    CallbackGLFWWindowClose(GLFWwindow* const _glfwWindow)
    {
        WindowEvent event{ WindowEvent::EType::Closed };

        //DJO_LOG_CORE_INFO << "CallbackGLFWWindowClose" << std::endl;

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    /*
    void
    CallbackGLFWWindowRefresh()
    {
    }
    */

    /*
    void
    CallbackGLFWWindowFocus()
    {
    }
    */

    /*
    void
    CallbackGLFWWindowIconify()
    {
    }
    */

    /*
    void
    CallbackGLFWWindowMaximize()
    {
    }
    */

    void
    CallbackGLFWFrameBufferSize(GLFWwindow* const _glfwWindow, const int _width, const int _height)
    {
        // Redundant with WindowResized ?

        DJO_TODO("Have a particular event when user finishes to resize?");

        WindowEvent event{ WindowEvent::EType::FrameBufferResized };
        event.size.width = _width;
        event.size.height = _height;

        DJO_LOG_CORE_INFO << "CallbackGLFWFrameBufferSize : " << '[' << _width << ", " << _height << ']' << std::endl;

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    /*
    void
    CallbackGLFWWindowContentScale()
    {
    }
    */

    // Input callback functions

    void
    CallbackGLFWKey(GLFWwindow* const _glfwWindow, const int _key, const int /*_scancode*/, const int _action, const int _mods)
    {
        WindowEvent event;
        event.key.code = FromGLFWKey(_key);
        event.key.mods = FromGLFWMods(_mods);

        //DJO_LOG_CORE_INFO << "CallbackGLFWWindowKey : " << " k:" << _key << " (" << GLFWKeyToStr(_key) << ") sc:" << _scancode << " a:" << _action << " (" << GLFWActionToStr(_action) << ") m:" << std::hex << _mods << std::endl;

        DJO_STATIC_ASSERT(GLFW_RELEASE == 0);
        DJO_STATIC_ASSERT(GLFW_PRESS   == 1);
        DJO_STATIC_ASSERT(GLFW_REPEAT  == 2);
        constexpr WindowEvent::EType c_events[3]
        {
            WindowEvent::EType::KeyReleased, // GLFW_RELEASE
            WindowEvent::EType::KeyPressed,  // GLFW_PRESS
            WindowEvent::EType::KeyPressed,  // GLFW_REPEAT
        };
        if (0 <= _action && _action < 3)
        {
            event.type = c_events[_action];
        }
        else
        {
            DJO_FAILED_ASSERTION("glfw action unsupported");
            event.type = WindowEvent::EType::Invalid;
        }

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    /*
    void
    CallbackGLFWChar(GLFWwindow* const _glfwWindow, const unsigned int _key)
    {
    }
    */

    /*
    void
    CallbackGLFWCharMods()
    {
    }
    */

    void
    CallbackGLFWMousePosition(GLFWwindow* const _glfwWindow, const double _x, const double _y)
    {
        WindowEvent event{ WindowEvent::EType::MouseMoved };
        event.mousePos.x = (f32)(_x);
        event.mousePos.y = (f32)(_y);

        //DJO_LOG_CORE_INFO << "CallbackGLFWWindowMousePosition : " << '[' << _x << ", " << _y << ']' << std::endl;

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    void
    CallbackGLFWMouseButton(GLFWwindow* const _glfwWindow, const int _button, const int _action, const int /*_mods*/)
    {
        WindowEvent event;
        event.mouseButton.button = FromGLFWMouseButton(_button);
        double x, y;
        ::glfwGetCursorPos(_glfwWindow, &x, &y);
        event.mouseButton.x = (f32)(x);
        event.mouseButton.y = (f32)(y);

        //DJO_LOG_CORE_INFO << "CallbackGLFWWindowMouseButton : " << " b:" << _button << " (" << GLFWMouseButtonToStr(_button) << ") a:" << _action << " (" << GLFWActionToStr(_action) << ") m:" << std::hex << _mods << std::endl;

        DJO_STATIC_ASSERT(GLFW_RELEASE == 0);
        DJO_STATIC_ASSERT(GLFW_PRESS == 1);
        constexpr WindowEvent::EType c_events[2]
        {
            WindowEvent::EType::MouseButtonReleased, // GLFW_RELEASE
            WindowEvent::EType::MouseButtonPressed,  // GLFW_PRESS
        };
        if (0 <= _action && _action < 2)
        {
            event.type = c_events[_action];
        }
        else
        {
            DJO_FAILED_ASSERTION("glfw action unsupported");
            event.type = WindowEvent::EType::Invalid;
        }

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    /*
    void
    CallbackGLFWCursorEnter()
    {
    }
    */

    void
    CallbackGLFWScroll(GLFWwindow* const _glfwWindow, const double /*_xOffset*/, const double _yOffset)
    {
        WindowEvent event{ WindowEvent::EType::MouseWheelScrolled };
        event.mouseWheel.delta = (f32)(_yOffset);
        double x, y;
        ::glfwGetCursorPos(_glfwWindow, &x, &y);
        event.mouseWheel.x = (f32)(x);
        event.mouseWheel.y = (f32)(y);

        //DJO_LOG_CORE_INFO << "CallbackGLFWWindowMouseWheelScroll : " << " x:" << _xOffset << " y:" << _yOffset << std::endl;

        static_cast<TIEventListener<WindowEvent>*>(::glfwGetWindowUserPointer(_glfwWindow))->OnEvent(event);
    }

    /*
    void
    CallbackGLFWDrop()
    {
    }
    */

    /*
    void
    CallbackGLFWJoystick()
    {
    }
    */
}
