#pragma once

#include <Math/Vector2.hpp>
#include <Core/StdString.hpp>


namespace Djo
{
    // fw
    struct WindowEvent;
    template <typename TEvent>
    class TIEventListener;

    struct WindowInitDesc
    {
        std::string name{};
        Vec2u initSize{ 0, 0 };
        TIEventListener<WindowEvent>* windowEventListener{ nullptr };
    };
}
