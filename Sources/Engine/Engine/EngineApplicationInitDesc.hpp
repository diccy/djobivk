#pragma once

#include <Math/Vector2.hpp>
#include <System/Path.hpp>


namespace Djo
{
    struct EngineApplicationInitDesc
    {
        Path appAssetsPath{ "" };
        std::string windowName{ "" };
        Vec2u windowInitSize{ 0, 0 };
    #if defined(DJO_PROFILING_ENABLED)
        bool profileOnLaunch{ false };
    #endif
    };
}
