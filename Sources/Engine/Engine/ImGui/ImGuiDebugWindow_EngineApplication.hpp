#pragma once

#include <ImGui/IImGuiDebugWindowImpl.hpp>
#include <Core/Averager.hpp>
#include <Core/Time.hpp>


namespace Djo
{
    // fw
    class EngineApplication;

    class ImGuiDebugWindow_EngineApplication
        : public IImGuiDebugWindowImpl
    {
        DJO_NO_DEFAULT_CTOR(ImGuiDebugWindow_EngineApplication);
        DJO_NO_COPY_NO_MOVE(ImGuiDebugWindow_EngineApplication);

    public:

        static const char* s_menuName;
        static const char* s_windowName;

        ImGuiDebugWindow_EngineApplication(EngineApplication& _app);
        ~ImGuiDebugWindow_EngineApplication();

        virtual const char* GetMenuName() const override;
        virtual void UpdateImGuiMenuItem() override;
        virtual void UpdateImGuiWindow() override;

    private:

        EngineApplication& m_app;
        Averager<Milliseconds64> m_dtAverager;
        bool m_isWindowOpen;
    };
}
