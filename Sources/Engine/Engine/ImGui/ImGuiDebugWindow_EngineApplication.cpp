
#include <Engine/ImGui/ImGuiDebugWindow_EngineApplication.hpp>

#include <Engine/EngineApplication.hpp>
#include <ImGui/ImGuiManager.hpp>
#include <ImGui/ImGui.hpp>


namespace Djo
{
    const char* ImGuiDebugWindow_EngineApplication::s_menuName{ "Application" };
    const char* ImGuiDebugWindow_EngineApplication::s_windowName{ "Debug Engine Application" };

    ImGuiDebugWindow_EngineApplication::ImGuiDebugWindow_EngineApplication(EngineApplication& _app)
        : m_app{ _app }
        , m_dtAverager{ 10, 1000. / 60. }
        , m_isWindowOpen{ false }
    {
    }

    ImGuiDebugWindow_EngineApplication::~ImGuiDebugWindow_EngineApplication()
    {
    }

    const char*
    ImGuiDebugWindow_EngineApplication::GetMenuName() const
    {
        return s_menuName;
    }

    void
    ImGuiDebugWindow_EngineApplication::UpdateImGuiMenuItem()
    {
        ::ImGui::MenuItem(s_windowName, nullptr, &m_isWindowOpen);
    }

    void
    ImGuiDebugWindow_EngineApplication::UpdateImGuiWindow()
    {
        if (m_isWindowOpen)
        {
            if (::ImGui::Begin(s_windowName, &m_isWindowOpen))
            {
                m_dtAverager.Push(m_app.m_currentDt);
                const Milliseconds64 averageDt{ m_dtAverager.GetWithMaxSum(0.5) };
                const u32 averageFps = (u32)(1. / averageDt.Sec());
                ::ImGui::Text("FPS: %u", averageFps);
                ::ImGui::Checkbox("Low framerate", &m_app.m_isLowFramerateActive);

                if (::ImGui::TreeNode("Init description"))
                {
                    ::ImGui::Text("Assets path: %s", m_app.m_initDesc.appAssetsPath.GetFullPath().c_str());
                    ::ImGui::Text("Window name: %s", m_app.m_initDesc.windowName.c_str());
                    ::ImGui::Text("Window init size: %u,%u", m_app.m_initDesc.windowInitSize.x, m_app.m_initDesc.windowInitSize.y);
                #if defined(DJO_PROFILING_ENABLED)
                    ::ImGui::Text("Profile on launch: %d", m_app.m_initDesc.profileOnLaunch);
                #endif
                    ::ImGui::TreePop();
                }
            }
            ::ImGui::End();
        }
    }
}
