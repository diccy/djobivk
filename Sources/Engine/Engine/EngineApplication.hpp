#pragma once

#include <Engine/EngineApplicationInitDesc.hpp>
#include <Window/WindowEvent.hpp>
#include <Core/Pointers.hpp>
#include <Core/Time.hpp>
#include <Core/StdVector.hpp>
#include <Core/EventListener.hpp>


namespace Djo
{
    // fw
    class Window;
    class Renderer;
    class ImGuiManager;

    class EngineApplication
        : public TIEventListener<WindowEvent>
    {
        DJO_NO_COPY_NO_MOVE(EngineApplication);

    public:

        using InitDesc = EngineApplicationInitDesc;

        EngineApplication();
        virtual ~EngineApplication();

    #define DJO_APPLICATION_DECLARE_GETTERS(class_, member_)\
        DJO_INLINE       class_& Get##class_()        { return *member_; }\
        DJO_INLINE const class_& Get##class_() const  { return *member_; }

        DJO_APPLICATION_DECLARE_GETTERS(Window, m_window);
        DJO_APPLICATION_DECLARE_GETTERS(Renderer, m_renderer);
        DJO_APPLICATION_DECLARE_GETTERS(ImGuiManager, m_imGuiDebugManager);

    #undef DJO_APPLICATION_DECLARE_GETTERS

        virtual EResult OnAfterInit() { return EResult::OK; }
        virtual void OnBeforeDestroy() {}
        virtual void OnWindowEvent(const WindowEvent& /*_event*/) {}
        virtual void OnUpdate(Milliseconds64 /*_dt*/) {}
        virtual void OnImGuiUpdate() {}

        bool DoCaptureKeyboard() const;
        bool DoCaptureMouse() const;
        bool IsKeyPressed(EKeyboardKey _key) const;
        bool IsMouseButtonPressed(EMouseButton _button) const;

    protected:

        void Shutdown();

    private:

        friend class Engine;
        friend class ImGuiDebugWindow_EngineApplication;

        EResult Init(const InitDesc& _initDesc);
        EResult InitWindow(const InitDesc& _initDesc);
        EResult InitInput();
        EResult InitImGui();
        EResult InitRenderer();
        void Destroy();
        void DestroyRenderer();
        void DestroyImGui();
        void DestroyInput();
        void DestroyWindow();

        EResult Run();

        virtual void OnEvent(const WindowEvent& _event) override final;
        void DispatchWindowEvents();

        void BeginImGuiFrame();
        void EndImGuiFrame();
        void UpdateImGui();

        EngineApplicationInitDesc m_initDesc;
        std::vector<WindowEvent> m_windowEvents;
        UPtr<Window> m_window;
        UPtr<Renderer> m_renderer;
        UPtr<ImGuiManager> m_imGuiDebugManager;
        UPtr<ImGuiDebugWindow_EngineApplication> m_imGuiDebugWindow;
        Milliseconds64 m_currentDt;
        Seconds m_globalProfilerTimeRemaining;
        bool m_isRunning;
        bool m_isLowFramerateActive;
    };
}
