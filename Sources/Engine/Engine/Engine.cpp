
#include <Engine/Engine.hpp>

#include <Engine/EngineApplication.hpp>
#include <System/StringSinks.hpp>
#include <System/GlobalFS.hpp>
#include <System/GlobalProfiler.hpp>
#include <Core/GlobalLog.hpp>


namespace Djo
{
    EResult
    Engine::Run_Internal(UPtr<EngineApplication>&& _app, const EngineApplicationInitDesc& _initDesc)
    {
        EResult result;

        // Init Log (keep first! used by DJO_ASSERT macro all over the code)

        DJO_OK_OR_RETURN(GlobalLog::Init());

        const ShPtr<StringSinkVisualConsoleOutput> vsConsoleOutputSink{ DJO_CORE_NEW StringSinkVisualConsoleOutput{} };
        const ShPtr<StringSinkFile> coreLogFileSink{ DJO_CORE_NEW StringSinkFile{ "Log_DjobiEngineCore.txt" } };
        const ShPtr<StringSinkFile> appLogFileSink{ DJO_CORE_NEW StringSinkFile{ "Log_Application.txt" } };
        if (!coreLogFileSink.Get()->GetFile().IsOpen())
        {
            return MakeResult(EResult::FAIL, "Can't init GlobalLog's core sink file");
        }
        if (!appLogFileSink.Get()->GetFile().IsOpen())
        {
            return MakeResult(EResult::FAIL, "Can't init GlobalLog's app sink file");
        }

        //DJO_LOGGER_CORE.AddSink(stdCoutSink);
        DJO_LOGGER_CORE.AddSink(vsConsoleOutputSink);
        DJO_LOGGER_CORE.AddSink(coreLogFileSink);
        //DJO_LOGGER.AddSink(stdCoutSink);
        DJO_LOGGER.AddSink(vsConsoleOutputSink);
        DJO_LOGGER.AddSink(appLogFileSink);

        // Init global file systems

        DJO_OK_OR_RETURN(GlobalFS::Init(_initDesc.appAssetsPath));

        // Init global profiler

    #if defined(DJO_PROFILING_ENABLED)
        DJO_OK_OR_RETURN(GlobalProfiler::Init());
    #endif

        // Init app

        DJO_PROFILE_BEGIN_SESSION("Profile_Init.json");
        result = _app->Init(_initDesc);
        DJO_PROFILE_END_SESSION();
        if (result != EResult::OK)
        {
            return result;
        }

        // Run app

    #if defined(DJO_PROFILING_ENABLED)
        GlobalProfiler::AllowRecording(_initDesc.profileOnLaunch);
    #endif

        DJO_PROFILE_BEGIN_SESSION("Profile_Runtime.json");
        result = _app->Run();
        DJO_PROFILE_END_SESSION();

    #if defined(DJO_PROFILING_ENABLED)
        GlobalProfiler::AllowRecording(true);
    #endif

        // Destroy app

        DJO_PROFILE_BEGIN_SESSION("Profile_Destroy.json");
        _app->Destroy();
        DJO_PROFILE_END_SESSION();

        // Destroy global profiler

    #if defined(DJO_PROFILING_ENABLED)
        GlobalProfiler::Shutdown();
    #endif

        // Destroy global file systems

        GlobalFS::Shutdown();

        // Destroy Log (keep last! used by DJO_ASSERT macro all over the code)

        GlobalLog::Shutdown();

        return result;
    }
}
