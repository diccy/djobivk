
#include <Engine/Input.hpp>

#include <Engine/EngineApplication.hpp>
#include <Window/Window.hpp>

#include <glm/geometric.hpp>


namespace Djo
{
    UPtr<Input> Input::s_input{ nullptr };

    Input::Input()
        : m_app{ nullptr }
        , m_mousePosition{ 0.f, 0.f }
        , m_previousMousePosition{ 0.f, 0.f }
        , m_mouseMove{ 0.f, 0.f }
        , m_mouseMoveSpeed{ 0.f }
        , m_mouseWheelScroll{ 0.f }
        , m_doesMouseMoved{ false }
    {
    }

    Input::~Input()
    {
        Shutdown();
    }

    bool
    Input::IsKeyPressed(const EKeyboardKey _key)
    {
        return m_app->IsKeyPressed(_key);
    }

    bool
    Input::IsMouseButtonPressed(const EMouseButton _button)
    {
        return m_app->IsMouseButtonPressed(_button);
    }

    EResult
    Input::Init(EngineApplication& _app)
    {
        DJO_ASSERT(m_app == nullptr);

        m_app = &_app;
        return EResult::OK;
    }

    void
    Input::StartEvents()
    {
        DJO_ASSERT(m_app != nullptr);

        m_previousMousePosition = m_mousePosition;
        m_mouseMove = { 0.f, 0.f };
        m_mouseMoveSpeed = 0.f;
        m_mouseWheelScroll = 0.f;
        m_doesMouseMoved = false;
    }

    void
    Input::OnEvent(const WindowEvent& _event)
    {
        if (_event.type == WindowEvent::EType::MouseWheelScrolled)
        {
            m_mouseWheelScroll += _event.mouseWheel.delta;
        }
        else if (_event.type == WindowEvent::EType::MouseMoved)
        {
            m_mouseMove.x = _event.mousePos.x;
            m_mouseMove.y = _event.mousePos.y;
            m_doesMouseMoved = true;
        }
    }

    void
    Input::EndEvents()
    {
        m_mousePosition = m_app->GetWindow().GetMousePosition();
        if (m_doesMouseMoved)
        {
            m_mouseMove = m_previousMousePosition - m_mouseMove;
            m_mouseMoveSpeed = glm::length(m_mouseMove);
        }
    }

    void
    Input::Shutdown()
    {
        m_app = nullptr;
    }
}
