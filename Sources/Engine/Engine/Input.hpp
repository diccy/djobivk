#pragma once

#include <Window/Keyboard.hpp>
#include <Window/Mouse.hpp>
#include <Math/Vector2.hpp>
#include <Core/Pointers.hpp>
#include <Core/ResultEnum.hpp>


namespace Djo
{
    // fw
    class EngineApplication;
    struct WindowEvent;

    class Input
    {
        DJO_NO_COPY_NO_MOVE(Input);

    public:

        ~Input();

        DJO_INLINE static Input* Get()  { return s_input.Get(); }

        bool IsKeyPressed(EKeyboardKey _key);
        bool IsMouseButtonPressed(EMouseButton _button);
        DJO_INLINE const Vec2f& GetMousePosition() const  { return m_mousePosition; }
        DJO_INLINE const Vec2f& GetMouseMove() const      { return m_mouseMove; }
        DJO_INLINE f32 GetMouseMoveSpeed() const          { return m_mouseMoveSpeed; }
        DJO_INLINE f32 GetMouseScroll() const             { return m_mouseWheelScroll; }

    private:

        friend class EngineApplication;
        static UPtr<Input> s_input;

        Input();

        EResult Init(EngineApplication& _app);
        void Shutdown();

        void StartEvents();
        void OnEvent(const WindowEvent& _event);
        void EndEvents();

        EngineApplication* m_app;
        Vec2f m_mousePosition;
        Vec2f m_previousMousePosition;
        Vec2f m_mouseMove;
        f32 m_mouseMoveSpeed;
        f32 m_mouseWheelScroll;
        bool m_doesMouseMoved;
    };
}
