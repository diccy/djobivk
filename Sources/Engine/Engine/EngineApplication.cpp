
#include <Engine/EngineApplication.hpp>

#include <Engine/ImGui/ImGuiDebugWindow_EngineApplication.hpp>
#include <Engine/Input.hpp>
#include <Graphics/Renderer.hpp>
#include <Window/WindowEvent.hpp>
#include <Window/Window.hpp>
#include <System/GlobalProfiler.hpp>
#include <System/Timer.hpp>
#include <ImGui/ImGuiManager.hpp>

#include <imgui_impl_glfw.h>

#include <thread>
#include <chrono>


namespace Djo
{
    EngineApplication::EngineApplication()
        : m_initDesc{}
        , m_windowEvents{}
        , m_window{ nullptr }
        , m_renderer{ nullptr }
        , m_imGuiDebugManager{ nullptr }
        , m_imGuiDebugWindow{ nullptr }
        , m_currentDt{ 0.f }
        , m_globalProfilerTimeRemaining{ 0.f }
        , m_isRunning{ false }
        , m_isLowFramerateActive{ false }
    {
    }

    EngineApplication::~EngineApplication()
    {
    }

    EResult
    EngineApplication::Init(const InitDesc& _initDesc)
    {
        DJO_PROFILE_FUNCTION();

        m_initDesc = _initDesc;

        DJO_OK_OR_RETURN(InitWindow(_initDesc));
        DJO_OK_OR_RETURN(InitInput());
        DJO_OK_OR_RETURN(InitImGui());
        DJO_OK_OR_RETURN(InitRenderer());

        // Client application callback after init, keep it in the end of the Init function
        const EResult afterInitResult = OnAfterInit();
        return afterInitResult;
    }

    EResult
    EngineApplication::InitWindow(const InitDesc& _initDesc)
    {
        DJO_PROFILE_FUNCTION();

        UPtr<Window> window{ DJO_CORE_NEW Window{} };
        WindowInitDesc windowInitDesc;
        windowInitDesc.name = _initDesc.windowName;
        windowInitDesc.initSize = _initDesc.windowInitSize;
        windowInitDesc.windowEventListener = this;
        DJO_OK_OR_RETURN(window->Init(windowInitDesc));
        m_window.Swap(window);

        return EResult::OK;
    }

    EResult
    EngineApplication::InitInput()
    {
        Input::s_input.Reset(DJO_CORE_NEW Input{});
        DJO_OK_OR_RETURN(Input::Get()->Init(*this));
        return EResult::OK;
    }

    EResult
    EngineApplication::InitImGui()
    {
        DJO_PROFILE_FUNCTION();

        // Init ImGui API

        if (!IMGUI_CHECKVERSION())
        {
            return MakeResult(EResult::FAIL, "IMGUI_CHECKVERSION failed");
        }
        if (::ImGui::CreateContext() == nullptr)
        {
            return MakeResult(EResult::FAIL, "ImGui::CreateContext failed");
        }
        //::ImGuiIO& io = ::ImGui::GetIO();
        //io.ConfigFlags |= ::ImGuiConfigFlags_NavEnableKeyboard;
        //io.ConfigFlags |= ::ImGuiConfigFlags_NavEnableGamepad;
        ::ImGui::StyleColorsDark();

        GLFWwindow* const glfwWindow = GetWindow().GetGLFWWindow();
        if (!::ImGui_ImplGlfw_InitForVulkan(glfwWindow, true))
        {
            return MakeResult(EResult::FAIL, "ImGui_ImplGlfw_InitForVulkan failed");
        }

        // Init ImGui manager

        m_imGuiDebugManager.Reset(DJO_CORE_NEW ImGuiManager{});

        m_imGuiDebugWindow.Reset(DJO_CORE_NEW ImGuiDebugWindow_EngineApplication{ *this });
        m_imGuiDebugManager->RegisterMainMenuBarItem(m_imGuiDebugWindow.Get());
        m_imGuiDebugManager->RegisterWindow(m_imGuiDebugWindow.Get());

        return EResult::OK;
    }

    EResult
    EngineApplication::InitRenderer()
    {
        DJO_PROFILE_FUNCTION();

        UPtr<Renderer> renderer{ DJO_CORE_NEW Renderer{} };
        DJO_OK_OR_RETURN(renderer->Init(GetWindow().GetGLFWWindow(), DoInitImGui{ true }));
        DJO_OK_OR_RETURN(renderer->InitImGuiDebugWindow(*m_imGuiDebugManager));
        m_renderer.Swap(renderer);
        return EResult::OK;
    }

    void
    EngineApplication::Destroy()
    {
        DJO_PROFILE_FUNCTION();

        OnBeforeDestroy();

        DestroyRenderer();
        DestroyImGui();
        DestroyInput();
        DestroyWindow();
    }

    void
    EngineApplication::DestroyRenderer()
    {
        m_renderer->DestroyImGuiDebugWindow(*m_imGuiDebugManager);
        m_renderer->Destroy();
        m_renderer.Release();
    }

    void
    EngineApplication::DestroyImGui()
    {
        DJO_PROFILE_FUNCTION();

        m_imGuiDebugManager->UnregisterWindow(m_imGuiDebugWindow.Get());
        m_imGuiDebugManager->UnregisterMainMenuBarItem(m_imGuiDebugWindow.Get());
        m_imGuiDebugWindow.Release();
        m_imGuiDebugManager.Release();

        ::ImGui_ImplGlfw_Shutdown();
        ::ImGui::DestroyContext();
    }

    void
    EngineApplication::DestroyInput()
    {
        Input::Get()->Shutdown();
        Input::s_input.Release();
    }

    void
    EngineApplication::DestroyWindow()
    {
        m_window->Close();
        m_window.Release();
    }

    EResult
    EngineApplication::Run()
    {
        Timer timer{};
        m_currentDt = 1000. / 60.;

        m_isRunning = true;
        while (m_isRunning)
        {
        #if defined(DJO_PROFILING_ENABLED)
            // Profiler
            if (m_globalProfilerTimeRemaining > 0.f)
            {
                m_globalProfilerTimeRemaining = m_globalProfilerTimeRemaining - (f32)m_currentDt.Sec();
                if (m_globalProfilerTimeRemaining <= 0.f)
                    GlobalProfiler::AllowRecording(false);
            }
        #endif

            // Inputs
            Input::Get()->StartEvents();
            m_window->PollEvents();
            DispatchWindowEvents();
            Input::Get()->EndEvents();
            ::ImGui_ImplGlfw_NewFrame();

            // Update step
            OnUpdate(m_currentDt);
            UpdateImGui();

            // Render
            m_renderer->Render(m_currentDt);

            // Finalize render
            m_window->Display();

            // For debug and gameplay tests purpose
            if (m_isLowFramerateActive)
                std::this_thread::sleep_for(std::chrono::milliseconds(100));

            m_currentDt = timer.Restart().Ms();
        }

        return EResult::OK;
    }

    void
    EngineApplication::OnEvent(const WindowEvent& _event)
    {
        m_windowEvents.push_back(_event);
    }

    void
    EngineApplication::DispatchWindowEvents()
    {
        for (WindowEvent& event : m_windowEvents)
        {
            bool consumedEvent = false;
            switch (event.type)
            {
                case WindowEvent::EType::KeyPressed:
                case WindowEvent::EType::KeyReleased:
                {
                    consumedEvent = ::ImGui::GetIO().WantCaptureKeyboard;
                    break;
                }
                case WindowEvent::EType::MouseButtonPressed:
                case WindowEvent::EType::MouseButtonReleased:
                case WindowEvent::EType::MouseMoved:
                case WindowEvent::EType::MouseWheelScrolled:
                {
                    consumedEvent = ::ImGui::GetIO().WantCaptureMouse;
                    break;
                }
            }
            if (consumedEvent)
                continue;

            Input::Get()->OnEvent(event);

            switch (event.type)
            {
                case WindowEvent::EType::Closed:
                {
                    Shutdown();
                    break;
                }
                case WindowEvent::EType::WindowResized:
                case WindowEvent::EType::FrameBufferResized:
                {
                    const bool isMinimized = (event.size.width == 0 && event.size.height == 0);
                    if (!isMinimized)
                    {
                        // TODO
                    }
                    break;
                }
                case WindowEvent::EType::KeyPressed:
                {
                    if (event.key.code == EKeyboardKey::Square2)
                        m_imGuiDebugManager->ToggleVisibility();
                    break;
                }
            }

            OnEvent(event);
        }

        m_windowEvents.clear();
    }

    bool
    EngineApplication::DoCaptureKeyboard() const
    {
        if (::ImGui::GetIO().WantCaptureKeyboard)
            return false;
        return m_window->HasFocus();
    }

    bool
    EngineApplication::DoCaptureMouse() const
    {
        if (::ImGui::GetIO().WantCaptureMouse)
            return false;
        return m_window->HasFocus();
    }

    bool
    EngineApplication::IsKeyPressed(const EKeyboardKey _key) const
    {
        return DoCaptureKeyboard() && m_window->IsKeyPressed(_key);
    }

    bool
    EngineApplication::IsMouseButtonPressed(const EMouseButton _button) const
    {
        return DoCaptureMouse() && m_window->IsMouseButtonPressed(_button);
    }

    void
    EngineApplication::Shutdown()
    {
        m_isRunning = false;
    }

    void
    EngineApplication::BeginImGuiFrame()
    {
        DJO_PROFILE_FUNCTION();

        ::ImGui::NewFrame();
    }

    void
    EngineApplication::EndImGuiFrame()
    {
        DJO_PROFILE_FUNCTION();

        ::ImGuiIO& io = ::ImGui::GetIO();
        const Vec2u viewportSize = GetWindow().GetFrameBufferSize();
        io.DisplaySize = ImVec2{ (f32)viewportSize.x, (f32)viewportSize.y };

        ::ImGui::Render();
    }

    void
    EngineApplication::UpdateImGui()
    {
        BeginImGuiFrame();
        m_imGuiDebugManager->Update();
        OnImGuiUpdate();
        EndImGuiFrame();
    }
}
