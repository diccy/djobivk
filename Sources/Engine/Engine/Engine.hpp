#pragma once

#include <Core/ResultEnum.hpp>
#include <Core/Pointers.hpp>

#include <type_traits>


namespace Djo
{
    // fw
    class EngineApplication;
    struct EngineApplicationInitDesc;

    class Engine
    {
    public:

        template <class TEngineApplication, DJO_TARG_ENABLE_IF((std::is_base_of_v<EngineApplication, TEngineApplication>))>
        static
        EResult
        Run(const EngineApplicationInitDesc& _initDesc)
        {
            return Run_Internal(UPtr<EngineApplication>{ DJO_CORE_NEW TEngineApplication{} }, _initDesc);
        }

    private:

        static EResult Run_Internal(UPtr<EngineApplication>&& _app, const EngineApplicationInitDesc& _initDesc);
    };
}
