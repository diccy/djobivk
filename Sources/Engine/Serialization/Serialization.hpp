#pragma once

#include <Core/StringView.hpp>


// Very inspired from https://github.com/USCiLab/cereal

namespace Djo
{
    namespace Srl
    {
        namespace _Internal
        {
            template <typename T>
            using NoConstNoRef_t = std::remove_const_t<std::remove_reference_t<T>>;

        #define DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(name_, func_, const_)\
            template <class TSerializer, class TData>\
            struct name_\
            {\
                template <class USerializer, class UData>\
                static auto Test(int)\
                    -> decltype(\
                        func_(std::declval<USerializer&>(), std::declval<const_ NoConstNoRef_t<UData>&>()), std::true_type());\
                template <class, class>\
                static std::false_type Test(...);\
                static constexpr bool c_value =\
                    std::is_same< decltype(Test<TSerializer, TData>(0)), std::true_type >::value;\
                static constexpr unsigned int c_count = c_value ? 1 : 0;\
            }

            DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(HasMemberFunctionReadWrite, Access::ReadWrite, );
            DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(HasMemberFunctionRead, Access::Read, );
            DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(HasMemberFunctionWrite, Access::Write, );
            DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(ExistsFunctionReadWriteData, ReadWriteData, );
            DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(ExistsFunctionReadData, ReadData, );
            DJO_SRL_INTERNAL_TEST_EXISTING_FUNC(ExistsFunctionWriteData, WriteData, );

        #undef DJO_SRL_INTERNAL_TEST_EXISTING_FUNC

            template <class TSerializer, class TData>
            static constexpr bool c_isReader =
                HasMemberFunctionReadWrite<TSerializer, TData>::c_value ||
                HasMemberFunctionRead<TSerializer, TData>::c_value ||
                ExistsFunctionReadWriteData<TSerializer, TData>::c_value ||
                ExistsFunctionReadData<TSerializer, TData>::c_value;
            template <class TSerializer, class TData>
            static constexpr bool c_isWriter =
                HasMemberFunctionReadWrite<TSerializer, TData>::c_value ||
                HasMemberFunctionWrite<TSerializer, const TData>::c_value ||
                ExistsFunctionReadWriteData<TSerializer, TData>::c_value ||
                ExistsFunctionWriteData<TSerializer, const TData>::c_value;
        }

        // Serializer base class
        // Usage
        // class MySerializer : public Djo::Srl::Serializer<MySerializer> {}

        template <class TImpl>
        class Serializer
        {
            DJO_NO_COPY_NO_MOVE(Serializer<TImpl>);

        public:
            using ThisType = Serializer<TImpl>;
            using ImplType = typename TImpl;

            Serializer(ImplType& _impl, const Version _version = { 1, 0, 0 })
                : m_impl{ _impl }
                , m_version{ _version }
            {
            }

            ~Serializer()
                = default;

            template <class ...TArgs>
            DJO_INLINE
            void
            Apply(TArgs&&... _args)
            {
                ReadWrite_Internal(std::forward<TArgs>(_args)...);
            }

            DJO_INLINE void SetVersion(const Version _v)  { m_version = _v; }
            DJO_INLINE Version GetVersion() const         { return m_version; }

        private:
            template <class T, class ...TArgs>
            DJO_INLINE
            void
            ReadWrite_Internal(T&& _head, TArgs&&... _tail)
            {
                ReadWrite_Internal<T>(std::forward<T>(_head));
                ReadWrite_Internal(std::forward<TArgs>(_tail)...);
            }
            template <class TData>
            DJO_INLINE
            void
            ReadWrite_Internal(TData&& _data)
            {
                constexpr u32 c_implCount =
                    _Internal::HasMemberFunctionReadWrite<ImplType, TData>::c_count +
                    _Internal::HasMemberFunctionRead<ImplType, TData>::c_count +
                    _Internal::HasMemberFunctionWrite<ImplType, TData>::c_count +
                    _Internal::ExistsFunctionReadWriteData<ImplType, TData>::c_count +
                    _Internal::ExistsFunctionReadData<ImplType, TData>::c_count +
                    _Internal::ExistsFunctionWriteData<ImplType, TData>::c_count;
                DJO_STATIC_ASSERT_MSG(c_implCount > 0u,
                    "No Serialize_Impl function implementation found for given TData type");
                DJO_STATIC_ASSERT_MSG(c_implCount < 2u,
                    "Several Serialize_Impl function implementations found for given TData type");

                BeforeSerialization(m_impl, _data);
                ReadWrite_Impl<_Internal::NoConstNoRef_t<TData>>(_data);
                AfterSerialization(m_impl, _data);
            }

            // Member access

            template <class TData, DJO_TARG_ENABLE_IF((_Internal::HasMemberFunctionReadWrite<ImplType, TData>::c_value))>
            DJO_INLINE
            void
            ReadWrite_Impl(const TData& _data)
            {
                Access::ReadWrite(m_impl, const_cast<TData&>(_data));
            }

            template <class TData, DJO_TARG_ENABLE_IF((_Internal::HasMemberFunctionRead<ImplType, TData>::c_value))>
            DJO_INLINE
            void
            ReadWrite_Impl(const TData& _data)
            {
                Access::Read(m_impl, const_cast<TData&>(_data));
            }

            template <class TData, DJO_TARG_ENABLE_IF((_Internal::HasMemberFunctionWrite<ImplType, TData>::c_value))>
            DJO_INLINE
            void
            ReadWrite_Impl(const TData& _data)
            {
                Access::Write(m_impl, _data);
            }

            // Generic data

            template <class TData, DJO_TARG_ENABLE_IF((_Internal::ExistsFunctionReadWriteData<ImplType, TData>::c_value))>
            DJO_INLINE
            void
            ReadWrite_Impl(const TData& _data)
            {
                ReadWriteData(m_impl, const_cast<TData&>(_data));
            }

            template <class TData, DJO_TARG_ENABLE_IF((_Internal::ExistsFunctionReadData<ImplType, TData>::c_value))>
            DJO_INLINE
            void
            ReadWrite_Impl(const TData& _data)
            {
                ReadData(m_impl, const_cast<TData&>(_data));
            }

            template <class TData, DJO_TARG_ENABLE_IF((_Internal::ExistsFunctionWriteData<ImplType, TData>::c_value))>
            DJO_INLINE
            void
            ReadWrite_Impl(const TData& _data)
            {
                WriteData(m_impl, _data);
            }

            ImplType& m_impl;
            Version m_version;
        };

        template <class TSerializer, class TData>
        DJO_INLINE
        void
        BeforeSerialization(TSerializer&, const TData&)
        {
            // empty
        }

        template <class TSerializer, class TData>
        DJO_INLINE
        void
        AfterSerialization(TSerializer&, const TData&)
        {
            // empty
        }

        // Call to Serialize member function
        // add friend struct Djo::Srl::Access if Serialize function is private
        struct Access
        {
            template <class TSerializer, class TData>
            DJO_INLINE static
            auto
            ReadWrite(TSerializer& _srl, TData& _data)
                -> decltype(_data.ReadWrite(_srl)) // needed for traits tests
            {
                return _data.ReadWrite(_srl);
            }

            template <class TSerializer, class TData>
            DJO_INLINE static
            auto
            Read(TSerializer& _srl, TData& _data)
                -> decltype(_data.Read(_srl)) // needed for traits tests
            {
                return _data.Read(_srl);
            }

            template <class TSerializer, class TData>
            DJO_INLINE static
            auto
            Write(TSerializer& _srl, TData& _data)
                -> decltype(_data.Write(_srl)) // needed for traits tests
            {
                return _data.Write(_srl);
            }
        };

        // Macro utils

    #define DJO_SRL_MEMBERS_FUNC(fn_)\
        template <class TSerializer>\
        DJO_INLINE\
        void\
        fn_(TSerializer& _srl)

    #define DJO_SRL_MEMBERS_DEC_IMPL(fn_, dec_, ...)\
        DJO_SRL_MEMBERS_FUNC(fn_)\
        {\
            _srl.Apply(DJO_APPLY_SEP(dec_, DJO_COMMA, __VA_ARGS__));\
        }

    #define DJO_SRL_MEMBERS_IMPL(fn_, ...)\
        DJO_SRL_MEMBERS_FUNC(fn_)\
        {\
            _srl.Apply(__VA_ARGS__);\
        }

        // NameDataPair utility struct, often needed for human readable data

        template <class TData>
        struct NameDataPair
        {
            StringView name; // const char*? 2 versions?
            TData& data;
        };

        template <class TData>
        constexpr
        NameDataPair<TData>
        MakeNDP(const StringView& _name, TData& _data)
        {
            return { _name, _data };
        }

    #define DJO_SRL_NDP(data_)  ::Djo::Srl::MakeNDP(DJO_STR(data_), data_)

        template <class TData>
        struct NameHashDataPair
        {
            u64 nameHash;
            TData& data;
        };

        template <class TData>
        constexpr
        NameHashDataPair<TData>
        MakeNHDP(const StringView& _name, TData& _data)
        {
            return { Hasher<StringView>{}(_name), _data };
        }

        template <class TSerializer, class TData, DJO_TARG_ENABLE_IF((!_Internal::c_isWriter<TSerializer, TData>))>
        constexpr
        NameHashDataPair<TData>
        MakeNHDP(const StringView& _name, TData& _data)
        {
            return { Hasher<StringView>{}(_name), _data };
        }

        template <class TSerializer, class TData, DJO_TARG_ENABLE_IF((_Internal::c_isWriter<TSerializer, TData>))>
        constexpr
        NameDataPair<TData>
        MakeNHDP(const StringView& _name, TData& _data)
        {
            return MakeNDP<TData>(_name, _data);
        }

    #define DJO_SRL_NHDP_NAME(name_, data_)      ::Djo::Srl::MakeNHDP(name_, data_)
    #define DJO_SRL_NHDP_NAME_SRL(name_, data_)  ::Djo::Srl::MakeNHDP<TSerializer>(name_, data_)
    #define DJO_SRL_NHDP(data_)                  DJO_SRL_NHDP_NAME(DJO_STR(data_), data_)
    #define DJO_SRL_NHDP_SRL(data_)              DJO_SRL_NHDP_NAME_SRL(DJO_STR(data_), data_)

    }
}
