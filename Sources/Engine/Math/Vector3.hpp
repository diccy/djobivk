#pragma once

#include <Math/GlmConfig.hpp>
#include <Core/Types.hpp>

#include <glm/vec3.hpp>


namespace Djo
{
    template <typename T>
    using Vec3T = glm::tvec3<T, c_glmQualifier>;
    using Vec3f = Vec3T<f32>;
    using Vec3d = Vec3T<f64>;
    using Vec3i = Vec3T<s32>;
    using Vec3u = Vec3T<u32>;

    DJO_STATIC_ASSERT(sizeof(Vec3f) == sizeof(f32) * 3);
    DJO_STATIC_ASSERT(sizeof(Vec3d) == sizeof(f64) * 3);
    DJO_STATIC_ASSERT(sizeof(Vec3i) == sizeof(s32) * 3);
    DJO_STATIC_ASSERT(sizeof(Vec3u) == sizeof(u32) * 3);
}
