#pragma once

#include <Math/GlmConfig.hpp>
#include <Core/Types.hpp>

#include <glm/vec4.hpp>


namespace Djo
{
    template <typename T>
    using Vec4T = glm::tvec4<T, c_glmQualifier>;
    using Vec4f = Vec4T<f32>;
    using Vec4d = Vec4T<f64>;
    using Vec4u8 = Vec4T<u8>;
    using Vec4i = Vec4T<s32>;
    using Vec4u = Vec4T<u32>;

    DJO_STATIC_ASSERT(sizeof(Vec4f) == sizeof(f32) * 4);
    DJO_STATIC_ASSERT(sizeof(Vec4d) == sizeof(f64) * 4);
    DJO_STATIC_ASSERT(sizeof(Vec4u8) == sizeof(u8) * 4);
    DJO_STATIC_ASSERT(sizeof(Vec4i) == sizeof(s32) * 4);
    DJO_STATIC_ASSERT(sizeof(Vec4u) == sizeof(u32) * 4);
}
