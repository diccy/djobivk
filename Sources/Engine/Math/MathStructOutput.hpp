#pragma once

#include <Math/Vector2.hpp>
#include <Math/Quaternion.hpp>
#include <Math/Matrix33.hpp>
#include <Math/Matrix44.hpp>

#include <ostream>


namespace Djo
{
    template <typename C, typename T>
    std::basic_ostream<C, std::char_traits<C>>&
    operator << (std::basic_ostream<C, std::char_traits<C>>& _os, const Vec2T<T>& _v)
    {
        _os << "v2[" << (_v.x) << ", " << (_v.y) << ']';
        return _os;
    }

    template <typename C, typename T>
    std::basic_ostream<C, std::char_traits<C>>&
    operator << (std::basic_ostream<C, std::char_traits<C>>& _os, const Vec3T<T>& _v)
    {
        _os << "v3[" << (_v.x) << ", " << (_v.y) << ", " << (_v.z) << ']';
        return _os;
    }

    template <typename C, typename T>
    std::basic_ostream<C, std::char_traits<C>>&
    operator << (std::basic_ostream<C, std::char_traits<C>>& _os, const Vec4T<T>& _v)
    {
        _os << "v4[" << (_v.x) << ", " << (_v.y) << ", " << (_v.z) << ", " << (_v.w) << ']';
        return _os;
    }

    template <typename C, typename T>
    std::basic_ostream<C, std::char_traits<C>>&
    operator << (std::basic_ostream<C, std::char_traits<C>>& _os, const QuatT<T>& _q)
    {
        _os << "q[" << (_q.x) << ", " << (_q.y) << ", " << (_q.z) << ", " << (_q.w) << ']';
        return _os;
    }

    template <typename C, typename T>
    std::basic_ostream<C, std::char_traits<C>>&
    operator << (std::basic_ostream<C, std::char_traits<C>>& _os, const Mat33T<T>& _m)
    {
        _os << '\n';
        _os << "m33[" << (_m[0][0]) << ", " << (_m[0][1]) << ", " << (_m[0][2]) << "]\n";
        _os << "m33[" << (_m[1][0]) << ", " << (_m[1][1]) << ", " << (_m[1][2]) << "]\n";
        _os << "m33[" << (_m[2][0]) << ", " << (_m[2][1]) << ", " << (_m[2][2]) << "]\n";
        return _os;
    }

    template <typename C, typename T>
    std::basic_ostream<C, std::char_traits<C>>&
    operator << (std::basic_ostream<C, std::char_traits<C>>& _os, const Mat44T<T>& _m)
    {
        _os << '\n';
        _os << "m44[" << (_m[0][0]) << ", " << (_m[0][1]) << ", " << (_m[0][2]) << ", " << (_m[0][3]) << "]\n";
        _os << "m44[" << (_m[1][0]) << ", " << (_m[1][1]) << ", " << (_m[1][2]) << ", " << (_m[1][3]) << "]\n";
        _os << "m44[" << (_m[2][0]) << ", " << (_m[2][1]) << ", " << (_m[2][2]) << ", " << (_m[2][3]) << "]\n";
        _os << "m44[" << (_m[3][0]) << ", " << (_m[3][1]) << ", " << (_m[3][2]) << ", " << (_m[3][3]) << "]\n";
        return _os;
    }
}
