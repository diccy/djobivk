#pragma once

#include <Math/GlmConfig.hpp>
#include <Core/Types.hpp>

#include <glm/gtc/quaternion.hpp>


namespace Djo
{
    template <typename T>
    using QuatT = glm::tquat<T, c_glmQualifier>;
    using Quatf = QuatT<f32>;
    using Quatd = QuatT<f64>;

    DJO_STATIC_ASSERT(sizeof(Quatf) == sizeof(f32) * 4);
    DJO_STATIC_ASSERT(sizeof(Quatd) == sizeof(f64) * 4);
}
