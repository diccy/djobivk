#pragma once

#include <Math/Vector3.hpp>

#include <glm/mat3x3.hpp>
#include <glm/gtc/matrix_transform.hpp>


namespace Djo
{
    template <typename T>
    using Mat33T = glm::tmat3x3<T, c_glmQualifier>;
    using Mat33f = Mat33T<f32>;
    using Mat33d = Mat33T<f64>;

    DJO_STATIC_ASSERT(sizeof(Mat33f) == sizeof(f32) * 3 * 3);
    DJO_STATIC_ASSERT(sizeof(Mat33d) == sizeof(f64) * 3 * 3);

    template <typename T>
    constexpr
    const Vec3T<T>&
    Mat33Right(const Mat33T<T>& _m) noexcept
    {
        return _m[0];
    }

    template <typename T>
    constexpr
    const Vec3T<T>&
    Mat33Up(const Mat33T<T>& _m) noexcept
    {
        return _m[1];
    }

    template <typename T>
    constexpr
    const Vec3T<T>&
    Mat33Forward(const Mat33T<T>& _m) noexcept
    {
        return _m[2];
    }
}
