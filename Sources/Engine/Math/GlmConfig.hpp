#pragma once

#include <Core/Core.hpp>

//#define GLM_FORCE_AVX
#define GLM_FORCE_DEPTH_ZERO_TO_ONE // Vulkan
#define GLM_FORCE_LEFT_HANDED // Vulkan

#include <glm/detail/qualifier.hpp>


namespace Djo
{
    constexpr glm::qualifier c_glmQualifier{ glm::highp };
}
