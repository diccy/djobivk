#pragma once

#include <Graphics/Camera.hpp>
#include <Core/Time.hpp>


namespace Djo
{
    class CameraController
    {
        DJO_NO_COPY_NO_MOVE(CameraController);

    public:

        CameraController();
        ~CameraController();

        DJO_INLINE void SetCamera(Camera* const _camera)      { m_camera = _camera; }
        DJO_INLINE void SetCameraWorldPos(const Vec3f& _pos)  { m_cameraGoalWorldPos = _pos; m_camera->SetWorldPosition(_pos); }
        DJO_INLINE void SetTargetWorldPos(const Vec3f& _pos)  { m_targetGoalWorldPos = _pos; m_targetWorldPos = _pos; }
        DJO_INLINE void SetMoveSpeed(const f32 _speed)        { m_moveSpeed = _speed; }

        void Update(Milliseconds64 _dt);

    private:

        friend void ImGuiDebugWidget(CameraController&);

        void MoveCamera(const Vec3f& _move);
        void MoveCameraAroundTarget(f32 _angle, const Vec3f& _axis);
        void MoveTarget(const Vec3f& _move);
        void MoveTargetAroundCamera(f32 _angle, const Vec3f& _axis);
        void ZoomToTarget(f32 _factor);

        void LookAtTarget(f32 _factor);
        void AvoidUpAxis();

        Camera* m_camera;
        Vec3f m_targetWorldPos;
        Vec3f m_cameraGoalWorldPos;
        Vec3f m_targetGoalWorldPos;
        f32 m_moveSpeed;
    };
}
