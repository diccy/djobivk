
#include <Game/CameraController.hpp>

#include <Math/Math.hpp>
#include <Engine/Input.hpp>


namespace Djo
{
    CameraController::CameraController()
        : m_camera{ nullptr }
        , m_targetWorldPos{ 0.f, 0.f, 0.f }
        , m_cameraGoalWorldPos{ 0.f, 0.f, 0.f }
        , m_targetGoalWorldPos{ 0.f, 0.f, 0.f }
        , m_moveSpeed{ 0.f }
    {
    }

    CameraController::~CameraController()
    {
    }

    void
    CameraController::Update(const Milliseconds64 _dt)
    {
        // Slide
        const bool moveForwardPressed = Input::Get()->IsKeyPressed(EKeyboardKey::W);
        const bool moveLeftPressed = Input::Get()->IsKeyPressed(EKeyboardKey::A);
        const bool moveBackPressed = Input::Get()->IsKeyPressed(EKeyboardKey::S);
        const bool moveRightPressed = Input::Get()->IsKeyPressed(EKeyboardKey::D);
        const Vec2f keyboardMove{
            (moveForwardPressed ? 1.f : 0.f) - (moveBackPressed ? 1.f : 0.f),
            (moveRightPressed ? 1.f : 0.f) - (moveLeftPressed ? 1.f : 0.f),
        };
        if (!Equals(glm::dot(keyboardMove, keyboardMove), 0.f, 0.01f))
        {
            constexpr f32 c_moveFactorMultiplier{ 0.5f };
            const f32 distFromTarget = glm::length(m_targetGoalWorldPos - m_cameraGoalWorldPos);
            const f64 moveFactor = Max(1.f, m_moveSpeed * distFromTarget * c_moveFactorMultiplier) * _dt.Sec();
            const Vec3f right = glm::normalize(Mat44Right(m_camera->GetWorldTransform())); // avoid cross products with goals positions, use direct camera transform as approximation
            const Vec3f forward = glm::normalize(Mat44Forward(m_camera->GetWorldTransform()));
            const Vec3f move = (forward * (f32)moveFactor * keyboardMove.x) + (right * (f32)moveFactor * keyboardMove.y);
            MoveCamera(move);
            MoveTarget(move);
        }

        // Zoom
        const f32 mouseScroll = Input::Get()->GetMouseScroll();
        if (!Equals(mouseScroll, 0.f, 0.01f))
        {
            constexpr f32 c_moveFactorMultiplier{ 3.f };
            const f32 distFromTarget = glm::length(m_targetGoalWorldPos - m_cameraGoalWorldPos);
            const f64 moveFactor = Max(1.f, m_moveSpeed * distFromTarget * c_moveFactorMultiplier) * _dt.Sec();
            const f32 moveFactorY = (f32)moveFactor * mouseScroll;
            ZoomToTarget(moveFactorY);
        }

        // Look around
        const bool doMoveTargetAroundCamera = Input::Get()->IsMouseButtonPressed(EMouseButton::Middle);
        const bool doMoveCameraAroundTarget = Input::Get()->IsMouseButtonPressed(EMouseButton::Right);
        if (doMoveTargetAroundCamera || doMoveCameraAroundTarget)
        {
            constexpr f32 c_rotFactorMultiplier{ 0.001f };
            const Vec2f mouseMove = Input::Get()->GetMouseMove();
            const f32 rotFactor = m_moveSpeed * c_rotFactorMultiplier;
            if (Abs(mouseMove.x) > 0.f)
            {
                const f32 rotFactorX = rotFactor * mouseMove.x;
                if (doMoveCameraAroundTarget)
                    MoveCameraAroundTarget(rotFactorX, c_cameraUp);
                else
                    MoveTargetAroundCamera(rotFactorX, c_cameraUp);
            }
            if (Abs(mouseMove.y) > 0.f)
            {
                const Vec3f dir = glm::normalize(m_targetGoalWorldPos - m_cameraGoalWorldPos);
                const Vec3f axis = glm::normalize(glm::cross(c_cameraUpClose, dir));
                const f32 angularSpeedFactor = Min(1.f, 1.2f - Abs(glm::dot(dir, c_cameraUp)));
                const f32 rotFactorY = rotFactor * angularSpeedFactor * mouseMove.y;
                if (doMoveCameraAroundTarget)
                    MoveCameraAroundTarget(rotFactorY, axis);
                else
                    MoveTargetAroundCamera(rotFactorY, axis);
            }
        }

        // Update
        // adapt lerp factor to framerate
        const f32 lerpFactor = (f32)Lerp(0.3, 0.6, LerpFactor<f64>(1. / 120., 0.1, _dt.Sec()));
        m_targetWorldPos = Lerp(m_targetWorldPos, m_targetGoalWorldPos, lerpFactor);
        m_camera->SetWorldPosition(Lerp(m_camera->GetWorldPosition(), m_cameraGoalWorldPos, lerpFactor));
        LookAtTarget(lerpFactor);
    }

    void
    CameraController::MoveCamera(const Vec3f& _move)
    {
        m_cameraGoalWorldPos += _move;
    }

    void
    CameraController::MoveCameraAroundTarget(const f32 _angle, const Vec3f& _axis)
    {
        const Vec3f fromOrigin = m_cameraGoalWorldPos - m_targetGoalWorldPos;
        m_cameraGoalWorldPos = (glm::angleAxis(_angle, _axis) * fromOrigin) + m_targetGoalWorldPos;
    }

    void
    CameraController::MoveTarget(const Vec3f& _move)
    {
        m_targetGoalWorldPos += _move;
    }

    void
    CameraController::MoveTargetAroundCamera(const f32 _angle, const Vec3f& _axis)
    {
        const Vec3f fromOrigin = m_targetGoalWorldPos - m_cameraGoalWorldPos;
        m_targetGoalWorldPos = (glm::angleAxis(_angle, _axis) * fromOrigin) + m_cameraGoalWorldPos;
    }

    void
    CameraController::ZoomToTarget(const f32 _factor)
    {
        constexpr f32 c_maxSmooth = 5.f;
        constexpr f32 c_minSmooth = 1.f;

        const Vec3f fromOrigin = m_targetGoalWorldPos - m_cameraGoalWorldPos;
        const f32 dirLength = glm::length(fromOrigin);
        const Vec3f dir = fromOrigin / dirLength;
        f32 smoothFactor = 1.f;
        if (dirLength < c_maxSmooth && _factor > 0.f)
            smoothFactor = Lerp(0.f, 1.f, (Max(c_minSmooth, dirLength) - c_minSmooth) / (c_maxSmooth - c_minSmooth));

        MoveCamera(dir * (_factor * smoothFactor));
    }

    void
    CameraController::AvoidUpAxis()
    {
        constexpr f32 c_maxDot = 0.95f;

        const Vec3f fromOrigin = m_targetGoalWorldPos - m_cameraGoalWorldPos;
        const f32 dirLength = glm::length(fromOrigin);
        const Vec3f dir = fromOrigin / dirLength;
        const f32 dot = glm::dot(dir, c_cameraUp);
        const f32 absDot = Abs(dot);
        if (absDot > c_maxDot)
        {
            const Vec3f axis = (Equals(absDot, 1.f, 0.01f)) ? c_cameraRight : glm::normalize(glm::cross(c_cameraUp, dir));
            const f32 angle = ::acosf(dot > 0.f ? c_maxDot : -c_maxDot) - glm::radians(180.f);
            const Vec3f newPos = ((glm::angleAxis(angle, axis) * c_cameraUp) * dirLength) + m_targetGoalWorldPos;
            m_camera->SetWorldPosition(newPos);
        }
    }

    void
    CameraController::LookAtTarget(const f32 _factor)
    {
        AvoidUpAxis();
        const Vec3f dir = glm::normalize(m_targetWorldPos - m_camera->GetWorldPosition());
        const Quatf rotLookAt = glm::quatLookAt(dir, c_cameraUp);
        m_camera->SetWorldRotation(glm::slerp(m_camera->GetWorldRotation(), rotLookAt, _factor));
    }
}
