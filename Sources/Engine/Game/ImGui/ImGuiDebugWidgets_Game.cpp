
#include <Game/ImGui/ImGuiDebugWidgets_Game.hpp>

#include <Game/CameraController.hpp>
#include <Graphics/ImGui/ImGuiDebugWidgets_Graphics.hpp>
#include <ImGui/ImGui.hpp>


namespace Djo
{
    void
    ImGuiDebugWidget(CameraController& _cameraController)
    {
        ::ImGui::InputFloat3("Cam goal pos", &_cameraController.m_cameraGoalWorldPos[0]);
        ::ImGui::InputFloat3("Target goal pos", &_cameraController.m_targetGoalWorldPos[0]);
        ::ImGui::InputFloat3("Target pos", &_cameraController.m_targetWorldPos[0]);
        {
            const f32 minDistance = 1.f;
            const f32 maxDistance = 1000.f;
            f32 targetDistance = glm::distance(_cameraController.m_cameraGoalWorldPos, _cameraController.m_targetGoalWorldPos);
            const bool isInBetweenBefore = (minDistance <= targetDistance && targetDistance <= maxDistance);
            if (::ImGui::SliderFloat("Target dist", &targetDistance, minDistance, maxDistance, "%.1f", ::ImGuiSliderFlags_Logarithmic))
            {
                if (isInBetweenBefore)
                {
                    const Vec3f dir = glm::normalize(_cameraController.m_targetGoalWorldPos - _cameraController.m_cameraGoalWorldPos);
                    _cameraController.m_targetGoalWorldPos = _cameraController.m_cameraGoalWorldPos + (dir * targetDistance);
                }
            }
        }
        ::ImGui::InputFloat("Move speed", &_cameraController.m_moveSpeed);

        if (_cameraController.m_camera != nullptr)
        {
            if (::ImGui::TreeNode("Camera"))
            {
                ImGuiDebugWidget(*_cameraController.m_camera);
                ::ImGui::TreePop();
            }
        }
        else
        {
            ::ImGui::Text("[Camera nullptr]");
        }
    }
}
