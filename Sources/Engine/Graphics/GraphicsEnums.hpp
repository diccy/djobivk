#pragma once

#include <Core/Types.hpp>
#include <Core/TypeTraits.hpp>
#include <Core/StructUtils.hpp>


namespace Djo
{
    enum class EDataType : u32
    {
        Byte = 0,
        UByte,
        Short,
        UShort,
        Int,
        UInt,
        Float,
        Half,
        Double,

        DJO_ENUM_COUNT_INVALID
    };

    DJO_LUT_START(c_lutDataElemSize, ECount<EDataType>, EDataType, u32, c_invalidUint<u32>)
        DJO_LUT_MATCH(EValue(EDataType::Byte),    sizeof(s8))
        DJO_LUT_MATCH(EValue(EDataType::UByte),   sizeof(u8))
        DJO_LUT_MATCH(EValue(EDataType::Short),   sizeof(s16))
        DJO_LUT_MATCH(EValue(EDataType::UShort),  sizeof(u16))
        DJO_LUT_MATCH(EValue(EDataType::Int),     sizeof(s32))
        DJO_LUT_MATCH(EValue(EDataType::UInt),    sizeof(u32))
        DJO_LUT_MATCH(EValue(EDataType::Float),   sizeof(f32))
        DJO_LUT_MATCH(EValue(EDataType::Half),    sizeof(u16))
        DJO_LUT_MATCH(EValue(EDataType::Double),  sizeof(f64))
    DJO_LUT_END();

    constexpr u32 GetDataElemSize(const EDataType _type)  { return c_lutDataElemSize.a[EValue(_type)]; }


    enum class EBufferTarget : u32
    {
        Array = 0,
        ElementArray,
        PixelPack,
        PixelUnpack,
        Uniform,
        Texture,
        TransformFeedback,
        CopyRead,
        CopyWrite,
        DrawIndirect,
        ShaderStorage,
        DispatchIndirect,
        Query,
        AtomicCounter,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EBufferUsage : u32
    {
        StaticDraw = 0,
        DynamicDraw,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EBufferAccess : u32
    {
        Read = 0,
        Write,
        ReadWrite,

        DJO_ENUM_COUNT_INVALID
    };

    enum class ELayoutDataType : u32
    {
        Float = 0,
        Float2,
        Float3,
        Float4,
        Float44,
        Short,
        UShort,
        Int,
        UInt,

        DJO_ENUM_COUNT_INVALID
    };

    DJO_LUT_START(c_lutLayoutDataElemCount, ECount<ELayoutDataType>, ELayoutDataType, u32, c_invalidUint<u32>)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float),   1)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float2),  2)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float3),  3)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float4),  4)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float44), 16)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Short),   1)
        DJO_LUT_MATCH(EValue(ELayoutDataType::UShort),  1)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Int),     1)
        DJO_LUT_MATCH(EValue(ELayoutDataType::UInt),    1)
    DJO_LUT_END();

    constexpr u32 GetLayoutDataElemCount(const ELayoutDataType _type) { return c_lutLayoutDataElemCount.a[EValue(_type)]; }

    DJO_LUT_START(c_lutLayoutDataElemSize, ECount<ELayoutDataType>, ELayoutDataType, u32, c_invalidUint<u32>)
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float),    sizeof(f32))
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float2),   sizeof(f32))
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float3),   sizeof(f32))
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float4),   sizeof(f32))
        DJO_LUT_MATCH(EValue(ELayoutDataType::Float44),  sizeof(f32))
        DJO_LUT_MATCH(EValue(ELayoutDataType::Short),    sizeof(s16))
        DJO_LUT_MATCH(EValue(ELayoutDataType::UShort),   sizeof(u16))
        DJO_LUT_MATCH(EValue(ELayoutDataType::Int),      sizeof(s32))
        DJO_LUT_MATCH(EValue(ELayoutDataType::UInt),     sizeof(u32))
    DJO_LUT_END();

    constexpr u32 GetLayoutDataElemSize(const ELayoutDataType _type)  { return c_lutLayoutDataElemSize.a[EValue(_type)]; }

    constexpr
    u32
    GetLayoutDataSize(const ELayoutDataType _type)
    {
        return GetLayoutDataElemSize(_type) * GetLayoutDataElemCount(_type);
    }

    enum class ELayoutPropertySemantic : u32
    {
        Unknown = 0,

        Position,
        Rotation,
        Scale,
        Color,
        TextureCoord,
        Transform,
        Normal,
        Tangent,
        Bitangent,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EShaderStage : u32
    {
        Vertex = 0,
        Geometry,
        Fragment,
        Compute,

        DJO_ENUM_COUNT_INVALID
    };

    enum class ETextureTarget : u32
    {
        Tex1D = 0,
        Tex2D,
        Tex3D,
        CubeMap,

        DJO_ENUM_COUNT_INVALID
    };

    enum class ETextureWrap : u32
    {
        ClampToEdge = 0,
        ClampToBorder,
        Repeat,
        MirrorClampToEdge,
        MirrorRepeat,

        DJO_ENUM_COUNT_INVALID
    };

    enum class ETextureFilter : u32
    {
        Nearest = 0,
        Linear,
        NearestMipmapLinear,
        NearestMipmapNearest,
        LinearMipmapNearest,
        LinearMipmapLinear,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EBufferFormat : u32
    {
        R = 0,
        RI,
        RG,
        RGB,
        RGBA,
        Depth,
        Stencil,
        DepthStencil,

        DJO_ENUM_COUNT_INVALID
    };

    DJO_LUT_START(c_lutBufferFormatChannelCount, ECount<EBufferFormat>, EBufferFormat, u32, c_invalidUint<u32>)
        DJO_LUT_MATCH(EValue(EBufferFormat::R),             1)
        DJO_LUT_MATCH(EValue(EBufferFormat::RI),            1)
        DJO_LUT_MATCH(EValue(EBufferFormat::RG),            2)
        DJO_LUT_MATCH(EValue(EBufferFormat::RGB),           3)
        DJO_LUT_MATCH(EValue(EBufferFormat::RGBA),          4)
        DJO_LUT_MATCH(EValue(EBufferFormat::Depth),         1)
        DJO_LUT_MATCH(EValue(EBufferFormat::Stencil),       1)
        DJO_LUT_MATCH(EValue(EBufferFormat::DepthStencil),  2)
    DJO_LUT_END();

    constexpr u32 GetBufferFormatChannelCount(const EBufferFormat _format)  { return c_lutBufferFormatChannelCount.a[EValue(_format)]; }

    /*
    constexpr
    EBufferFormat
    GetRGBABufferFormat(const u32 _channelCount)
    {
        if (0 < _channelCount && _channelCount < 4)
        {
            constexpr EBufferFormat c_formats[4]
            {
                EBufferFormat::R,
                EBufferFormat::RG,
                EBufferFormat::RGB,
                EBufferFormat::RGBA
            };
            return c_formats[_channelCount - 1];
        }
        return EBufferFormat::Invalid;
    }
    */

    enum class EBufferFormatSized : u32
    {
        R8 = 0,
        R32I,
        RG8,
        RGB8,
        sRGB8,
        RGBA8,
        sRGBA8,
        RGBA32F,
        D32F,
        S8,
        D24S8,

        DJO_ENUM_COUNT_INVALID
    };

    DJO_LUT_START(c_lutBufferFormatSizedChannelCount, ECount<EBufferFormatSized>, EBufferFormatSized, u32, c_invalidUint<u32>)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::R8),       1)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::R32I),     1)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RG8),      3)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RGB8),     3)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::sRGB8),    4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RGBA8),    4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::sRGBA8),   4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RGBA32F),  4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::D32F),     1)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::S8),       1)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::D24S8),    2)
    DJO_LUT_END();

    constexpr u32 GetBufferFormatChannelCount(const EBufferFormatSized _formatSized)  { return c_lutBufferFormatSizedChannelCount.a[EValue(_formatSized)]; }

    DJO_LUT_START(c_lutBufferFormatSizedSize, ECount<EBufferFormatSized>, EBufferFormatSized, u32, c_invalidUint<u32>)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::R8),       sizeof(u8))
        DJO_LUT_MATCH(EValue(EBufferFormatSized::R32I),     sizeof(u32))
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RG8),      sizeof(u8) * 2)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RGB8),     sizeof(u8) * 3)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::sRGB8),    sizeof(u8) * 3)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RGBA8),    sizeof(u8) * 4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::sRGBA8),   sizeof(u8) * 4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::RGBA32F),  sizeof(f32) * 4)
        DJO_LUT_MATCH(EValue(EBufferFormatSized::D32F),     sizeof(f32))
        DJO_LUT_MATCH(EValue(EBufferFormatSized::S8),       sizeof(u8))
        DJO_LUT_MATCH(EValue(EBufferFormatSized::D24S8),    sizeof(u32))
    DJO_LUT_END();

    constexpr u32 GetBufferFormatSize(const EBufferFormatSized _formatSized)  { return c_lutBufferFormatSizedSize.a[EValue(_formatSized)]; }

    /*
    constexpr
    EBufferFormatSized
    GetRGBA8BufferFormatSized(const u32 _channelCount)
    {
        if (0 < _channelCount && _channelCount < 4)
        {
            constexpr EBufferFormatSized c_formats[4]
            {
                EBufferFormatSized::R8,
                EBufferFormatSized::RG8,
                EBufferFormatSized::RGB8,
                EBufferFormatSized::RGBA8
            };
            return c_formats[_channelCount - 1];
        }
        return EBufferFormatSized::Invalid;
    }
    */

    enum class EBufferMapFlag : u32
    {
        Read = Flag(0),
        Write = Flag(1),
        InvalidateRange = Flag(2),
        InvalidateBuffer = Flag(3),
        FlushExplicit = Flag(4),
        Unsychronized = Flag(5),
        Persistent = Flag(6),
        Coherent = Flag(7),

        DJO_ENUM_FLAG_COUNT
    };

    enum class ERenderBufferAttachment : u32
    {
        Depth = 0,
        Stencil,
        DepthStencil,
        Color,

        DJO_ENUM_COUNT_INVALID
    };
    using RenderBufferAttachment = std::underlying_type_t<ERenderBufferAttachment>;
    template <u32 N>
    constexpr RenderBufferAttachment c_colorAttachment{ EValue(ERenderBufferAttachment::Color) + N };

    enum class EPrimitiveTopology : u32
    {
        Points = 0,
        Lines,
        LinesLoop,
        LinesStrip,
        Triangles,
        TriangleStrip,
        TriangleFan,
        LinesAdjacency,
        LinesStripAdjacency,
        TrianglesAdjacency,
        TriangleStripAdjacency,
        Patches,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EPolygonMode : u32
    {
        Point = 0,
        Line,
        Fill,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EFrontFace : u32
    {
        CW = 0,
        CCW,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EBlendFactor : u32
    {
        Zero = 0,
        One,
        SourceColor,
        OneMinusSourceColor,
        DestColor,
        OneMinusDestColor,
        SourceAlpha,
        OneMinusSourceAlpha,
        DestAlpha,
        OneMinusDestAlpha,
        ConstantColor,
        OneMinusConstantColor,
        ConstantAlpha,
        OneMinusContantAlpha,
        SourceAlphaSaturate,
        Source1Color,
        OneMinusSource1Color,
        Source1Alpha,
        OneMinusSource1Alpha,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EBlendOperation : u32
    {
        Add = 0,
        Subtract,
        ReverseSubtract,
        Min,
        Max,

        DJO_ENUM_COUNT_INVALID
    };

    enum class EComparison : u32
    {
        Never = 0,
        Less,
        Equal,
        LessOrEqual,
        Greater,
        NotEqual,
        GreaterOrEqual,
        Always,

        DJO_ENUM_COUNT_INVALID
    };
}
