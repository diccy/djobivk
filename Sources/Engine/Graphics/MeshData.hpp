#pragma once

#include <Graphics/BufferLayout.hpp>
#include <Core/StdVector.hpp>


namespace Djo
{
    struct MeshData
    {
        BufferLayout vertexLayout;
        std::vector<f32> vertices;
        std::vector<u32> indices;

        DJO_INLINE u32 GetVertexCount() const  { return (u32)vertices.size() / vertexLayout.GetElementCount(); }
        DJO_INLINE u32 GetIndexCount() const   { return (u32)indices.size(); }
        DJO_INLINE u32 GetFaceCount() const    { return GetIndexCount() / 3; }
        
        DJO_INLINE
        bool
        IsValid() const
        {
            return !vertices.empty()
                && vertexLayout.GetElementCount() > 0
                && (vertices.size() % vertexLayout.GetElementCount()) == 0;
        }
    };
}
