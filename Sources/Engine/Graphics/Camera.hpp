#pragma once

#include <Scene/SceneNode.hpp>


namespace Djo
{
    enum class ECameraProjection
    {
        Perspective,
        Orthographic,
    };

    #if (GLM_COORDINATE_SYSTEM == GLM_RIGHT_HANDED)

        static constexpr Vec3f c_cameraRight{ 1.f, 0.f, 0.f };
        static constexpr Vec3f c_cameraUp{ 0.f, 1.f, 0.f };
        static constexpr Vec3f c_cameraForward{ 0.f, 0.f, -1.f };
        const Vec3f c_cameraUpClose{ glm::normalize(Vec3f{ 0.01f, 1.f, 0.f }) }; // glm::normalize not constexpr

    #else
        // TODO
        #error
    #endif

    struct CameraDesc
    {
        f32 near;
        f32 far;
        f32 ratio;
    };
    struct PerspectiveCameraDesc
        : public CameraDesc
    {
        f32 fov; // radians
    };
    struct OrthographicCameraDesc
        : public CameraDesc
    {
        f32 width;
    };

    struct ViewContext
    {
        Mat44f viewProjection;
        Mat44f view;
        Mat44f projection;
        Vec3f viewPos;
        PerspectiveCameraDesc desc;
    };

    class Camera
        : public SceneNode
    {
        DJO_NO_COPY_NO_MOVE(Camera);

    public:

        Camera();
        ~Camera() override;

        DJO_INLINE const PerspectiveCameraDesc& GetDesc() const  { return m_desc; }

        DJO_INLINE void SetNear(const f32 _near)        { m_desc.near = _near; }
        DJO_INLINE void SetFar(const f32 _far)          { m_desc.far = _far; }
        DJO_INLINE void SetRatio(const f32 _ratio)      { m_desc.ratio = _ratio; }
        DJO_INLINE void SetFieldOfView(const f32 _fov)  { m_desc.fov = _fov; }

        ViewContext BuildViewContext() const;

    private:

        friend void ImGuiDebugWidget(Camera&);

        PerspectiveCameraDesc m_desc;
    };
}
