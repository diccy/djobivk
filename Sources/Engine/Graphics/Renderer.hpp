#pragma once

#include <Graphics/Vulkan/VulkanContext.hpp>
#include <System/LocalFile.hpp>
#include <Window/Window.hpp>
#include <Math/Matrix44.hpp>
#include <Math/Vector3.hpp>
#include <Core/Time.hpp>
#include <Core/Utils.hpp>

#include <unordered_map>


// fw
struct GLFWwindow;

namespace Djo
{
    // fw
    class Camera;
    class ImGuiManager;

    using DoInitImGui = HardAlias<bool>;

    struct VertexInputDescription
    {
        std::vector<::VkVertexInputBindingDescription> bindings;
        std::vector<::VkVertexInputAttributeDescription> attributes;
        ::VkPipelineVertexInputStateCreateFlags flags;
    };
    struct Vertex
    {
        Vec3f pos;
        Vec3f normal;
        Vec3f color;

        static VertexInputDescription GetDesc();
    };
    struct Mesh
    {
        std::vector<Vertex> vertices;
        AllocatedVkBuffer vkBuffer;
    };
    struct MeshPushConstants
    {
        Mat44f model;
    };
    struct Material
    {
        ::VkPipeline pipeline;
        ::VkPipelineLayout pipelineLayout;
    };
    struct RenderableObject
    {
        Mat44f transform;
        Mesh* mesh;
        Material* material;
    };
    struct GPUCameraData
    {
        Mat44f view;
        Mat44f proj;
        Mat44f viewProj;
    };
    struct GPUSceneData
    {
        Vec4f fogColor; // w for exponent
        Vec4f fogDistances; // x min, y max, zw unused
        Vec4f ambientColor;
        Vec4f sunlightDirection; // w sun power
        Vec4f sunlightColor;
    };
    struct GPUObjectsData
    {
        Mat44f transform;
    };

    class Renderer
    {
        DJO_NO_COPY_NO_MOVE(Renderer);

    public:

        Renderer();
        ~Renderer();

        EResult Init(::GLFWwindow* _window, DoInitImGui _initImGui);
        void Destroy();

        EResult InitImGuiDebugWindow(ImGuiManager& _imGuiManager);
        void DestroyImGuiDebugWindow(ImGuiManager& _imGuiManager);

        DJO_INLINE void SetCamera(Camera* const _camera) { m_camera = _camera; }

        void Render(Milliseconds64 _dt);

    private:

        friend class ImGuiDebugWindow_Renderer;

        struct FrameData
        {
            ::VkCommandPool commandPool;
            ::VkCommandBuffer commandBuffer;
            ::VkSemaphore presentSemaphore;
            ::VkSemaphore renderSemaphore;
            ::VkFence renderFence;
            ::VkDescriptorSet globalDescSet;
            AllocatedVkBuffer cameraBuffer;
            ::VkDescriptorSet objectsDescSet;
            AllocatedVkBuffer objectsBuffer;
        };

        EResult EnumerateRequiredExtensions(std::vector<const char*>* _outExtensions) const;
        sz GetAlignedBufferSize(sz _baseSize) const;

        EResult InitCommandPool();
        void DestroyCommandPool();
        EResult InitCommandBuffer();
        void DestroyCommandBuffer();
        EResult InitDefaultRenderPass();
        void DestroyDefaultRenderPass();
        EResult InitFrameBuffers();
        void DestroyFrameBuffers();
        EResult InitSynchronization();
        void DestroySynchronization();
        EResult InitDescriptors();
        void DestroyDescriptors();
        EResult InitGraphicsPipeline();
        EResult LoadShaderModuleFromFile(LocalFile& _shaderFile, ::VkShaderModule* _outShaderModule);
        void DestroyGraphicsPipeline();
        EResult InitImGui();
        void DestroyImGui();
        EResult LoadMeshes();
        void DestroyMeshes();

        Material* CreateMaterial(const std::string& _name, ::VkPipeline _pipeline, ::VkPipelineLayout _layout);
        Material* GetMaterial(const std::string& _name);
        void RenderObjects(const FrameData& _currentFrameData, const MemView<const RenderableObject>& _objects);
        EResult InitScene();
        void DestroyScene();

        void RenderImGui(const FrameData& _currentFrameData);

        Vulkan::Context m_context;

        static constexpr u32 c_concurrentFrameCount{ 3 };
        FrameData m_frames[c_concurrentFrameCount];
        u32 m_frameNumber;

        ::VkRenderPass m_defaultRenderPass;
        std::vector<::VkFramebuffer> m_frameBuffers;

        ::VkDescriptorSetLayout m_globalDescSetLayout;
        ::VkDescriptorSetLayout m_objectsDescSetLayout;
        ::VkDescriptorPool m_descPool;
        GPUSceneData m_sceneData;
        AllocatedVkBuffer m_sceneDataBuffer;

        ::VkDescriptorPool m_imguiDescPool;

        std::vector<RenderableObject> m_renderables;
        std::unordered_map<std::string, Material> m_materials;
        std::vector<Mesh> m_meshes;

        UPtr<ImGuiDebugWindow_Renderer> m_imGuiDebugWindow;

        Camera* m_camera;

        bool m_isImGuiInitialized;
    };
}
