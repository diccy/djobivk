#pragma once

#include <Graphics/MeshData.hpp>
#include <Core/ResultEnum.hpp>


namespace Djo
{
    enum class EMeshDataPrimitive : u32
    {
        Quad = 0u,
        Cube,

        DJO_ENUM_COUNT_INVALID
    };

    namespace MeshDataPrimitiveFactory
    {
        struct Desc
        {
            MemView<const ELayoutPropertySemantic> propertySemantics{};
            EMeshDataPrimitive primitive{ EMeshDataPrimitive::Invalid };
        };

        EResult Build(const Desc& _desc, MeshData* _outMeshData);
    }
}
