
#include <Graphics/MeshDataPrimitive.hpp>

#include <Graphics/MeshDataPrimitiveRaw.hpp>


namespace Djo
{
    namespace MeshDataPrimitiveFactory
    {
        using namespace MeshDataPrimitiveRaw;

        EResult
        Build(const Desc& _desc, MeshData* const _outResult)
        {
            if (_desc.primitive >= EMeshDataPrimitive::Invalid)
                return MakeResult(EResult::FAIL, "Invalid primitive requested");

            u32 askedSemanticsMask = 0;
            for (const ELayoutPropertySemantic semantic : _desc.propertySemantics)
            {
                askedSemanticsMask |= Flag(EValue(semantic));
            }
            if (!TestFlag(c_primitivePropertySemanticsMask, askedSemanticsMask))
                return MakeResult(EResult::FAIL, "Unsupported property requested");

            // Build lookup table

            const RawMeshData& rawMeshData = c_lutRawMeshData.a[EValue(_desc.primitive)];

            struct PropRawData
            {
                const f32* v; // pointer to data
                u32 rawId; // data id
                u32 elemCount; // element count per property
            };
            MaxArray<PropRawData, BufferLayout::c_maxPropertyCount> lutProperties;
            for (const ELayoutPropertySemantic semantic : _desc.propertySemantics)
            {
                for (u32 i = 0; i < c_primitivePropertyCount; ++i)
                {
                    if (c_primitivePropertySemanticsLayout[i] == semantic)
                    {
                        lutProperties.PushBack({ rawMeshData.properties[i].data, i, GetLayoutDataElemCount(c_primitiveBufferLayout[i].type) });
                        break;
                    }
                }
            }

            // Build mesh data

            MeshData meshData;

            // set buffer layout
            for (const PropRawData& r : lutProperties)
            {
                meshData.vertexLayout.Add(c_primitiveBufferLayout[r.rawId]);
            }

            // grab vertices (interleaved)
            const u32 vertexElementCount = meshData.vertexLayout.GetElementCount();
            meshData.vertices.clear();
            meshData.vertices.resize(vertexElementCount * rawMeshData.verticesCount);
            f32* vertices = meshData.vertices.data();
            for (u32 v = 0; v < rawMeshData.verticesCount; ++v)
            {
                for (PropRawData& r : lutProperties)
                {
                    DJO_ASSERT(vertices + r.elemCount <= (meshData.vertices.data() + meshData.vertices.size()));
                    DJO_ASSERT(r.v + r.elemCount <= rawMeshData.properties[r.rawId].GetEnd());

                    ::memcpy(vertices, r.v, r.elemCount * sizeof(f32));
                    vertices += r.elemCount;
                    r.v += r.elemCount;
                }
            }

            // grab indices
            meshData.indices.assign(rawMeshData.indices.begin(), rawMeshData.indices.end());

            *_outResult = std::move(meshData);
            return EResult::OK;
        }
    }
}
