
#include <Graphics/BufferLayout.hpp>


namespace Djo
{
    BufferLayout::BufferLayout(std::initializer_list<PropertyDesc> _propDescs)
        : BufferLayout{}
    {
        Set({ _propDescs.begin(), _propDescs.size() });
    }

    void
    BufferLayout::Add(const PropertyDesc& _propDesc)
    {
        DJO_ASSERT(m_properties.GetSize() < c_maxPropertyCount);

        Property property{ _propDesc };
        property.size = GetLayoutDataSize(_propDesc.type);
        property.offset = m_stride;
        m_stride += property.size;
        m_elementCount += GetLayoutDataElemCount(property.desc.type);
        m_properties.PushBack(std::move(property));
    }

    void
    BufferLayout::Set(const MemView<const PropertyDesc>& _propDescs)
    {
        m_properties.Resize(0);
        m_stride = 0;
        m_elementCount = 0;
        for (const PropertyDesc& propDesc : _propDescs)
        {
            Add(propDesc);
        }
    }

    bool
    BufferLayout::operator == (const BufferLayout& _other) const
    {
        if (m_properties.GetSize() != _other.m_properties.GetSize()
            || m_stride != _other.m_stride
            || m_elementCount != _other.m_elementCount)
        {
            return false;
        }
        const u32 propCount = m_properties.GetSize();
        for (u32 i = 0; i < propCount; ++i)
        {
            const Property& p1 = m_properties[i];
            const Property& p2 = _other.m_properties[i];
            if (p1.desc.type != p2.desc.type || p1.desc.normalized != p2.desc.normalized
                || p1.offset != p2.offset || p1.size != p2.size)
            {
                return false;
            }
        }
        return true;
    }
}
