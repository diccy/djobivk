#pragma once

#include <Graphics/MeshDataPrimitive.hpp>


namespace Djo
{
    namespace MeshDataPrimitiveRaw
    {
        ///////////////////////////////////////////////////////////////////////
        // These must be consistent

        constexpr u32 c_primitivePropertyCount{ 4 };

        constexpr BufferLayout::PropertyDesc c_primitiveBufferLayout[c_primitivePropertyCount]{
            { ELayoutDataType::Float3, false }, // position
            { ELayoutDataType::Float2, false }, // uv
            { ELayoutDataType::Float3, false }, // normal
            { ELayoutDataType::Float4, false }, // color
        };

        constexpr ELayoutPropertySemantic c_primitivePropertySemanticsLayout[c_primitivePropertyCount]{
            ELayoutPropertySemantic::Position,
            ELayoutPropertySemantic::TextureCoord,
            ELayoutPropertySemantic::Normal,
            ELayoutPropertySemantic::Color,
        };

        constexpr u32 c_primitivePropertySemanticsMask = []() -> u32
        {
            u32 mask = 0;
            for (const ELayoutPropertySemantic semantic : c_primitivePropertySemanticsLayout)
                mask |= Flag(EValue(semantic));
            return mask;
        }();

        ///////////////////////////////////////////////////////////////////////

        namespace Quad
        {
            constexpr u32 c_verticesCount{ 4 };
            const u32 c_indices[] =
            {
                0, 1, 2, 0, 2, 3,
            };
            const f32 c_pos[] =
            {
                -1.0f,  1.0f,  0.0f,
                -1.0f, -1.0f,  0.0f,
                 1.0f, -1.0f,  0.0f,
                 1.0f,  1.0f,  0.0f,
            };
            const f32 c_uv[] =
            {
                0.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
            };
            const f32 c_normal[] =
            {
                 0.0f,  0.0f,  1.0f,
                 0.0f,  0.0f,  1.0f,
                 0.0f,  0.0f,  1.0f,
                 0.0f,  0.0f,  1.0f,
            };
            const f32 c_color[] =
            {
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
            };
        }
        namespace Cube
        {
            constexpr u32 c_verticesCount{ 24 };
            const u32 c_indices[] =
            {
                 0,  1,  2,  0,  2,  3,
                 4,  5,  6,  4,  6,  7,
                 8,  9, 10,  8, 10, 11,
                12, 13, 14, 12, 14, 15,
                16, 17, 18, 16, 18, 19,
                20, 21, 22, 20, 22, 23,
            };
            const f32 c_pos[] =
            {
                 0.5f, -0.5f, -0.5f,
                 0.5f,  0.5f, -0.5f,
                 0.5f,  0.5f,  0.5f,
                 0.5f, -0.5f,  0.5f,

                 0.5f,  0.5f,  0.5f,
                 0.5f,  0.5f, -0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f,  0.5f,  0.5f,

                -0.5f, -0.5f,  0.5f,
                 0.5f, -0.5f,  0.5f,
                 0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,

                -0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f,  0.5f,

                -0.5f, -0.5f, -0.5f,
                 0.5f, -0.5f, -0.5f,
                 0.5f, -0.5f,  0.5f,
                -0.5f, -0.5f,  0.5f,

                 0.5f,  0.5f, -0.5f,
                 0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f,  0.5f, -0.5f,
            };
            const f32 c_uv[] =
            {
                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,

                1.0f, 1.0f,
                1.0f, 0.0f,
                0.0f, 0.0f,
                0.0f, 1.0f,

                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,

                1.0f, 1.0f,
                1.0f, 0.0f,
                0.0f, 0.0f,
                0.0f, 1.0f,

                0.0f, 0.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
                0.0f, 1.0f,

                1.0f, 1.0f,
                1.0f, 0.0f,
                0.0f, 0.0f,
                0.0f, 1.0f,
            };
            const f32 c_normal[] =
            {
                 1.0f,  0.0f,  0.0f,
                 1.0f,  0.0f,  0.0f,
                 1.0f,  0.0f,  0.0f,
                 1.0f,  0.0f,  0.0f,

                 0.0f,  1.0f,  0.0f,
                 0.0f,  1.0f,  0.0f,
                 0.0f,  1.0f,  0.0f,
                 0.0f,  1.0f,  0.0f,

                 0.0f,  0.0f,  1.0f,
                 0.0f,  0.0f,  1.0f,
                 0.0f,  0.0f,  1.0f,
                 0.0f,  0.0f,  1.0f,

                -1.0f,  0.0f,  0.0f,
                -1.0f,  0.0f,  0.0f,
                -1.0f,  0.0f,  0.0f,
                -1.0f,  0.0f,  0.0f,

                 0.0f, -1.0f,  0.0f,
                 0.0f, -1.0f,  0.0f,
                 0.0f, -1.0f,  0.0f,
                 0.0f, -1.0f,  0.0f,

                 0.0f,  0.0f, -1.0f,
                 0.0f,  0.0f, -1.0f,
                 0.0f,  0.0f, -1.0f,
                 0.0f,  0.0f, -1.0f,
            };
            const f32 c_color[] =
            {
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,

                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,

                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,

                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,

                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,

                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
                1.f, 1.f, 1.f, 1.f,
            };
        }

        struct RawMeshData
        {
            u32 verticesCount;
            MemView<const u32> indices;
            MemView<const f32> properties[c_primitivePropertyCount];
        };

        DJO_LUT_START(c_lutRawMeshData, ECount<EMeshDataPrimitive>, EMeshDataPrimitive, RawMeshData, {})
            #define DJO_LUT_MATCH_PRIMITIVE(primEnum_, primNamespace_)\
                DJO_LUT_MATCH(EValue(primEnum_), DJO_UNPAR(({\
                    primNamespace_::c_verticesCount,\
                    primNamespace_::c_indices,\
                    primNamespace_::c_pos,\
                    primNamespace_::c_uv,\
                    primNamespace_::c_normal,\
                    primNamespace_::c_color\
                })))
            DJO_LUT_MATCH_PRIMITIVE(EMeshDataPrimitive::Quad, Quad)
            DJO_LUT_MATCH_PRIMITIVE(EMeshDataPrimitive::Cube, Cube)
            #undef DJO_LUT_MATCH_PRIMITIVE
        DJO_LUT_END();
    }
}
