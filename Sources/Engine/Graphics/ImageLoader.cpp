
#include <Graphics/ImageLoader.hpp>

#include <Graphics/ImageData.hpp>
#include <System/LocalFile.hpp>
#include <Core/Utils.hpp>
#include <Core/TypeTraits.hpp>

#include <stb_image.h>


namespace Djo
{
    namespace ImageLoader
    {
        EResult
        FromContent(const MemView<const u8>& _content, ImageData* const _outImageData, const u32 _requiredChannelCount /*= 0*/)
        {
            int width, height, channelCount, desiredChannelCount;
            desiredChannelCount = 0; // 0 == no desired channel count, take image as is
            if (_requiredChannelCount != 0)
            {
                DJO_ASSERT(1 <= _requiredChannelCount && _requiredChannelCount <= 4);
                desiredChannelCount = Clamp<u32>(_requiredChannelCount, 1, 4);
            }

            ::stbi_set_flip_vertically_on_load(1);
            ::stbi_uc* const stbImageData = ::stbi_load_from_memory(_content.data, (int)_content.GetMemSize(), &width, &height, &channelCount, desiredChannelCount);
            if (stbImageData == nullptr)
                return MakeResult(EResult::FAIL, "stbi_load_from_memory failed");

            // std_image doc: "If desired_channels is non-zero, *channels has the number of components that would have been output otherwise."
            channelCount = desiredChannelCount == 0 ? channelCount : desiredChannelCount;

            DJO_TODO("ImageLoader : can avoid data copy?");
            const u32 bitmapDataSize = width * height * channelCount;
            _outImageData->bitmap.resize(bitmapDataSize);
            ::memcpy((void*)_outImageData->bitmap.data(), (const void*)stbImageData, bitmapDataSize);
            _outImageData->size.x = width;
            _outImageData->size.y = height;
            DJO_ASSERT(_outImageData->GetChannelCount() == (u32)channelCount);
            ::stbi_image_free(stbImageData);

            return EResult::OK;
        }

        EResult
        FromFile(LocalFile& _file, ImageData* const _outImageData, const u32 _requiredChannelCount /*= 0*/)
        {
            DJO_OK_OR_RETURN(_file.Open(EOpenModeFlag::Read | EOpenModeFlag::Binary));
            DJO_OK_OR_RETURN(_file.Read(&_outImageData->bitmap));
            return FromContent(ToView(_outImageData->bitmap), _outImageData, _requiredChannelCount);
        }
    }
}
