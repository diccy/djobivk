#pragma once

#include <Graphics/GraphicsEnums.hpp>
#include <Core/ClassTraits.hpp>
#include <Core/MaxArray.hpp>


namespace Djo
{
    class BufferLayout
    {
        DJO_ALL_DEFAULT(BufferLayout);

    public:

        struct PropertyDesc
        {
            ELayoutDataType type;
            bool normalized;
        };
        struct Property
        {
            PropertyDesc desc;
            u32 size;
            u32 offset;
        };
        using IsInterleaved = HardAlias<bool>;

        static constexpr u32 c_maxPropertyCount{ 8u };
        using PropertiesArray = MaxArray<Property, c_maxPropertyCount>;

        BufferLayout(std::initializer_list<PropertyDesc> _properties);

        bool operator == (const BufferLayout& _other) const;

        DJO_INLINE const PropertiesArray& GetProperties() const  { return m_properties; }
        DJO_INLINE u32 GetElementCount() const                   { return m_elementCount; }
        DJO_INLINE u32 GetStride() const                         { return m_stride; }

        void Add(const PropertyDesc& _property);
        void Set(const MemView<const PropertyDesc>& _properties);

    private:

        PropertiesArray m_properties{};
        u32 m_elementCount{ 0 };
        u32 m_stride{ 0 };
    };

    using PropertySemanticsArray = MaxArray<ELayoutPropertySemantic, BufferLayout::c_maxPropertyCount>;
}
