#pragma once

#include <Math/Vector4.hpp>
#include <Core/StructUtils.hpp>
#include <Core/ClassTraits.hpp>
#include <Core/TypeTraits.hpp>


namespace Djo
{
    // fw
    enum class EColor : u32;

    struct Color
        : public Vec4f
    {
        DJO_ALL_DEFAULT(Color);

        Color(const f32 _r, const f32 _g, const f32 _b, const f32 _a = 1.f)
            : Vec4f{ _r, _g, _b, _a }
        {
        }

        constexpr
        Color(const Vec4f& _v)
            : Vec4f{ _v }
        {
        }

        explicit
        Color(const Vec4u8& _v)
            : Vec4f{ (1.f / 255.f) * Vec4f{_v} }
        {
        }

        constexpr explicit
        Color(const u32 _rgba)
            : Vec4f{}
        {
            const U32ToU8 v{ _rgba };
            r = (f32)(v.i8_3) / 255.f;
            g = (f32)(v.i8_2) / 255.f;
            b = (f32)(v.i8_1) / 255.f;
            a = (f32)(v.i8_0) / 255.f;
        }

        constexpr explicit
        Color(const EColor _rgba)
            : Color{ EValue(_rgba) }
        {
        }

        constexpr explicit
        operator u32() const
        {
            return U8ToU32{ (u8)(a * 255.f),
                            (u8)(b * 255.f),
                            (u8)(g * 255.f),
                            (u8)(r * 255.f) }.i32;
        }
    };

    struct Color32
        : public Vec4u8
    {
        DJO_ALL_DEFAULT(Color32);

        Color32(const u8 _r, const u8 _g, const u8 _b, const u8 _a = 1.f)
            : Vec4u8{ _r, _g, _b, _a }
        {
        }

        constexpr
        Color32(const Vec4u8& _v)
            : Vec4u8{ _v }
        {
        }

        constexpr explicit
        Color32(const Vec4f& _v)
            : Vec4u8{ _v * 255.f }
        {
        }

        constexpr explicit
        Color32(const u32 _rgba)
            : Vec4u8{}
        {
            const U32ToU8 v{ _rgba };
            r = v.i8_3;
            g = v.i8_2;
            b = v.i8_1;
            a = v.i8_0;
        }

        constexpr explicit
        Color32(const EColor _rgba)
            : Color32{ EValue(_rgba) }
        {
        }

        constexpr
        operator u32() const
        {
            return U8ToU32{ a, b, g, r }.i32;
        }
    };

    // ALL BELOW ASSUMES BIG ENDIANESS (rgba) vvv

    #define DJO_COLOR_U32_RGBA(r_, g_, b_, a_)      ((((u32)(r_) << 24) & 0xFF000000u) | (((u32)(g_) << 16) & 0x00FF0000u) | (((u32)(b_) << 8) & 0x0000FF00u) | ((u32)(a_) & 0x000000FFu))
    #define DJO_COLOR_U32_RGB(r_, g_, b_)           DJO_COLOR_U32_RGBA(r_, g_, b_, 0xFF)
    #define DJO_COLOR_U32_ALPHA(u32color_, alpha_)  (((u32)(u32color_) & 0xFFFFFF00u) | ((u32)(alpha_) & 0x000000FFu))

    enum class EColor : u32
    {
        // basics
        BLACK           = 0x000000FF,
        GRAY            = 0x808080FF,
        BLUE            = 0x0000FFFF,
        CYAN            = 0x00FFFFFF,
        GREEN           = 0x00FF00FF,
        YELLOW          = 0xFFFF00FF,
        RED             = 0xFF0000FF,
        MAGENTA         = 0xFF00FFFF,

        // useful ones
        NEUTRAL_NORMAL_MAP = 0x8080FFFF,

        // others from http://msdn.microsoft.com/en-us/library/system.windows.media.colors(v=vs.110).aspx
        ALICEBLUE       = 0xF0F8FFFF,
        ANTIQUEWHITE    = 0xFAEBD7FF,
        AQUA            = 0x00FFFFFF,
        AQUAMARINE      = 0x7FFFD4FF,
        AZURE           = 0xF0FFFFFF,
        BEIGE           = 0xF5F5DCFF,
        BISQUE          = 0xFFE4C4FF,
        BLANCHEDALMOND  = 0xFFEBCDFF,
        BLUEVIOLET      = 0x8A2BE2FF,
        BROWN           = 0xA52A2AFF,
        BURLYWOOD       = 0xDEB887FF,
        CADETBLUE       = 0x5F9EA0FF,
        CHARTREUSE      = 0x7FFF00FF,
        CHOCOLATE       = 0xD2691EFF,
        CORAL           = 0xFF7F50FF,
        CORNFLOWERBLUE  = 0x6495EDFF,
        CORNSILK        = 0xFFF8DCFF,
        CRIMSON         = 0xDC143CFF,
        DARKBLUE        = 0x00008BFF,
        DARKCYAN        = 0x008B8BFF,
        DARKGOLDENROD   = 0xB8860BFF,
        DARKGRAY        = 0xA9A9A9FF,
        DARKGREEN       = 0x006400FF,
        DARKKHAKI       = 0xBDB76BFF,
        DARKMAGENTA     = 0x8B008BFF,
        DARKOLIVEGREEN  = 0x556B2FFF,
        DARKORANGE      = 0xFF8C00FF,
        DARKORCHID      = 0x9932CCFF,
        DARKRED         = 0x8B0000FF,
        DARKSALMON      = 0xE9967AFF,
        DARKSEAGREEN    = 0x8FBC8FFF,
        DARKSLATEBLUE   = 0x483D8BFF,
        DARKSLATEGRAY   = 0x2F4F4FFF,
        DARKTURQUOISE   = 0x00CED1FF,
        DARKVIOLET      = 0x9400D3FF,
        DEEPPINK        = 0xFF1493FF,
        DEEPSKYBLUE     = 0x00BFFFFF,
        DIMGRAY         = 0x696969FF,
        DODGERBLUE      = 0x1E90FFFF,
        FIREBRICK       = 0xB22222FF,
        FLORALWHITE     = 0xFFFAF0FF,
        FORESTGREEN     = 0x228B22FF,
        FUCHSIA         = 0xFF00FFFF,
        GAINSBORO       = 0xDCDCDCFF,
        GHOSTWHITE      = 0xF8F8FFFF,
        GOLD            = 0xFFD700FF,
        GOLDENROD       = 0xDAA520FF,
        GREENYELLOW     = 0xADFF2FFF,
        HONEYDEW        = 0xF0FFF0FF,
        HOTPINK         = 0xFF69B4FF,
        INDIANRED       = 0xCD5C5CFF,
        INDIGO          = 0x4B0082FF,
        IVORY           = 0xFFFFF0FF,
        KHAKI           = 0xF0E68CFF,
        LAVENDER        = 0xE6E6FAFF,
        LAVENDERBLUSH   = 0xFFF0F5FF,
        LAWNGREEN       = 0x7CFC00FF,
        LEMONCHIFFON    = 0xFFFACDFF,
        LIGHTBLUE       = 0xADD8E6FF,
        LIGHTCORAL      = 0xF08080FF,
        LIGHTCYAN       = 0xE0FFFFFF,
        LIGHTGOLDENRODYELLOW = 0xFAFAD2FF,
        LIGHTGRAY       = 0xD3D3D3FF,
        LIGHTGREEN      = 0x90EE90FF,
        LIGHTPINK       = 0xFFB6C1FF,
        LIGHTSALMON     = 0xFFA07AFF,
        LIGHTSEAGREEN   = 0x20B2AAFF,
        LIGHTSKYBLUE    = 0x87CEFAFF,
        LIGHTSLATEGRAY  = 0x778899FF,
        LIGHTSTEELBLUE  = 0xB0C4DEFF,
        LIGHTYELLOW     = 0xFFFFE0FF,
        LIME            = 0x00FF00FF,
        LIMEGREEN       = 0x32CD32FF,
        LINEN           = 0xFAF0E6FF,
        MAROON          = 0x800000FF,
        MEDIUMAQUAMARINE = 0x66CDAAFF,
        MEDIUMBLUE      = 0x0000CDFF,
        MEDIUMORCHID    = 0xBA55D3FF,
        MEDIUMPURPLE    = 0x9370DBFF,
        MEDIUMSEAGREEN  = 0x3CB371FF,
        MEDIUMSLATEBLUE = 0x7B68EEFF,
        MEDIUMSPRINGGREEN = 0x00FA9AFF,
        MEDIUMTURQUOISE = 0x48D1CCFF,
        MEDIUMVIOLETRED = 0xC71585FF,
        MIDNIGHTBLUE    = 0x191970FF,
        MINTCREAM       = 0xF5FFFAFF,
        MISTYROSE       = 0xFFE4E1FF,
        MOCCASIN        = 0xFFE4B5FF,
        NAVAJOWHITE     = 0xFFDEADFF,
        NAVY            = 0x000080FF,
        OLDLACE         = 0xFDF5E6FF,
        OLIVE           = 0x808000FF,
        OLIVEDRAB       = 0x6B8E23FF,
        ORANGE          = 0xFFA500FF,
        ORANGERED       = 0xFF4500FF,
        ORCHID          = 0xDA70D6FF,
        PALEGOLDENROD   = 0xEEE8AAFF,
        PALEGREEN       = 0x98FB98FF,
        PALETURQUOISE   = 0xAFEEEEFF,
        PALEVIOLETRED   = 0xDB7093FF,
        PAPAYAWHIP      = 0xFFEFD5FF,
        PEACHPUFF       = 0xFFDAB9FF,
        PERU            = 0xCD853FFF,
        PINK            = 0xFFC0CBFF,
        PLUM            = 0xDDA0DDFF,
        POWDERBLUE      = 0xB0E0E6FF,
        PURPLE          = 0x800080FF,
        ROSYBROWN       = 0xBC8F8FFF,
        ROYALBLUE       = 0x4169E1FF,
        SADDLEBROWN     = 0x8B4513FF,
        SALMON          = 0xFA8072FF,
        SANDYBROWN      = 0xF4A460FF,
        SEAGREEN        = 0x2E8B57FF,
        SEASHELL        = 0xFFF5EEFF,
        SIENNA          = 0xA0522DFF,
        SILVER          = 0xC0C0C0FF,
        SKYBLUE         = 0x87CEEBFF,
        SLATEBLUE       = 0x6A5ACDFF,
        SLATEGRAY       = 0x708090FF,
        SNOW            = 0xFFFAFAFF,
        SPRINGGREEN     = 0x00FF7FFF,
        STEELBLUE       = 0x4682B4FF,
        TAN             = 0xD2B48CFF,
        TEAL            = 0x008080FF,
        THISTLE         = 0xD8BFD8FF,
        TOMATO          = 0xFF6347FF,
        TURQUOISE       = 0x40E0D0FF,
        VIOLET          = 0xEE82EEFF,
        WHEAT           = 0xF5DEB3FF,
        WHITE           = 0xFFFFFFFF,
        WHITESMOKE      = 0xF5F5F5FF,
        YELLOWGREEN     = 0x9ACD32FF,
    };
}
