#pragma once

#include <Graphics/GraphicsEnums.hpp>
#include <Graphics/Camera.hpp>
#include <Math/Vector2.hpp>


namespace Djo
{
    struct ViewportContext
    {
        Vec2i offset;
        Vec2u size;
    };

    struct Scissor
    {
        ViewportContext viewport;
        bool enable;
    };

    struct Rasterization
    {
        EPolygonMode polygon;
        EFrontFace front;
        bool enableFaceCulling;
    };

    struct Blend
    {
        EBlendFactor colorSrc;
        EBlendFactor colorDst;
        EBlendOperation colorOp;
        //EBlendFactor alphaSrc;
        //EBlendFactor alphaDst;
        //EBlendOperation alphaOp;
        bool enable;
    };

    struct Depth
    {
        EComparison comp;
        bool enable;
    };

    struct RenderContext
    {
        ViewportContext viewport;
        ViewContext view;
        Scissor scissor;
        Rasterization rasterization;
        Blend blend;
        Depth depth;
        Vec2u mousePosition; // move it from here?
    };

    struct TextureDesc
    {
        ETextureTarget target;
        ETextureWrap wrap;
        ETextureFilter minFilter;
        ETextureFilter magFilter;
        EBufferFormat format;
        EBufferFormatSized formatSized;
        EDataType dataType;
    };
}
