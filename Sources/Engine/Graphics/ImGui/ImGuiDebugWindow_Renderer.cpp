
#include <Graphics/ImGui/ImGuiDebugWindow_Renderer.hpp>

#include <Graphics/Renderer.hpp>
#include <ImGui/ImGuiManager.hpp>
#include <ImGui/ImGui.hpp>


namespace Djo
{
    const char* ImGuiDebugWindow_Renderer::s_menuName{ "Renderer" };
    const char* ImGuiDebugWindow_Renderer::s_windowName{ "Debug Renderer" };

    ImGuiDebugWindow_Renderer::ImGuiDebugWindow_Renderer(Renderer& _renderer)
        : m_renderer{ _renderer }
        , m_isWindowOpen{ false }
    {
    }

    ImGuiDebugWindow_Renderer::~ImGuiDebugWindow_Renderer()
    {
    }

    const char*
    ImGuiDebugWindow_Renderer::GetMenuName() const
    {
        return s_menuName;
    }

    void
    ImGuiDebugWindow_Renderer::UpdateImGuiMenuItem()
    {
        ::ImGui::MenuItem(s_windowName, nullptr, &m_isWindowOpen);
    }

    void
    ImGuiDebugWindow_Renderer::UpdateImGuiWindow()
    {
        if (m_isWindowOpen)
        {
            if (::ImGui::Begin(s_windowName, &m_isWindowOpen))
            {
                UpdateVulkanContext();
            }
            ImGui::End();
        }
    }

    void
    ImGuiDebugWindow_Renderer::UpdateVulkanContext()
    {
        if (::ImGui::CollapsingHeader("Vulkan context"))
        {
            UpdatePhysicalDevice();
        }
    }

    void
    ImGuiDebugWindow_Renderer::UpdatePhysicalDevice()
    {
        if (::ImGui::TreeNode("Physical device"))
        {
            const ::VkPhysicalDeviceProperties& p = m_renderer.m_context.physicalDeviceProperties;
            ::ImGui::Text("api version: %u", p.apiVersion);
            ::ImGui::Text("driver version: %u", p.driverVersion);
            ::ImGui::Text("vendor id: %u", p.vendorID);
            ::ImGui::Text("device id: %u", p.deviceID);
            ::ImGui::Text("device type: %d", p.deviceType);
            ::ImGui::Text("device name: %s", p.deviceName);
            //::ImGui::Text("pipeline cache uuid: %d", p.pipelineCacheUUID);
            if (::ImGui::TreeNode("Limits"))
            {
                const ::VkPhysicalDeviceLimits& l = p.limits;
                ::ImGui::Text("minUniformBufferOffsetAlignment: %zu", l.minUniformBufferOffsetAlignment);
                ::ImGui::TreePop();
            }
            if (::ImGui::TreeNode("Sparse properties"))
            {
                const ::VkPhysicalDeviceSparseProperties sp = p.sparseProperties;
                ::ImGui::TreePop();
            }
            ::ImGui::TreePop();
        }
    }
}
