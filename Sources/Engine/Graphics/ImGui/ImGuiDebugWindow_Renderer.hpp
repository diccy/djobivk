#pragma once

#include <ImGui/IImGuiDebugWindowImpl.hpp>
#include <Core/ClassTraits.hpp>


namespace Djo
{
    // fw
    class Renderer;

    class ImGuiDebugWindow_Renderer
        : public IImGuiDebugWindowImpl
    {
        DJO_NO_DEFAULT_CTOR(ImGuiDebugWindow_Renderer);
        DJO_NO_COPY_NO_MOVE(ImGuiDebugWindow_Renderer);

    public:

        static const char* s_menuName;
        static const char* s_windowName;

        ImGuiDebugWindow_Renderer(Renderer& _renderer);
        ~ImGuiDebugWindow_Renderer();

        virtual const char* GetMenuName() const override;
        virtual void UpdateImGuiMenuItem() override;
        virtual void UpdateImGuiWindow() override;

    private:

        void UpdateVulkanContext();
        void UpdatePhysicalDevice();

        Renderer& m_renderer;
        bool m_isWindowOpen;
    };
}
