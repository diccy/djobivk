
#include <Graphics/ImGui/ImGuiDebugWidgets_Graphics.hpp>

#include <Graphics/Camera.hpp>
#include <Scene/ImGui/ImGuiDebugWidgets_Scene.hpp>
#include <ImGui/ImGui.hpp>
#include <Core/Utils.hpp>


namespace Djo
{
    void
    ImGuiDebugWidget(Camera& _camera)
    {
        {
            f32 fovDegrees = glm::degrees(_camera.m_desc.fov);
            if (::ImGui::SliderFloat("FOV", &fovDegrees, 10.f, 150.f, "%.1f"))
            {
                _camera.m_desc.fov = glm::radians(fovDegrees);
            }
        }
        {
            f32 nearFar[2]{ _camera.m_desc.near, _camera.m_desc.far };
            if (::ImGui::SliderFloat2("Near Far", nearFar, 0.01f, 5000.f, "%.3f", ::ImGuiSliderFlags_Logarithmic))
            {
                _camera.m_desc.near = Min(nearFar[0], nearFar[1]);
                _camera.m_desc.far = Max(nearFar[0], nearFar[1]);
            }
        }

        ::ImGui::Text("Ratio: %.3f", _camera.m_desc.ratio);

        if (::ImGui::TreeNode("SceneNode"))
        {
            ImGuiDebugWidget(*static_cast<SceneNode*>(&_camera));
            ::ImGui::TreePop();
        }
    }
}
