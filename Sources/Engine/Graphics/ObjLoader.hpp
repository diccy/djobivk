#pragma once

#include <Graphics/MeshData.hpp>
#include <System/FSPath.hpp>
#include <Math/Vector3.hpp>
#include <Core/ResultEnum.hpp>
#include <Core/StdVector.hpp>
#include <Core/StdString.hpp>


// dev from https://github.com/Bly7/OBJ-Loader/blob/master/Source/OBJ_Loader.h

namespace Djo
{
    class ObjLoader
    {
        DJO_NO_COPY_NO_MOVE(ObjLoader);

    public:

        struct Params
        {
            FSPath fsp{};
            bool computeNormals{ true };
        };

        struct Material
        {
            FSPath sourceFsp;
            std::string name;

            using umask = u8; // adapt to flag count
            umask mask; // used numeric properties flags

            // Phong model
            Vec3f ka; // ambient
            static constexpr umask c_flag_ka = Flag<umask>(0);
            Vec3f kd; // diffuse
            static constexpr umask c_flag_kd = Flag<umask>(1);
            Vec3f ks; // spec
            static constexpr umask c_flag_ks = Flag<umask>(2);
            f32 ns; // spec exponent
            static constexpr umask c_flag_ns = Flag<umask>(3);
            f32 d; // dissolve (alpha) (always set, default to 1.0)

            // Texture Phong model
            std::string ka_texName; // ambient map
            std::string kd_texName; // diffuse map
            std::string ks_texName; // spec map
            std::string ns_texName; // spec exponent map
            std::string d_texName; // dissolve (alpha)
            std::string bump_texName; // normal map
            bool useTextures;
        };

        struct Mesh
        {
            std::string name;
            MeshData data;
            PropertySemanticsArray layoutSemantics;
            u32 materialId;
        };

        ObjLoader();
        ~ObjLoader();

        const Params& GetParams() const                    { return m_params; }
        const std::vector<Material>& GetMaterials() const  { return m_materials; }
        const std::vector<Mesh>& GetMeshes() const         { return m_meshes; }

        EResult Load(const Params& _param);

    private:

        Params m_params;
        std::vector<Material> m_materials;
        std::vector<Mesh> m_meshes;
    };
}
