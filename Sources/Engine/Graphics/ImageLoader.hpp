#pragma once

#include <Core/ResultEnum.hpp>
#include <Core/StdVector.hpp>


namespace Djo
{
    // fw
    class LocalFile;
    struct ImageData;

    namespace ImageLoader
    {
        EResult FromContent(const MemView<const u8>& _content, ImageData* _outImageData, u32 _requiredChannelCount = 0);
        EResult FromFile(LocalFile& _file, ImageData* _outImageData, u32 _requiredChannelCount = 0);
    }
}
