
#include <Graphics/Vulkan/Vulkan.hpp>

#include <Core/GlobalLog.hpp>


namespace Djo
{
#if defined (DJO_VK_ENABLE_VALIDATION_LAYERS)

    namespace VulkanHelper
    {
        ::VkResult
        CreateDebugUtilsMessengerEXT(::VkInstance const _instance, const ::VkDebugUtilsMessengerCreateInfoEXT* const _createInfo, const ::VkAllocationCallbacks* const _allocator, ::VkDebugUtilsMessengerEXT* const _debugMessenger)
        {
            const auto func = (::PFN_vkCreateDebugUtilsMessengerEXT)::vkGetInstanceProcAddr(_instance, "vkCreateDebugUtilsMessengerEXT");
            return (func != nullptr) ? func(_instance, _createInfo, _allocator, _debugMessenger) : VK_ERROR_EXTENSION_NOT_PRESENT;
        }

        void
        DestroyDebugUtilsMessengerEXT(::VkInstance const _instance, ::VkDebugUtilsMessengerEXT const _debugMessenger, const ::VkAllocationCallbacks* const _allocator)
        {
            const auto func = (::PFN_vkDestroyDebugUtilsMessengerEXT)::vkGetInstanceProcAddr(_instance, "vkDestroyDebugUtilsMessengerEXT");
            (func != nullptr) ? func(_instance, _debugMessenger, _allocator) : VK_ERROR_EXTENSION_NOT_PRESENT;
        }

        VKAPI_ATTR
        ::VkBool32
        VKAPI_CALL
        DebugCallback(const ::VkDebugUtilsMessageSeverityFlagBitsEXT _severity, const ::VkDebugUtilsMessageTypeFlagsEXT _type, const ::VkDebugUtilsMessengerCallbackDataEXT* const _data, void* const /*_userData*/)
        {
            ELogLevel logLevel;
            switch (_severity)
            {
                case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                {
                    logLevel = ELogLevel::Info;
                    break;
                }
                case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                {
                    logLevel = ELogLevel::Warning;
                    break;
                }
                case ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                {
                    logLevel = ELogLevel::Error;
                    break;
                }
                default:
                {
                    // Must never fall here
                    DJO_ASSERT_MSG(false, "Unknown severity level");
                    return VK_FALSE;
                }
            }

            if (logLevel == ELogLevel::Warning || logLevel == ELogLevel::Error)
            {
                DJO_LOG_CORE(logLevel) << "########## VULKAN ERROR ###########\n";
                DJO_LOG_CORE(logLevel) << _data->pMessage << '\n';

                #define DJO_LOG_VK_TYPE_ENUM(enum_) case enum_: { DJO_LOG_CORE(logLevel) << #enum_ << '\n'; break; }
                switch (_type)
                {
                    DJO_LOG_VK_TYPE_ENUM(VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT);
                    DJO_LOG_VK_TYPE_ENUM(VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT);
                    DJO_LOG_VK_TYPE_ENUM(VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT);
                    default:
                    {
                        DJO_LOG_CORE(logLevel) << "0x" << std::hex << _type << '\n';
                        DJO_ASSERT_MSG(false, "Unknown message type");
                        break;
                    }
                }
                #undef DJO_LOG_VK_TYPE_ENUM

                #define DJO_LOG_VK_SEVERITY_ENUM(enum_) case enum_: { DJO_LOG_CORE(logLevel) << #enum_ << '\n'; break; }
                switch (_severity)
                {
                    DJO_LOG_VK_SEVERITY_ENUM(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT);
                    DJO_LOG_VK_SEVERITY_ENUM(VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT);
                    DJO_LOG_VK_SEVERITY_ENUM(VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT);
                    DJO_LOG_VK_SEVERITY_ENUM(VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT);
                }
                #undef DJO_LOG_VK_SEVERITY_ENUM

                DJO_LOG_CORE(logLevel) << "###################################" << std::endl;

                // Breaks here, after message print, if error
                DJO_ASSERT(logLevel != ELogLevel::Error);
            }

            /*
                The callback returns a boolean that indicates if the Vulkan call that triggered the validation layer message should be aborted.
                If the callback returns true, then the call is aborted with the VK_ERROR_VALIDATION_FAILED_EXT error.
                This is normally only used to test the validation layers themselves, so you should always return VK_FALSE.
            */
            return VK_FALSE;
        }
    }

#endif // DJO_VK_ENABLE_VALIDATION_LAYERS
}
