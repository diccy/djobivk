#pragma once

#include <Core/Core.hpp>

// 4100 : unreferenced formal parameter
// 4127 : conditional expression is constant
// 4189 : local variable is initialized but not referenced
// 4324 : structure was padded due to alignment specifier
#pragma warning(push)
#pragma warning(disable : 4100 4127 4189 4324)
#include <vma/vk_mem_alloc.h>
#pragma warning(pop)


namespace Djo
{
    struct AllocatedVkBuffer
    {
        ::VkBuffer buffer{ VK_NULL_HANDLE };
        ::VmaAllocation alloc{ VK_NULL_HANDLE };
    };

    struct AllocatedVkImage
    {
        ::VkImage image{ VK_NULL_HANDLE };
        ::VmaAllocation alloc{ VK_NULL_HANDLE };
    };

    DJO_INLINE
    auto
    CreateAllocatedVkBuffer(const ::VmaAllocator _allocator, const ::VkDeviceSize _size, const ::VkBufferUsageFlags _usage, const ::VmaMemoryUsage _memUsage)
    {
        ::VkBufferCreateInfo bufferCreateInfo{};
        bufferCreateInfo.sType = ::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.pNext = nullptr;
        bufferCreateInfo.flags = 0;
        bufferCreateInfo.size = _size;
        bufferCreateInfo.usage = _usage;
        bufferCreateInfo.sharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
        bufferCreateInfo.queueFamilyIndexCount = 0;
        bufferCreateInfo.pQueueFamilyIndices = nullptr;

        ::VmaAllocationCreateInfo vmaAllocInfo{};
        vmaAllocInfo.flags = 0;
        vmaAllocInfo.usage = _memUsage;
        vmaAllocInfo.requiredFlags = 0;
        vmaAllocInfo.preferredFlags = 0;
        vmaAllocInfo.memoryTypeBits = 0;
        vmaAllocInfo.pool = VK_NULL_HANDLE;
        vmaAllocInfo.pUserData = nullptr;
        vmaAllocInfo.priority = 0;

        struct { AllocatedVkBuffer b; ::VkResult r; } ret{};
        ret.r = ::vmaCreateBuffer(
            _allocator,
            &bufferCreateInfo, &vmaAllocInfo,
            &ret.b.buffer, &ret.b.alloc,
            nullptr);
        return ret;
    }

    DJO_INLINE
    void
    DestroyAllocatedVkBuffer(const ::VmaAllocator _allocator, AllocatedVkBuffer& _allocatedBuffer)
    {
        ::vmaDestroyBuffer(_allocator, _allocatedBuffer.buffer, _allocatedBuffer.alloc);
        _allocatedBuffer.buffer = VK_NULL_HANDLE;
        _allocatedBuffer.alloc = VK_NULL_HANDLE;
    }
}
