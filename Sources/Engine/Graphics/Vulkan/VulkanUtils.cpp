
#include <Graphics/Vulkan/VulkanUtils.hpp>


namespace Djo
{
    namespace Vulkan
    {
        namespace _Internal
        {
            const char* GetName(const ::VkExtensionProperties& _p)  { return _p.extensionName; }
            const char* GetName(const ::VkLayerProperties& _p)      { return _p.layerName; }

            template <typename T>
            bool
            CheckAvailability(const MemView<const char* const>& _names, const MemView<const T>& _properties)
            {
                bool available = true;
                const MemView<const T>::ConstIterator itBegin = _properties.begin();
                const MemView<const T>::ConstIterator itEnd = _properties.end();
                const char* name = nullptr;
                const auto pred = [&name](const T& _p) { return ::strcmp(name, GetName(_p)) == 0; };
                for (const char* n : _names)
                {
                    name = n;
                    if (std::find_if(itBegin, itEnd, pred) == itEnd)
                    {
                        available = false;
                        break;
                    }
                }
                return available;
            }
        }

        bool
        CheckInstanceExtensionsAvailability(const MemView<const char* const>& _extensions)
        {
            ::VkResult vkresult;
            u32 extensionCount = 0;
            vkresult = ::vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
            if (vkresult != ::VK_SUCCESS)
            {
                DJO_FAILED_ASSERTION(false);
                return false;
            }
            std::vector<::VkExtensionProperties> extensionsProperties{};
            extensionsProperties.resize(extensionCount);
            vkresult = ::vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensionsProperties.data());
            if (vkresult != ::VK_SUCCESS)
            {
                DJO_FAILED_ASSERTION(false);
                return false;
            }
            return _Internal::CheckAvailability(_extensions, ToConstView(extensionsProperties));
        }

        bool
        CheckDeviceExtensionsAvailability(::VkPhysicalDevice const _physicalDevice, const MemView<const char* const>& _extensions)
        {
            ::VkResult vkresult;
            u32 extensionCount = 0;
            vkresult = ::vkEnumerateDeviceExtensionProperties(_physicalDevice, nullptr, &extensionCount, nullptr);
            if (vkresult != ::VK_SUCCESS)
            {
                DJO_FAILED_ASSERTION(false);
                return false;
            }
            std::vector<::VkExtensionProperties> extensionsProperties{};
            extensionsProperties.resize(extensionCount);
            vkresult = ::vkEnumerateDeviceExtensionProperties(_physicalDevice, nullptr, &extensionCount, extensionsProperties.data());
            if (vkresult != ::VK_SUCCESS)
            {
                DJO_FAILED_ASSERTION(false);
                return false;
            }
            return _Internal::CheckAvailability(_extensions, ToConstView(extensionsProperties));
        }

        bool
        CheckLayersAvailability(const MemView<const char* const>& _layers)
        {
            ::VkResult vkresult;
            u32 layerCount = 0;
            vkresult = ::vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
            if (vkresult != ::VK_SUCCESS)
            {
                DJO_FAILED_ASSERTION(false);
                return false;
            }
            std::vector<::VkLayerProperties> layersProperties{};
            layersProperties.resize(layerCount);
            vkresult = ::vkEnumerateInstanceLayerProperties(&layerCount, layersProperties.data());
            if (vkresult != ::VK_SUCCESS)
            {
                DJO_FAILED_ASSERTION(false);
                return false;
            }
            return _Internal::CheckAvailability(_layers, ToConstView(layersProperties));
        }

        QueueFamilyIndices
        GetQueueFamilyIndices(::VkPhysicalDevice const _physicalDevice, ::VkSurfaceKHR const _surface)
        {
            QueueFamilyIndices indices;
            indices.graphics = c_invalidUint<u32>;
            indices.present = c_invalidUint<u32>;
            u32 queueFamilyCount = 0;
            ::vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevice, &queueFamilyCount, nullptr);
            std::vector<::VkQueueFamilyProperties> queueFamilies{ queueFamilyCount };
            ::vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevice, &queueFamilyCount, queueFamilies.data());
            for (u32 i = 0; i < queueFamilyCount; ++i)
            {
                // Check if graphics queue
                if (TestFlag<VkQueueFlags>(queueFamilies[i].queueFlags, ::VK_QUEUE_GRAPHICS_BIT))
                    indices.graphics = i;

                // Check if present queue
                ::VkBool32 presentSupport = false;
                const ::VkResult vkresult = ::vkGetPhysicalDeviceSurfaceSupportKHR(_physicalDevice, i, _surface, &presentSupport);
                DJO_ASSERT(vkresult == ::VK_SUCCESS); DJO_UNUSED_VAR(vkresult);
                if (presentSupport == VK_TRUE)
                    indices.present = i;

                // When both, leave
                if (indices.graphics != c_invalidUint<u32> && indices.present != c_invalidUint<u32>)
                    break;
            }
            return indices;
        }
    }
}
