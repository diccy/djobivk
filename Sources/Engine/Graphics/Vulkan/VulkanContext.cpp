
#include <Graphics/Vulkan/VulkanContext.hpp>

#include <Window/GLFW/GLFW.hpp>


namespace Djo
{
    namespace Vulkan
    {
        EResult
        Context::Init(::GLFWwindow* const _glfwWindow, const MemView<const char* const>& _requiredExtensions, const MemView<const char* const>& _validationLayers)
        {
            DJO_OK_OR_RETURN(InitInstance(_requiredExtensions, _validationLayers));
        #if defined (DJO_VK_ENABLE_VALIDATION_LAYERS)
            DJO_OK_OR_RETURN(InitDebugMessenger());
        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS
            DJO_OK_OR_RETURN(InitSurface(_glfwWindow));
            DJO_OK_OR_RETURN(PickPhysicalDevice());
            DJO_OK_OR_RETURN(InitLogicalDevice());
            DJO_OK_OR_RETURN(InitAllocator());
            DJO_OK_OR_RETURN(InitSwapchain(_glfwWindow));

            return EResult::OK;
        }

        void
        Context::Destroy()
        {
            DestroySwapchain();
            DestroyAllocator();
            DestroyLogicalDevice();
            DestroySurface();
        #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)
            DestroyDebugMessenger();
        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS
            DestroyInstance();
        }

        EResult
        Context::InitInstance(const MemView<const char* const>& _requiredExtensions, const MemView<const char* const>& _validationLayers)
        {
            // Application infos

            ::VkApplicationInfo appInfo{};
            appInfo.sType = ::VK_STRUCTURE_TYPE_APPLICATION_INFO;
            appInfo.pNext = nullptr;
            appInfo.pApplicationName = "Djobi Engine";
            appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
            appInfo.pEngineName = "DjobiVk";
            appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
            appInfo.apiVersion = VK_MAKE_VERSION(1, 3, 2);

        #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)

            // Add debug messenger for vkCreateInstance/vkDestroyInstance functions

            // For vkCreateInstance/vkDestroyInstance debugging
            ::VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
            debugMessengerCreateInfo.sType = ::VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
            debugMessengerCreateInfo.pNext = nullptr;
            debugMessengerCreateInfo.flags = 0;
            debugMessengerCreateInfo.messageSeverity = ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
            debugMessengerCreateInfo.messageType = ::VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
            debugMessengerCreateInfo.pfnUserCallback = VulkanHelper::DebugCallback;
            debugMessengerCreateInfo.pUserData = nullptr;
        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS

            // Create instance

            ::VkInstanceCreateInfo instanceCreateInfo{};
            instanceCreateInfo.sType = ::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)
            instanceCreateInfo.pNext = &debugMessengerCreateInfo;
        #else
            instanceCreateInfo.pNext = nullptr;
        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS
            instanceCreateInfo.flags = 0;
            instanceCreateInfo.pApplicationInfo = &appInfo;
        #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)
            instanceCreateInfo.enabledLayerCount = (u32)_validationLayers.count;
            instanceCreateInfo.ppEnabledLayerNames = _validationLayers.data;
        #else
            (void)_validationLayers; // unused
            instanceCreateInfo.enabledLayerCount = 0;
            instanceCreateInfo.ppEnabledLayerNames = nullptr;
        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS
            instanceCreateInfo.enabledExtensionCount = (u32)_requiredExtensions.count;
            instanceCreateInfo.ppEnabledExtensionNames = _requiredExtensions.data;

            if (::vkCreateInstance(&instanceCreateInfo, nullptr, &this->instance) != VK_SUCCESS)
            {
                return MakeResult(EResult::FAIL, "vkCreateInstance failed");
            }

            return EResult::OK;
        }

        void
        Context::DestroyInstance()
        {
            if (this->instance != VK_NULL_HANDLE)
            {
                this->physicalDevice = VK_NULL_HANDLE;
                ::vkDestroyInstance(this->instance, nullptr);
                this->instance = VK_NULL_HANDLE;
            }
        }

    #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)

        EResult
        Context::InitDebugMessenger()
        {
            ::VkDebugUtilsMessengerCreateInfoEXT createInfo{};
            createInfo.sType = ::VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.messageSeverity = ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
            createInfo.messageType = ::VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | ::VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
            createInfo.pfnUserCallback = VulkanHelper::DebugCallback;
            createInfo.pUserData = nullptr;

            const ::VkResult vkresult = VulkanHelper::CreateDebugUtilsMessengerEXT(this->instance, &createInfo, nullptr, &this->debugMessenger);
            DJO_ASSERT(vkresult == ::VK_SUCCESS);

            return EResult::OK;
        }

        void
        Context::DestroyDebugMessenger()
        {
            if (this->instance != VK_NULL_HANDLE && this->debugMessenger != VK_NULL_HANDLE)
            {
                VulkanHelper::DestroyDebugUtilsMessengerEXT(this->instance, this->debugMessenger, nullptr);
                this->debugMessenger = VK_NULL_HANDLE;
            }
        }

    #endif // DJO_VK_ENABLE_VALIDATION_LAYERS

        EResult
        Context::InitSurface(::GLFWwindow* const _glfwWindow)
        {
            const ::VkResult vkresult = ::glfwCreateWindowSurface(this->instance, _glfwWindow, nullptr, &this->surface);
            DJO_ASSERT(vkresult == ::VK_SUCCESS); DJO_UNUSED_VAR(vkresult);

            DJO_TODO("Handle glfwCreateWindowSurface failure");

            return EResult::OK;
        }

        void
        Context::DestroySurface()
        {
            if (this->instance != VK_NULL_HANDLE && this->surface != VK_NULL_HANDLE)
            {
                ::vkDestroySurfaceKHR(this->instance, this->surface, nullptr);
                this->surface = VK_NULL_HANDLE;
            }
        }

        EResult
        Context::PickPhysicalDevice()
        {
            ::VkResult vkresult;
            u32 devicesCount{ 0 };
            vkresult = ::vkEnumeratePhysicalDevices(this->instance, &devicesCount, nullptr);
            if (vkresult != ::VK_SUCCESS || devicesCount == 0)
                return MakeResult(EResult::FAIL, "vkEnumeratePhysicalDevices failed");

            std::vector<::VkPhysicalDevice> physicalDevices{ devicesCount };
            vkresult = ::vkEnumeratePhysicalDevices(this->instance, &devicesCount, physicalDevices.data());
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            for (const ::VkPhysicalDevice pd : physicalDevices)
            {
                // Filter type
                ::VkPhysicalDeviceProperties deviceProperties;
                //::VkPhysicalDeviceFeatures deviceFeatures;
                ::vkGetPhysicalDeviceProperties(pd, &deviceProperties);
                //::vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);
                if (deviceProperties.deviceType == ::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
                {
                    // Filter extensions
                    if (CheckDeviceExtensionsAvailability(pd, { c_deviceExtensions }))
                    {
                        // Filter queue family availables
                        QueueFamilyIndices queueFamilyIndices = GetQueueFamilyIndices(pd, this->surface);
                        if (queueFamilyIndices.graphics != c_invalidUint<u32> && queueFamilyIndices.present != c_invalidUint<u32>)
                        {
                            // Filter swap chain support
                            u32 swapchainFormatCount{ 0 };
                            vkresult = ::vkGetPhysicalDeviceSurfaceFormatsKHR(pd, this->surface, &swapchainFormatCount, nullptr);
                            DJO_ASSERT(vkresult == ::VK_SUCCESS);
                            u32 swapchainPresentModeCount{ 0 };
                            vkresult = ::vkGetPhysicalDeviceSurfacePresentModesKHR(pd, this->surface, &swapchainPresentModeCount, nullptr);
                            DJO_ASSERT(vkresult == ::VK_SUCCESS);
                            if (swapchainFormatCount > 0 && swapchainPresentModeCount > 0)
                            {
                                // OK get this one
                                this->physicalDevice = pd;
                                this->graphicsQueue.familyIndex = queueFamilyIndices.graphics;
                                this->presentQueue.familyIndex = queueFamilyIndices.present;
                                break;
                            }
                        }
                    }
                }
            }
            if (this->physicalDevice == VK_NULL_HANDLE)
                return MakeResult(EResult::FAIL, "Can't pick physical device");

            ::vkGetPhysicalDeviceProperties(this->physicalDevice, &this->physicalDeviceProperties);

            return EResult::OK;
        }

        EResult
        Context::InitLogicalDevice()
        {
            std::vector<::VkDeviceQueueCreateInfo> queueCreateInfos{};
            const f32 queuePriority = 1.f;
            ::VkDeviceQueueCreateInfo queueCreateInfo{};
            queueCreateInfo.sType = ::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.pNext = nullptr;
            queueCreateInfo.queueFamilyIndex = this->graphicsQueue.familyIndex;
            queueCreateInfo.queueCount = 1;
            queueCreateInfo.pQueuePriorities = &queuePriority;
            queueCreateInfos.push_back(queueCreateInfo);
            if (this->presentQueue.familyIndex != this->graphicsQueue.familyIndex)
            {
                queueCreateInfo.queueFamilyIndex = this->presentQueue.familyIndex;
                queueCreateInfos.push_back(queueCreateInfo);
            }

            ::VkPhysicalDeviceFeatures physicalDeviceFeatures;
            ::vkGetPhysicalDeviceFeatures(this->physicalDevice, &physicalDeviceFeatures);

            ::VkPhysicalDeviceShaderDrawParametersFeatures shaderDrawParametersFeatures{};
            shaderDrawParametersFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES;
            shaderDrawParametersFeatures.pNext = nullptr;
            shaderDrawParametersFeatures.shaderDrawParameters = VK_TRUE;

            /*
            Previous implementations of Vulkan made a distinction between instance and device specific validation layers,
            but this is no longer the case. That means that the enabledLayerCount and ppEnabledLayerNames fields of
            VkDeviceCreateInfo are ignored by up-to-date implementations.
            However, it is still a good idea to set them anyway to be compatible with older implementations.
            */
            ::VkDeviceCreateInfo createInfo{};
            createInfo.sType = ::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
            createInfo.pNext = &shaderDrawParametersFeatures;
            createInfo.flags = 0;
            createInfo.queueCreateInfoCount = (u32)queueCreateInfos.size();
            createInfo.pQueueCreateInfos = queueCreateInfos.data();
            createInfo.enabledLayerCount = 0;
            createInfo.ppEnabledLayerNames = nullptr;
            createInfo.enabledExtensionCount = ArraySize(c_deviceExtensions);
            createInfo.ppEnabledExtensionNames = c_deviceExtensions;
            createInfo.pEnabledFeatures = &physicalDeviceFeatures;

            const ::VkResult vkresult = ::vkCreateDevice(this->physicalDevice, &createInfo, nullptr, &this->device);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateDevice failed");

            ::vkGetDeviceQueue(this->device, this->graphicsQueue.familyIndex, 0, &this->graphicsQueue.queue);
            ::vkGetDeviceQueue(this->device, this->presentQueue.familyIndex, 0, &this->presentQueue.queue);

            return EResult::OK;
        }

        void
        Context::DestroyLogicalDevice()
        {
            if (this->device != VK_NULL_HANDLE)
            {
                this->graphicsQueue.queue = VK_NULL_HANDLE;
                this->presentQueue.queue = VK_NULL_HANDLE;
                ::vkDestroyDevice(this->device, nullptr);
                this->device = VK_NULL_HANDLE;
            }
        }

        EResult
        Context::InitAllocator()
        {
            ::VmaAllocatorCreateInfo allocatorInfo{};
            allocatorInfo.flags = 0;
            allocatorInfo.physicalDevice = this->physicalDevice;
            allocatorInfo.device = this->device;
            allocatorInfo.preferredLargeHeapBlockSize = 0;
            allocatorInfo.pAllocationCallbacks = nullptr;
            allocatorInfo.pDeviceMemoryCallbacks = nullptr;
            allocatorInfo.pHeapSizeLimit = nullptr;
            allocatorInfo.pVulkanFunctions = nullptr;
            allocatorInfo.instance = this->instance;
            allocatorInfo.vulkanApiVersion = DJO_VK_API_VERSION;
        #if defined (VMA_EXTERNAL_MEMORY)
            allocatorInfo.pTypeExternalMemoryHandleTypes = nullptr;
        #endif // VMA_EXTERNAL_MEMORY
            const ::VkResult vkresult = ::vmaCreateAllocator(&allocatorInfo, &this->allocator);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vmaCreateAllocator failed");

            return EResult::OK;
        }

        void
        Context::DestroyAllocator()
        {
            if (this->allocator != VK_NULL_HANDLE)
            {
                ::vmaDestroyAllocator(this->allocator);
                this->allocator = VK_NULL_HANDLE;
            }
        }

        EResult
        Context::InitSwapchain(::GLFWwindow* const _glfwWindow)
        {
            ::VkResult vkresult;

            // Choose extent
            ::VkExtent2D chosenExtent{};
            ::VkSurfaceCapabilitiesKHR capabilities{};
            vkresult = ::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(this->physicalDevice, this->surface, &capabilities);
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            if (capabilities.currentExtent.width != c_invalidUint<u32>)
            {
                chosenExtent = capabilities.currentExtent;
            }
            else
            {
                int width, height;
                ::glfwGetFramebufferSize(_glfwWindow, &width, &height);
                chosenExtent.width = (u32)(width);
                chosenExtent.height = (u32)(height);
            }
            if (chosenExtent.width == 0 || chosenExtent.height == 0)
                return MakeResult(EResult::FAIL, "Invalid chosen extent");

            chosenExtent.width = Clamp<u32>(chosenExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
            chosenExtent.height = Clamp<u32>(chosenExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

            // Choose format
            ::VkSurfaceFormatKHR chosenFormat{};
            u32 formatCount{ 0 };
            vkresult = ::vkGetPhysicalDeviceSurfaceFormatsKHR(this->physicalDevice, this->surface, &formatCount, nullptr);
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            std::vector<::VkSurfaceFormatKHR> formats{ formatCount };
            vkresult = ::vkGetPhysicalDeviceSurfaceFormatsKHR(this->physicalDevice, this->surface, &formatCount, formats.data());
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            chosenFormat = formats[0]; // Kinda ok fallback if not fully suitable is found
            for (const ::VkSurfaceFormatKHR& format : formats)
            {
                if (format.format == ::VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == ::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                {
                    chosenFormat = format;
                    break;
                }
            }

            // Choose present mode
            ::VkPresentModeKHR chosenPresentMode{ ::VK_PRESENT_MODE_FIFO_KHR }; // This mode is guaranteed to be available
            u32 presentModeCount{ 0 };
            vkresult = ::vkGetPhysicalDeviceSurfacePresentModesKHR(this->physicalDevice, this->surface, &presentModeCount, nullptr);
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            std::vector<::VkPresentModeKHR> presentModes{ presentModeCount };
            vkresult = ::vkGetPhysicalDeviceSurfacePresentModesKHR(this->physicalDevice, this->surface, &presentModeCount, presentModes.data());
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            for (const ::VkPresentModeKHR presentMode : presentModes)
            {
                if (presentMode == ::VK_PRESENT_MODE_MAILBOX_KHR)
                {
                    chosenPresentMode = presentMode;
                    break;
                }
            }

            // Setup queue family indices
            const u32 queueFamilyIndices[]{ this->graphicsQueue.familyIndex, this->presentQueue.familyIndex };

            // Create swapchain
            ::VkSwapchainCreateInfoKHR createInfo{};
            createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
            createInfo.pNext = nullptr;
            createInfo.flags = 0;
            createInfo.surface = this->surface;
            createInfo.minImageCount = Clamp<u32>(capabilities.minImageCount + 1, 0, capabilities.maxImageCount);
            createInfo.imageFormat = chosenFormat.format;
            createInfo.imageColorSpace = chosenFormat.colorSpace;
            createInfo.imageExtent = chosenExtent;
            createInfo.imageArrayLayers = 1;
            createInfo.imageUsage = ::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT; // It is also possible that you'll render images to a separate image first to perform operations like post-processing. In that case you may use a value like VK_IMAGE_USAGE_TRANSFER_DST_BIT instead and use a memory operation to transfer the rendered image to a swap chain image.
            if (this->graphicsQueue.familyIndex != this->presentQueue.familyIndex)
            {
                createInfo.imageSharingMode = ::VK_SHARING_MODE_CONCURRENT;
                createInfo.queueFamilyIndexCount = ArraySize(queueFamilyIndices);
                createInfo.pQueueFamilyIndices = queueFamilyIndices;
            }
            else
            {
                createInfo.imageSharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
                createInfo.queueFamilyIndexCount = 0;
                createInfo.pQueueFamilyIndices = nullptr;
            }
            createInfo.preTransform = capabilities.currentTransform;
            createInfo.compositeAlpha = ::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
            createInfo.presentMode = chosenPresentMode;
            createInfo.clipped = VK_TRUE;
            createInfo.oldSwapchain = VK_NULL_HANDLE;

            vkresult = ::vkCreateSwapchainKHR(this->device, &createInfo, nullptr, &this->swapchain);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateSwapchainKHR failed");

            // Store these infos
            this->swapchainFormat = chosenFormat.format;
            this->swapchainExtent = chosenExtent;

            // Retrieve images
            u32 imageCount;
            vkresult = ::vkGetSwapchainImagesKHR(this->device, this->swapchain, &imageCount, nullptr);
            DJO_ASSERT(vkresult == ::VK_SUCCESS);
            this->swapchainImages.resize(imageCount);
            vkresult = ::vkGetSwapchainImagesKHR(this->device, this->swapchain, &imageCount, this->swapchainImages.data());
            DJO_ASSERT(vkresult == ::VK_SUCCESS);

            // Init image views
            this->swapchainImageViews.resize(imageCount, VK_NULL_HANDLE);
            for (u32 i = 0; i < imageCount; ++i)
            {
                ::VkImageViewCreateInfo imageViewCreateInfo{
                    Vulkan::ImageViewCreateInfo2D(
                        this->swapchainImages[i],
                        this->swapchainFormat,
                        ::VK_IMAGE_ASPECT_COLOR_BIT
                ) };
                vkresult = ::vkCreateImageView(this->device, &imageViewCreateInfo, nullptr, &this->swapchainImageViews[i]);
                if (vkresult != ::VK_SUCCESS)
                    return MakeResult(EResult::FAIL, "vkCreateImageView failed");
            }

            // Depth image

            const ::VkExtent3D depthImageExtent{ chosenExtent.width, chosenExtent.height, 1 };
            this->depthFormat = ::VK_FORMAT_D32_SFLOAT;

            ::VkImageCreateInfo depthImageInfo{
                ImageCreateInfo2D(this->depthFormat, depthImageExtent, ::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
            };

            ::VmaAllocationCreateInfo vmaDepthImageAllocInfo{};
            vmaDepthImageAllocInfo.flags = 0;
            vmaDepthImageAllocInfo.usage = ::VMA_MEMORY_USAGE_GPU_ONLY;
            vmaDepthImageAllocInfo.requiredFlags = ::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
            vmaDepthImageAllocInfo.preferredFlags = 0;
            vmaDepthImageAllocInfo.memoryTypeBits = 0;
            vmaDepthImageAllocInfo.pool = VK_NULL_HANDLE;
            vmaDepthImageAllocInfo.pUserData = nullptr;
            vmaDepthImageAllocInfo.priority = 0;

            vkresult = ::vmaCreateImage(this->allocator, &depthImageInfo, &vmaDepthImageAllocInfo,
                                        &this->depthImage.image,
                                        &this->depthImage.alloc,
                                        nullptr);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vmaCreateImage failed");

            const ::VkImageViewCreateInfo depthImageViewCreateInfo{
                    Vulkan::ImageViewCreateInfo2D(
                        this->depthImage.image,
                        this->depthFormat,
                        ::VK_IMAGE_ASPECT_DEPTH_BIT
            ) };
            vkresult = ::vkCreateImageView(this->device, &depthImageViewCreateInfo, nullptr, &this->depthImageView);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateImageView failed");

            return EResult::OK;
        }

        void
        Context::DestroySwapchain()
        {
            if (this->depthImageView != VK_NULL_HANDLE)
            {
                ::vkDestroyImageView(this->device, this->depthImageView, nullptr);
                this->depthImageView = VK_NULL_HANDLE;
            }
            if (this->depthImage.image != VK_NULL_HANDLE)
            {
                ::vmaDestroyImage(this->allocator, this->depthImage.image, this->depthImage.alloc);
                this->depthImage.image = VK_NULL_HANDLE;
            }
            for (::VkImageView imageView : this->swapchainImageViews)
            {
                if (imageView != VK_NULL_HANDLE)
                {
                    ::vkDestroyImageView(this->device, imageView, nullptr);
                    imageView = VK_NULL_HANDLE;
                }
            }
            this->swapchainImageViews.clear();
            if (this->swapchain != VK_NULL_HANDLE)
            {
                this->swapchainImages.clear();
                ::vkDestroySwapchainKHR(this->device, this->swapchain, nullptr);
                this->swapchain = VK_NULL_HANDLE;
            }
        }
    }
}
