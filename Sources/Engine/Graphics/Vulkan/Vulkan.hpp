#pragma once

#include <vulkan/vulkan.h>

#define DJO_VK_API_VERSION VK_API_VERSION_1_3
#if defined (VK_API_VERSION_1_4)
    #error "Outdated DJO_VK_API_VERSION macro"
#endif

#if defined (__DJO_DEBUG__)

    #define DJO_VK_ENABLE_VALIDATION_LAYERS 1

#endif // __DJO_DEBUG__


namespace Djo
{
#if defined (DJO_VK_ENABLE_VALIDATION_LAYERS)

    namespace VulkanHelper
    {
        ::VkResult CreateDebugUtilsMessengerEXT(::VkInstance _instance, const ::VkDebugUtilsMessengerCreateInfoEXT* _createInfo, const ::VkAllocationCallbacks* _allocator, ::VkDebugUtilsMessengerEXT* _debugMessenger);
        void DestroyDebugUtilsMessengerEXT(::VkInstance _instance, ::VkDebugUtilsMessengerEXT _debugMessenger, const ::VkAllocationCallbacks* _allocator);
        VKAPI_ATTR ::VkBool32 VKAPI_CALL DebugCallback(::VkDebugUtilsMessageSeverityFlagBitsEXT _severity, ::VkDebugUtilsMessageTypeFlagsEXT _type, const ::VkDebugUtilsMessengerCallbackDataEXT* _data, void* _userData);
    }

#endif // DJO_VK_ENABLE_VALIDATION_LAYERS
}
