#pragma once

#include <Graphics/Vulkan/Vulkan.hpp>
#include <Core/StdVector.hpp>
#include <Core/ResultEnum.hpp>


// fw
struct GLFWwindow;

namespace Djo
{
    namespace Vulkan
    {
        constexpr const char* c_validationLayers[]
        {
            "VK_LAYER_KHRONOS_validation"
        };

        constexpr const char* c_deviceExtensions[]
        {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };

        bool CheckInstanceExtensionsAvailability(const MemView<const char* const>& _extensions);
        bool CheckDeviceExtensionsAvailability(::VkPhysicalDevice _physicalDevice, const MemView<const char* const>& _extensions);
        bool CheckLayersAvailability(const MemView<const char* const>& _layers);

        struct QueueFamilyIndices
        {
            u32 graphics{ c_invalidUint<u32> };
            u32 present{ c_invalidUint<u32> };
        };
        QueueFamilyIndices GetQueueFamilyIndices(::VkPhysicalDevice _physicalDevice, ::VkSurfaceKHR _surface);

        struct Queue
        {
            ::VkQueue queue{ VK_NULL_HANDLE };
            u32 familyIndex{ c_invalidUint<u32> };
        };

        // Vulkan struct initializers /////////////////////////////////////////

        constexpr
        ::VkCommandPoolCreateInfo
        CommandPoolCreateInfo(const ::VkCommandPoolCreateFlags _flags, const u32 _queueFamilyIndex)
        {
            ::VkCommandPoolCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = _flags;
            info.queueFamilyIndex = _queueFamilyIndex;
            return info;
        }

        constexpr
        ::VkCommandBufferAllocateInfo
        CommandBufferAllocateInfo(const ::VkCommandPool _commandPool, const ::VkCommandBufferLevel _level, const u32 _count)
        {
            ::VkCommandBufferAllocateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            info.pNext = nullptr;
            info.commandPool = _commandPool;
            info.level = _level;
            info.commandBufferCount = _count;
            return info;
        }

        constexpr
        ::VkCommandBufferBeginInfo
        CommandBufferBeginInfo(const ::VkCommandBufferUsageFlags _flags)
        {
            ::VkCommandBufferBeginInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            info.pNext = nullptr;
            info.flags = _flags;
            info.pInheritanceInfo = nullptr;
            return info;
        }

        constexpr
        ::VkSubmitInfo
        SubmitInfo(const MemView<const ::VkSemaphore>& _waitSemaphores, const MemView<const ::VkPipelineStageFlags>& _waitDstStageMask, const MemView<const ::VkCommandBuffer>& _cmds, const MemView<const ::VkSemaphore>& _signalSemaphores)
        {
            ::VkSubmitInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_SUBMIT_INFO;
            info.pNext = nullptr;
            info.waitSemaphoreCount = (u32)_waitSemaphores.count;
            info.pWaitSemaphores = _waitSemaphores.data;
            info.pWaitDstStageMask = _waitDstStageMask.data;
            info.commandBufferCount = (u32)_cmds.count;
            info.pCommandBuffers = _cmds.data;
            info.signalSemaphoreCount = (u32)_signalSemaphores.count;
            info.pSignalSemaphores = _signalSemaphores.data;
            return info;
        }

        constexpr
        ::VkFramebufferCreateInfo
        FramebufferCreateInfo(const ::VkRenderPass _renderPass, const MemView<const ::VkImageView>& _attachments, const ::VkExtent2D _extent)
        {
            ::VkFramebufferCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.renderPass = _renderPass;
            info.attachmentCount = (u32)_attachments.count;
            info.pAttachments = _attachments.data;
            info.width = _extent.width;
            info.height = _extent.height;
            info.layers = 1;
            return info;
        }

        constexpr
        ::VkFenceCreateInfo
        FenceCreateInfo(const ::VkFenceCreateFlags _flags)
        {
            ::VkFenceCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = _flags;
            return info;
        }

        constexpr
        ::VkSemaphoreCreateInfo
        SemaphoreCreateInfo(const ::VkSemaphoreCreateFlags _flags)
        {
            ::VkSemaphoreCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = _flags;
            return info;
        }

        constexpr
        ::VkRenderPassCreateInfo
        RenderPassCreateInfo(const MemView<const ::VkAttachmentDescription>& _attachments, const MemView<const ::VkSubpassDescription>& _subpasses, const MemView<const ::VkSubpassDependency>& _dependencies)
        {
            ::VkRenderPassCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.attachmentCount = (u32)_attachments.count;
            info.pAttachments = _attachments.data;
            info.subpassCount = (u32)_subpasses.count;
            info.pSubpasses = _subpasses.data;
            info.dependencyCount = (u32)_dependencies.count;
            info.pDependencies = _dependencies.data;
            return info;
        }

        constexpr
        ::VkPipelineShaderStageCreateInfo
        PipelineShaderStageCreateInfo(const ::VkShaderStageFlagBits _stage, const ::VkShaderModule _module)
        {
            ::VkPipelineShaderStageCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.stage = _stage;
            info.module = _module;
            info.pName = "main";
            info.pSpecializationInfo = nullptr;
            return info;
        }

        template <typename T>
        constexpr
        ::VkShaderModuleCreateInfo
        ShaderModuleCreateInfo(const MemView<const T>& _source)
        {
            ::VkShaderModuleCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.codeSize = _source.GetMemSize();
            info.pCode = reinterpret_cast<const u32*>(_source.data);
            return info;
        }

        constexpr
        ::VkPipelineVertexInputStateCreateInfo
        PipelineVertexInputStateCreateInfo(const MemView<const ::VkVertexInputBindingDescription>& _bindings, const MemView<const ::VkVertexInputAttributeDescription>& _attributes)
        {
            ::VkPipelineVertexInputStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.vertexBindingDescriptionCount = (u32)_bindings.count;
            info.pVertexBindingDescriptions = _bindings.data;
            info.vertexAttributeDescriptionCount = (u32)_attributes.count;
            info.pVertexAttributeDescriptions = _attributes.data;
            return info;
        }

        constexpr
        ::VkPipelineInputAssemblyStateCreateInfo
        PipelineInputAssemblyStateCreateInfo(const ::VkPrimitiveTopology _topology)
        {
            ::VkPipelineInputAssemblyStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.topology = _topology;
            info.primitiveRestartEnable = VK_FALSE;
            return info;
        }

        constexpr
        ::VkPipelineRasterizationStateCreateInfo
        PipelineRasterizationStateCreateInfo(const ::VkPolygonMode _polygonMode, const ::VkCullModeFlags _cullMode)
        {
            ::VkPipelineRasterizationStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.depthClampEnable = VK_FALSE;
            info.rasterizerDiscardEnable = VK_FALSE;
            info.polygonMode = _polygonMode;
            info.cullMode = _cullMode;
            info.frontFace = ::VK_FRONT_FACE_CLOCKWISE;
            info.depthBiasEnable = VK_FALSE;
            info.depthBiasConstantFactor = 0.f;
            info.depthBiasClamp = 0.f;
            info.depthBiasSlopeFactor = 0.f;
            info.lineWidth = 1.f;
            return info;
        }

        constexpr
        ::VkPipelineMultisampleStateCreateInfo
        PipelineMultisampleStateCreateInfo(const ::VkSampleCountFlagBits _samples)
        {
            ::VkPipelineMultisampleStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.rasterizationSamples = _samples;
            info.sampleShadingEnable = VK_FALSE;
            info.minSampleShading = 1.f;
            info.pSampleMask = nullptr;
            info.alphaToCoverageEnable = VK_FALSE;
            info.alphaToOneEnable = VK_FALSE;
            return info;
        }

        constexpr
        ::VkPipelineColorBlendStateCreateInfo
        PipelineColorBlendStateCreateInfo(const MemView<const ::VkPipelineColorBlendAttachmentState>& _colorBlendAttachments)
        {
            ::VkPipelineColorBlendStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.logicOpEnable = VK_FALSE;
            info.logicOp = ::VK_LOGIC_OP_COPY;
            info.attachmentCount = (u32)_colorBlendAttachments.count;
            info.pAttachments = _colorBlendAttachments.data;
            info.blendConstants[0] = 0.f;
            info.blendConstants[1] = 0.f;
            info.blendConstants[2] = 0.f;
            info.blendConstants[3] = 0.f;
            return info;
        }

        constexpr
        ::VkPipelineDepthStencilStateCreateInfo
        PipelineDepthStencilStateCreateInfo(const bool _depthTestAndWrite, const ::VkCompareOp _compareOp)
        {
            ::VkPipelineDepthStencilStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.depthTestEnable = _depthTestAndWrite ? VK_TRUE : VK_FALSE;
            info.depthWriteEnable = _depthTestAndWrite ? VK_TRUE : VK_FALSE;
            info.depthCompareOp = _depthTestAndWrite ? _compareOp : ::VK_COMPARE_OP_ALWAYS;
            info.depthBoundsTestEnable = VK_FALSE;
            info.stencilTestEnable = VK_FALSE;
            //info.front;
            //info.back;
            info.minDepthBounds = 0.f;
            info.maxDepthBounds = 1.f;
            return info;
        }

        constexpr
        ::VkPipelineViewportStateCreateInfo
        PipelineViewportStateCreateInfo(const MemView<const ::VkViewport>& _viewports, const MemView<const ::VkRect2D>& _scissors)
        {
            ::VkPipelineViewportStateCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.viewportCount = (u32)_viewports.count;
            info.pViewports = _viewports.data;
            info.scissorCount = (u32)_scissors.count;
            info.pScissors = _scissors.data;
            return info;
        }

        constexpr
        ::VkPipelineLayoutCreateInfo
        PipelineLayoutCreateInfo(const MemView<const ::VkDescriptorSetLayout>& _descSetLayouts, const MemView<const ::VkPushConstantRange>& _pushConstantRanges)
        {
            ::VkPipelineLayoutCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.setLayoutCount = (u32)_descSetLayouts.count;
            info.pSetLayouts = _descSetLayouts.data;
            info.pushConstantRangeCount = (u32)_pushConstantRanges.count;
            info.pPushConstantRanges = _pushConstantRanges.data;
            return info;
        }

        struct GraphicsPipelineCreateInfoData
        {
            std::vector<::VkPipelineShaderStageCreateInfo> shaderStages{};
            ::VkPipelineVertexInputStateCreateInfo vertexInput{};
            ::VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
            ::VkPipelineViewportStateCreateInfo viewport{};
            ::VkPipelineRasterizationStateCreateInfo rasterizer{};
            ::VkPipelineColorBlendStateCreateInfo colorBlend{};
            ::VkPipelineDepthStencilStateCreateInfo depthStencil{};
            ::VkPipelineMultisampleStateCreateInfo multisampling{};
            ::VkPipelineLayout pipelineLayout{ VK_NULL_HANDLE };
            ::VkRenderPass renderPass{ VK_NULL_HANDLE };
        };

        DJO_INLINE
        ::VkGraphicsPipelineCreateInfo
        GraphicsPipelineCreateInfo(const GraphicsPipelineCreateInfoData& _data)
        {
            ::VkGraphicsPipelineCreateInfo pipelineInfo{};
            pipelineInfo.sType = ::VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            pipelineInfo.pNext = nullptr;
            pipelineInfo.flags = 0;
            pipelineInfo.stageCount = (u32)_data.shaderStages.size();
            pipelineInfo.pStages = _data.shaderStages.data();
            pipelineInfo.pVertexInputState = &_data.vertexInput;
            pipelineInfo.pInputAssemblyState = &_data.inputAssembly;
            pipelineInfo.pTessellationState = nullptr;
            pipelineInfo.pViewportState = &_data.viewport;
            pipelineInfo.pRasterizationState = &_data.rasterizer;
            pipelineInfo.pMultisampleState = &_data.multisampling;
            pipelineInfo.pDepthStencilState = &_data.depthStencil;
            pipelineInfo.pColorBlendState = &_data.colorBlend;
            pipelineInfo.pDynamicState = nullptr;
            pipelineInfo.layout = _data.pipelineLayout;
            pipelineInfo.renderPass = _data.renderPass;
            pipelineInfo.subpass = 0;
            pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
            pipelineInfo.basePipelineIndex = -1;
            return pipelineInfo;
        }

        constexpr
        ::VkBufferCreateInfo
        BufferCreateInfo(const ::VkDeviceSize _size, const ::VkBufferUsageFlags _usage, const ::VkSharingMode _sharingMode)
        {
            ::VkBufferCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.size = _size;
            info.usage = _usage;
            info.sharingMode = _sharingMode;
            info.queueFamilyIndexCount = 0;
            info.pQueueFamilyIndices = nullptr;
            return info;
        }

        constexpr
        ::VkImageCreateInfo
        ImageCreateInfo(const ::VkImageType _type, const ::VkFormat _format, const ::VkExtent3D _extent, const ::VkImageUsageFlags _usage)
        {
            ::VkImageCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.imageType = _type;
            info.format = _format;
            info.extent = _extent;
            info.mipLevels = 1;
            info.arrayLayers = 1;
            info.samples = ::VK_SAMPLE_COUNT_1_BIT;
            info.tiling = ::VK_IMAGE_TILING_OPTIMAL;
            info.usage = _usage;
            info.sharingMode = ::VK_SHARING_MODE_EXCLUSIVE;
            info.queueFamilyIndexCount = 0;
            info.pQueueFamilyIndices = nullptr;
            info.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;
            return info;
        }

        constexpr
        ::VkImageCreateInfo
        ImageCreateInfo2D(const ::VkFormat _format, const ::VkExtent3D _extent, const ::VkImageUsageFlags _usage)
        {
            return ImageCreateInfo(::VK_IMAGE_TYPE_2D, _format, _extent, _usage);
        }

        constexpr
        ::VkImageViewCreateInfo
        ImageViewCreateInfo(const ::VkImage _image, const::VkImageViewType _type, const ::VkFormat _format, const ::VkImageAspectFlags _aspect)
        {
            ::VkImageViewCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.image = _image;
            info.viewType = _type;
            info.format = _format;
            info.components.r = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.g = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.b = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.a = ::VK_COMPONENT_SWIZZLE_IDENTITY;
            info.subresourceRange.aspectMask = _aspect;
            info.subresourceRange.baseMipLevel = 0;
            info.subresourceRange.levelCount = 1;
            info.subresourceRange.baseArrayLayer = 0;
            info.subresourceRange.layerCount = 1;
            return info;
        }

        constexpr
        ::VkImageViewCreateInfo
        ImageViewCreateInfo2D(const ::VkImage _image, const ::VkFormat _format, const ::VkImageAspectFlags _aspect)
        {
            return ImageViewCreateInfo(_image, ::VK_IMAGE_VIEW_TYPE_2D, _format, _aspect);
        }

        constexpr
        ::VkRenderPassBeginInfo
        RenderPassBeginInfo(const ::VkRenderPass _renderPass, const ::VkFramebuffer _frameBuffer, const ::VkRect2D _renderArea, const MemView<const ::VkClearValue>& _clearValues)
        {
            ::VkRenderPassBeginInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            info.pNext = nullptr;
            info.renderPass = _renderPass;
            info.framebuffer = _frameBuffer;
            info.renderArea = _renderArea;
            info.clearValueCount = (u32)_clearValues.count;
            info.pClearValues = _clearValues.data;
            return info;
        }

        constexpr
        ::VkDescriptorPoolCreateInfo
        DescriptorPoolCreateInfo(const u32 _maxSets, const MemView<const ::VkDescriptorPoolSize>& _poolSizes)
        {
            ::VkDescriptorPoolCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.maxSets = _maxSets;
            info.poolSizeCount = (u32)_poolSizes.count;
            info.pPoolSizes = _poolSizes.data;
            return info;
        }

        constexpr
        ::VkDescriptorSetLayoutBinding
        DescriptorSetLayoutBinding(const u32 _bind, const ::VkDescriptorType _descType, const ::VkShaderStageFlags _stageFlags)
        {
            ::VkDescriptorSetLayoutBinding binding{};
            binding.binding = _bind;
            binding.descriptorType = _descType;
            binding.descriptorCount = 1;
            binding.stageFlags = _stageFlags;
            binding.pImmutableSamplers = nullptr;
            return binding;
        }

        constexpr
        ::VkDescriptorSetLayoutCreateInfo
        DescriptorSetLayoutCreateInfo(const MemView<const ::VkDescriptorSetLayoutBinding>& _bindings)
        {
            ::VkDescriptorSetLayoutCreateInfo info{};
            info.sType = ::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            info.pNext = nullptr;
            info.flags = 0;
            info.bindingCount = (u32)_bindings.count;
            info.pBindings = _bindings.data;
            return info;
        }

        constexpr
        ::VkDescriptorSetAllocateInfo
        DescriptorSetAllocateInfo(const ::VkDescriptorPool _descPool, const MemView<const ::VkDescriptorSetLayout>& _setLayouts)
        {
            ::VkDescriptorSetAllocateInfo info{};
            info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            info.pNext = nullptr;
            info.descriptorPool = _descPool;
            info.descriptorSetCount = (u32)_setLayouts.count;
            info.pSetLayouts = _setLayouts.data;
            return info;
        }

        constexpr
        ::VkWriteDescriptorSet
        InitWriteDescriptorSet(const ::VkDescriptorSet _dst, const u32 _binding, const u32 _arrayElem, const u32 _count, const ::VkDescriptorType _type)
        {
            ::VkWriteDescriptorSet info{};
            info.sType = ::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            info.pNext = nullptr;
            info.dstSet = _dst;
            info.dstBinding = _binding;
            info.dstArrayElement = _arrayElem;
            info.descriptorCount = _count;
            info.descriptorType = _type;
            info.pImageInfo = nullptr;
            info.pBufferInfo = nullptr;
            info.pTexelBufferView = nullptr;
            return info;
        }
    }
}
