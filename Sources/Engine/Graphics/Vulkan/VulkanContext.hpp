#pragma once

#include <Graphics/Vulkan/VulkanUtils.hpp>
#include <Graphics/Vulkan/VulkanMemoryAllocator.hpp>
#include <Core/StdVector.hpp>
#include <Core/ResultEnum.hpp>


// fw
struct GLFWwindow;

namespace Djo
{
    namespace Vulkan
    {
        struct Context
        {
            // Instance
            ::VkInstance instance{ VK_NULL_HANDLE };

        #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)

            ::VkDebugUtilsMessengerEXT debugMessenger{ VK_NULL_HANDLE };

        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS

            // Surface
            ::VkSurfaceKHR surface{ VK_NULL_HANDLE };

            // Device
            ::VkPhysicalDevice physicalDevice{ VK_NULL_HANDLE };
            ::VkPhysicalDeviceProperties physicalDeviceProperties{};
            ::VkDevice device{ VK_NULL_HANDLE };
            Queue graphicsQueue{};
            Queue presentQueue{};

            // Allocator
            ::VmaAllocator allocator{ VK_NULL_HANDLE };

            // Swapchain
            ::VkSwapchainKHR swapchain{ VK_NULL_HANDLE };
            ::VkFormat swapchainFormat{ ::VK_FORMAT_UNDEFINED };
            ::VkExtent2D swapchainExtent{};
            std::vector<::VkImage> swapchainImages{};
            std::vector<::VkImageView> swapchainImageViews{};
            ::VkFormat depthFormat{ ::VK_FORMAT_UNDEFINED };
            ::VkImageView depthImageView{ VK_NULL_HANDLE };
            AllocatedVkImage depthImage{};

            EResult Init(::GLFWwindow* _glfwWindow, const MemView<const char* const>& _requiredExtensions, const MemView<const char* const>& _validationLayers);
            void Destroy();

        private:

            EResult InitInstance(const MemView<const char* const>& _requiredExtensions, const MemView<const char* const>& _validationLayers);
            void DestroyInstance();
        #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)
            EResult InitDebugMessenger();
            void DestroyDebugMessenger();
        #endif // DJO_VK_ENABLE_VALIDATION_LAYERS
            EResult InitSurface(::GLFWwindow* _glfwWindow);
            void DestroySurface();
            EResult PickPhysicalDevice();
            EResult InitLogicalDevice();
            void DestroyLogicalDevice();
            EResult InitAllocator();
            void DestroyAllocator();
            EResult InitSwapchain(::GLFWwindow* _glfwWindow);
            void DestroySwapchain();
        };
    }
}
