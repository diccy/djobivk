#pragma once

#include <Math/Vector2.hpp>
#include <Core/StdVector.hpp>


namespace Djo
{
    struct ImageData
    {
        std::vector<u8> bitmap;
        Vec2u size;

        DJO_INLINE u32 GetChannelCount() const  { return (u32)bitmap.size() / (size.x * size.y); }
    };
}
