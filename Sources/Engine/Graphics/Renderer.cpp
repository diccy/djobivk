
#include <Graphics/Renderer.hpp>

#include <Graphics/ImGui/ImGuiDebugWindow_Renderer.hpp>
#include <Graphics/Vulkan/VulkanUtils.hpp>
#include <Graphics/Camera.hpp>
#include <Graphics/ObjLoader.hpp>
#include <Window/GLFW/GLFW.hpp>
#include <System/GlobalFS.hpp>
#include <ImGui/ImGuiManager.hpp>
#include <Math/Math.hpp>

#include <imgui_impl_vulkan.h>


namespace Djo
{
    VertexInputDescription
    Vertex::GetDesc()
    {
        VertexInputDescription desc{};

        ::VkVertexInputBindingDescription mainBinding{};
        mainBinding.binding = 0;
        mainBinding.stride = sizeof(Vertex);
        mainBinding.inputRate = ::VK_VERTEX_INPUT_RATE_VERTEX;
        desc.bindings.push_back(mainBinding);

        ::VkVertexInputAttributeDescription positionAttribute{};
        positionAttribute.location = 0;
        positionAttribute.binding = 0;
        positionAttribute.format = ::VK_FORMAT_R32G32B32_SFLOAT;
        positionAttribute.offset = offsetof(Vertex, pos);

        ::VkVertexInputAttributeDescription normalAttribute{};
        normalAttribute.location = 1;
        normalAttribute.binding = 0;
        normalAttribute.format = ::VK_FORMAT_R32G32B32_SFLOAT;
        normalAttribute.offset = offsetof(Vertex, normal);

        ::VkVertexInputAttributeDescription colorAttribute{};
        colorAttribute.location = 2;
        colorAttribute.binding = 0;
        colorAttribute.format = ::VK_FORMAT_R32G32B32_SFLOAT;
        colorAttribute.offset = offsetof(Vertex, color);

        desc.attributes.push_back(positionAttribute);
        desc.attributes.push_back(normalAttribute);
        desc.attributes.push_back(colorAttribute);

        return desc;
    }

    Renderer::Renderer()
        : m_context{}
        , m_frames{}
        , m_frameNumber{ 0 }
        , m_defaultRenderPass{ VK_NULL_HANDLE }
        , m_frameBuffers{}
        , m_globalDescSetLayout{ VK_NULL_HANDLE }
        , m_objectsDescSetLayout{ VK_NULL_HANDLE }
        , m_descPool{ VK_NULL_HANDLE }
        , m_sceneData{}
        , m_sceneDataBuffer{}
        , m_imguiDescPool{ VK_NULL_HANDLE }
        , m_renderables{}
        , m_materials{}
        , m_meshes{}
        , m_imGuiDebugWindow{ nullptr }
        , m_camera{ nullptr }
        , m_isImGuiInitialized{ false }
    {
    }

    Renderer::~Renderer()
    {
        DJO_ASSERT(m_context.instance == VK_NULL_HANDLE);
    }

    EResult
    Renderer::Init(::GLFWwindow* const _glfwWindow, const DoInitImGui _initImGui)
    {
        DJO_ASSERT(_glfwWindow != nullptr);
        DJO_ASSERT(m_context.instance == VK_NULL_HANDLE);

        std::vector<const char*> extensions;
        DJO_OK_OR_RETURN(EnumerateRequiredExtensions(&extensions));
        DJO_OK_OR_RETURN(m_context.Init(_glfwWindow, ToView(extensions), { Vulkan::c_validationLayers }));

        DJO_OK_OR_RETURN(InitCommandPool());
        DJO_OK_OR_RETURN(InitCommandBuffer());
        DJO_OK_OR_RETURN(InitDefaultRenderPass());
        DJO_OK_OR_RETURN(InitFrameBuffers());
        DJO_OK_OR_RETURN(InitSynchronization());
        DJO_OK_OR_RETURN(InitDescriptors());
        DJO_OK_OR_RETURN(InitGraphicsPipeline());

        if (_initImGui)
        {
            DJO_OK_OR_RETURN(InitImGui());
        }

        DJO_OK_OR_RETURN(LoadMeshes());
        DJO_OK_OR_RETURN(InitScene());

        return EResult::OK;
    }

    void
    Renderer::Destroy()
    {
        if (m_context.device != VK_NULL_HANDLE)
        {
            const ::VkResult vkresult = ::vkDeviceWaitIdle(m_context.device);
            DJO_ASSERT(vkresult == ::VK_SUCCESS); DJO_UNUSED_VAR(vkresult);
        }

        DestroyScene();
        DestroyMeshes();

        DestroyImGui();

        DestroyGraphicsPipeline();
        DestroyDescriptors();
        DestroySynchronization();
        DestroyFrameBuffers();
        DestroyDefaultRenderPass();
        DestroyCommandBuffer();
        DestroyCommandPool();

        m_context.Destroy();
    }

    EResult
    Renderer::InitImGuiDebugWindow(ImGuiManager& _imGuiManager)
    {
        m_imGuiDebugWindow.Reset(DJO_CORE_NEW ImGuiDebugWindow_Renderer{ *this });
        _imGuiManager.RegisterMainMenuBarItem(m_imGuiDebugWindow.Get());
        _imGuiManager.RegisterWindow(m_imGuiDebugWindow.Get());
        return EResult::OK;
    }

    void
    Renderer::DestroyImGuiDebugWindow(ImGuiManager& _imGuiManager)
    {
        _imGuiManager.UnregisterWindow(m_imGuiDebugWindow.Get());
        _imGuiManager.UnregisterMainMenuBarItem(m_imGuiDebugWindow.Get());
    }

    void
    Renderer::Render(const Milliseconds64 _dt)
    {
        constexpr u64 c_oneSecTimeout{ 1000000000 };

        static Seconds time{ 0.f };
        time += (f32)_dt.Sec();

        const u32 currentFrameId = m_frameNumber % c_concurrentFrameCount;
        FrameData& currentFrameData = m_frames[currentFrameId];
        const ::VkCommandBuffer cmd{ currentFrameData.commandBuffer };

        // wait until the GPU has finished rendering the last frame, timeout of 1 second
        ::vkWaitForFences(m_context.device, 1, &currentFrameData.renderFence, true, c_oneSecTimeout);
        ::vkResetFences(m_context.device, 1, &currentFrameData.renderFence);

        // request image from the swapchain, one second timeout
        u32 swapchainImageIndex;
        ::vkAcquireNextImageKHR(m_context.device, m_context.swapchain, c_oneSecTimeout, currentFrameData.presentSemaphore, nullptr, &swapchainImageIndex);

        // now that we are sure that the commands finished executing, we can safely reset the command buffer to begin recording again
        ::vkResetCommandBuffer(cmd, 0);

        // begin the command buffer recording
        // we will use this command buffer exactly once, so we want to let Vulkan know that
        const ::VkCommandBufferBeginInfo cmdBeginInfo{
            Vulkan::CommandBufferBeginInfo(::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT)
        };
        ::vkBeginCommandBuffer(cmd, &cmdBeginInfo);

        ::VkClearValue clearValue{};
        const f32 flash = std::fabs(std::sinf(time * 0.1f)) * 0.1f;
        clearValue.color = { { 0.0f, 0.0f, flash, 1.0f } };

        ::VkClearValue depthClear{};
        depthClear.depthStencil.depth = 1.f;

        // start the main renderpass
        // we will use the clear color from above, and the framebuffer of the index the swapchain gave us
        const ::VkClearValue clearValues[2]{ clearValue, depthClear };
        const ::VkRenderPassBeginInfo renderPassBeginInfo{
            Vulkan::RenderPassBeginInfo(
                m_defaultRenderPass,
                m_frameBuffers[swapchainImageIndex],
                { { 0, 0 }, m_context.swapchainExtent },
                clearValues)
        };
        ::vkCmdBeginRenderPass(cmd, &renderPassBeginInfo, ::VK_SUBPASS_CONTENTS_INLINE);

        if (m_camera != nullptr)
        {
            // camera
            {
                const ViewContext viewContext = m_camera->BuildViewContext();
                GPUCameraData cam{};
                cam.view = viewContext.view;
                cam.proj = viewContext.projection;
                cam.viewProj = viewContext.viewProjection;
                void* data;
                const ::VkResult vkresult = ::vmaMapMemory(m_context.allocator, currentFrameData.cameraBuffer.alloc, &data);
                if (vkresult != ::VK_SUCCESS)
                    return;

                ::memcpy(data, &cam, sizeof(GPUCameraData));
                ::vmaUnmapMemory(m_context.allocator, currentFrameData.cameraBuffer.alloc);
            }

            // scene data
            {
                const f32 s = std::sinf(time * 0.1f) * 0.1f;
                const f32 c = std::cosf(time * 0.1f) * 0.1f;
                m_sceneData.ambientColor = { s, 0.f, c, 1.f };
                char* data;
                const ::VkResult vkresult = ::vmaMapMemory(m_context.allocator, m_sceneDataBuffer.alloc, (void**)&data);
                if (vkresult != ::VK_SUCCESS)
                    return;

                data += (sz)currentFrameId * GetAlignedBufferSize(sizeof(GPUSceneData));
                ::memcpy(data, &m_sceneData, sizeof(GPUSceneData));
                ::vmaUnmapMemory(m_context.allocator, m_sceneDataBuffer.alloc);
            }

            // objects
            RenderObjects(currentFrameData, ToConstView(m_renderables));
        }

        // ImGui pass
        if (m_isImGuiInitialized)
        {
            RenderImGui(currentFrameData);
        }

        // finalize the render pass
        ::vkCmdEndRenderPass(cmd);

        // finalize the command buffer (we can no longer add commands, but it can now be executed)
        ::vkEndCommandBuffer(cmd);

        // prepare the submission to the queue
        // we want to wait on the m_presentSemaphore, as that semaphore is signaled when the swapchain is ready
        // we will signal the m_renderSemaphore, to signal that rendering has finished
        const ::VkPipelineStageFlags waitStage = ::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        const ::VkSubmitInfo submit{
            Vulkan::SubmitInfo(
                { &currentFrameData.presentSemaphore, 1 },
                { &waitStage , 1 },
                { &currentFrameData.commandBuffer, 1 },
                { &currentFrameData.renderSemaphore, 1 }
            )
        };
        ::vkQueueSubmit(m_context.graphicsQueue.queue, 1, &submit, currentFrameData.renderFence);

        // this will put the image we just rendered into the visible window
        // we want to wait on the m_renderSemaphore for that,
        // as it's necessary that drawing commands have finished before the image is displayed to the user
        ::VkPresentInfoKHR presentInfo{};
        presentInfo.sType = ::VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.pNext = nullptr;
        presentInfo.pSwapchains = &m_context.swapchain;
        presentInfo.swapchainCount = 1;
        presentInfo.pWaitSemaphores = &currentFrameData.renderSemaphore;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pImageIndices = &swapchainImageIndex;
        presentInfo.pResults = nullptr;
        ::vkQueuePresentKHR(m_context.graphicsQueue.queue, &presentInfo);

        ++m_frameNumber;
    }

    sz
    Renderer::GetAlignedBufferSize(const sz _baseSize) const
    {
        DJO_ASSERT(IsPow2(m_context.physicalDeviceProperties.limits.minUniformBufferOffsetAlignment));
        return AlignPow2(_baseSize, m_context.physicalDeviceProperties.limits.minUniformBufferOffsetAlignment);
    }

    EResult
    Renderer::EnumerateRequiredExtensions(std::vector<const char*>* const _outExtensions) const
    {
        DJO_ASSERT(_outExtensions != nullptr);

        // GLFW required extensions
        u32 glfwExtensionCount = 0;
        const char* const* const glfwExtensions = ::glfwGetRequiredInstanceExtensions(&glfwExtensionCount); // pointer memory is handeled by GLFW
        if (Vulkan::CheckInstanceExtensionsAvailability({ glfwExtensions, glfwExtensionCount }))
        {
            _outExtensions->insert(std::end(*_outExtensions), glfwExtensions, glfwExtensions + glfwExtensionCount);
        }
        else
        {
            return MakeResult(EResult::FAIL, "Can't init glfw required extensions");
        }

    #if defined(DJO_VK_ENABLE_VALIDATION_LAYERS)

        if (Vulkan::CheckLayersAvailability({ Vulkan::c_validationLayers }))
        {
            _outExtensions->push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }
        else
        {
            return MakeResult(EResult::FAIL, "Can't init VK_LAYER_KHRONOS_validation extension");
        }

    #endif

        return EResult::OK;
    }

    EResult
    Renderer::InitCommandPool()
    {
        const ::VkCommandPoolCreateInfo poolInfo{
            Vulkan::CommandPoolCreateInfo(::VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, m_context.graphicsQueue.familyIndex)
        };
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            const ::VkResult vkresult = ::vkCreateCommandPool(m_context.device, &poolInfo, nullptr, &m_frames[i].commandPool);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateCommandPool failed");
        }

        return EResult::OK;
    }

    void
    Renderer::DestroyCommandPool()
    {
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            if (m_frames[i].commandPool != VK_NULL_HANDLE)
            {
                ::vkDestroyCommandPool(m_context.device, m_frames[i].commandPool, nullptr);
                m_frames[i].commandPool = VK_NULL_HANDLE;
            }
        }
    }

    EResult
    Renderer::InitCommandBuffer()
    {
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            const ::VkCommandBufferAllocateInfo allocInfo{
                Vulkan::CommandBufferAllocateInfo(m_frames[i].commandPool, ::VK_COMMAND_BUFFER_LEVEL_PRIMARY, 1)
            };
            const ::VkResult vkresult = ::vkAllocateCommandBuffers(m_context.device, &allocInfo, &m_frames[i].commandBuffer);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkAllocateCommandBuffers failed");
        }

        return EResult::OK;
    }

    void
    Renderer::DestroyCommandBuffer()
    {
        // It�s not possible to individually destroy VkCommandBuffer,
        // as destroying their parent pool will destroy all of the command buffers allocated from it.
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            m_frames[i].commandBuffer = VK_NULL_HANDLE;
        }
    }

    EResult
    Renderer::InitDefaultRenderPass()
    {
        ::VkAttachmentDescription colorAttachment{};
        colorAttachment.flags = 0;
        colorAttachment.format = m_context.swapchainFormat;
        colorAttachment.samples = ::VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = ::VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = ::VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = ::VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = ::VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = ::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        ::VkAttachmentReference colorAttachmentRef{};
        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = ::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        ::VkAttachmentDescription depthAttachment{};
        depthAttachment.flags = 0;
        depthAttachment.format = m_context.depthFormat;
        depthAttachment.samples = ::VK_SAMPLE_COUNT_1_BIT;
        depthAttachment.loadOp = ::VK_ATTACHMENT_LOAD_OP_CLEAR;
        depthAttachment.storeOp = ::VK_ATTACHMENT_STORE_OP_STORE;
        depthAttachment.stencilLoadOp = ::VK_ATTACHMENT_LOAD_OP_CLEAR;
        depthAttachment.stencilStoreOp = ::VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthAttachment.initialLayout = ::VK_IMAGE_LAYOUT_UNDEFINED;
        depthAttachment.finalLayout = ::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        ::VkAttachmentReference depthAttachmentRef{};
        depthAttachmentRef.attachment = 1;
        depthAttachmentRef.layout = ::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        ::VkSubpassDescription subpass{};
        subpass.flags = 0;
        subpass.pipelineBindPoint = ::VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.inputAttachmentCount = 0;
        subpass.pInputAttachments = nullptr;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;
        subpass.pResolveAttachments = nullptr;
        subpass.pDepthStencilAttachment = &depthAttachmentRef;
        subpass.preserveAttachmentCount = 0;
        subpass.pPreserveAttachments = nullptr;

        ::VkSubpassDependency colorDependency{};
        colorDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        colorDependency.dstSubpass = 0;
        colorDependency.srcStageMask = ::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        colorDependency.dstStageMask = ::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        colorDependency.srcAccessMask = 0;
        colorDependency.dstAccessMask = ::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        colorDependency.dependencyFlags = 0;
        ::VkSubpassDependency depthDependency{};
        depthDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        depthDependency.dstSubpass = 0;
        depthDependency.srcStageMask = ::VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | ::VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        depthDependency.dstStageMask = ::VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | ::VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
        depthDependency.srcAccessMask = 0;
        depthDependency.dstAccessMask = ::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        depthDependency.dependencyFlags = 0;

        const ::VkAttachmentDescription attachments[2]{ colorAttachment, depthAttachment };
        const ::VkSubpassDependency dependencies[2]{ colorDependency, depthDependency };
        const ::VkRenderPassCreateInfo renderPassInfo{
            Vulkan::RenderPassCreateInfo(attachments, { &subpass, 1 }, dependencies)
        };
        const ::VkResult vkresult = ::vkCreateRenderPass(m_context.device, &renderPassInfo, nullptr, &m_defaultRenderPass);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vkCreateRenderPass failed");

        return EResult::OK;
    }

    void
    Renderer::DestroyDefaultRenderPass()
    {
        if (m_defaultRenderPass != VK_NULL_HANDLE)
        {
            ::vkDestroyRenderPass(m_context.device, m_defaultRenderPass, nullptr);
            m_defaultRenderPass = VK_NULL_HANDLE;
        }
    }

    EResult
    Renderer::InitFrameBuffers()
    {
        for (const ::VkImageView& imageView : m_context.swapchainImageViews)
        {
            const ::VkImageView attachments[2]{ imageView, m_context.depthImageView };
            const ::VkFramebufferCreateInfo framebufferInfo{
                Vulkan::FramebufferCreateInfo(m_defaultRenderPass, attachments, m_context.swapchainExtent)
            };
            ::VkFramebuffer frameBuffer{ VK_NULL_HANDLE };
            const ::VkResult vkresult = ::vkCreateFramebuffer(m_context.device, &framebufferInfo, nullptr, &frameBuffer);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateFramebuffer failed");

            m_frameBuffers.emplace_back(frameBuffer);
        }
        return EResult::OK;
    }

    void
    Renderer::DestroyFrameBuffers()
    {
        for (::VkFramebuffer& frameBuffer : m_frameBuffers)
        {
            if (frameBuffer != VK_NULL_HANDLE)
            {
                ::vkDestroyFramebuffer(m_context.device, frameBuffer, nullptr);
                frameBuffer = VK_NULL_HANDLE;
            }
        }
        m_frameBuffers.clear();
    }

    EResult
    Renderer::InitSynchronization()
    {
        ::VkResult vkresult;
        const ::VkFenceCreateInfo fenceInfo{ Vulkan::FenceCreateInfo(::VK_FENCE_CREATE_SIGNALED_BIT) };
        const ::VkSemaphoreCreateInfo semaphoreInfo{ Vulkan::SemaphoreCreateInfo(0) };
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            vkresult = ::vkCreateFence(m_context.device, &fenceInfo, nullptr, &m_frames[i].renderFence);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateFence failed");

            for (::VkSemaphore* const semaphoreToInit : { &m_frames[i].presentSemaphore, &m_frames[i].renderSemaphore })
            {
                vkresult = ::vkCreateSemaphore(m_context.device, &semaphoreInfo, nullptr, semaphoreToInit);
                if (vkresult != ::VK_SUCCESS)
                    return MakeResult(EResult::FAIL, "vkCreateSemaphore failed");
            }
        }

        return EResult::OK;
    }

    void
    Renderer::DestroySynchronization()
    {
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            for (::VkSemaphore* const semaphoreToDestroy : { &m_frames[i].presentSemaphore, &m_frames[i].renderSemaphore })
            {
                if (*semaphoreToDestroy != VK_NULL_HANDLE)
                {
                    ::vkDestroySemaphore(m_context.device, *semaphoreToDestroy, nullptr);
                    *semaphoreToDestroy = VK_NULL_HANDLE;
                }
            }
            if (m_frames[i].renderFence != VK_NULL_HANDLE)
            {
                ::vkDestroyFence(m_context.device, m_frames[i].renderFence, nullptr);
                m_frames[i].renderFence = VK_NULL_HANDLE;
            }
        }
    }

    EResult
    Renderer::InitDescriptors()
    {
        const ::VkDescriptorPoolSize descPoolSizes[]
        {
            { ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 10 },
            { ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 10 },
            { ::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 10 },
        };
        const ::VkDescriptorPoolCreateInfo descPoolInfo{
            Vulkan::DescriptorPoolCreateInfo(10, descPoolSizes)
        };
        ::VkResult vkresult = ::vkCreateDescriptorPool(m_context.device, &descPoolInfo, nullptr, &m_descPool);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vkCreateDescriptorPool failed");

        // global descriptor set
        {
            const ::VkDescriptorSetLayoutBinding globalBindings[]
            {
                // camera
                Vulkan::DescriptorSetLayoutBinding(0,
                    ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    ::VK_SHADER_STAGE_VERTEX_BIT),
                // scene
                Vulkan::DescriptorSetLayoutBinding(1,
                    ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
                    ::VK_SHADER_STAGE_VERTEX_BIT | ::VK_SHADER_STAGE_FRAGMENT_BIT)
            };

            const ::VkDescriptorSetLayoutCreateInfo globalDescSetLayoutCreateInfo{
                Vulkan::DescriptorSetLayoutCreateInfo(globalBindings)
            };
            vkresult = ::vkCreateDescriptorSetLayout(m_context.device, &globalDescSetLayoutCreateInfo, nullptr, &m_globalDescSetLayout);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateDescriptorSetLayout failed");

            const ::VkDescriptorSetLayoutBinding objectsBinding{
                Vulkan::DescriptorSetLayoutBinding(0,
                    ::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                    ::VK_SHADER_STAGE_VERTEX_BIT)
            };

            const ::VkDescriptorSetLayoutCreateInfo objectsDescSetLayoutCreateInfo{
                Vulkan::DescriptorSetLayoutCreateInfo({ &objectsBinding, 1 })
            };
            vkresult = ::vkCreateDescriptorSetLayout(m_context.device, &objectsDescSetLayoutCreateInfo, nullptr, &m_objectsDescSetLayout);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vkCreateDescriptorSetLayout failed");

            {
                const auto [buffer, allocVkresult] = CreateAllocatedVkBuffer(
                    m_context.allocator,
                    c_concurrentFrameCount * GetAlignedBufferSize(sizeof(GPUSceneData)),
                    ::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                    ::VMA_MEMORY_USAGE_CPU_TO_GPU
                );
                if (allocVkresult != ::VK_SUCCESS)
                    return MakeResult(EResult::FAIL, "CreateAllocatedVkBuffer failed");

                m_sceneDataBuffer = buffer;
            }

            constexpr u32 c_maxObjectsCount{ 10000 };
            constexpr u32 c_objectsBufferSize{ sizeof(GPUObjectsData) * c_maxObjectsCount };

            for (u32 i = 0; i < c_concurrentFrameCount; ++i)
            {
                {
                    const auto [buffer, allocVkresult] = CreateAllocatedVkBuffer(
                        m_context.allocator,
                        sizeof(GPUCameraData),
                        ::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                        ::VMA_MEMORY_USAGE_CPU_TO_GPU
                    );
                    if (allocVkresult != ::VK_SUCCESS)
                        return MakeResult(EResult::FAIL, "CreateAllocatedVkBuffer failed");

                    m_frames[i].cameraBuffer = buffer;
                }

                {
                    const auto [buffer, allocVkresult] = CreateAllocatedVkBuffer(
                        m_context.allocator,
                        c_objectsBufferSize,
                        ::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                        ::VMA_MEMORY_USAGE_CPU_TO_GPU
                    );
                    if (allocVkresult != ::VK_SUCCESS)
                        return MakeResult(EResult::FAIL, "CreateAllocatedVkBuffer failed");

                    m_frames[i].objectsBuffer = buffer;
                }

                const ::VkDescriptorSetAllocateInfo globalDescSetAllocInfo{
                    Vulkan::DescriptorSetAllocateInfo(m_descPool, { &m_globalDescSetLayout, 1 })
                };
                vkresult = ::vkAllocateDescriptorSets(m_context.device, &globalDescSetAllocInfo, &m_frames[i].globalDescSet);
                if (vkresult != ::VK_SUCCESS)
                    return MakeResult(EResult::FAIL, "vkAllocateDescriptorSets failed");

                const ::VkDescriptorSetAllocateInfo objectsDescSetAllocInfo{
                    Vulkan::DescriptorSetAllocateInfo(m_descPool, { &m_objectsDescSetLayout, 1 })
                };
                vkresult = ::vkAllocateDescriptorSets(m_context.device, &objectsDescSetAllocInfo, &m_frames[i].objectsDescSet);
                if (vkresult != ::VK_SUCCESS)
                    return MakeResult(EResult::FAIL, "vkAllocateDescriptorSets failed");

                ::VkDescriptorBufferInfo camDescBufferInfo{};
                camDescBufferInfo.buffer = m_frames[i].cameraBuffer.buffer;
                camDescBufferInfo.offset = 0;
                camDescBufferInfo.range = sizeof(GPUCameraData);

                ::VkDescriptorBufferInfo sceneDescBufferInfo{};
                sceneDescBufferInfo.buffer = m_sceneDataBuffer.buffer;
                sceneDescBufferInfo.offset = 0;
                sceneDescBufferInfo.range = sizeof(GPUSceneData);

                ::VkDescriptorBufferInfo objectsDescBufferInfo{};
                objectsDescBufferInfo.buffer = m_frames[i].objectsBuffer.buffer;
                objectsDescBufferInfo.offset = 0;
                objectsDescBufferInfo.range = c_objectsBufferSize;

                ::VkWriteDescriptorSet descSetWrites[]{
                    Vulkan::InitWriteDescriptorSet(
                        m_frames[i].globalDescSet,
                        0, 0, 1,
                        ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER),
                    Vulkan::InitWriteDescriptorSet(
                        m_frames[i].globalDescSet,
                        1, 0, 1,
                        ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC),
                    Vulkan::InitWriteDescriptorSet(
                        m_frames[i].objectsDescSet,
                        0, 0, 1,
                        ::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER),
                };
                descSetWrites[0].pBufferInfo = &camDescBufferInfo;
                descSetWrites[1].pBufferInfo = &sceneDescBufferInfo;
                descSetWrites[2].pBufferInfo = &objectsDescBufferInfo;
                ::vkUpdateDescriptorSets(m_context.device, 3, descSetWrites, 0, nullptr);
            }
        }

        return EResult::OK;
    }

    void
    Renderer::DestroyDescriptors()
    {
        for (u32 i = 0; i < c_concurrentFrameCount; ++i)
        {
            if (m_frames[i].objectsBuffer.buffer != VK_NULL_HANDLE)
                DestroyAllocatedVkBuffer(m_context.allocator, m_frames[i].objectsBuffer);

            if (m_frames[i].cameraBuffer.buffer != VK_NULL_HANDLE)
                DestroyAllocatedVkBuffer(m_context.allocator, m_frames[i].cameraBuffer);
        }
        if (m_sceneDataBuffer.buffer != VK_NULL_HANDLE)
            DestroyAllocatedVkBuffer(m_context.allocator, m_sceneDataBuffer);

        if (m_objectsDescSetLayout != VK_NULL_HANDLE)
        {
            ::vkDestroyDescriptorSetLayout(m_context.device, m_objectsDescSetLayout, nullptr);
            m_objectsDescSetLayout = VK_NULL_HANDLE;
        }
        if (m_globalDescSetLayout != VK_NULL_HANDLE)
        {
            ::vkDestroyDescriptorSetLayout(m_context.device, m_globalDescSetLayout, nullptr);
            m_globalDescSetLayout = VK_NULL_HANDLE;
        }
        if (m_descPool != VK_NULL_HANDLE)
        {
            ::vkDestroyDescriptorPool(m_context.device, m_descPool, nullptr);
            m_descPool = VK_NULL_HANDLE;
        }
    }

    EResult
    Renderer::InitGraphicsPipeline()
    {
        LocalFileSystem* const fs = DJO_ENGINE_FS;
        Vulkan::GraphicsPipelineCreateInfoData graphicsPipelineData{};

        ::VkShaderModule defaultLitVertexShaderModule = VK_NULL_HANDLE;
        {
            LocalFile shaderFile = fs->GetLocalFile("Shaders/default_lit.vert.glsl.spv");
            DJO_OK_OR_RETURN(LoadShaderModuleFromFile(shaderFile, &defaultLitVertexShaderModule));
        }
        ::VkShaderModule defaultLitFragmentShaderModule = VK_NULL_HANDLE;
        {
            LocalFile shaderFile = fs->GetLocalFile("Shaders/default_lit.frag.glsl.spv");
            DJO_OK_OR_RETURN(LoadShaderModuleFromFile(shaderFile, &defaultLitFragmentShaderModule));
        }

        graphicsPipelineData.shaderStages.emplace_back(
            Vulkan::PipelineShaderStageCreateInfo(::VK_SHADER_STAGE_VERTEX_BIT, defaultLitVertexShaderModule)
        );
        graphicsPipelineData.shaderStages.emplace_back(
            Vulkan::PipelineShaderStageCreateInfo(::VK_SHADER_STAGE_FRAGMENT_BIT, defaultLitFragmentShaderModule)
        );

        const VertexInputDescription vertexDesc{ Vertex::GetDesc() };
        graphicsPipelineData.vertexInput = Vulkan::PipelineVertexInputStateCreateInfo(
            ToView(vertexDesc.bindings),
            ToView(vertexDesc.attributes)
        );

        graphicsPipelineData.inputAssembly =
            Vulkan::PipelineInputAssemblyStateCreateInfo(::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
        graphicsPipelineData.rasterizer =
            Vulkan::PipelineRasterizationStateCreateInfo(::VK_POLYGON_MODE_FILL, ::VK_CULL_MODE_BACK_BIT);
        graphicsPipelineData.multisampling =
            Vulkan::PipelineMultisampleStateCreateInfo(::VK_SAMPLE_COUNT_1_BIT); // multisampling defaulted to no multisampling (1 sample per pixel)

        ::VkPipelineColorBlendAttachmentState colorBlendAttachment{}; // per attached framebuffer
        colorBlendAttachment.blendEnable = VK_FALSE;
        //colorBlendAttachment.srcColorBlendFactor = ::VK_BLEND_FACTOR_ONE;
        //colorBlendAttachment.dstColorBlendFactor = ::VK_BLEND_FACTOR_ZERO;
        //colorBlendAttachment.colorBlendOp = ::VK_BLEND_OP_ADD;
        //colorBlendAttachment.srcAlphaBlendFactor = ::VK_BLEND_FACTOR_ONE;
        //colorBlendAttachment.dstAlphaBlendFactor = ::VK_BLEND_FACTOR_ZERO;
        //colorBlendAttachment.alphaBlendOp = ::VK_BLEND_OP_ADD;
        colorBlendAttachment.colorWriteMask = ::VK_COLOR_COMPONENT_R_BIT | ::VK_COLOR_COMPONENT_G_BIT | ::VK_COLOR_COMPONENT_B_BIT | ::VK_COLOR_COMPONENT_A_BIT;

        // global parameters
        graphicsPipelineData.colorBlend =
            Vulkan::PipelineColorBlendStateCreateInfo({ &colorBlendAttachment, 1 });

        graphicsPipelineData.depthStencil =
            Vulkan::PipelineDepthStencilStateCreateInfo(true, ::VK_COMPARE_OP_LESS_OR_EQUAL);

        // Viewport flipping for Vulkan see:
        // https://www.saschawillems.de/blog/2019/03/29/flipping-the-vulkan-viewport/
        ::VkViewport viewport{};
        viewport.x = 0.f;
        viewport.y = (f32)(m_context.swapchainExtent.height);
        viewport.width = (f32)(m_context.swapchainExtent.width);
        viewport.height = -(f32)(m_context.swapchainExtent.height);
        viewport.minDepth = 0.f;
        viewport.maxDepth = 1.f;
        ::VkRect2D scissor{};
        scissor.offset = { 0, 0 };
        scissor.extent = m_context.swapchainExtent;
        graphicsPipelineData.viewport =
            Vulkan::PipelineViewportStateCreateInfo({ &viewport, 1 }, { &scissor, 1 });

        //::VkPushConstantRange pushConstantRange{};
        //pushConstantRange.stageFlags = ::VK_SHADER_STAGE_VERTEX_BIT;
        //pushConstantRange.offset = 0;
        //pushConstantRange.size = sizeof(MeshPushConstants);

        const ::VkDescriptorSetLayout descSetLayouts[]{ m_globalDescSetLayout, m_objectsDescSetLayout };
        const ::VkPipelineLayoutCreateInfo pipelineLayoutInfo{
            Vulkan::PipelineLayoutCreateInfo(descSetLayouts , { nullptr, 0 })
        };
        ::VkPipelineLayout pipelineLayout{ VK_NULL_HANDLE };
        ::VkResult vkresult = ::vkCreatePipelineLayout(m_context.device, &pipelineLayoutInfo, nullptr, &pipelineLayout);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vkCreatePipelineLayout failed");

        graphicsPipelineData.pipelineLayout = pipelineLayout;
        graphicsPipelineData.renderPass = m_defaultRenderPass;

        const ::VkGraphicsPipelineCreateInfo pipelineInfo{
            Vulkan::GraphicsPipelineCreateInfo(graphicsPipelineData)
        };
        ::VkPipeline graphicsPipeline{ VK_NULL_HANDLE };
        vkresult = ::vkCreateGraphicsPipelines(m_context.device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vkCreateGraphicsPipelines failed");

        ::vkDestroyShaderModule(m_context.device, defaultLitVertexShaderModule, nullptr);
        ::vkDestroyShaderModule(m_context.device, defaultLitFragmentShaderModule, nullptr);

        CreateMaterial("default", graphicsPipeline, pipelineLayout);

        return EResult::OK;
    }

    EResult
    Renderer::LoadShaderModuleFromFile(LocalFile& _shaderFile, ::VkShaderModule* const _outShaderModule)
    {
        std::vector<u8> shaderSource;
        DJO_OK_OR_RETURN(_shaderFile.Open(EOpenModeFlag::Read | EOpenModeFlag::Binary));

        // spirv expects the buffer to be on uint32, so make sure to reserve an int vector big enough for the entire file
        _shaderFile.ComputeSize();
        shaderSource.reserve(Align(_shaderFile.GetSize(), sizeof(u32)));
        DJO_OK_OR_RETURN(_shaderFile.Read(&shaderSource));

        DJO_ASSERT(shaderSource.capacity() >= Align(shaderSource.size(), sizeof(u32)));
        _shaderFile.Close();

        const ::VkShaderModuleCreateInfo createInfo{
            Vulkan::ShaderModuleCreateInfo(ToConstView(shaderSource))
        };
        const ::VkResult vkresult = ::vkCreateShaderModule(m_context.device, &createInfo, nullptr, _outShaderModule);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vkCreateShaderModule failed");

        return EResult::OK;
    }

    void
    Renderer::DestroyGraphicsPipeline()
    {
        for (auto& kv : m_materials)
        {
            Material& material = kv.second;
            if (material.pipeline != VK_NULL_HANDLE)
            {
                ::vkDestroyPipeline(m_context.device, material.pipeline, nullptr);
                material.pipeline = VK_NULL_HANDLE;
            }
            if (material.pipelineLayout != VK_NULL_HANDLE)
            {
                ::vkDestroyPipelineLayout(m_context.device, material.pipelineLayout, nullptr);
                material.pipelineLayout = VK_NULL_HANDLE;
            }
        }
        m_materials.clear();
    }

    EResult
    Renderer::LoadMeshes()
    {
        ::VmaAllocationCreateInfo vmaAllocInfo{};
        vmaAllocInfo.flags = 0;
        vmaAllocInfo.usage = ::VMA_MEMORY_USAGE_CPU_TO_GPU;
        vmaAllocInfo.requiredFlags = 0;
        vmaAllocInfo.preferredFlags = 0;
        vmaAllocInfo.memoryTypeBits = 0;
        vmaAllocInfo.pool = VK_NULL_HANDLE;
        vmaAllocInfo.pUserData = nullptr;
        vmaAllocInfo.priority = 0;

        // triangle mesh

        Mesh triangleMesh{};

        constexpr u32 c_vertexCount{ 3 };
        constexpr u32 c_bufferSize{ c_vertexCount * sizeof(Vertex) };

        triangleMesh.vertices.resize(c_vertexCount);
        triangleMesh.vertices[0].pos = { 1.f, 1.f, 0.f };
        triangleMesh.vertices[0].color = { 0.5f, 1.f, 0.f };
        triangleMesh.vertices[1].pos = { -1.f, 1.f, 0.f };
        triangleMesh.vertices[1].color = { 0.f, 1.f, 0.5f };
        triangleMesh.vertices[2].pos = { 0.f,-1.f, 0.f };
        triangleMesh.vertices[2].color = { 0.f, 0.5f, 0.f };

        ::VkBufferCreateInfo bufferInfo{
            Vulkan::BufferCreateInfo(
                c_bufferSize,
                ::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                ::VK_SHARING_MODE_EXCLUSIVE
        )};
        ::VkResult vkresult = ::vmaCreateBuffer(m_context.allocator, &bufferInfo, &vmaAllocInfo,
                                                &triangleMesh.vkBuffer.buffer,
                                                &triangleMesh.vkBuffer.alloc,
                                                nullptr);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vmaCreateBuffer failed");

        void* data;
        vkresult = ::vmaMapMemory(m_context.allocator, triangleMesh.vkBuffer.alloc, &data);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vmaMapMemory failed");

        ::memcpy(data, triangleMesh.vertices.data(), c_bufferSize);
        ::vmaUnmapMemory(m_context.allocator, triangleMesh.vkBuffer.alloc);

        m_meshes.emplace_back(std::move(triangleMesh));


        // loaded mesh

        ObjLoader objLoader;
        ObjLoader::Params objLoaderParams;
        objLoaderParams.fsp = FSPath{ "Models/sponza/sponza.obj", DJO_ENGINE_FS };
        EResult result = objLoader.Load(objLoaderParams);
        if (result != EResult::OK || objLoader.GetMeshes().empty())
        {
            return result;
        }

        m_meshes.reserve(m_meshes.size() + objLoader.GetMeshes().size());
        for (const ObjLoader::Mesh& objLoaderMesh : objLoader.GetMeshes())
        {
            Mesh loadedMesh{};
            const PropertySemanticsArray& loadedLayoutSemantics = objLoaderMesh.layoutSemantics;
            const MeshData& loadedMeshData = objLoaderMesh.data;
            const BufferLayout& loadedBufferLayout = loadedMeshData.vertexLayout;

            MaxArray<sz, BufferLayout::c_maxPropertyCount> offsets{};
            for (const ELayoutPropertySemantic outSemantic : {
                ELayoutPropertySemantic::Position,
                ELayoutPropertySemantic::Normal,
                ELayoutPropertySemantic::Normal
            })
            {
                for (u32 i = 0; i < loadedLayoutSemantics.GetSize(); ++i)
                {
                    const ELayoutPropertySemantic inSemantic = loadedLayoutSemantics[i];
                    if (inSemantic == outSemantic)
                    {
                        offsets.PushBack(loadedBufferLayout.GetProperties()[i].offset / sizeof(f32));
                        DJO_ASSERT(loadedBufferLayout.GetProperties()[i].size == (3 * sizeof(f32)));
                        break;
                    }
                }
            }

            const u32 vertCount = (u32)loadedMeshData.indices.size();
            loadedMesh.vertices.resize(vertCount);
            const u32 loadedVertexFloatStride = loadedBufferLayout.GetStride() / sizeof(f32);
            for (u32 i = 0; i < vertCount; ++i)
            {
                Vertex& vert = loadedMesh.vertices[i];
                const f32* v = &loadedMeshData.vertices[loadedMeshData.indices[i] * loadedVertexFloatStride];
                vert.pos = *reinterpret_cast<const Vec3f*>(v + offsets[0]);
                vert.normal = *reinterpret_cast<const Vec3f*>(v + offsets[1]);
                vert.color = *reinterpret_cast<const Vec3f*>(v + offsets[2]);
            }

            const ::VkDeviceSize loadedBufferSize = loadedMesh.vertices.size() * sizeof(Vertex);
            bufferInfo = Vulkan::BufferCreateInfo(
                loadedBufferSize,
                ::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                ::VK_SHARING_MODE_EXCLUSIVE);

            vkresult = ::vmaCreateBuffer(
                m_context.allocator, &bufferInfo, &vmaAllocInfo,
                &loadedMesh.vkBuffer.buffer, &loadedMesh.vkBuffer.alloc, nullptr);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vmaCreateBuffer failed");

            vkresult = ::vmaMapMemory(m_context.allocator, loadedMesh.vkBuffer.alloc, &data);
            if (vkresult != ::VK_SUCCESS)
                return MakeResult(EResult::FAIL, "vmaMapMemory failed");

            ::memcpy(data, loadedMesh.vertices.data(), loadedBufferSize);
            ::vmaUnmapMemory(m_context.allocator, loadedMesh.vkBuffer.alloc);

            m_meshes.emplace_back(std::move(loadedMesh));
        }

        return EResult::OK;
    }

    void
    Renderer::DestroyMeshes()
    {
        for (Mesh& mesh : m_meshes)
        {
            if (mesh.vkBuffer.buffer != VK_NULL_HANDLE)
            {
                ::vmaDestroyBuffer(m_context.allocator, mesh.vkBuffer.buffer, mesh.vkBuffer.alloc);
                mesh.vkBuffer.buffer = VK_NULL_HANDLE;
            }
            mesh.vertices.clear();
        }
        m_meshes.clear();
    }

    EResult
    Renderer::InitScene()
    {
        Material* const material = GetMaterial("default");
        {
            //Mesh& mesh = m_meshes[0];
            //for (s32 x = -1; x <= 1; x++)
            //{
            //    for (s32 y = -1; y <= 1; y++)
            //    {
            //        RenderableObject loaded;
            //        loaded.transform = glm::translate(Mat44f{ 1.f }, Vec3f{ x * 20.f, 0.f, y * 20.f });
            //        loaded.mesh = &mesh;
            //        loaded.material = material;
            //        m_renderables.emplace_back(loaded);
            //    }
            //}
        }
        for (Mesh& mesh : m_meshes)
        {
            for (s32 x = 0; x < 1; x++)
            {
                for (s32 y = 0; y < 1; y++)
                {
                    RenderableObject tri;
                    const Mat44f translation = glm::translate(Mat44f{ 1.f }, Vec3f{ x * 30.f, 0.f, y * 30.f });
                    const Mat44f scale = glm::scale(Mat44f{ 1.f }, Vec3f{ 0.01f, 0.01f, 0.01f });
                    tri.transform = translation * scale;
                    tri.mesh = &mesh;
                    tri.material = material;
                    m_renderables.emplace_back(tri);
                }
            }
        }

        return EResult::OK;
    }

    void
    Renderer::DestroyScene()
    {
        m_renderables.clear();
    }

    EResult
    Renderer::InitImGui()
    {
        // see https://github.com/ocornut/imgui/blob/master/examples/example_glfw_vulkan/main.cpp
        
        if (m_context.device == VK_NULL_HANDLE)
            return MakeResult(EResult::FAIL, "Vulkan Renderer not initialized");

        const ::VkDescriptorPoolSize descPoolSizes[] =
        {
            { ::VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
            { ::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
            { ::VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
            { ::VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
            { ::VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
            { ::VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
            { ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
            { ::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
            { ::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
            { ::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
            { ::VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
        };
        const ::VkDescriptorPoolCreateInfo descPoolInfo{
            Vulkan::DescriptorPoolCreateInfo(1000, descPoolSizes)
        };
        ::VkResult vkresult = ::vkCreateDescriptorPool(m_context.device, &descPoolInfo, nullptr, &m_imguiDescPool);
        if (vkresult != ::VK_SUCCESS)
            return MakeResult(EResult::FAIL, "vkCreateDescriptorPool failed");

        ::ImGui_ImplVulkan_InitInfo imguiInitInfo{};
        imguiInitInfo.Instance = m_context.instance;
        imguiInitInfo.PhysicalDevice = m_context.physicalDevice;
        imguiInitInfo.Device = m_context.device;
        imguiInitInfo.QueueFamily = m_context.graphicsQueue.familyIndex;
        imguiInitInfo.Queue = m_context.graphicsQueue.queue;
        imguiInitInfo.PipelineCache = VK_NULL_HANDLE;
        imguiInitInfo.DescriptorPool = m_imguiDescPool;
        imguiInitInfo.Subpass = 0;
        imguiInitInfo.MinImageCount = c_concurrentFrameCount;
        imguiInitInfo.ImageCount = c_concurrentFrameCount;
        imguiInitInfo.MSAASamples = ::VK_SAMPLE_COUNT_1_BIT;
        imguiInitInfo.Allocator = VK_NULL_HANDLE;
        imguiInitInfo.CheckVkResultFn = VK_NULL_HANDLE;
        ::ImGui_ImplVulkan_Init(&imguiInitInfo, m_defaultRenderPass);

        constexpr u64 c_oneSecTimeout{ 1000000000 };
        const u32 currentFrameId = m_frameNumber % c_concurrentFrameCount;
        const FrameData& currentFrameData = m_frames[currentFrameId];
        const ::VkCommandBuffer cmd{ currentFrameData.commandBuffer };
        const ::VkFence renderFence{ currentFrameData.renderFence };

        ::vkWaitForFences(m_context.device, 1, &renderFence, true, c_oneSecTimeout);
        ::vkResetFences(m_context.device, 1, &renderFence);

        const ::VkCommandBufferBeginInfo cmdBeginInfo{
            Vulkan::CommandBufferBeginInfo(::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT)
        };
        ::vkBeginCommandBuffer(cmd, &cmdBeginInfo);
        ::ImGui_ImplVulkan_CreateFontsTexture(cmd);
        ::vkEndCommandBuffer(cmd);
        const ::VkSubmitInfo submit{ Vulkan::SubmitInfo({}, {}, { &cmd, 1 }, {}) };
        ::vkQueueSubmit(m_context.graphicsQueue.queue, 1, &submit, renderFence);
        ::vkWaitForFences(m_context.device, 1, &renderFence, true, c_oneSecTimeout);
        ::vkResetFences(m_context.device, 1, &renderFence);
        ::vkResetCommandPool(m_context.device, currentFrameData.commandPool, 0);

        ::ImGui_ImplVulkan_DestroyFontUploadObjects();

        m_isImGuiInitialized = true;
        return EResult::OK;
    }

    void
    Renderer::DestroyImGui()
    {
        if (m_context.device != VK_NULL_HANDLE)
        {
            const ::VkResult vkresult = ::vkDeviceWaitIdle(m_context.device);
            DJO_ASSERT(vkresult == ::VK_SUCCESS); DJO_UNUSED_VAR(vkresult);
        }

        ::ImGui_ImplVulkan_Shutdown();

        if (m_imguiDescPool != VK_NULL_HANDLE)
        {
            ::vkDestroyDescriptorPool(m_context.device, m_imguiDescPool, nullptr);
            m_imguiDescPool = VK_NULL_HANDLE;
        }
    }

    void
    Renderer::RenderImGui(const FrameData& _currentFrameData)
    {
        const ImDrawData* const drawData = ::ImGui::GetDrawData();
        if (drawData != nullptr)
        {
            ::ImGui_ImplVulkan_NewFrame();
            ::ImGui_ImplVulkan_RenderDrawData(drawData, _currentFrameData.commandBuffer);
        }
    }

    Material*
    Renderer::CreateMaterial(const std::string& _name, const ::VkPipeline _pipeline, const ::VkPipelineLayout _layout)
    {
        Material mat{};
        mat.pipeline = _pipeline;
        mat.pipelineLayout = _layout;
        const auto [it, inserted] = m_materials.emplace(_name, mat);
        return inserted ? &it->second : nullptr;
    }

    Material*
    Renderer::GetMaterial(const std::string& _name)
    {
        const auto it = m_materials.find(_name);
        if (it == m_materials.end())
            return nullptr;

        return &it->second;
    }

    void
    Renderer::RenderObjects(const FrameData& _currentFrameData, const MemView<const RenderableObject>& _objects)
    {
        const u32 currentFrameId = m_frameNumber % c_concurrentFrameCount;
        const ::VkCommandBuffer cmd{ _currentFrameData.commandBuffer };
        const ::VkDescriptorSet globalDescSet{ _currentFrameData.globalDescSet };
        const ::VkDescriptorSet objectsDescSet{ _currentFrameData.objectsDescSet };
        const u32 sceneDataDynamicOffset = (u32)GetAlignedBufferSize(sizeof(GPUSceneData)) * currentFrameId;
        const u32 objectCount = (u32)_objects.count;

        GPUObjectsData* objectsData;
        ::vmaMapMemory(m_context.allocator, _currentFrameData.objectsBuffer.alloc, (void**)&objectsData);
        for (u32 i = 0; i < objectCount; ++i)
        {
            const RenderableObject& o = _objects[i];
            objectsData[i].transform = o.transform;
        }
        ::vmaUnmapMemory(m_context.allocator, _currentFrameData.objectsBuffer.alloc);

        Mesh* currentMesh = nullptr;
        Material* currentMat = nullptr;
        for (u32 i = 0; i < objectCount; ++i)
        {
            const RenderableObject& o = _objects[i];

            if (o.material != currentMat)
            {
                currentMat = o.material;
                const VkPipelineBindPoint bindPoint{ ::VK_PIPELINE_BIND_POINT_GRAPHICS };
                ::vkCmdBindPipeline(cmd, bindPoint, currentMat->pipeline);
                ::vkCmdBindDescriptorSets(cmd, bindPoint, currentMat->pipelineLayout, 0, 1, &globalDescSet, 1, &sceneDataDynamicOffset);
                ::vkCmdBindDescriptorSets(cmd, bindPoint, currentMat->pipelineLayout, 1, 1, &objectsDescSet, 0, nullptr);
            }

            //MeshPushConstants pushConstants{};
            //pushConstants.model = o.transform;
            //::vkCmdPushConstants(cmd, currentMat->pipelineLayout, ::VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(MeshPushConstants), &pushConstants);

            if (o.mesh != currentMesh)
            {
                currentMesh = o.mesh;
                const ::VkDeviceSize offset = 0;
                ::vkCmdBindVertexBuffers(cmd, 0, 1, &currentMesh->vkBuffer.buffer, &offset);
            }

            ::vkCmdDraw(cmd, (u32)currentMesh->vertices.size(), 1, 0, i);
        }
    }
}
