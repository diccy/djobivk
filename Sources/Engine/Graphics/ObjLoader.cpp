
#include <Graphics/ObjLoader.hpp>

#include <Core/GlobalLog.hpp>
#include <System/Timer.hpp>
#include <System/LocalFile.hpp>

#include <glm/geometric.hpp>

#include <unordered_map>
#include <unordered_set>


// obj specs http://www.martinreddy.net/gfx/3d/OBJ.spec
// mtl specs http://paulbourke.net/dataformats/mtl/

namespace Djo
{
    namespace _Internal
    {
        EResult
        OpenReadAndClose(IFile& _file, std::string* const _outContent)
        {
            EResult result = _file.Open(EValue(EOpenModeFlag::Read));
            if (result != EResult::OK)
            {
                return result;
            }
            result = _file.Read(_outContent);
            if (result != EResult::OK)
            {
                return result;
            }
            _file.Close();
            return EResult::OK;
        }

        constexpr bool IsEol(const char c)               { return c == '\n' || c == '\r'; }
        constexpr bool IsStrictWhiteSpace(const char c)  { return c == ' ' || c == '\t'; }
        constexpr bool IsWhiteSpace(const char c)        { return IsStrictWhiteSpace(c) || IsEol(c); }

        bool
        GetNextLine(const char* const _itStart, const char* const _itEnd, StringView* const _outLine, const char** const _outItEol = nullptr)
        {
            // shrink left whitespaces
            const char* itStart = _itStart;
            while (itStart != _itEnd && IsWhiteSpace(*itStart))
            {
                ++itStart;
            }
            if (itStart == _itEnd)
            {
                return false;
            }
            // advance
            const char* itEol = itStart;
            while (itEol != _itEnd && !IsEol(*itEol))
            {
                ++itEol;
            }
            if (_outItEol)
            {
                *_outItEol = itEol;
            }
            // shrink right whitespaces
            while (IsWhiteSpace(*itEol))
            {
                --itEol;
            }
            ++itEol; // itEol must land on a whitespace
            *_outLine = StringView{ itStart, (sz)std::distance(itStart, itEol) };
            return true;
        }

        bool
        GetNextWord(const char* const _itStart, const char* const _itEnd, StringView* const _outWord, const char** const _outItEow = nullptr)
        {
            // shrink left whitespaces
            const char* itStart = _itStart;
            while (itStart != _itEnd && IsWhiteSpace(*itStart))
            {
                ++itStart;
            }
            if (itStart == _itEnd)
            {
                return false;
            }
            // advance
            const char* itEow = itStart;
            while (itEow != _itEnd && !IsWhiteSpace(*itEow))
            {
                ++itEow;
            }
            if (itEow == itStart)
            {
                return false;
            }
            if (_outItEow)
            {
                *_outItEow = itEow;
            }
            *_outWord = StringView{ itStart, (sz)std::distance(itStart, itEow) };
            return true;
        }

        bool
        GetLastWord(const char* const _itStart, const char* const _itEnd, StringView* const _outWord, const char** const _outItEow = nullptr)
        {
            // shrink left whitespaces
            const char* itEow = _itEnd - 1;
            while (itEow != _itStart && IsWhiteSpace(*itEow))
            {
                --itEow;
            }
            ++itEow;
            if (itEow == _itStart)
            {
                return false;
            }
            // advance
            const char* itStart = itEow - 1;
            while (itStart != _itStart && !IsWhiteSpace(*itStart))
            {
                --itStart;
            }
            if (itStart == itEow)
            {
                return false;
            }
            if (_outItEow)
            {
                *_outItEow = _itEnd;
            }
            *_outWord = StringView{ itStart, (sz)std::distance(itStart, itEow) };
            return true;
        }

        using id3mask = u8;
        static constexpr id3mask c_id3flag_p = Flag<id3mask>(0);
        static constexpr id3mask c_id3flag_t = Flag<id3mask>(1);
        static constexpr id3mask c_id3flag_n = Flag<id3mask>(2);
        struct ReadId3
        {
            s32 p, t, n;
        };
        struct id3
        {
            u32 p, t, n;
        };

        struct Group
        {
            std::string name;
            std::vector<id3> indices;
            u32 materialId;
            id3mask id3mask;
        };

        using MaterialNameIdMap = std::unordered_map<sz, u32>;
        struct RawContent
        {
            std::vector<f32> positions; //  v x y z
            std::vector<f32> normals;   // vn x y z
            std::vector<f32> texcoords; // vt x y
            std::vector<Group> groups;
            MaterialNameIdMap materialNameIdMap;
            std::vector<ObjLoader::Material> materials;
        };

        void
        ReadOptionalFloats(const char* _it, const char* const _itEnd, f32* const _floats, const u32 _n)
        {
            // Reads [1, _n] float (and duplicate first if no entry to fill _n)
            char* q;
            const f32 f0 = std::strtof(_it, &q); // first is mandatory
            _floats[0] = f0;
            _it = q;
            u32 i = 1;
            for (; i < _n; ++i)
            {
                _floats[i] = std::strtof(_it, &q);
                if (q > _itEnd || q == _it)
                {
                    break;
                }
                _it = q;
            }
            for (; i < _n; ++i)
            {
                _floats[i] = f0;
            }
        }
        void
        ReadFloats(const char* _it, const char* const _itEnd, f32* const _floats, const u32 _n)
        {
            // Reads _n floats
            char* q;
            for (u32 i = 0; i < _n; ++i)
            {
                _floats[i] = std::strtof(_it, &q);
                DJO_ASSERT(q <= _itEnd && q != _it);
                _it = q;
            }
            (void)_itEnd;
        }
        void
        ReadFloats(const char* _it, const char* const _itEnd, std::vector<f32>& _floats, const u32 _n)
        {
            // Reads _n floats
            const u32 i = (u32)_floats.size();
            _floats.resize(_floats.size() + _n);
            ReadFloats(_it, _itEnd, &_floats[i], _n);
        }

        id3mask
        ReadIndices3(const char* _it, const char* const _itEnd, ReadId3* const _outId3)
        {
            id3mask mask = 0;
            char* q;
            _outId3->p = std::strtol(_it, &q, 10);
            mask |= c_id3flag_p;
            DJO_ASSERT(_outId3->p != 0);
            DJO_ASSERT(q != _it);
            DJO_ASSERT(q <= _itEnd);
            if (*q == '/')
            {
                ++q;
                if (*q != '/')
                {
                    _it = q;
                    _outId3->t = std::strtol(_it, &q, 10);
                    mask |= c_id3flag_t;
                    DJO_ASSERT(_outId3->t != 0);
                    DJO_ASSERT(q != _it);
                    DJO_ASSERT(q <= _itEnd);
                }
                else
                {
                    _outId3->t = 0;
                }
                if (*q == '/')
                {
                    _it = ++q;
                    _outId3->n = std::strtol(_it, &q, 10);
                    mask |= c_id3flag_n;
                    DJO_ASSERT(_outId3->n != 0);
                    DJO_ASSERT(q != _it);
                    DJO_ASSERT(q <= _itEnd);
                }
                else
                {
                    _outId3->n = 0;
                }
            }
            else
            {
                _outId3->t = 0;
                _outId3->n = 0;
            }
            (void)_itEnd;
            return mask;
        }
        void
        ReadIndices(const char* _it, const char* const _itEnd, std::vector<ReadId3>* const _id3s, id3mask* const _outMask)
        {
            StringView id3Str;
            ReadId3 id;
            id3mask mask = 0;
            _id3s->clear();
            while (GetNextWord(_it, _itEnd, &id3Str, &_it))
            {
                const id3mask newMask = ReadIndices3(&id3Str[0], id3Str.GetEnd(), &id);
                if (mask == 0)
                {
                    mask = newMask;
                }
                else if (mask != newMask || newMask == 0)
                {
                    // invalid indices
                    DJO_ASSERT(false);
                    _id3s->clear();
                    *_outMask = 0;
                    break;
                }
                _id3s->push_back(id);
            }
            *_outMask = mask;
        }

        EResult
        GetMtlContent(const FSPath& _mltFsp, const std::string& _mtlContent, MaterialNameIdMap& _materialNameIdMap, std::vector<ObjLoader::Material>& _materials)
        {
            StringView line;
            const char* mtlContentIt = &*_mtlContent.begin();
            const char* const mtlContentItEnd = mtlContentIt + _mtlContent.size();
            ObjLoader::Material* currentMaterial{ nullptr };
            while (_Internal::GetNextLine(mtlContentIt, mtlContentItEnd, &line, &mtlContentIt))
            {
                switch (line[0])
                {
                    case 'K':
                    {
                        if (currentMaterial != nullptr)
                        {
                            if (line[1] == 'a' && _Internal::IsStrictWhiteSpace(line[2]))
                            {
                                // Ka r
                                // Ka r g b (other Ka entries not supported here)
                                DJO_ASSERT(!TestFlag(currentMaterial->mask, ObjLoader::Material::c_flag_ka));
                                ReadOptionalFloats(&line[3], line.GetEnd(), reinterpret_cast<f32*>(&currentMaterial->ka), 3);
                                currentMaterial->mask |= ObjLoader::Material::c_flag_ka;
                            }
                            else if (line[1] == 'd' && _Internal::IsStrictWhiteSpace(line[2]))
                            {
                                // Kd r
                                // Kd r g b (other Kd entries not supported here)
                                DJO_ASSERT(!TestFlag(currentMaterial->mask, ObjLoader::Material::c_flag_kd));
                                ReadOptionalFloats(&line[3], line.GetEnd(), reinterpret_cast<f32*>(&currentMaterial->kd), 3);
                                currentMaterial->mask |= ObjLoader::Material::c_flag_kd;
                            }
                            else if (line[1] == 's' && _Internal::IsStrictWhiteSpace(line[2]))
                            {
                                // Ks r
                                // Ks r g b (other Ks entries not supported here)
                                DJO_ASSERT(!TestFlag(currentMaterial->mask, ObjLoader::Material::c_flag_ks));
                                ReadOptionalFloats(&line[3], line.GetEnd(), reinterpret_cast<f32*>(&currentMaterial->ks), 3);
                                currentMaterial->mask |= ObjLoader::Material::c_flag_ks;
                            }
                            else
                            {
                                // unsupported keyword
                                DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                                DJO_LOG_WARN << line << std::endl;
                                //DJO_ASSERT(false);
                            }
                        }
                        else
                        {
                            // no current material
                            DJO_LOG_ERROR << "No current material" << std::endl;
                            DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'N':
                    {
                        if (currentMaterial != nullptr)
                        {
                            if (line[1] == 's' && _Internal::IsStrictWhiteSpace(line[2]))
                            {
                                // Ns n
                                DJO_ASSERT(!TestFlag(currentMaterial->mask, ObjLoader::Material::c_flag_ns));
                                ReadFloats(&line[3], line.GetEnd(), reinterpret_cast<f32*>(&currentMaterial->ns), 1);
                                currentMaterial->mask |= ObjLoader::Material::c_flag_ns;
                            }
                        }
                        else
                        {
                            // no current material
                            DJO_LOG_ERROR << "No current material" << std::endl;
                            DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'd':
                    {
                        if (currentMaterial != nullptr)
                        {
                            if (_Internal::IsStrictWhiteSpace(line[1]))
                            {
                                // d factor
                                // d -halo factor (option not supported and ignored here)
                                StringView factor;
                                GetLastWord(&line[2], line.GetEnd(), &factor);
                                ReadFloats(&factor[0], factor.GetEnd(), reinterpret_cast<f32*>(&currentMaterial->d), 1);
                            }
                        }
                        else
                        {
                            // no current material
                            DJO_LOG_ERROR << "No current material" << std::endl;
                            DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'm':
                    {
                        if (currentMaterial != nullptr)
                        {
                            if (line[1] == 'a' && line[2] == 'p' && line[3] == '_')
                            {
                                switch (line[4])
                                {
                                    case 'K':
                                    {
                                        if (line[5] == 'a' && _Internal::IsStrictWhiteSpace(line[6]))
                                        {
                                            // map_Ka -options args filename (options not supported and ignored here)
                                            StringView fileName;
                                            GetLastWord(&line[7], line.GetEnd(), &fileName);
                                            currentMaterial->ka_texName = fileName;
                                            currentMaterial->useTextures = true;
                                        }
                                        else if (line[5] == 'd' && _Internal::IsStrictWhiteSpace(line[6]))
                                        {
                                            // map_Kd -options args filename (options not supported and ignored here)
                                            StringView fileName;
                                            GetLastWord(&line[7], line.GetEnd(), &fileName);
                                            currentMaterial->kd_texName = fileName;
                                            currentMaterial->useTextures = true;
                                        }
                                        else if (line[5] == 's' && _Internal::IsStrictWhiteSpace(line[6]))
                                        {
                                            // map_Ks -options args filename (options not supported and ignored here)
                                            StringView fileName;
                                            GetLastWord(&line[7], line.GetEnd(), &fileName);
                                            currentMaterial->ks_texName = fileName;
                                            currentMaterial->useTextures = true;
                                        }
                                        else
                                        {
                                            // unsupported keyword
                                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                                            DJO_LOG_WARN << line << std::endl;
                                            //DJO_ASSERT(false);
                                        }
                                        break;
                                    }
                                    case 'N':
                                    {
                                        if (line[5] == 's' && _Internal::IsStrictWhiteSpace(line[6]))
                                        {
                                            // map_Ns -options args filename (options not supported and ignored here)
                                            StringView fileName;
                                            GetLastWord(&line[7], line.GetEnd(), &fileName);
                                            currentMaterial->ns_texName = fileName;
                                            currentMaterial->useTextures = true;
                                        }
                                        break;
                                    }
                                    case 'd':
                                    {
                                        if (_Internal::IsStrictWhiteSpace(line[5]))
                                        {
                                            // map_d -options args filename (options not supported and ignored here)
                                            StringView fileName;
                                            GetLastWord(&line[6], line.GetEnd(), &fileName);
                                            currentMaterial->d_texName = fileName;
                                            currentMaterial->useTextures = true;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // no current material
                            DJO_LOG_ERROR << "No current material" << std::endl;
                            DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'n':
                    {
                        if (line[1] == 'e' && line[2] == 'w' && line[3] == 'm' && line[4] == 't' && line[5] == 'l' && _Internal::IsStrictWhiteSpace(line[6]))
                        {
                            // newmtl name (material name cannot include blank)
                            StringView matName;
                            GetNextWord(&line[7], line.GetEnd(), &matName);
                            if (matName.count > 0)
                            {
                                const sz matNameHash = Hasher<StringView>{}(matName);
                                if (_materialNameIdMap.find(matNameHash) == _materialNameIdMap.end())
                                {
                                    ObjLoader::Material newMat{};
                                    newMat.sourceFsp = _mltFsp;
                                    newMat.name = matName;
                                    newMat.mask = 0;
                                    newMat.d = 1.f; // always set, default to 1.0
                                    newMat.useTextures = false;
                                    _materials.push_back(std::move(newMat));
                                    _materialNameIdMap.emplace(matNameHash, (u32)_materials.size() - 1);
                                    currentMaterial = &_materials.back();
                                }
                                else
                                {
                                    // existing material overriden
                                    DJO_LOG_ERROR << "Existing material overriden:\n";
                                    DJO_LOG_ERROR << line << std::endl;
                                    DJO_ASSERT(false);
                                    currentMaterial = nullptr;
                                }
                            }
                            else
                            {
                                // invalid name
                                DJO_LOG_ERROR << "Invalid material name:\n";
                                DJO_LOG_ERROR << line << std::endl;
                                DJO_ASSERT(false);
                                currentMaterial = nullptr;
                            }
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'b':
                    {
                        if (currentMaterial != nullptr)
                        {
                            if (line[1] == 'u' && line[2] == 'm' && line[3] == 'p' && _Internal::IsStrictWhiteSpace(line[4]))
                            {
                                // bump -options args filename (options not supported and ignored here)
                                StringView fileName;
                                GetLastWord(&line[7], line.GetEnd(), &fileName);
                                currentMaterial->bump_texName = fileName;
                            }
                        }
                        else
                        {
                            // no current material
                            DJO_LOG_ERROR << "No current material" << std::endl;
                            DJO_ASSERT(false);
                        }
                        break;
                    }
                    case '#':
                    {
                        // Comment
                        break;
                    }
                    default:
                    {
                        // unsupported keyword
                        DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                        DJO_LOG_WARN << line << std::endl;
                        //DJO_ASSERT(false);
                        break;
                    }
                }
            }
            return EResult::OK;
        }

        EResult
        GetRawContent(const FSPath& _objFsp, const std::string& _objContent, RawContent* _outRawContent)
        {
            RawContent rawContent;

            std::unordered_set<sz> mtllibs;
            u32 lastMaterialId = c_invalidUint<u32>;

            std::vector<ReadId3> currentReadIndices;
            std::vector<id3> currentIndices;
            currentReadIndices.reserve(10);
            currentIndices.reserve(10);

            StringView line;
            const char* objContentIt = &*_objContent.begin();
            const char* const objContentItEnd = objContentIt + _objContent.size();
            while (_Internal::GetNextLine(objContentIt, objContentItEnd, &line, &objContentIt))
            {
                switch (line[0])
                {
                    case 'v':
                    {
                        if (_Internal::IsStrictWhiteSpace(line[1]))
                        {
                            // v x y z
                            ReadFloats(&line[2], line.GetEnd(), rawContent.positions, 3);
                        }
                        else if (line[1] == 'n' && _Internal::IsStrictWhiteSpace(line[2]))
                        {
                            // vn x y z
                            ReadFloats(&line[3], line.GetEnd(), rawContent.normals, 3);
                        }
                        else if (line[1] == 't' && _Internal::IsStrictWhiteSpace(line[2]))
                        {
                            // vt u v w (ignored w here)
                            ReadFloats(&line[3], line.GetEnd(), rawContent.texcoords, 2);
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'f':
                    {
                        if (_Internal::IsStrictWhiteSpace(line[1]))
                        {
                            // f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3 ...
                            if (rawContent.groups.size() > 0)
                            {
                                id3mask mask;
                                ReadIndices(&line[2], line.GetEnd(), &currentReadIndices, &mask);
                                if (currentReadIndices.size() >= 3 && TestFlag(mask, c_id3flag_p))
                                {
                                    if (rawContent.groups.back().id3mask == 0)
                                    {
                                        rawContent.groups.back().id3mask = mask;
                                    }
                                    bool validIndices = (mask == rawContent.groups.back().id3mask);
                                    if (validIndices)
                                    {
                                        const s32 rawPosCount = (s32)rawContent.positions.size();
                                        const s32 rawTexCoordCount = (s32)rawContent.texcoords.size();
                                        const s32 rawNormCount = (s32)rawContent.normals.size();
                                        for (const ReadId3& readId : currentReadIndices)
                                        {
                                            validIndices = (readId.p != 0 && std::abs(readId.p) < rawPosCount)
                                                && (readId.t == 0 || std::abs(readId.t) < rawTexCoordCount)
                                                && (readId.n == 0 || std::abs(readId.n) < rawNormCount);
                                            if (!validIndices)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    if (validIndices)
                                    {
                                        // convert signed indices to unsigned
                                        // set first index to 0
                                        currentIndices.clear();
                                        for (const ReadId3& readId : currentReadIndices)
                                        {
                                            id3 i3{ c_invalidUint<u32>, c_invalidUint<u32>, c_invalidUint<u32> };
                                            i3.p = (readId.p > 0) ? readId.p - 1 : (u32)(rawContent.positions.size() / 3) + readId.p;
                                            if (readId.t != 0)
                                            {
                                                i3.t = (readId.t > 0) ? readId.t - 1 : (u32)(rawContent.texcoords.size() / 2) + readId.t;
                                            }
                                            if (readId.n != 0)
                                            {
                                                i3.n = (readId.n > 0) ? readId.n - 1 : (u32)rawContent.normals.size() / 3 + readId.n;
                                            }
                                            currentIndices.push_back(i3);
                                        }
                                        DJO_ASSERT(currentIndices.size() == currentReadIndices.size());
                                        if (currentIndices.size() > 3)
                                        {
                                            DJO_TODO("ObjLoader : robust triangulation");

                                            // Assume polygon is convex and vertices are coplanar
                                            // https://en.wikipedia.org/wiki/Fan_triangulation
                                            const u32 verticesCount = (u32)(currentIndices.size());
                                            currentIndices.resize((verticesCount - 2) * 3);
                                            currentIndices.back() = currentIndices[verticesCount - 1];
                                            for (u32 i = verticesCount - 2; i >= 2; --i)
                                            {
                                                const u32 j = ((i - 2) * 3) + 2;
                                                currentIndices[j] = currentIndices[i];
                                                currentIndices[j + 2] = currentIndices[i];
                                            }
                                            const u32 indicesCount = (u32)currentIndices.size();
                                            const id3 firstId = currentIndices[0];
                                            for (u32 i = 3; i < indicesCount; i += 3)
                                            {
                                                currentIndices[i] = firstId;
                                            }
                                        }
                                        std::vector<id3>& groupIndices = rawContent.groups.back().indices;
                                        groupIndices.insert(groupIndices.end(), currentIndices.begin(), currentIndices.end());
                                    }
                                    else
                                    {
                                        // invalid indices
                                        DJO_LOG_ERROR << "Invalid indices:\n";
                                        DJO_LOG_ERROR << line << std::endl;
                                        DJO_ASSERT(false);
                                    }
                                }
                                else
                                {
                                    // invalid face
                                    DJO_LOG_ERROR << "Invalid face:\n";
                                    DJO_LOG_ERROR << line << std::endl;
                                    DJO_ASSERT(false);
                                }
                            }
                            else
                            {
                                // no current group
                                // Is it mandatory? Use a default group in this case?
                                DJO_LOG_ERROR << "No current group:\n";
                                DJO_LOG_ERROR << line << std::endl;
                                DJO_ASSERT(false);
                            }
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'm':
                    {
                        if (line[1] == 't' && line[2] == 'l' && line[3] == 'l' && line[4] == 'i' && line[5] == 'b' && _Internal::IsStrictWhiteSpace(line[6]))
                        {
                            // mtllib filename1 filename2 ... (does not support multiple names here)
                            StringView fileName;
                            GetNextWord(&line[7], line.GetEnd(), &fileName);
                            if (fileName.count >= 5)
                            {
                                const sz fileNameHash = Hasher<StringView>{}(fileName);
                                if (mtllibs.count(fileNameHash) == 0)
                                {
                                    FSPath mtlFsp{ _objFsp };
                                    mtlFsp.GetPath().ReplaceLast(fileName);
                                    UPtr<IFile> mtlFile{ mtlFsp.NewFile() };
                                    std::string mtlFileContent;
                                    EResult result = _Internal::OpenReadAndClose(*mtlFile, &mtlFileContent);
                                    if (result != EResult::OK)
                                    {
                                        // can't open file
                                        DJO_LOG_ERROR << "Can't open file:\n";
                                        DJO_LOG_ERROR << line << std::endl;
                                        DJO_ASSERT(false);
                                    }
                                    else
                                    {
                                        result = _Internal::GetMtlContent(mtlFsp, mtlFileContent, rawContent.materialNameIdMap, rawContent.materials);
                                        if (result != EResult::OK)
                                        {
                                            // can't parse file
                                            DJO_LOG_ERROR << "Can't parse file:\n";
                                            DJO_LOG_ERROR << line << std::endl;
                                            DJO_ASSERT(false);
                                        }
                                    }
                                    mtllibs.insert(fileNameHash);
                                }
                                else
                                {
                                    // already imported mtllib
                                    DJO_LOG_ERROR << "Already imported mtllib:\n";
                                    DJO_LOG_ERROR << line << std::endl;
                                    //DJO_ASSERT(false);
                                }
                            }
                            else
                            {
                                // invalid filename
                                DJO_LOG_ERROR << "Invalid filename:\n";
                                DJO_LOG_ERROR << line << std::endl;
                                DJO_ASSERT(false);
                            }
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'u':
                    {
                        if (line[1] == 's' && line[2] == 'e' && line[3] == 'm' && line[4] == 't' && line[5] == 'l' && _Internal::IsStrictWhiteSpace(line[6]))
                        {
                            // usemtl matname
                            StringView matName;
                            GetNextWord(&line[7], line.GetEnd(), &matName);
                            if (matName.count > 0)
                            {
                                const sz matNameHash = Hasher<StringView>{}(matName);
                                const MaterialNameIdMap::const_iterator it = rawContent.materialNameIdMap.find(matNameHash);
                                if (it != rawContent.materialNameIdMap.end())
                                {
                                    if (rawContent.groups.size() > 0)
                                    {
                                        if (rawContent.groups.back().indices.empty())
                                        {
                                            rawContent.groups.back().materialId = it->second;
                                        }
                                        else
                                        {
                                            // Manage "multi materials" by splitting object
                                            Group group{};
                                            group.name = rawContent.groups.back().name;
                                            group.materialId = it->second;
                                            group.id3mask = 0;
                                            rawContent.groups.push_back(std::move(group));
                                        }
                                    }
                                    lastMaterialId = it->second;
                                }
                                else
                                {
                                    // unknown material
                                    DJO_LOG_ERROR << "Unknow material:\n";
                                    DJO_LOG_ERROR << line << std::endl;
                                    DJO_ASSERT(false);
                                }
                            }
                            else
                            {
                                // invalid matname
                                DJO_LOG_ERROR << "Invalid material name:\n";
                                DJO_LOG_ERROR << line << std::endl;
                                DJO_ASSERT(false);
                            }
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 'g': // group
                    case 'o': // object
                    {
                        // Groups and objetcs are treated the same simple way
                        if (_Internal::IsStrictWhiteSpace(line[1]))
                        {
                            // g groupname1 groupname2 ... (does not support multiple names here)
                            // o objectname
                            StringView groupName;
                            GetNextWord(&line[2], line.GetEnd(), &groupName);
                            if (groupName.count > 0)
                            {
                                Group group{};
                                group.name = groupName;
                                group.materialId = lastMaterialId;
                                group.id3mask = 0;
                                rawContent.groups.push_back(std::move(group));
                            }
                            else
                            {
                                // invalid groupname
                                DJO_LOG_ERROR << "Invalid group/object name:\n";
                                DJO_LOG_ERROR << line << std::endl;
                                DJO_ASSERT(false);
                            }
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case 's':
                    {
                        if (_Internal::IsStrictWhiteSpace(line[1]))
                        {
                            // s group_number
                            DJO_TODO("ObjLoader : smoothing groups");
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        else
                        {
                            // unsupported keyword
                            DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                            DJO_LOG_WARN << line << std::endl;
                            //DJO_ASSERT(false);
                        }
                        break;
                    }
                    case '#':
                    {
                        // Comment
                        break;
                    }
                    default:
                    {
                        // unsupported keyword
                        DJO_LOG_WARN << "Unsupported keyword (ignored line):\n";
                        DJO_LOG_WARN << line << std::endl;
                        //DJO_ASSERT(false);
                        break;
                    }
                }
            }

            *_outRawContent = std::move(rawContent);
            return EResult::OK;
        }

        /*
        // Calculate normals
        // https://github.com/microsoft/DirectXMesh/blob/main/DirectXMesh/DirectXMeshNormals.cpp
        using CWOrder = HardAlias<bool>;
        EResult
        ComputeNormals(const MemView<const f32>& _vertices, const MemView<const u32>& _indices, const MemView<f32>& _normals, const CWOrder _cw = CWOrder{ true })
        {
            DJO_STATIC_ASSERT(sizeof(Vec3f) == sizeof(f32) * 3);

            if (_indices.count % 3 != 0 || _vertices.count % 3 != 0 || _normals.count != _vertices.count)
            {
                return EResult::FAIL;
            }

            const u32* indices = _indices.data;
            const u32 faceCount = (u32)_indices.count / 3;

            const Vec3f* const vertices = reinterpret_cast<const Vec3f*>(_vertices.data);
            const u32 vertexCount = (u32)_vertices.count / 3;

            ::memset(_normals.data, 0, _normals.Size());
            Vec3f* const normals = reinterpret_cast<Vec3f*>(_normals.data);

            for (u32 f = 0; f < faceCount; ++f)
            {
                const u32 i0 = indices[f * 3];
                const u32 i1 = indices[f * 3 + 1];
                const u32 i2 = indices[f * 3 + 2];

                DJO_ASSERT(i0 < vertexCount&& i1 < vertexCount&& i2 < vertexCount);
                const Vec3f& v0 = vertices[i0];
                const Vec3f& v1 = vertices[i1];
                const Vec3f& v2 = vertices[i2];

                const Vec3f n = glm::normalize(glm::cross(v2 - v0, v1 - v0));
                normals[i0] += n;
                normals[i1] += n;
                normals[i2] += n;
            }

            if (_cw)
            {
                for (u32 v = 0; v < vertexCount; ++v)
                {
                    normals[v] = glm::normalize(normals[v]);
                }
            }
            else
            {
                for (u32 v = 0; v < vertexCount; ++v)
                {
                    normals[v] = -glm::normalize(normals[v]);
                }
            }

            return EResult::OK;
        }
        */
    }

    ObjLoader::ObjLoader()
        : m_params{}
        , m_materials{}
        , m_meshes{}
    {
    }

    ObjLoader::~ObjLoader()
    {
    }

    EResult
    ObjLoader::Load(const ObjLoader::Params& _param)
    {
        m_params = _param;

        Timer timer;
        timer.Start();

        DJO_LOG_INFO << "ObjLoader::Load " << m_params.fsp.AsPrettyString() << std::endl;

        std::string objFileContent;
        UPtr<IFile> file{ m_params.fsp.NewFile() };
        EResult result = _Internal::OpenReadAndClose(*file, &objFileContent);
        if (result != EResult::OK)
        {
            DJO_LOG_ERROR << "Can't open " << m_params.fsp.AsPrettyString() << std::endl;
            return result;
        }

        _Internal::RawContent rawContent;
        result = _Internal::GetRawContent(m_params.fsp, objFileContent, &rawContent);
        if (result != EResult::OK)
        {
            DJO_LOG_ERROR << "Parsing failed " << m_params.fsp.AsPrettyString() << std::endl;
            return result;
        }

        timer.Stop();
        DJO_LOG_TRACE << "ObjLoader : " << m_params.fsp.GetPath().GetFullPath() << std::endl;
        DJO_LOG_TRACE << "Loading time: " << timer.Get().Ms() << "ms" << std::endl;

        m_materials.clear();
        m_meshes.clear();
        m_materials = std::move(rawContent.materials);
        m_meshes.reserve(rawContent.groups.size());

        // map id3 to local mesh id
        struct EqualId3 { bool operator()(const _Internal::id3& _l, const _Internal::id3& _r) const noexcept { return _l.p == _r.p && _l.t == _r.t && _l.n == _r.n; } };
        std::unordered_map<_Internal::id3, u32, Hasher<_Internal::id3>, EqualId3> idMap;
        for (const _Internal::Group& group : rawContent.groups)
        {
            const u32 indexCount = (u32)group.indices.size();
            if (indexCount > 0)
            {
                Mesh newMesh;
                newMesh.name = group.name;
                newMesh.materialId = group.materialId;
                MeshData& newMeshData = newMesh.data;
                PropertySemanticsArray& layoutSemantics = newMesh.layoutSemantics;

                // Buffer layout
                const _Internal::id3mask id3mask = group.id3mask;
                const bool needComputeNormals = (!TestFlag(id3mask, _Internal::c_id3flag_n) && _param.computeNormals);
                u32 layoutNormalOffset = c_invalidUint<u32>;
                BufferLayout& newBufferLayout = newMeshData.vertexLayout;
                DJO_ASSERT(TestFlag(id3mask, _Internal::c_id3flag_p));
                newBufferLayout.Add({ ELayoutDataType::Float3, false });
                layoutSemantics.PushBack(ELayoutPropertySemantic::Position);
                if (TestFlag(id3mask, _Internal::c_id3flag_n) || needComputeNormals)
                {
                    layoutNormalOffset = newBufferLayout.GetElementCount();
                    newBufferLayout.Add({ ELayoutDataType::Float3, false });
                    layoutSemantics.PushBack(ELayoutPropertySemantic::Normal);
                }
                if (TestFlag(id3mask, _Internal::c_id3flag_t))
                {
                    newBufferLayout.Add({ ELayoutDataType::Float2, false });
                    layoutSemantics.PushBack(ELayoutPropertySemantic::TextureCoord);
                }

                // Map ids to local mesh id
                std::vector<u32>& newMeshDataIndices = newMeshData.indices;
                newMeshDataIndices.reserve(group.indices.size());
                idMap.clear();
                u32 j = 0;
                for (const _Internal::id3& i3 : group.indices)
                {
                    if (idMap.count(i3) == 0)
                    {
                        idMap[i3] = j;
                        newMeshDataIndices.push_back(j);
                        ++j;
                    }
                    else
                    {
                        newMeshDataIndices.push_back(idMap[i3]);
                    }
                }

                // Populate vertex buffer
                const u32 uniqueVertexCount = (u32)idMap.size();
                const u32 vertexLayoutSize = newBufferLayout.GetElementCount();
                std::vector<f32>& newMeshDataVertices = newMeshData.vertices;
                newMeshDataVertices.resize(uniqueVertexCount * vertexLayoutSize, 0); // normal computing requires 0 initialization
                for (const auto& id3ToId : idMap)
                {
                    const _Internal::id3 i3 = id3ToId.first;
                    u32 mId = id3ToId.second * vertexLayoutSize;
                    const u32 pId = i3.p * 3;
                    newMeshDataVertices[mId++] = rawContent.positions[pId + 0];
                    newMeshDataVertices[mId++] = rawContent.positions[pId + 1];
                    newMeshDataVertices[mId++] = rawContent.positions[pId + 2];
                    if (TestFlag(id3mask, _Internal::c_id3flag_n))
                    {
                        const u32 nId = i3.n * 3;
                        newMeshDataVertices[mId++] = rawContent.normals[nId + 0];
                        newMeshDataVertices[mId++] = rawContent.normals[nId + 1];
                        newMeshDataVertices[mId++] = rawContent.normals[nId + 2];
                    }
                    else if (needComputeNormals)
                    {
                        mId += 3;
                    }
                    if (TestFlag(id3mask, _Internal::c_id3flag_t))
                    {
                        const u32 tId = i3.t * 2;
                        newMeshDataVertices[mId++] = rawContent.texcoords[tId + 0];
                        newMeshDataVertices[mId++] = rawContent.texcoords[tId + 1];
                    }
                    DJO_ASSERT(mId == (id3ToId.second * vertexLayoutSize) + vertexLayoutSize);
                }

                // Compute normals if needed
                if (needComputeNormals)
                {
                    DJO_LOG_INFO << "ObjLoader::Load Compute normals for " << group.name << std::endl;

                    DJO_STATIC_ASSERT(sizeof(Vec3f) == sizeof(f32) * 3);
                    DJO_ASSERT(layoutNormalOffset != c_invalidUint<u32>);
                    const u32 faceCount = (u32)newMeshDataIndices.size() / 3;
                    const u32 vertexDataSize = (u32)newMeshDataVertices.size();
                    for (u32 f = 0; f < faceCount; ++f)
                    {
                        const u32 i0 = newMeshDataIndices[f * 3] * vertexLayoutSize;
                        const u32 i1 = newMeshDataIndices[f * 3 + 1] * vertexLayoutSize;
                        const u32 i2 = newMeshDataIndices[f * 3 + 2] * vertexLayoutSize;

                        DJO_ASSERT(i0 < vertexDataSize&& i1 < vertexDataSize&& i2 < vertexDataSize);
                        const Vec3f& v0 = reinterpret_cast<const Vec3f&>(newMeshDataVertices[i0]);
                        const Vec3f& v1 = reinterpret_cast<const Vec3f&>(newMeshDataVertices[i1]);
                        const Vec3f& v2 = reinterpret_cast<const Vec3f&>(newMeshDataVertices[i2]);

                        const Vec3f n = glm::normalize(glm::cross(v2 - v0, v1 - v0));
                        reinterpret_cast<Vec3f&>(newMeshDataVertices[i0 + layoutNormalOffset]) += n;
                        reinterpret_cast<Vec3f&>(newMeshDataVertices[i1 + layoutNormalOffset]) += n;
                        reinterpret_cast<Vec3f&>(newMeshDataVertices[i2 + layoutNormalOffset]) += n;
                    }
                    for (u32 v = 0; v < vertexDataSize; v += vertexLayoutSize)
                    {
                        Vec3f& n = reinterpret_cast<Vec3f&>(newMeshDataVertices[v + layoutNormalOffset]);
                        n = -glm::normalize(n);
                    }
                }

                m_meshes.push_back(std::move(newMesh));
            }
        }

        DJO_LOG_INFO << "ObjLoader::Load loading succeeded " << m_params.fsp.AsPrettyString() << std::endl;

        return EResult::OK;
    }
}
