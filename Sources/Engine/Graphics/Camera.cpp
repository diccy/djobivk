
#include <Graphics/Camera.hpp>


namespace Djo
{
    Camera::Camera()
        : m_desc{ 0.f, 0.f, 0.f, 0.f }
    {
    }

    Camera::~Camera()
    {
    }

    ViewContext
    Camera::BuildViewContext() const
    {
        const Mat44f view{ glm::inverse(GetWorldTransform()) };
        const Mat44f projection{ glm::perspective(m_desc.fov, m_desc.ratio, m_desc.near, m_desc.far) };
        return ViewContext{
            projection * view,
            view,
            projection,
            GetWorldPosition(),
            GetDesc()
        };
    }
}
