
#include <djon/ParsingUtils.hpp>

#include <Core/StringView.hpp>


namespace Djo
{
    bool
    AdvanceWhile(CharPredicate _pred, const char* const _itStart, const char* const _itEnd, const char** const _outIt)
    {
        const char* it = _itStart;
        while (_pred(*it) && it != _itEnd)
            ++it;
        *_outIt = it;
        return (it != _itStart);
    }
    bool
    AdvanceUntil(CharPredicate _pred, const char* const _itStart, const char* const _itEnd, const char** const _outIt)
    {
        const char* it = _itStart;
        while (!_pred(*it) && it != _itEnd)
            ++it;
        *_outIt = it;
        return (it != _itStart);
    }

    bool
    GetNextLine(const char* const _itStart, const char* const _itEnd,
                StringView* const _outLine, const char** const _outItEol /*= nullptr*/)
    {
        // shrink left whitespaces
        const char* itStart;
        AdvanceWhile(IsWhiteSpace, _itStart, _itEnd, &itStart);
        if (itStart == _itEnd)
            return false;

        // advance
        const char* itEol;
        AdvanceUntil(IsEol, itStart, _itEnd, &itEol);
        if (_outItEol)
            *_outItEol = itEol;

        // shrink right whitespaces
        if (itEol != _itEnd)
        {
            while (IsWhiteSpace(*itEol))
                --itEol;
            ++itEol; // itEol must land on a whitespace
        }

        _outLine->data = itStart;
        _outLine->count = (sz)(itEol - itStart);
        return true;
    }
}
