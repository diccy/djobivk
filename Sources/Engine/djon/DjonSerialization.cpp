
#include <djon/DjonSerialization.hpp>


namespace Djo
{
    namespace djon
    {
        // Reader

        Reader::Reader()
            : Srl::Serializer<Reader>{ *this }
            , m_value{ nullptr }
        {
        }

        Reader::Reader(const Value& _value)
            : Srl::Serializer<Reader>{ *this }
            , m_value{ &_value }
        {
        }

        Reader::~Reader()
        {
        }


        // Writer

        namespace _Internal
        {
            static const char c_spaces[]{ "                                                                                                                               " };
        }

        ReadableWriter::ReadableWriter()
            : Srl::Serializer<ReadableWriter>{ *this }
            , m_stream{}
            , m_tab{ 0 }
            , m_tabSize{ c_defaultTabSize }
            , m_attr{ EAttribute::None }
            , m_previouslyWroteName{ false }
            , m_isFirstEntry{ true }
        {
        }

        ReadableWriter::~ReadableWriter()
        {
            DJO_ASSERT(m_tab == 0);
        }

        void
        ReadableWriter::NewLine()
        {
            if (m_attr == EAttribute::Oneline)
            {
                Space();
            }
            else
            {
                m_stream << '\n';
                if (m_tab > 0)
                    m_stream << StringView{ _Internal::c_spaces, m_tab * m_tabSize };
            }
        }

        void
        ReadableWriter::NewLineIfNotFirstEntry()
        {
            if (!m_isFirstEntry) // [[likely]]
                NewLine();

            m_isFirstEntry = false;
        }

        void
        ReadableWriter::BeforeValueSpace()
        {
            if (m_previouslyWroteName)
            {
                Space();
                m_previouslyWroteName = false;
            }
            else
            {
                NewLineIfNotFirstEntry();
            }
        }

        void
        ReadableWriter::BeforeValueNewLine()
        {
            if (m_previouslyWroteName)
            {
                NewLine();
                m_previouslyWroteName = false;
            }
            else
            {
                NewLineIfNotFirstEntry();
            }
        }

        void
        ReadableWriter::Tab(const s32 _tab)
        {
            DJO_ASSERT((s32)m_tab + _tab >= 0);
            m_tab += _tab;
        }

        void
        ReadableWriter::Open(const char _c)
        {
            BeforeValueNewLine();
            m_stream << _c;
            Tab(1);
        }

        void
        ReadableWriter::Close(const char _c)
        {
            Tab(-1);
            NewLineIfNotFirstEntry();
            m_stream << _c;
        }

        void
        ReadableWriter::Clear()
        {
            m_stream.flush();
            m_stream.str({});
            m_nextName = {};
            m_tab = 0;
            m_attr = EAttribute::None;
            m_previouslyWroteName = false;
            m_isFirstEntry = true;
        };
    }
}
