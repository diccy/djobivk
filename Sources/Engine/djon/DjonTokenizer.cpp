
#include <djon/DjonTokenizer.hpp>

#include <djon/ParsingUtils.hpp>
#include <Core/StdString.hpp>
#include <Core/StdVector.hpp>
#include <Core/StructUtils.hpp>

#include <charconv>


namespace Djo
{
    namespace djon
    {
        namespace _Internal
        {
            constexpr
            bool
            IsOfName(const char c)
            {
                return IsAlphaNum(c) || c == '_';
            }

            DJO_INLINE
            bool
            AdvanceWhileWhiteSpaceOrComment(const char* const _itStart, const char* const _itEnd, const char** const _outIt)
            {
                const char* it = _itStart;
                do
                    if (*it == c_comm)
                        AdvanceUntil(IsEol, it, _itEnd, &it);
                while (AdvanceWhile(IsWhiteSpace, it, _itEnd, &it));
                *_outIt = it;
                return (it != _itStart);
            }
        }

        Tokenizer::Tokenizer()
            : m_tokens{}
            , m_elemCount{ 0 }
            , m_valueCount{ 0 }
        {
        }

        Tokenizer::~Tokenizer()
        {
        }

        EResult
        Tokenizer::Tokenize(const StringView& _content, std::string* const _outErrorMsg)
        {
            m_tokens.clear();
            m_tokens.reserve(_content.count / sizeof(Token) / 5); // TODO : benchmark it

            Token token;
            token.type = ETokenType::None;
            token.contentCount = 0;
            m_tokens.push_back(token);

            const char* it = _content.GetStart();
            const char* const itEnd = _content.GetEnd();

            #define DJO_SET_ONE_CHAR_TOKEN(type_)\
                token.type = type_;\
                token.token.data = it;\
                token.token.count = 1;\
                m_tokens.push_back(token)

            #define DJO_SET_CONDITION_TOKEN(type_, cond_)\
                token.type = type_;\
                token.token.data = it;\
                ++it;\
                while (it != itEnd && (cond_))\
                    ++it;\
                token.token.count = (sz)(it - token.token.data);\
                m_tokens.push_back(token)

            for (; it != itEnd; ++it)
            {
                _Internal::AdvanceWhileWhiteSpaceOrComment(it, itEnd, &it);
                if (it == itEnd)
                    break;

                switch (*it)
                {
                    case c_str:        { DJO_SET_CONDITION_TOKEN(ETokenType::Value, (*it != c_str)); break; }
                    case c_arrayStart: { DJO_SET_ONE_CHAR_TOKEN(ETokenType::ArrayStart); break; }
                    case c_arrayEnd:   { DJO_SET_ONE_CHAR_TOKEN(ETokenType::ArrayEnd); break; }
                    case c_objStart:   { DJO_SET_ONE_CHAR_TOKEN(ETokenType::ObjStart); break; }
                    case c_objEnd:     { DJO_SET_ONE_CHAR_TOKEN(ETokenType::ObjEnd); break; }
                    default:
                    {
                        if (IsAlpha(*it)) // name starts with letter only
                        {
                            DJO_SET_CONDITION_TOKEN(ETokenType::ElemName, _Internal::IsOfName(*it));
                            --it;
                        }
                        else if (IsOfScalar(*it))
                        {
                            DJO_SET_CONDITION_TOKEN(ETokenType::Value, IsOfScalar(*it));
                            --it;
                        }
                        else
                        {
                            _outErrorMsg->append("Entry not expected");
                            return MakeResult(EResult::FAIL, _outErrorMsg->c_str());
                        }
                        break;
                    }
                }
            }

            #undef DJO_SET_CONDITION_TOKEN
            #undef DJO_SET_ONE_CHAR_TOKEN

            return EResult::OK;
        }

        DJO_LUT_START(s_lutTokenExpect, ECount<ETokenType>, ETokenType, u32, c_invalidUint<u32>)
            DJO_LUT_MATCH(EValue(ETokenType::None),       Flag(EValue(ETokenType::None))     | Flag(EValue(ETokenType::ElemName)))
            DJO_LUT_MATCH(EValue(ETokenType::ElemName),   Flag(EValue(ETokenType::Value))    | Flag(EValue(ETokenType::ObjStart)) | Flag(EValue(ETokenType::ArrayStart)))
            DJO_LUT_MATCH(EValue(ETokenType::Value),      Flag(EValue(ETokenType::None))     | Flag(EValue(ETokenType::ElemName)) | Flag(EValue(ETokenType::Value))    | Flag(EValue(ETokenType::ObjEnd))     | Flag(EValue(ETokenType::ArrayEnd)))
            DJO_LUT_MATCH(EValue(ETokenType::ObjStart),   Flag(EValue(ETokenType::ElemName)))
            DJO_LUT_MATCH(EValue(ETokenType::ObjEnd),     Flag(EValue(ETokenType::None))     | Flag(EValue(ETokenType::ElemName)) | Flag(EValue(ETokenType::ObjStart)) | Flag(EValue(ETokenType::ObjEnd))     | Flag(EValue(ETokenType::ArrayEnd)))
            DJO_LUT_MATCH(EValue(ETokenType::ArrayStart), Flag(EValue(ETokenType::Value))    | Flag(EValue(ETokenType::ObjStart)))
            DJO_LUT_MATCH(EValue(ETokenType::ArrayEnd),   Flag(EValue(ETokenType::None))     | Flag(EValue(ETokenType::ElemName)) | Flag(EValue(ETokenType::ObjEnd))   | Flag(EValue(ETokenType::ArrayStart)) | Flag(EValue(ETokenType::ArrayEnd)))
        DJO_LUT_END();
        DJO_LUT_START(s_lutTokenName, ECount<ETokenType>, ETokenType, const char*, "")
            DJO_LUT_MATCH(EValue(ETokenType::None),       "(none)")
            DJO_LUT_MATCH(EValue(ETokenType::ElemName),   "(element name)")
            DJO_LUT_MATCH(EValue(ETokenType::Value),      "(element value)")
            DJO_LUT_MATCH(EValue(ETokenType::ObjStart),   "(object start \'{\')")
            DJO_LUT_MATCH(EValue(ETokenType::ObjEnd),     "(object end \'}\')")
            DJO_LUT_MATCH(EValue(ETokenType::ArrayStart), "(array start \'[\')")
            DJO_LUT_MATCH(EValue(ETokenType::ArrayEnd),   "(array end \']\')")
        DJO_LUT_END();

        EResult
        Tokenizer::VerifyTokens(std::string* const _outErrorMsg) const
        {
            ETokenType previousType = ETokenType::None;

            u32 objectLevel = 0;
            u32 arrayLevel = 0;

            const auto OnTypeError = [&previousType, &_outErrorMsg](const ETokenType _tokenType) -> EResult
            {
                _outErrorMsg->reserve(_outErrorMsg->size() + 100);
                _outErrorMsg->
                    append(StringView{ "Unexpected token: after " })
                    .append(s_lutTokenName.a[EValue(previousType)])
                    .append(StringView{ ", got " })
                    .append(s_lutTokenName.a[EValue(_tokenType)])
                    .append(StringView{ ", waiting for:" });
                const u32 expectedTokensFlags = s_lutTokenExpect.a[EValue(previousType)];
                for (u32 i = 0; i < ECount<ETokenType>; ++i)
                    if (TestFlag(expectedTokensFlags, Flag(i)))
                        _outErrorMsg->append(" ").append(s_lutTokenName.a[i]);
                return MakeResult(EResult::FAIL, _outErrorMsg->c_str());
            };

            const auto OnHierarchyError = [&_outErrorMsg](const StringView& _hierarchy) -> EResult
            {
                _outErrorMsg->append(StringView{ "Hierarchy error on " }).append(_hierarchy);
                return MakeResult(EResult::FAIL, _outErrorMsg->c_str());
            };

            for (const Token& token : m_tokens)
            {
                if (!TestFlag(s_lutTokenExpect.a[EValue(previousType)], Flag(EValue(token.type))))
                    return OnTypeError(token.type);

                switch (token.type)
                {
                    case ETokenType::ObjStart:
                        ++objectLevel;
                        break;
                    case ETokenType::ObjEnd:
                        if (objectLevel == 0)
                            return OnHierarchyError(StringView{ "object" });
                        --objectLevel;
                        break;
                    case ETokenType::ArrayStart:
                        ++arrayLevel;
                        break;
                    case ETokenType::ArrayEnd:
                        if (arrayLevel == 0)
                            return OnHierarchyError(StringView{ "array" });
                        --arrayLevel;
                        break;
                }

                previousType = token.type;
            }

            if (!TestFlag(s_lutTokenExpect.a[EValue(previousType)], Flag(EValue(ETokenType::None))))
                return OnTypeError(ETokenType::None);

            if (objectLevel > 0)
                return OnHierarchyError(StringView{ "object" });
            if (arrayLevel > 0)
                return OnHierarchyError(StringView{ "array" });

            return EResult::OK;
        }

        EResult
        Tokenizer::CountContent()
        {
            DJO_TODO("Tokenizer::CountContent : try benchmark unroll and allocate stack with alloca");
            u32 elemCount = 1; // implicit root elem
            u32 valueCount = 0;
            const std::vector<Token>::const_iterator itEnd = m_tokens.cend();
            const auto CountContentRec = [itEnd, &elemCount, &valueCount]
            (std::vector<Token>::iterator _it, auto&& _countContentRec)
            -> std::vector<Token>::iterator
            {
                Token& parentToken = *_it++;
                for (; _it != itEnd; ++_it)
                {
                    switch (_it->type)
                    {
                        case ETokenType::ElemName:
                            ++elemCount;
                            ++parentToken.contentCount;
                            break;
                        case ETokenType::Value:
                            ++valueCount;
                            if (parentToken.type == ETokenType::ArrayStart)
                                ++parentToken.contentCount;
                            break;
                        case ETokenType::ArrayStart:
                        case ETokenType::ObjStart:
                            ++valueCount;
                            if (parentToken.type == ETokenType::ArrayStart)
                                ++parentToken.contentCount;
                            _it = _countContentRec(_it, _countContentRec);
                            break;
                        case ETokenType::ArrayEnd:
                        case ETokenType::ObjEnd:
                            return _it;
                    }
                }
                return _it;
            };
            const std::vector<Token>::iterator it = CountContentRec(m_tokens.begin(), CountContentRec);
            DJO_ASSERT(it == itEnd); DJO_UNUSED_VAR(it);
            m_elemCount = elemCount;
            m_valueCount = valueCount;
            return EResult::OK;
        }

        ValueType
        ReadTokenValue(const StringView& _token, Value* const _outValue)
        {
            if (_token[0] == c_str)
            {
                _outValue->s = StringView{ _token.data + 1, _token.count - 1 };
                return EValue(EValueType::String);
            }
            else
            {
                DJO_ASSERT(IsOfScalar(_token[0]));
                _outValue->n = 0.;
                std::from_chars(_token.data, _token.GetEnd(), _outValue->n);
                return EValue(EValueType::Number);
            }
        }
    }
}
