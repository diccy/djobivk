#pragma once


namespace Djo
{
    // fw
    struct StringView;

    constexpr bool IsEol(const char c)               { return c == '\n' || c == '\r'; }
    constexpr bool IsStrictWhiteSpace(const char c)  { return c == ' '  || c == '\t'; }
    constexpr bool IsWhiteSpace(const char c)        { return IsStrictWhiteSpace(c) || IsEol(c); }
    constexpr bool IsAlpha(const char c)             { return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z'); }
    constexpr bool IsNumber(const char c)            { return '0' <= c && c <= '9'; }
    constexpr bool IsAlphaNum(const char c)          { return IsAlpha(c) || IsNumber(c); }
    constexpr bool IsOfScalar(const char c)          { return IsNumber(c) || c == '+' || c == '-' || c == '.'; }

    using CharPredicate = bool (*)(char);
    bool AdvanceWhile(CharPredicate _pred, const char* const _itStart, const char* const _itEnd, const char** const _outIt);
    bool AdvanceUntil(CharPredicate _pred, const char* const _itStart, const char* const _itEnd, const char** const _outIt);

    bool GetNextLine(const char* _itStart, const char* _itEnd, StringView* _outLine, const char** _outItEol = nullptr);
}
