#pragma once

#include <djon/Djon.hpp>
#include <Core/StdVector.hpp>


namespace Djo
{
    namespace djon
    {
        /*
        // Value reading

        template <typename T>
        struct ReadValueImpl
        {
            DJO_INLINE
            void
            operator()(const Value& _value, const ValueType _type, T* const _outData)
            {
                DJO_ASSERT(GetBaseValueType(_type) == EValueType::Number); DJO_UNUSED_VAR(_type);
                *_outData = static_cast<T>(_value.n);
            }
        };

        template <typename T>
        DJO_INLINE
        void
        ReadValue(const Value& _value, const ValueType _type, T* const _outData)
        {
            ReadValueImpl<T>{}(_value, _type, _outData);
        }

        // Utils macros

    #define DJO_DJON_READIMPL_T(typenames_, type_, impl_)\
        template <typenames_>\
        struct ReadValueImpl<type_>\
        {\
            DJO_INLINE\
            void\
            operator()(const Value& _value, const ValueType _type, type_* const _outData)\
            {\
                impl_;\
            }\
        }

    #define DJO_DJON_READIMPL(type_, impl_)  DJO_DJON_READIMPL_T(, type_, impl_)


        // string impl
        DJO_DJON_READIMPL(StringView,
        {
            DJO_ASSERT(GetBaseValueType(_type) == EValueType::String); DJO_UNUSED_VAR(_type);
            *_outData = _value.s;
        });
        DJO_DJON_READIMPL(std::string,
        {
            DJO_ASSERT(GetBaseValueType(_type) == EValueType::String); DJO_UNUSED_VAR(_type);
            *_outData = _value.s;
        });

        // std::vector impl
        DJO_DJON_READIMPL_T(
            DJO_UNPAR((typename U, typename A)),
            DJO_UNPAR((std::vector<U, A>)),
            {
                DJO_ASSERT(IsArray(_type)); DJO_UNUSED_VAR(_type);
                const u32 count = (u32)_value.a.count;
                _outData->resize(count);
                for (u32 i = 0; i < count; ++i)
                    ReadValueImpl<U>{}(_value.a[i], EValue(GetBaseValueType(_type)), &_outData->at(i));
            }
        );


        // Struct member reading declaration

        #define DJO_DJON_READ_MEMBER_NAME(fileName_, structName_)\
        {\
            DJO_ASSERT(GetBaseValueType(_type) == EValueType::Object); DJO_UNUSED_VAR(_type);\
            if (const Elem* elem__ = GetChild(_value, DJO_CAT(fileName_, _h64)))\
                ReadValue(*elem__->value, elem__->type, &_outData->structName_);\
        }

        #define DJO_DJON_READ_MEMBER(name_)  DJO_DJON_READ_MEMBER_NAME(DJO_STR(name_), name_)


        // Value writing

        template <typename T>
        struct WriteValueImpl
        {
            DJO_INLINE
            void
            operator()(Writer& _writer, const T& _data)
            {
                _writer.Write(_data);
            }
        };

        template <typename T>
        DJO_INLINE
        void
        WriteValue(Writer& _writer, const T& _data)
        {
            if constexpr (std::is_convertible_v<T, StringView>)
                WriteValueImpl<StringView>{}(_writer, StringView{ _data });
            else
                WriteValueImpl<T>{}(_writer, _data);
        }

        template <typename T>
        DJO_INLINE
        void
        WriteValueAttr(Writer& _writer, const T& _data, const EWriterAttribute _attr)
        {
            const EWriterAttribute previousAttr = _writer.SetAttr(_attr);
            WriteValue(_writer, _data);
            _writer.SetAttr(previousAttr);
        }

        template <typename T>
        DJO_INLINE
        void
        WriteElem(Writer& _writer, StringView _name, const T& _data, const EWriterAttribute _attr)
        {
            const EWriterAttribute previousAttr = _writer.SetAttr(_attr);
            WriteValue(_writer.Write(_name).NewLine(), _data);
            _writer.SetAttr(previousAttr);
        }

    #define DJO_DJON_WRITEIMPL_T(typenames_, type_, impl_)\
        template <typenames_>\
        struct WriteValueImpl<type_>\
        {\
            DJO_INLINE\
            void\
            operator()(Writer& _writer, const type_& _data)\
            {\
                impl_;\
            }\
        }

    #define DJO_DJON_WRITEIMPL(type_, impl_)  DJO_DJON_WRITEIMPL_T(, type_, impl_)

        DJO_DJON_WRITEIMPL(StringView, _writer.Write('"').Write(_data).Write('"'));
        DJO_DJON_WRITEIMPL(bool, _writer.Write(_data ? 1u : 0u));

        DJO_DJON_WRITEIMPL_T(
            DJO_UNPAR((typename U, typename A)),
            DJO_UNPAR((std::vector<U, A>)),
            _writer.OpenArray();
            const u32 count = (u32)_data.size();
            for (u32 i = 0; i < count; ++i)
            {
                WriteValue(_writer, _data[i]);
                if (i < count - 1)
                    _writer.NewLine();
            }
            _writer.CloseArray();
        );

    #define DJO_DJON_WRITEIMPL_OBJECT(type_, impl_)\
        DJO_DJON_WRITEIMPL(type_,\
            _writer.OpenObject();\
            impl_;\
            _writer.CloseObject();\
        )

    #define DJO_DJON_WRITE_MEMBER_NAME_ATTR(fileName_, structName_, attr_)\
        WriteValueAttr(_writer.Write(fileName_).NewLine(), _data.structName_, attr_)

    #define DJO_DJON_WRITE_MEMBER_ATTR(name_, attr_)  DJO_DJON_WRITE_MEMBER_NAME_ATTR(DJO_STR(name_), name_, attr_)

    #define DJO_DJON_WRITE_MEMBER_NAME(fileName_, structName_)\
        WriteValue(_writer.Write(fileName_).NewLine(), _data.structName_)

    #define DJO_DJON_WRITE_MEMBER(name_)  DJO_DJON_WRITE_MEMBER_NAME(DJO_STR(name_), name_)

        template <typename TInput, typename TData>
        struct Reader
        {
            bool
            operator()(TInput& _input, TData* const _outData)
            {
                return _input.Read(_outData);
            }
        };

        template <typename TOutput, typename TData>
        struct Writer
        {
            bool
            operator()(TOutput& _output, const TData& _data)
            {
                return _output.Write(_data);
            }
        };

        template <typename TInput, typename TData>
        DJO_INLINE
        bool
        ReadInput(TInput& _input, const typename TInput::Id _id, TData* const _outData)
        {
            const typename TInput::Id previousId = _input.GetId();
            if (_input.Goto(_id))
            {
                const bool readResult = Reader<TInput, TData>{}(_input, _outData);
                _input.Goto(previousId);
                return readResult;
            }
            return false;
        }

        template <typename TOutput, typename TData>
        DJO_INLINE
        bool
        WriteData(TOutput& _output, const typename TOutput::Id _id, const TData& _data)
        {
            _output.SetNextId(_id);
            return Writer<TOutput, TData>{}(_output, _data);
        }

        struct DjonSerializationInput
        {
            using Id = StringView;

            Id GetId() const { return this->id; }
            bool Goto(const Id _id) { this->id = _id; return true; }

            bool Read(int* const _outData) { return true; }
            bool Read(float* const _outData) { return true; }
            bool Read(std::string* const _outData) { return true; }

            StringView id{};
        };

        struct DjonSerializationOutput
        {
            using Id = StringView;

            bool SetNextId(const Id _id) { this->id = _id; return true; }

            bool Write(int _data) { return true; }
            bool Write(float _data) { return true; }
            bool Write(const std::string& _data) { return true; }

            StringView id{};
        };

        template <typename TData>
        DJO_INLINE
        bool
        ReadDjonInput(DjonSerializationInput& _input, const DjonSerializationInput::Id _id, TData* const _outData)
        {
            return ReadInput<DjonSerializationInput, TData>(_input, _id, _outData);
        }

        template <typename TData>
        DJO_INLINE
        bool
        WriteDjonData(DjonSerializationOutput& _output, const DjonSerializationOutput::Id _id, const TData& _data)
        {
            return WriteData<DjonSerializationOutput, TData>(_output, _id, _data);
        }
        */
    }
}
