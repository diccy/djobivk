
#include <djon/DjonParser.hpp>

#include <djon/ParsingUtils.hpp>
#include <System/IFile.hpp>

#include <charconv>


namespace Djo
{
    namespace djon
    {
        static const char s_rootName[]{ "__RootElem" };
        static const u64 s_rootNameHash{ "__RootElem"_h64 };

        Parser::Parser()
            : m_data{ {}, {}, nullptr }
            , m_tokenizer{}
            , m_errorMsg{}
            , m_contentVersion{ c_version }
        {
        }

        Parser::~Parser()
        {
        }

        void
        Parser::Clear()
        {
            m_data.content.clear();
            m_data.data.clear();
            m_data.root = nullptr;
            m_contentVersion = c_version;
        }

        sz
        Parser::ReadVersion(const StringView& _content)
        {
            constexpr sz c_versionStrLength{ sizeof(c_versionStr) - 1 }; // strip ending zero

            const char* it = _content.GetStart();
            const char* const itEnd = _content.GetEnd();

            AdvanceWhile(IsWhiteSpace, it, itEnd, &it);
            const bool looksLikeVersion =
                *it == c_comm &&
                std::distance(it + 1, itEnd) >= c_versionStrLength &&
                std::strncmp(it + 1, c_versionStr, c_versionStrLength) == 0;
            if (!looksLikeVersion)
                return 0;

            it += 1 + c_versionStrLength;
            Version version{ 0, 0, 0 };
            const char* const itEol = itEnd; AdvanceUntil(IsEol, it, itEnd, const_cast<const char**>(&itEol));
            AdvanceWhile(IsStrictWhiteSpace, it, itEol, &it);

            const char* itEon;
            const auto ReadNumber = [&it, &itEon, itEol](auto* _versionValue) -> void
            {
                if (it != itEol && IsNumber(*it))
                {
                    AdvanceWhile(IsNumber, it, itEol, &itEon);
                    std::from_chars(it, itEon, *_versionValue);
                    it = itEon;
                }
            };
            ReadNumber(&version.major);
            if (*it == c_versionSeparator) { ++it; ReadNumber(&version.minor); }
            if (*it == c_versionSeparator) { ++it; ReadNumber(&version.patch); }

            DJO_ASSERT(version <= c_version);
            m_contentVersion = version;

            return (sz)std::distance(_content.GetStart(), itEol);
        }

        EResult
        Parser::Parse(IFile& _file)
        {
            Clear();

            const bool wasFileOpen = _file.IsOpen();
            if (!wasFileOpen)
                DJO_OK_OR_RETURN(_file.Open());

            DJO_OK_OR_RETURN(_file.Read(&m_data.content));
            if (!wasFileOpen)
                _file.Close();

            return Parse(StringView{ m_data.content });
        }

        EResult
        Parser::Parse(const StringView& _content)
        {
            Clear();

            StringView content{ _content };
            {
                const sz versionOffset = ReadVersion(content);
                content.data += versionOffset;
                content.count -= versionOffset;
            }

            DJO_OK_OR_RETURN(m_tokenizer.Tokenize(content, &m_errorMsg));
            DJO_OK_OR_RETURN(m_tokenizer.VerifyTokens(&m_errorMsg));
            DJO_OK_OR_RETURN(m_tokenizer.CountContent());

            // allocate elems and values data
            const u32 elemCount = m_tokenizer.GetElemCount();
            const u32 valueCount = m_tokenizer.GetValueCount() + 1;
            const sz elemsMemorySize = elemCount * sizeof(Elem);
            const sz valuesMemorySize = valueCount * sizeof(Value);
            m_data.data.resize(elemsMemorySize + valuesMemorySize);
            std::fill(m_data.data.begin(), m_data.data.end(), '\0');
            MemView<Elem> elems{ reinterpret_cast<Elem*>(m_data.data.data()), elemCount };
            MemView<Value> values{ reinterpret_cast<Value*>(elems.end()), valueCount };
            DJO_ASSERT((void*)values.end() <= (void*)(m_data.data.data() + m_data.data.size()));

            std::vector<Token>::const_iterator itToken = m_tokenizer.GetTokens().cbegin();
            const std::vector<Token>::const_iterator itTokenEnd = m_tokenizer.GetTokens().cend();
            u32 nextElem = 0;
            u32 nextValue = 0;
            m_data.root = &elems[nextElem++];
            m_data.root->name = s_rootName;
            m_data.root->nameHash = s_rootNameHash;
            m_data.root->value = &values[nextValue++];
            m_data.root->type = EValue(EValueType::Object);
            m_data.root->value->o = MemView<Elem>{ &elems[nextElem], itToken->contentCount };
            nextElem += itToken->contentCount;
            nextValue += itToken->contentCount;
            ++itToken;
            const auto ParseRec = [&itToken, itTokenEnd, &nextElem, &nextValue, &elems, &values]
            (u32 _e, u32 _v, bool isArray, auto&& _parseRec) -> void
            {
                u32 recElem, recValue;
                for (; itToken != itTokenEnd; ++itToken)
                {
                    switch (itToken->type)
                    {
                        case ETokenType::ElemName:
                            ++_e; ++_v;
                            DJO_ASSERT(_e < elems.count);
                            DJO_ASSERT(_v < values.count);
                            elems[_e].name = itToken->token;
                            elems[_e].nameHash = Hasher<StringView>{}(elems[_e].name);
                            elems[_e].value = &values[_v];
                            elems[_e].type = 0;
                            break;
                        case ETokenType::Value:
                            DJO_ASSERT(_e < elems.count);
                            DJO_ASSERT(_v < values.count);
                            elems[_e].type |= ReadTokenValue(itToken->token, &values[_v]);
                            if (isArray) ++_v;
                            break;
                        case ETokenType::ArrayStart:
                            DJO_ASSERT(_e < elems.count);
                            DJO_ASSERT(_v < values.count);
                            elems[_e].type |= Flag(EValue(EValueType::Array)); // does not work for array of array (of array...)
                            values[_v].a = MemView<Value>{ &values[nextValue], itToken->contentCount };
                            recValue = nextValue;
                            nextValue += itToken->contentCount;
                            ++itToken;
                            _parseRec(_e, recValue, true, _parseRec);
                            if (isArray) ++_v;
                            break;
                        case ETokenType::ObjStart:
                            DJO_ASSERT(_e < elems.count);
                            DJO_ASSERT(_v < values.count);
                            elems[_e].type |= EValue(EValueType::Object);
                            values[_v].o = MemView<Elem>{ &elems[nextElem], itToken->contentCount };
                            recElem = nextElem;
                            recValue = nextValue;
                            nextElem += itToken->contentCount;
                            nextValue += itToken->contentCount;
                            ++itToken;
                            _parseRec(recElem - 1, recValue - 1, false, _parseRec);
                            if (isArray) ++_v;
                            break;
                        case ETokenType::ArrayEnd:
                        case ETokenType::ObjEnd:
                            return;
                    }
                }
            };
            ParseRec(0, 0, false, ParseRec);
            DJO_ASSERT(itToken == itTokenEnd);
            DJO_ASSERT(nextElem == elems.count);
            DJO_ASSERT(nextValue == values.count);

            return EResult::OK;
        }
    }
}
