#pragma once

#include <djon/Djon.hpp>
#include <Core/StdVector.hpp>
#include <Core/ResultEnum.hpp>


namespace Djo
{
    namespace djon
    {
        enum class ETokenType : u32
        {
            None = 0,
            ElemName,
            Value,
            ObjStart,
            ObjEnd,
            ArrayStart,
            ArrayEnd,

            DJO_ENUM_COUNT_INVALID
        };

        struct Token
        {
            StringView token;
            ETokenType type;
            u32 contentCount; // used if type is Obj or Array
        };

        class Tokenizer
        {
            DJO_NO_COPY_NO_MOVE(Tokenizer);

        public:

            Tokenizer();
            ~Tokenizer();

            DJO_INLINE const std::vector<Token>& GetTokens() const  { return m_tokens; }
            DJO_INLINE u32 GetElemCount() const                     { return m_elemCount; }
            DJO_INLINE u32 GetValueCount() const                    { return m_valueCount; }

            EResult Tokenize(const StringView& _content, std::string* _outErrorMsg);
            EResult VerifyTokens(std::string* _outErrorMsg) const;
            EResult CountContent();

        private:
            std::vector<Token> m_tokens;
            u32 m_elemCount;
            u32 m_valueCount;
        };

        ValueType ReadTokenValue(const StringView& _token, Value* _outValue);
    }
}
