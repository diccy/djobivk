#pragma once

#include <djon/Djon.hpp>
#include <djon/DjonData.hpp>
#include <djon/DjonTokenizer.hpp>


namespace Djo
{
    // fw
    class IFile;

    namespace djon
    {
        class Parser
        {
            DJO_NO_COPY_NO_MOVE(Parser);

        public:
            Parser();
            ~Parser();

            EResult Parse(IFile& _file);
            EResult Parse(const StringView& _content);
            void Clear();

            DJO_INLINE const Elem* GetRoot() const        { return m_data.root; }
            DJO_INLINE Version GetContentVersion() const  { return m_contentVersion; }

        private:
            sz ReadVersion(const StringView& _content);

            Data m_data;
            Tokenizer m_tokenizer;
            std::string m_errorMsg;
            Version m_contentVersion;
        };
    }
}
