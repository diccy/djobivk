#pragma once

#include <djon/Djon.hpp>
#include <Serialization/Serialization.hpp>
#include <Math/Vector2.hpp>
#include <Math/Vector3.hpp>
#include <Math/Vector4.hpp>
#include <Math/Quaternion.hpp>
#include <Math/Matrix33.hpp>
#include <Math/Matrix44.hpp>
#include <Math/Math.hpp>
#include <Core/StdVector.hpp>
#include <Core/StringView.hpp>
#include <Core/StdStringStream.hpp>
#include <Core/ResultEnum.hpp>


namespace Djo
{
    namespace djon
    {
        namespace _Internal
        {
            template <typename T>
            constexpr bool c_isNumber_v = std::is_arithmetic_v<T>;
            template <typename T>
            constexpr bool c_isString_v = c_isAnyOf_v<std::remove_cv_t<T>, StringView, std::string>;
            template <typename T>
            constexpr bool c_isBasicType_v = c_isNumber_v<T> || c_isString_v<T>;

            template <typename T>
            constexpr bool c_isUtilStruct_CV_v = false;
            template <typename T>
            constexpr bool c_isUtilStruct_CV_v<Srl::NameDataPair<T>> = true;
            template <typename T>
            constexpr bool c_isUtilStruct_CV_v<Srl::NameHashDataPair<T>> = true;
            template <typename T>
            constexpr bool c_isUtilStruct_v = c_isUtilStruct_CV_v<std::remove_cv_t<T>>;

            template <typename T>
            constexpr bool c_usesArray_CV_v = false;
            template <typename T>
            constexpr bool c_usesArray_CV_v<MemView<T>> = true;
            template <typename T, typename A>
            constexpr bool c_usesArray_CV_v<std::vector<T, A>> = true;
            template <typename T, glm::length_t S>
            constexpr bool c_usesArray_CV_v<glm::vec<S, T, c_glmQualifier>> = true;
            template <typename T>
            constexpr bool c_usesArray_CV_v<glm::qua<T, c_glmQualifier>> = true;
            template <typename T, glm::length_t R, glm::length_t C>
            constexpr bool c_usesArray_CV_v<glm::mat<R, C, T, c_glmQualifier>> = true;
            template <typename T>
            constexpr bool c_usesArray_v = c_usesArray_CV_v<std::remove_cv_t<T>>;

            template <typename T>
            constexpr bool c_usesObject_v = !(c_isBasicType_v<T> || c_isUtilStruct_v<T> || c_usesArray_v<T>);
        }


        // Reader

        class Reader
            : public Srl::Serializer<Reader>
        {
            DJO_NO_COPY_NO_MOVE(Reader);

        public:
            Reader();
            Reader(const Value& _value);
            ~Reader();

            DJO_INLINE void SetElem(const Elem& _elem)  { SetValue(*_elem.value, _elem.type); }
            DJO_INLINE void SetValue(const Value& _value, const ValueType _type)  { m_value = &_value; m_type = _type; }
            DJO_INLINE const Value* GetValue() const  { return m_value; }
            DJO_INLINE ValueType GetType() const      { return m_type; }

            template <typename T>
            DJO_INLINE
            void
            Read(T* const _data)
            {
                Apply(*_data);
            }

            template <typename T>
            EResult
            Read(const u64 _hashName, T* const _data)
            {
                const Elem* const newElem = djon::GetChild(*m_value, _hashName);
                if (newElem == nullptr)
                    return EResult::FAIL;
                ReadValue(*newElem->value, _data, newElem->type);
                return EResult::OK;
            }

            template <typename T>
            EResult
            Read(const StringView& _path, T* const _data)
            {
                const Elem* const newElem = djon::GetChild(*m_value, _path);
                if (newElem == nullptr)
                    return EResult::FAIL;
                ReadValue(*newElem->value, _data, newElem->type);
                return EResult::OK;
            }

        private:
            template <typename T>
            void
            ReadValue(const Value& _newValue, T* const _data, const ValueType _newType)
            {
                const Value* const previousValue = m_value;
                const ValueType previousType = m_type;
                SetValue(_newValue, _newType);
                Read(_data);
                SetValue(*previousValue, previousType);
            }

            const Value* m_value;
            ValueType m_type;
        };

        // ReadData impl

        // name + data
        template <typename T>
        void
        ReadData(Reader& _srl, Srl::NameDataPair<T>& _nameDataPair)
        {
            const Value* const previousValue = _srl.GetValue();
            const ValueType previousType = _srl.GetType();
            const Elem* const elem = GetChild(*previousValue, _nameDataPair.name);
            if (elem != nullptr)
            {
                _srl.SetElem(*elem);
                _srl.Apply(_nameDataPair.data);
                _srl.SetValue(*previousValue, previousType);
            }
        }
        template <typename T>
        void
        ReadData(Reader& _srl, Srl::NameHashDataPair<T>& _nameHashDataPair)
        {
            const Value* const previousValue = _srl.GetValue();
            const ValueType previousType = _srl.GetType();
            const Elem* const elem = GetChild(*previousValue, _nameHashDataPair.nameHash);
            if (elem != nullptr)
            {
                _srl.SetElem(*elem);
                _srl.Apply(_nameHashDataPair.data);
                _srl.SetValue(*previousValue, previousType);
            }
        }

        // base types
        template <typename T, DJO_TARG_ENABLE_IF(_Internal::c_isNumber_v<T>)>
        DJO_INLINE
        void
        ReadData(Reader& _srl, T& _outNumber)
        {
            ReadNumber(*_srl.GetValue(), &_outNumber, EValue(GetBaseValueType(_srl.GetType())));
        }
        template <class T, DJO_TARG_ENABLE_IF(_Internal::c_isString_v<T>)>
        DJO_INLINE
        void
        ReadData(Reader& _srl, T& _outStr)
        {
            ReadString(*_srl.GetValue(), &_outStr, EValue(GetBaseValueType(_srl.GetType())));
        }

        // contiguous data
        template <typename T>
        void
        ReadData(Reader& _srl, MemView<T>& _outView)
        {
            DJO_ASSERT(IsArray(_srl.GetType()));
            const Value* const previousValue = _srl.GetValue();
            const ValueType previousType = _srl.GetType();
            const u32 count = (u32)Min(_outView.count, previousValue->a.count);
            for (u32 i = 0; i < count; ++i)
            {
                _srl.SetValue(previousValue->a[i], previousType);
                _srl.Apply(_outView[i]);
            }
            _srl.SetValue(*previousValue, previousType);
        }
        template <typename T, typename A>
        void
        ReadData(Reader& _srl, std::vector<T, A>& _outVector)
        {
            DJO_ASSERT(IsArray(_srl.GetType()));
            const u32 count = (u32)_srl.GetValue()->a.count;
            _outVector.resize(count);
            MemView<T> mv{ ToView(_outVector) };
            ReadData(_srl, mv);
        }

        // math structs as contiguous data
        template <typename T, glm::length_t S>
        DJO_INLINE
        void
        ReadData(Reader& _srl, glm::vec<S, T, c_glmQualifier>& _outV)
        {
            MemView<T> mv{ &_outV[0], S };
            ReadData(_srl, mv);
        }
        template <typename T>
        DJO_INLINE
        void
        ReadData(Reader& _srl, glm::qua<T, c_glmQualifier>& _outQ)
        {
            MemView<T> mv{ &_outQ[0], 4 };
            ReadData(_srl, mv);
        }
        template <typename T, glm::length_t R, glm::length_t C>
        DJO_INLINE
        void
        ReadData(Reader& _srl, glm::mat<R, C, T, c_glmQualifier>& _outM)
        {
            MemView<T> mv{ &_outM[0], R * C };
            ReadData(_srl, mv);
        }


        // Writer

        class ReadableWriter
            : public Srl::Serializer<ReadableWriter>
        {
            DJO_NO_COPY_NO_MOVE(ReadableWriter);

        public:
            enum class EAttribute : u8
            {
                None,
                Oneline,
            };

            static constexpr u32 c_defaultTabSize{ 4 };

            ReadableWriter();
            ~ReadableWriter();

            DJO_INLINE std::string Get() const              { return m_stream.str(); };
            DJO_INLINE void SetTabSize(const u32 _tabSize)  { m_tabSize = _tabSize; }

            template <typename T>
            DJO_INLINE
            void
            Write(const T& _data)
            {
                Apply(_data);
            }
            template <typename T>
            DJO_INLINE
            void
            Write(const StringView& _name, const T& _data)
            {
                Write(Srl::MakeNDP(_name, _data));
            }

            template <typename T, DJO_TARG_ENABLE_IF(std::is_arithmetic_v<T>)>
            DJO_INLINE
            void
            WriteNumber(const T _data)
            {
                BeforeValueSpace();
                m_stream << _data;
            }

            DJO_INLINE
            void
            WriteString(const StringView& _sv)
            {
                BeforeValueSpace();
                m_stream << '"' << _sv << '"';
            }

            DJO_INLINE
            void
            WriteName(const StringView& _sv)
            {
                NewLineIfNotFirstEntry();
                m_stream << _sv;
                m_previouslyWroteName = true;
            }

            DJO_INLINE void Space()  { m_stream << ' '; }
            void NewLine();
            void Tab(s32 _tab);

            DJO_INLINE void OpenArray()    { Open(c_arrayStart); }
            DJO_INLINE void CloseArray()   { Close(c_arrayEnd); }
            
            DJO_INLINE
            void
            OpenObject()
            {
                if (!m_isFirstEntry) // [[likely]]
                    Open(c_objStart);
            }
            DJO_INLINE
            void
            CloseObject()
            {
                if (m_tab > 0) // [[likely]]
                    Close(c_objEnd);
            }

            void Clear();

            DJO_INLINE
            EAttribute
            SetAttr(const EAttribute _attr)
            {
                const EAttribute previousAttr = m_attr;
                m_attr = _attr;
                return previousAttr;
            }

        private:
            void Open(char _c);
            void Close(char _c);
            void NewLineIfNotFirstEntry();
            void BeforeValueSpace();
            void BeforeValueNewLine();

            std::stringstream m_stream;
            StringView m_nextName;
            u32 m_tab;
            u32 m_tabSize;
            EAttribute m_attr;
            bool m_previouslyWroteName;
            bool m_isFirstEntry;
        };


        // Before/After serialization steps

        template <class T, DJO_TARG_ENABLE_IF(_Internal::c_usesObject_v<T>)>
        DJO_INLINE
        void
        BeforeSerialization(ReadableWriter& _srl, const T&)
        {
            _srl.OpenObject();
        }
        template <class T, DJO_TARG_ENABLE_IF(_Internal::c_usesObject_v<T>)>
        DJO_INLINE
        void
        AfterSerialization(ReadableWriter& _srl, const T&)
        {
            _srl.CloseObject();
        }


        // WriteData impl

        // name + data
        template <typename T>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const Srl::NameDataPair<T>& _nameDataPair)
        {
            _srl.WriteName(_nameDataPair.name);
            _srl.Apply(_nameDataPair.data);
        }
        template <typename T>
        DJO_INLINE
        void
        WriteData(ReadableWriter&, const Srl::NameHashDataPair<T>&)
        {
            DJO_STATIC_ASSERT_MSG(false, "Makes no sense to write NameHashDataPair, use NameDataPair instead.");
        }

        // base types
        template <typename T, DJO_TARG_ENABLE_IF(_Internal::c_isNumber_v<T>)>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const T& _n)
        {
            _srl.WriteNumber<T>(_n);
        }
        template <class T, DJO_TARG_ENABLE_IF(_Internal::c_isString_v<T>)>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const T& _str)
        {
            _srl.WriteString(StringView{ _str });
        }

        // contiguous data
        namespace _Internal
        {
            template <typename T>
            DJO_INLINE
            void
            WriteData_Impl(ReadableWriter& _srl, const MemView<T>& _v)
            {
                _srl.OpenArray();
                const u32 count = (u32)_v.count;
                for (u32 i = 0; i < count; ++i)
                    _srl.Apply(_v[i]);
                _srl.CloseArray();
            }
        }
        template <typename T>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const MemView<T>& _v)
        {
            if constexpr (_Internal::c_isNumber_v<T>)
            {
                const ReadableWriter::EAttribute prevAttr = _srl.SetAttr(ReadableWriter::EAttribute::Oneline);
                _Internal::WriteData_Impl(_srl, _v);
                _srl.SetAttr(prevAttr);
            }
            else
            {
                _Internal::WriteData_Impl(_srl, _v);
            }
        }
        template <typename T, typename A>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const std::vector<T, A>& _v)
        {
            WriteData(_srl, ToView(_v));
        }

        // math structs as contiguous data
        template <typename T, glm::length_t S>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const glm::vec<S, T, c_glmQualifier>& _v)
        {
            WriteData(_srl, MemView<T>{ &_outV[0], S });
        }
        template <typename T>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const glm::qua<T, c_glmQualifier>& _q)
        {
            WriteData(_srl, MemView<T>{ &_outQ[0], 4 });
        }
        template <typename T, glm::length_t R, glm::length_t C>
        DJO_INLINE
        void
        WriteData(ReadableWriter& _srl, const glm::mat<R, C, T, c_glmQualifier>& _m)
        {
            WriteData(_srl, MemView<T>{ &_outM[0], R * C });
        }
    }
}
