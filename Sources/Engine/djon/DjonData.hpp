#pragma once

#include <djon/Djon.hpp>
#include <Core/StdVector.hpp>


namespace Djo
{
    namespace djon
    {
        struct Data
        {
            std::string content;
            std::vector<u8> data;
            Elem* root;
        };
    }
}
