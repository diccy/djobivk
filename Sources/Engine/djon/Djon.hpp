#pragma once

#include <Core/Pointers.hpp>
#include <Core/StringView.hpp>
#include <Core/TypeTraits.hpp>


// djon as DJobi Object Notation, json-like file format
// inspired from https://blog.molecular-matters.com/2014/01/23/translating-a-human-readable-json-like-data-format-into-c-structs/

namespace Djo
{
    namespace djon
    {
        constexpr Version c_version{ 1, 0, 0 };

        // char tokens
        constexpr char c_comm{ '#' };
        constexpr char c_str{ '"' };
        constexpr char c_objStart{ '{' };
        constexpr char c_objEnd{ '}' };
        constexpr char c_arrayStart{ '[' };
        constexpr char c_arrayEnd{ ']' };

        // comm extension
        constexpr char c_versionStr[]{ "version" };
        constexpr char c_versionSeparator{ '.' };

        // fw
        struct Value;
        struct Elem;

        using ValueType = u8;
        enum class EValueType : ValueType
        {
            Number = 1,
            String,
            Object,
            Array,
        };

        constexpr
        EValueType
        GetBaseValueType(const ValueType _t)
        {
            return (EValueType)(_t & (Flag(EValue(EValueType::Array)) - 1));
        }
        constexpr
        bool
        IsArray(const ValueType _t)
        {
            return TestFlag(_t, Flag(EValue(EValueType::Array)));
        }

        struct Value
        {
            union
            {
                MemView<Elem> o;   // object
                MemView<Value> a;  // array
                StringView s;      // string
                f64 n;             // number
            };
        };

        struct Elem
        {
            StringView name;
            u64 nameHash;
            PtrHandler<Value> value; // PtrHandler to propagate Elem constness
            ValueType type;
        };


        // Move deeper in file structure

        DJO_INLINE
        const Elem*
        GetChild(const Value& _value, const u64 _hashName)
        {
            for (const Elem& e : _value.o)
                if (e.nameHash == _hashName)
                    return &e;
            return nullptr;
        }
        DJO_INLINE
        const Elem*
        GetChild(const Elem& _elem, const u64 _hashName)
        {
            if (GetBaseValueType(_elem.type) != EValueType::Object)
                return nullptr;

            return GetChild(*_elem.value, _hashName);
        }

        DJO_INLINE
        const Elem*
        GetChild(const Value& _value, const StringView& _path)
        {
            if (_path.IsEmpty())
                return nullptr;

            const sz sep = _path.Find('/');
            const StringView current = _path.SubFront(sep);
            const Elem* child = GetChild(_value, Hasher<StringView>{}(current));
            if (child != nullptr && GetBaseValueType(child->type) == EValueType::Object && sep != StringView::npos)
                child = GetChild(*child->value, _path.SubBack(sep + 1));
            return child;
        }
        DJO_INLINE
        const Elem*
        GetChild(const Elem& _elem, const StringView& _path)
        {
            if (GetBaseValueType(_elem.type) != EValueType::Object)
                return nullptr;

            return GetChild(*_elem.value, _path);
        }


        // Value reading

        template <typename T, DJO_TARG_ENABLE_IF(std::is_arithmetic_v<T>)>
        DJO_INLINE
        void
        ReadNumber(const Value& _value, T* const _outNumber, const ValueType _type)
        {
            DJO_ASSERT(_type == EValue(EValueType::Number)); DJO_UNUSED_VAR(_type);
            *_outNumber = static_cast<T>(_value.n);
        }

        template <typename TStr, DJO_TARG_ENABLE_IF((std::is_same_v<TStr, StringView> || std::is_same_v<TStr, std::string>))>
        DJO_INLINE
        void
        ReadString(const Value& _value, TStr* const _outStr, const ValueType _type)
        {
            DJO_ASSERT(_type == EValue(EValueType::String)); DJO_UNUSED_VAR(_type);
            *_outStr = _value.s;
        }
    }
}
