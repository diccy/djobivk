#pragma once

#include <System/IFileSystem.hpp>
#include <System/Path.hpp>


namespace Djo
{
    class FSPath
    {
        DJO_DEFAULT_COPY_DEFAULT_MOVE(FSPath);

    public:

        FSPath();
        explicit FSPath(const char* _sz);
        explicit FSPath(std::string _path);
        explicit FSPath(Path _path);
        FSPath(Path _path, IFileSystem* _fs);
        ~FSPath();

        DJO_INLINE const Path& GetPath() const         { return m_path; }
        DJO_INLINE Path& GetPath()                     { return m_path; }
        DJO_INLINE IFileSystem* GetFileSystem() const  { return m_fs; }

        DJO_INLINE IFile* NewFile() const              { return m_fs->NewFile(m_path); }
        DJO_INLINE bool DoesFileExist() const          { return m_fs->DoesFileExist(m_path); }
        DJO_INLINE std::string AsPrettyString() const  { return m_fs->AsPrettyString(m_path); }

    private:

        Path m_path;
        IFileSystem* m_fs;
    };

    template <>
    struct Hasher<FSPath>
    {
        DJO_INLINE
        u64
        operator () (const FSPath& _fsp) const noexcept
        {
            return _fsp.GetFileSystem()->GetHash(_fsp.GetPath());
        }
    };

    std::ostream& operator << (std::ostream& _os, const FSPath& _fsp);
}
