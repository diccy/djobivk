
#include <System/GlobalFS.hpp>

#include <System/Path.hpp>


namespace Djo
{
    #define DJO_ASSETS_DIRECTORY           "Assets/"
    #define DJO_ENGINE_ASSETS_DIRECTORY    DJO_ASSETS_DIRECTORY "Engine/"

    namespace GlobalFS
    {
        VirtualFileSystem* g_vfs{ nullptr };
        LocalFileSystem* g_engineLocalFileSystem{ nullptr };
        LocalFileSystem* g_appLocalFileSystem{ nullptr };

        EResult
        Init(const Path& _appAssetsLocalPath)
        {
            // core engine assets local file system

            g_engineLocalFileSystem = DJO_CORE_NEW LocalFileSystem{};
            DJO_OK_OR_RETURN(g_engineLocalFileSystem->Init(DJO_ENGINE_ASSETS_DIRECTORY));

            // app local assets file system

            if (!_appAssetsLocalPath.IsEmpty())
            {
                g_appLocalFileSystem = DJO_CORE_NEW LocalFileSystem{};
                DJO_OK_OR_RETURN(g_appLocalFileSystem->Init(Path{ DJO_ASSETS_DIRECTORY }.Add(_appAssetsLocalPath)));
            }

            // global virtual file system

            g_vfs = DJO_CORE_NEW VirtualFileSystem{};
            DJO_OK_OR_RETURN(g_vfs->MountFileSystem(g_engineLocalFileSystem));

            if (g_appLocalFileSystem != nullptr)
                DJO_OK_OR_RETURN(g_vfs->MountFileSystem(g_appLocalFileSystem));

            return EResult::OK;
        }

        void
        Shutdown()
        {
            delete g_vfs;
            delete g_appLocalFileSystem;
            delete g_engineLocalFileSystem;
            g_vfs = nullptr;
            g_appLocalFileSystem = nullptr;
            g_engineLocalFileSystem = nullptr;
        }
    }
}
