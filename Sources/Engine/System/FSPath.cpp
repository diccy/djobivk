
#include <System/FSPath.hpp>
#include <System/GlobalFS.hpp>

#include <ostream>


namespace Djo
{
    FSPath::FSPath()
        : m_path{}
        , m_fs{ DJO_VFS }
    {
    }

    FSPath::FSPath(const char* const _path)
        : m_path{ _path }
        , m_fs{ DJO_VFS }
    {
    }

    FSPath::FSPath(std::string _path)
        : m_path{ std::move(_path) }
        , m_fs{ DJO_VFS }
    {
    }

    FSPath::FSPath(Path _path)
        : m_path{ std::move(_path) }
        , m_fs{ DJO_VFS }
    {
    }

    FSPath::FSPath(Path _path, IFileSystem* const _fs)
        : m_path{ std::move(_path) }
        , m_fs{ _fs != nullptr ? _fs : DJO_VFS }
    {
    }

    FSPath::~FSPath()
    {
        m_fs = nullptr;
    }


    std::ostream&
    operator << (std::ostream& _os, const FSPath& _fsp)
    {
        _os << _fsp.AsPrettyString();
        return _os;
    }
}
