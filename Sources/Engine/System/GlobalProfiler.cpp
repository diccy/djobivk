
#include <System/GlobalProfiler.hpp>


#if defined(DJO_PROFILING_ENABLED)

namespace Djo
{
    namespace GlobalProfiler
    {
        SimpleProfiler* g_profiler{ nullptr };

        EResult
        Init()
        {
            g_profiler = DJO_CORE_NEW SimpleProfiler{};
            return EResult::OK;
        }

        void
        Shutdown()
        {
            delete g_profiler;
            g_profiler = nullptr;
        }
    }
}

#endif // DJO_PROFILE