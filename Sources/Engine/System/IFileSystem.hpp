#pragma once

#include <System/IFile.hpp>


namespace Djo
{
    // fw
    class Path;

    using DoComputeSize = HardAlias<bool>;

    class IFileSystem
    {
        DJO_DEFAULT_CTOR_V_DTOR(IFileSystem);
        DJO_NO_COPY_NO_MOVE(IFileSystem);

    public:

        virtual u64 GetHash(const Path& _path) const = 0;
        virtual IFile* NewFile(const Path& _path, DoComputeSize _doComputeSize = { false }) const = 0;
        virtual bool DoesFileExist(const Path& _path) const = 0;
        virtual sz GetFileSize(const Path& _path) const = 0;
        virtual std::string AsPrettyString(const Path& _path) const = 0; // used for display
    };
}
