#pragma once

#include <Core/Core.hpp>


#if defined(DJO_PROFILING_ENABLED)

#include <System/SimpleProfiler.hpp>


namespace Djo
{
    namespace GlobalProfiler
    {
        extern SimpleProfiler* g_profiler;

        EResult Init();
        void Shutdown();

        DJO_INLINE EResult BeginSession(const Path& _path)  { return g_profiler ? g_profiler->BeginSession(_path) : MakeResult(EResult::FAIL, "GlobalProfiler not initialized"); }
        DJO_INLINE void AllowRecording(const bool _allow)   { if (g_profiler) g_profiler->AllowRecording(_allow); }
        DJO_INLINE bool IsRecordingAllowed()                { return g_profiler ? g_profiler->IsRecordingAllowed() : false; }
        DJO_INLINE void EndSession()                        { if (g_profiler) g_profiler->EndSession(); }
    }
}

#define DJO_PROFILE_BEGIN_SESSION(path_)  ::Djo::GlobalProfiler::BeginSession(path_)
#define DJO_PROFILE_SCOPE(name_)          ::Djo::SimpleProfilerTimer DJO_CAT(timer, __LINE__){ ::Djo::GlobalProfiler::g_profiler, name_ }
#define DJO_PROFILE_FUNCTION()            DJO_PROFILE_SCOPE(__FUNCTION__)
#define DJO_PROFILE_END_SESSION()         ::Djo::GlobalProfiler::EndSession()

#else

#define DJO_PROFILE_BEGIN_SESSION(path_)    do {} while (0)
#define DJO_PROFILE_SCOPE(name_)            do {} while (0)
#define DJO_PROFILE_FUNCTION()              do {} while (0)
#define DJO_PROFILE_END_SESSION()           do {} while (0)

#endif // DJO_PROFILING_ENABLED
