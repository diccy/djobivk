
#include <System/Timer.hpp>


namespace Djo
{
    Timer::Timer() noexcept
        : m_started{ TimePoint::duration::zero() }
        , m_stopped{ TimePoint::duration::zero() }
    {
        Start();
    }

    Timer::~Timer() noexcept
    {
    }

    void
    Timer::Start() noexcept
    {
        m_started = std::chrono::time_point_cast<TimePoint::duration>(std::chrono::steady_clock::now());
        m_stopped = TimePoint{ TimePoint::duration::zero() };
    }

    Microseconds64
    Timer::Get() const noexcept
    {
        TimePoint::duration elapsed;
        if (m_stopped.time_since_epoch() == TimePoint::duration::zero())
        {
            const TimePoint now = std::chrono::time_point_cast<TimePoint::duration>(std::chrono::steady_clock::now());
            elapsed = now.time_since_epoch() - m_started.time_since_epoch();
        }
        else
        {
            elapsed = m_stopped.time_since_epoch() - m_started.time_since_epoch();
        }
        return Microseconds64{ static_cast<f64>(elapsed.count()) };
    }

    Microseconds64
    Timer::Stop() noexcept
    {
        m_stopped = std::chrono::time_point_cast<TimePoint::duration>(std::chrono::steady_clock::now());
        const TimePoint::duration elapsed = m_stopped.time_since_epoch() - m_started.time_since_epoch();
        return Microseconds64{ static_cast<f64>(elapsed.count()) };
    }

    Microseconds64
    Timer::Restart() noexcept
    {
        const TimePoint now = std::chrono::time_point_cast<TimePoint::duration>(std::chrono::steady_clock::now());
        const TimePoint::duration elapsed = now.time_since_epoch() - m_started.time_since_epoch();
        m_started = now;
        m_stopped = TimePoint{ TimePoint::duration::zero() };
        return Microseconds64{ static_cast<f64>(elapsed.count()) };
    }
}
