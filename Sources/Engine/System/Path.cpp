
#include <System/Path.hpp>

#include <Core/Utils.hpp>


namespace Djo
{
    Path::Path()
        : m_str{}
    {
    }

    Path::Path(const char* const _path)
        : m_str{ _path }
    {
        Normalize();
    }

    Path::Path(std::string _path)
        : m_str{ std::move(_path) }
    {
        Normalize();
    }

    Path::~Path()
    {
    }

    namespace _Internal
    {
        // ignoring the very last separator (for folder path), if any
        sz
        FindLastSeparator(const StringView& _sv) noexcept
        {
            if (_sv.IsEmpty())
                return StringView::npos;
            const sz endSearch = (_sv.GetLast() == Path::c_separator) ? _sv.count - 1 : StringView::npos;
            return _sv.SubFront(endSearch).FindLast(Path::c_separator);
        }
    }

    StringView
    Path::GetParentDirPath(const StringView& _sv) noexcept
    {
        const sz end = _Internal::FindLastSeparator(_sv);
        return (end != StringView::npos)
            ? StringView{ _sv.data, end + 1 }
            : StringView{};
    }

    StringView
    Path::GetFullName(const StringView& _sv) noexcept
    {
        const sz begin = _Internal::FindLastSeparator(_sv);
        if (begin == StringView::npos)
            return _sv;

        const sz offset = begin + 1;
        return StringView{ _sv.data + offset, _sv.count - offset };
    }

    StringView
    Path::GetName(const StringView& _sv) noexcept
    {
        if (_sv.IsEmpty())
            return StringView{};

        sz begin = _Internal::FindLastSeparator(_sv);
        begin = (begin == StringView::npos) ? 0 : begin + 1;
        sz end = _sv.FindLast('.');
        if (end == StringView::npos || begin > end)
            end = _sv.count - (_sv.GetLast() == c_separator ? 1 : 0);

        return StringView{ _sv.data + begin, end - begin };
    }

    StringView
    Path::GetExtension(const StringView& _sv) noexcept
    {
        if (_sv.IsEmpty())
            return StringView{};

        if (_sv.GetLast() == c_separator)
        {
            const sz offset = _sv.count - 1;
            return StringView{ _sv.data + offset, 1 };
        }
        else
        {
            sz sep = _sv.FindLast(c_separator);
            sep = sep == StringView::npos ? 0 : sep + 1;
            const sz dot = _sv.FindLast('.');
            return (sep < dot && dot != StringView::npos)
                ? StringView{ _sv.data + dot, _sv.count - dot }
                : StringView{};
        }
    }

    Path&
    Path::Set(const char* const _path)
    {
        m_str = _path;
        Normalize();
        return *this;
    }

    Path&
    Path::Set(const StringView& _sv)
    {
        m_str = _sv;
        Normalize();
        return *this;
    }

    Path&
    Path::Add(const char* const _sz)
    {
        if (m_str.size() > 0)
        {
            m_str.push_back(c_separator);
            m_str.append(_sz);
            Normalize();
            return *this;
        }
        else
        {
            return Set(_sz);
        }
    }

    Path&
    Path::Add(const StringView& _sv)
    {
        if (m_str.size() > 0)
        {
            m_str.push_back(c_separator);
            m_str.append(_sv);
            Normalize();
            return *this;
        }
        else
        {
            return Set(_sv);
        }
    }

    Path&
    Path::ReplaceLast(const char* const _sz)
    {
        const sz end = m_str.find_last_of(c_separator);
        if (end != std::string::npos)
        {
            m_str.resize(end + 1);
            m_str.append(_sz);
            Normalize();
            return *this;
        }
        else
        {
            return Set(_sz);
        }
    }

    Path&
    Path::ReplaceLast(const StringView& _sv)
    {
        const sz end = m_str.find_last_of(c_separator);
        if (end != std::string::npos)
        {
            m_str.resize(end + 1);
            m_str.append(_sv);
            Normalize();
            return *this;
        }
        else
        {
            return Set(_sv);
        }
    }

    /*static*/
    bool
    Path::IsDirectory(const char* const _path)
    {
        const u32 length = (u32)::strlen(_path);
        return (length > 0) && (_path[length - 1] == c_separator);
    }

    namespace _Internal
    {
        void
        ApplyToken(char* const _token, const u32 _tokenSize, char* const _buffer, char** _b)
        {
            if (*_token == '.')
            {
                if (_tokenSize == 1)
                {
                    // token .
                    *_b = (_token == _buffer) ? _buffer : _token;
                }
                else if (_tokenSize == 2 && _token[1] == '.')
                {
                    // token ..
                    char* b = _token;
                    if (b != _buffer)
                    {
                        b = b - Min<uptr>(uptr(b) - uptr(_buffer), 2);
                        while (b != _buffer && *b != Path::c_separator)
                            --b;
                    }
                    *_b = (b == _buffer) ? b : b + 1;
                }
                else
                {
                    // something went wrong
                    DJO_FAILED_ASSERTION(false);
                }
            }
        }

        sz
        NormalizePath(const char* const _inPath, const char* const _inPathEnd, char* const _outPath)
        {
            // from  "./tata/titi\\./tutu/////../"
            // to    "tata/titi/"

            const char* p = _inPath;
            char* b = _outPath;
            char* token{ nullptr };
            u32 tokenSize{ 0 };
            while (*p && p < _inPathEnd)
            {
                const char c = *p++;
                switch (c)
                {
                    // tokens ./ or ../ starting or continuing
                    case '.':
                    {
                        if (token != nullptr)
                        {
                            // started token
                            if (*token == Path::c_separator)
                            {
                                // start new token
                                token = b;
                                tokenSize = 1;
                            }
                            else if (*token == '.')
                            {
                                if (tokenSize == 1)
                                {
                                    // continue token ../
                                    tokenSize = 2;
                                }
                                else
                                {
                                    // not a token anymore
                                    token = nullptr;
                                }
                            }
                        }
                        else
                        {
                            if (b == _outPath || *(b - 1) == Path::c_separator)
                            {
                                // start token
                                token = b;
                                tokenSize = 1;
                            }
                        }
                        *b++ = c;
                        break;
                    }

                    // replace separators, ignore multiple separators, tokens ./ or ../ ending and applying
                    case Path::c_separator:
                    case Path::c_notUsedSeparator:
                    {
                        if (token == nullptr)
                        {
                            token = b;
                            tokenSize = 1;
                            *b++ = Path::c_separator;
                        }
                        else
                        {
                            if (*token == Path::c_separator)
                            {
                                // ignore and do nothing
                            }
                            else if (*token == '.')
                            {
                                ApplyToken(token, tokenSize, _outPath, &b);
                                token = nullptr;
                            }
                        }
                        break;
                    }

                    // advance
                    default:
                    {
                        token = nullptr;
                        *b++ = c;
                        break;
                    }
                }
            }
            if (token != nullptr)
                ApplyToken(token, tokenSize, _outPath, &b);

            DJO_ASSERT(b >= _outPath);
            return uptr(b) - uptr(_outPath);
        }
    }

    /*static*/
    EResult
    Path::Normalize(const char* const _inPath, std::string* const _outPath)
    {
        DJO_ASSERT(_inPath != nullptr);
        DJO_ASSERT(_outPath != nullptr);

        const sz inPathLength = std::strlen(_inPath);
        if (inPathLength > _outPath->size())
            _outPath->resize(inPathLength);

        const sz newSize = _Internal::NormalizePath(_inPath, _inPath + inPathLength, _outPath->data());
        _outPath->resize(newSize);
        return EResult::OK;
    }

    /*static*/
    EResult
    Path::Normalize(const StringView& _inPath, std::string* const _outPath)
    {
        DJO_ASSERT(_outPath != nullptr);

        const sz inPathLength = _inPath.count;
        if (inPathLength > _outPath->size())
            _outPath->resize(_inPath.count);

        const sz newSize = _Internal::NormalizePath(&*_inPath.begin(), &*_inPath.begin() + inPathLength, _outPath->data());
        _outPath->resize(newSize);
        return EResult::OK;
    }
}
