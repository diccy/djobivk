#pragma once

#include <Core/ResultEnum.hpp>
#include <Core/StringView.hpp>
#include <Core/StructUtils.hpp>


namespace Djo
{
    // Fix separators in static C str
    #define DJO_PATH_SEP(str_)  ::Djo::_Internal::SeparatorsFix(str_).a

    class Path
    {
        DJO_DEFAULT_COPY_DEFAULT_MOVE(Path);

    public:

        static constexpr char c_separator{ '\\' };
        static constexpr char c_notUsedSeparator{ '/' };

        Path();
        /*implicit*/ Path(const char* _path);
        /*implicit*/ Path(std::string _path);
        ~Path();

        DJO_INLINE operator const std::string& () const noexcept              { return GetFullPath(); };
        DJO_INLINE bool operator == (const std::string& _str) const noexcept  { return GetFullPath() == _str; };
        DJO_INLINE bool operator != (const std::string& _str) const noexcept  { return !operator==(_str); };
        DJO_INLINE bool operator == (const StringView& _view) const noexcept  { return StringView{ GetFullPath() } == _view; };
        DJO_INLINE bool operator != (const StringView& _view) const noexcept  { return !operator==(_view); };
        DJO_INLINE bool operator == (const char* const _str) const noexcept   { return GetFullPath() == _str; };
        DJO_INLINE bool operator != (const char* const _str) const noexcept   { return !operator==(_str); };

        DJO_INLINE const std::string& GetFullPath() const noexcept  { return m_str; }
        DJO_INLINE u32 GetLength() const noexcept                   { return (u32)m_str.size(); }
        DJO_INLINE bool IsEmpty() const noexcept                    { return m_str.empty(); }

        static StringView GetParentDirPath(const StringView& _sv) noexcept;
        DJO_INLINE StringView GetParentDirPath() const noexcept         { return GetParentDirPath(StringView{ m_str }); }
        
        static StringView GetFullName(const StringView& _sv) noexcept;
        DJO_INLINE StringView GetFullName() const noexcept              { return GetFullName(StringView{ m_str }); }
        
        static StringView GetName(const StringView& _sv) noexcept;
        DJO_INLINE StringView GetName() const noexcept                  { return GetName(StringView{ m_str }); }
        
        static StringView GetExtension(const StringView& _sv) noexcept;
        DJO_INLINE StringView GetExtension() const noexcept             { return GetExtension(StringView{ m_str }); }

        Path& Set(const char* _sz);
        Path& Set(const StringView& _sv);
        DJO_INLINE Path& Set(const std::string& _path)  { return Set(_path.c_str()); }

        Path& Add(const char* const _sz);
        Path& Add(const StringView& _sv);
        DJO_INLINE Path& Add(const std::string& _s)  { return Add(_s.c_str()); }

        Path& ReplaceLast(const char* const _sz);
        Path& ReplaceLast(const StringView& _sv);
        DJO_INLINE Path& ReplaceLast(const std::string& _s)  { return ReplaceLast(_s.c_str()); }

        static EResult Normalize(const char* _inPath, std::string* _outPath);
        static EResult Normalize(const StringView& _inPath, std::string* _outPath);
        static DJO_INLINE EResult Normalize(const std::string& _inPath, std::string* const _outPath)  { return Normalize(_inPath.c_str(), _outPath); }
        static DJO_INLINE EResult Normalize(std::string* const _inOutPath)                            { return Normalize(_inOutPath->c_str(), _inOutPath); }
        DJO_INLINE EResult Normalize()                                                                { return Normalize(&m_str); }

        static bool IsDirectory(const char* _path);
        static DJO_INLINE bool IsDirectory(const StringView& _path)   { return !_path.IsEmpty() && (_path.GetLast() == c_separator); }
        static DJO_INLINE bool IsDirectory(const std::string& _path)  { return !_path.empty() && (_path.back() == c_separator); }
        DJO_INLINE bool IsDirectory() const                           { return IsDirectory(m_str); }

    protected:

        std::string m_str;
    };


    template <>
    struct Hasher<Path>
    {
        DJO_INLINE
        u64
        operator () (const Path& _path) const noexcept
        {
            return Hasher<std::string>{}(_path);
        }
    };

    namespace _Internal
    {
        template <sz N>
        constexpr
        ArrayWrapper<const char, N>
        SeparatorsFix(const char (&_str)[N]) noexcept
        {
            ArrayWrapper<const char, N> r{};
            char* p = const_cast<char*>(r.a);
            for (sz i = 0; i < N; ++i)
            {
                const char c = _str[i];
                p[i] = (c == Path::c_notUsedSeparator) ? Path::c_separator : c;
            }
            return r;
        }
    }
}
