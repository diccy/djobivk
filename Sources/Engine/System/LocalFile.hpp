#pragma once

#include <System/IFile.hpp>
#include <System/Path.hpp>

#include <fstream>


namespace Djo
{
    class LocalFile
        : public IFile
    {
        DJO_NO_COPY_DEFAULT_MOVE(LocalFile);

    public:

        LocalFile();
        explicit LocalFile(Path _path);
        virtual ~LocalFile() override;

        DJO_INLINE const Path& GetPath() const  { return m_path; }
        DJO_INLINE u32 GetOpenMode() const      { return m_openMode; }
        DJO_INLINE void SetPath(Path _path)     { m_path = std::move(_path); }

        virtual sz GetSize() const override;
        virtual bool IsOpen() const override;

        EResult Open(Path _path, u32 _openMode);
        virtual EResult Open(u32 _openMode = c_defaultFileOpenMode) override;
        virtual EResult Close() override;

        virtual EResult Read(std::vector<u8>* _outContent) override;
        virtual EResult Read(std::string* _outContent) override;
        virtual EResult Read(const MemView<u8>& _mem, sz* _outReadSize) override;
        virtual EResult Write(const std::string& _txt, DoFlushPendingWritings _flush = { false }) override;
        virtual EResult Write(const MemView<const u8>& _mem, DoFlushPendingWritings _flush = { false }) override;
        virtual EResult FlushPendingWritings() override;

        sz ComputeSize();

    protected:

        friend class LocalFileSystem;
        LocalFile(Path _path, sz _size);

        EResult EnsureValidSize();
        EResult Write_Internal(const char* _src, sz _size, DoFlushPendingWritings _flush);
        EResult Read_Internal(char* _dst, sz _size);

        std::fstream m_fstream;
        Path m_path;
        sz m_size;
        u32 m_openMode;
    };
}
