
#include <System/LocalFileSystem.hpp>

#include <Platform/WindowsHeader.hpp>


namespace Djo
{
    LocalFileSystem::LocalFileSystem()
        : m_root{}
        , m_hash{ c_invalidUint<u64> }
    {
    }

    LocalFileSystem::~LocalFileSystem()
    {
    }

    EResult
    LocalFileSystem::Init(const Path& _root)
    {
        if (!_root.IsDirectory())
            return MakeResult(EResult::FAIL, "Root path is not formatted as a directory");

        Path absoluteRootPath{ AbsolutePath(_root) };
        if (!DoesExist(absoluteRootPath.GetFullPath().c_str()))
        {
            auto BuildDirectory = [](const StringView& _path, auto&& _buildDirectoryRec, const u32 _rec) -> bool
            {
                const char backChar = _path.GetLast();
                const_cast<char&>(_path.GetLast()) = '\0'; // hack string view to work with zero-ended C string
                bool doesExist = DoesExist(_path.data);
                if (!doesExist && _rec > 0)
                {
                    const bool doesParentExist = _buildDirectoryRec(Path::GetParentDirPath(_path), _buildDirectoryRec, _rec - 1);
                    if (doesParentExist)
                        doesExist = (bool)::CreateDirectory(_path.data, NULL);
                }
                const_cast<char&>(_path.GetLast()) = backChar;
                return doesExist;
            };

            if (!BuildDirectory(StringView{ absoluteRootPath.GetFullPath() }, BuildDirectory, 20)) // arbitrary max rec count
                return MakeResult(EResult::FAIL, "Impossible to create directory");
        }

        if (!IsDirectory(absoluteRootPath.GetFullPath().c_str()))
            return MakeResult(EResult::FAIL, "Root path is not a directory");

        m_root = _root;
        m_hash = Hasher<Path>{}(m_root);
        return EResult::OK;
    }

    u64
    LocalFileSystem::GetHash(const Path& _path) const
    {
        return Hasher<u64[2]>{}({ m_hash, Hasher<Path>{}(_path) });
    }

    LocalFile*
    LocalFileSystem::NewFile(const Path& _path, const DoComputeSize _doComputeSize /*= false*/) const
    {
        return DJO_CORE_NEW(LocalFile){ GetLocalFile(_path, _doComputeSize) };
    }

    LocalFile
    LocalFileSystem::GetLocalFile(const Path& _path, const DoComputeSize _doComputeSize /*= false*/) const
    {
        Path absolutePath{ AbsolutePath(_path) };
        if (_doComputeSize)
            return LocalFile{ std::move(absolutePath), GetSize(absolutePath) };
        else
            return LocalFile{ std::move(absolutePath) };
    }

    bool
    LocalFileSystem::DoesFileExist(const Path& _path) const
    {
        Path absolutePath{ m_root };
        absolutePath.Add(_path);
        return DoesExist(absolutePath);
    }

    sz
    LocalFileSystem::GetFileSize(const Path& _path) const
    {
        Path absolutePath{ m_root };
        absolutePath.Add(_path);
        return GetSize(absolutePath);
    }

    std::string
    LocalFileSystem::AsPrettyString(const Path& _path) const
    {
        return AbsolutePath(_path).GetFullPath();
    }

    // Writes in _outPath only if function returns EResult::OK
    // Can be called as: AbsolutePath(path, &path)
    EResult
    LocalFileSystem::AbsolutePath(const Path& _localPath, Path* _outAbsolutePath) const
    {
        DJO_ASSERT(_outAbsolutePath != nullptr);

        Path absolutePath{ m_root };
        absolutePath.Add(_localPath);
        DJO_OK_OR_RETURN(Normalize(&absolutePath));
        *_outAbsolutePath = std::move(absolutePath);
        return EResult::OK;
    }

    Path
    LocalFileSystem::AbsolutePath(const Path& _localPath) const
    {
        Path absolutePath{};
        AbsolutePath(_localPath, &absolutePath);
        return absolutePath;
    }

    /*static*/
    bool
    LocalFileSystem::DoesExist(const char* const _path)
    {
        const DWORD fileAttr = ::GetFileAttributesA(_path);
        return fileAttr != INVALID_FILE_ATTRIBUTES;
    }

    /*static*/
    sz
    LocalFileSystem::GetSize(const char* const _path)
    {
        // https://stackoverflow.com/questions/14628141/faster-way-to-get-file-size-information-c
        WIN32_FIND_DATAA data;
        HANDLE h = ::FindFirstFileA(_path, &data);
        if (h == INVALID_HANDLE_VALUE)
            return c_invalidFileSize;

        ::FindClose(h);
        return ((sz)data.nFileSizeHigh << 32ull) | (sz)data.nFileSizeLow;
    }

    /*static*/
    bool
    LocalFileSystem::IsDirectory(const char* const _path)
    {
        const DWORD fileAttr = ::GetFileAttributesA(_path);
        return (fileAttr != INVALID_FILE_ATTRIBUTES) && (TestFlag<DWORD>(fileAttr, FILE_ATTRIBUTE_DIRECTORY));
    }

    // Writes in _outPath only if function returns EResult::OK
    // Can be called as: Normalize(path.GetFullPath().c_str(), &path)
    /*static*/
    EResult
    LocalFileSystem::Normalize(const char* const _inPath, Path* _outPath)
    {
        DJO_ASSERT(_inPath != nullptr);
        DJO_ASSERT(_inPath[0] != char{ 0 });
        DJO_ASSERT(_outPath != nullptr);

        constexpr u32 c_fullPathBufferSize{ 512 };
        char fullPathBuffer[c_fullPathBufferSize + 2] = { 0 }; // +2 -> thx Windows...
        const s32 fullPathLength = ::GetFullPathNameA(_inPath, c_fullPathBufferSize, fullPathBuffer, nullptr);
        DJO_ASSERT(fullPathLength < c_fullPathBufferSize);
        if (fullPathLength == 0)
            return MakeResult(EResult::FAIL, "GetFullPathNameA failed");

        _outPath->Set(fullPathBuffer);
        return EResult::OK;
    }
}