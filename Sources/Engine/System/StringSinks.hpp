#pragma once

#include <System/LocalFile.hpp>
#include <Core/IStringSink.hpp>


namespace Djo
{
    class StringSinkStdCout
        : public IStringSink
    {
    public:

        virtual void Output(const std::string& _str) override;
    };

    class StringSinkVisualConsoleOutput
        : public IStringSink
    {
    public:

        virtual void Output(const std::string& _str) override;
    };

    class StringSinkFile
        : public IStringSink
    {
        DJO_NO_DEFAULT_CTOR(StringSinkFile);
        DJO_NO_COPY_NO_MOVE(StringSinkFile);

    public:

        explicit StringSinkFile(const Path& _filePath);
        ~StringSinkFile();

        DJO_INLINE const LocalFile& GetFile() const  { return m_file; }

        virtual void Output(const std::string& _str) override;

    private:

        LocalFile m_file;
    };
}
