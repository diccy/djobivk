#pragma once

#include <System/IFileSystem.hpp>


namespace Djo
{
    class VirtualFileSystem
        : public IFileSystem
    {
    public:

        VirtualFileSystem();
        virtual ~VirtualFileSystem() override;

        virtual u64 GetHash(const Path& _path) const override;
        virtual IFile* NewFile(const Path& _path, DoComputeSize _doComputeSize = { false }) const override;
        virtual bool DoesFileExist(const Path& _path) const override;
        virtual sz GetFileSize(const Path& _path) const override;
        virtual std::string AsPrettyString(const Path& _path) const override;

        EResult MountFileSystem(IFileSystem* _fileSystem);
        EResult UnmountFileSystem(IFileSystem* _fileSystem);

        IFileSystem* GetFileSystemWhereExists(const Path& _path) const;

    private:

        std::vector<IFileSystem*> m_fileSystems;
    };
}
