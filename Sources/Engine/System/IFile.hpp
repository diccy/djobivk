#pragma once

#include <Core/StdVector.hpp>
#include <Core/StdString.hpp>
#include <Core/IntrusiveRefCount.hpp>
#include <Core/ResultEnum.hpp>
#include <Core/TypeTraits.hpp>
#include <Core/StructUtils.hpp>


namespace Djo
{
    // Mimic std::ios_base::openmode
    enum class EOpenModeFlag : u32
    {
        Read = Flag(0),
        Write = Flag(1),
        Binary = Flag(2),
        AtEnd = Flag(3),
        Append = Flag(4),
        Truncate = Flag(5),

        DJO_ENUM_FLAG_COUNT
    };

    constexpr u32 c_defaultFileOpenMode{ EOpenModeFlag::Read | EOpenModeFlag::Write | EOpenModeFlag::Binary };

    using DoFlushPendingWritings = HardAlias<bool>;

    static constexpr sz c_invalidFileSize{ c_invalidUint<sz> };

    class IFile
        : public IntrusiveRefCount
    {
    public:

        virtual ~IFile() = default;

        virtual sz GetSize() const = 0;
        virtual bool IsOpen() const = 0;

        virtual EResult Open(u32 _openMode = c_defaultFileOpenMode) = 0;
        virtual EResult Close() = 0;

        virtual EResult Read(std::vector<u8>* _outContent) = 0;
        virtual EResult Read(std::string* _outContent) = 0;
        virtual EResult Read(const MemView<u8>& _mem, sz* _outReadSize) = 0;
        virtual EResult Write(const std::string& _txt, DoFlushPendingWritings _flush = { false }) = 0;
        virtual EResult Write(const MemView<const u8>& _mem, DoFlushPendingWritings _flush = { false }) = 0;
        virtual EResult FlushPendingWritings() = 0;
    };
}
