#pragma once

#include <System/LocalFileSystem.hpp>
#include <System/VirtualFileSystem.hpp>


namespace Djo
{
    namespace GlobalFS
    {
        extern VirtualFileSystem* g_vfs;
        extern LocalFileSystem* g_engineLocalFileSystem;
        extern LocalFileSystem* g_appLocalFileSystem;

        EResult Init(const Path& _appAssetsLocalPath);
        void Shutdown();
    }
}

#define DJO_VFS        ::Djo::GlobalFS::g_vfs
#define DJO_ENGINE_FS  ::Djo::GlobalFS::g_engineLocalFileSystem
#define DJO_APP_FS     ::Djo::GlobalFS::g_appLocalFileSystem
