
#include <System/SimpleProfiler.hpp>

#include <Core/StdStringStream.hpp>
#include <Core/GlobalLog.hpp>
#include <Core/TypeTraits.hpp>

#include <iomanip>


namespace Djo
{
    SimpleProfiler::SimpleProfiler()
        : m_sessionFilePath{}
        , m_sessionFile{}
        , m_mutex{}
        , m_allowRecord{ true }
    {}

    SimpleProfiler::~SimpleProfiler()
    {
        EndSession();
    }

    void
    SimpleProfiler::AllowRecording(const bool _allow)
    {
        std::lock_guard lock(m_mutex);

        if (_allow != m_allowRecord)
        {
            m_allowRecord = _allow;
            if (m_allowRecord)
            {
                if (!m_sessionFilePath.IsEmpty())
                    BeginSession_NoLock(m_sessionFilePath);
            }
            else
            {
                EndSession_NoLock();
            }
        }
    }

    EResult
    SimpleProfiler::BeginSession(Path _filePath)
    {
        std::lock_guard lock(m_mutex);
        return BeginSession_NoLock(std::move(_filePath));
    }

    EResult
    SimpleProfiler::BeginSession_NoLock(Path _filePath)
    {
        if (m_sessionFile.IsOpen())
            EndSession_NoLock();

        m_sessionFilePath = std::move(_filePath);
        if (m_allowRecord)
        {
            DJO_LOG_CORE_TRACE << "SimpleProfiler: Open " << m_sessionFilePath.GetFullName() << std::endl;

            DJO_OK_OR_RETURN(m_sessionFile.Open(m_sessionFilePath, EOpenModeFlag::Write | EOpenModeFlag::Truncate));
            DJO_OK_OR_RETURN(m_sessionFile.Write("{\"otherData\": {},\"traceEvents\":[{}")); // header
        }

        return EResult::OK;
    }

    EResult
    SimpleProfiler::WriteProfile(const StringView& _name, const s64 _start, const s64 _duration, const std::thread::id _threadId)
    {
        std::lock_guard lock(m_mutex);

        if (m_allowRecord && m_sessionFile.IsOpen())
        {
            DJO_TODO("set stringstream as member and write in file only on EndSession?");

            std::stringstream json;
            json << std::setprecision(3) << std::fixed;
            json << ",{";
            json << "\"cat\":\"function\",";
            json << "\"dur\":" << _duration << ',';
            json << "\"name\":\""; json.write(_name.data, _name.count); json << "\",";
            json << "\"ph\":\"X\",";
            json << "\"pid\":0,";
            json << "\"tid\":" << _threadId << ",";
            json << "\"ts\":" << _start;
            json << "}";

            DJO_OK_OR_RETURN(m_sessionFile.Write(json.str()));
        }

        return EResult::OK;
    }

    EResult
    SimpleProfiler::EndSession()
    {
        std::lock_guard lock(m_mutex);
        return EndSession_NoLock();
    }

    EResult
    SimpleProfiler::EndSession_NoLock()
    {
        if (m_sessionFile.IsOpen())
        {
            DJO_OK_OR_RETURN(m_sessionFile.Write("]}")); // footer

            DJO_LOG_CORE_TRACE << "SimpleProfiler: Close " << m_sessionFilePath.GetFullName() << std::endl;

            m_sessionFile.Close();
            m_sessionFilePath = Path{};
        }
        return EResult::OK;
    }


    SimpleProfilerTimer::SimpleProfilerTimer(SimpleProfiler* _profiler, const StringView& _name)
        : m_profiler{ _profiler }
        , m_name{ _name }
        , m_start{}
    {
        if (m_profiler != nullptr)
            m_start = std::chrono::time_point_cast<TimePoint::duration>(std::chrono::steady_clock::now());
    }

    SimpleProfilerTimer::~SimpleProfilerTimer()
    {
        if (m_profiler != nullptr)
        {
            TimePoint end = std::chrono::time_point_cast<TimePoint::duration>(std::chrono::steady_clock::now());
            TimePoint::duration elapsed = end.time_since_epoch() - m_start.time_since_epoch();
            m_profiler->WriteProfile(m_name, m_start.time_since_epoch().count(), elapsed.count(), std::this_thread::get_id());
        }
    }
}
