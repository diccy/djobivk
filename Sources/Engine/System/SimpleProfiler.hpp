#pragma once

#include <System/LocalFile.hpp>
#include <System/Path.hpp>

#include <chrono>
#include <thread>
#include <mutex>


namespace Djo
{
    class SimpleProfiler
    {
        DJO_NO_COPY_NO_MOVE(SimpleProfiler);

    public:

        SimpleProfiler();
        ~SimpleProfiler();

        void AllowRecording(bool _allow);
        DJO_INLINE bool IsRecordingAllowed()  { return m_allowRecord; }
        EResult BeginSession(Path _filePath);
        EResult WriteProfile(const StringView& _name, s64 _start, s64 _duration, std::thread::id _threadId);
        EResult EndSession();

    private:

        EResult BeginSession_NoLock(Path _filePath);
        EResult EndSession_NoLock();

        Path m_sessionFilePath;
        LocalFile m_sessionFile;
        std::mutex m_mutex;
        bool m_allowRecord;
    };

    class SimpleProfilerTimer
    {
        DJO_NO_DEFAULT_CTOR(SimpleProfilerTimer);
        DJO_NO_COPY_NO_MOVE(SimpleProfilerTimer);

    public:

        SimpleProfilerTimer(SimpleProfiler* _profiler, const StringView& _name);
        ~SimpleProfilerTimer();

    private:

        using TimePoint = std::chrono::time_point<std::chrono::steady_clock, std::chrono::microseconds>;

        SimpleProfiler* m_profiler;
        const StringView m_name;
        TimePoint m_start;
    };
}
