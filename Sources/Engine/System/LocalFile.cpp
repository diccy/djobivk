
#include <System/LocalFile.hpp>

#include <System/LocalFileSystem.hpp>
#include <Core/TypeTraits.hpp>
#include <Core/Utils.hpp>


namespace Djo
{
    LocalFile::LocalFile()
        : m_fstream{}
        , m_path{}
        , m_size{ c_invalidFileSize }
        , m_openMode{ 0 }
    {
    }

    LocalFile::LocalFile(Path _path)
        : m_fstream{}
        , m_path{ std::move(_path) }
        , m_size{ c_invalidFileSize }
        , m_openMode{ 0 }
    {
    }

    // for LocalFileSystem usage only
    LocalFile::LocalFile(Path _path, const sz _size)
        : m_fstream{}
        , m_path{ std::move(_path) }
        , m_size{ _size }
        , m_openMode{ 0 }
    {
    }

    LocalFile::~LocalFile()
    {
        Close();
    }

    sz
    LocalFile::GetSize() const
    {
        return m_size;
    }

    bool
    LocalFile::IsOpen() const
    {
        return m_fstream.is_open();
    }

    sz
    LocalFile::ComputeSize()
    {
        if (IsOpen() && !m_fstream.fail())
        {
            const std::fstream::pos_type pos = m_fstream.tellg();
            m_fstream.seekg(0, std::ios_base::beg);
            m_fstream.ignore(std::numeric_limits<std::streamsize>::max());
            m_size = m_fstream.gcount();
            m_fstream.clear();
            m_fstream.seekg(pos, std::ios_base::beg);
        }
        return m_size;
    }

    EResult
    LocalFile::EnsureValidSize()
    {
        if (m_size == c_invalidFileSize)
        {
            ComputeSize();
            if (m_size == c_invalidFileSize)
                return MakeResult(EResult::FAIL, "Invalid cached file size");
        }
        return EResult::OK;
    }

    namespace _Internal
    {
        constexpr
        std::fstream::openmode
        GetStdOpenMode(const u32 _mode)
        {
            constexpr std::fstream::openmode c_lookup[EFlagCount<EOpenModeFlag>]
            {
                std::fstream::in,
                std::fstream::out,
                std::fstream::binary,
                std::fstream::ate,
                std::fstream::app,
                std::fstream::trunc
            };
            std::fstream::openmode stdMode = 0;
            for (u32 i = 0; i < EFlagCount<EOpenModeFlag>; ++i)
                if (TestFlag(_mode, Flag(i)))
                    stdMode |= c_lookup[i];

            return stdMode;
        }
    }

    EResult
    LocalFile::Open(Path _path, const u32 _openMode)
    {
        Close();
        m_path = std::move(_path);
        return Open(_openMode);
    }

    EResult
    LocalFile::Open(const u32 _openMode /*= c_defaultFileOpenMode*/)
    {
        m_openMode = _openMode;
        const std::fstream::openmode stdMode = _Internal::GetStdOpenMode(_openMode);

        m_fstream.clear();
        m_fstream.open(m_path.GetFullPath().c_str(), stdMode);
        if (!IsOpen() || m_fstream.fail())
        {
            DJO_FAILED_ASSERTION(false); // to have a breakpoint before the stream clear
            m_fstream.clear();
            return MakeResult(EResult::FAIL, "std::fstream open failed");
        }

        m_size = c_invalidFileSize;
        return EResult::OK;
    }

    EResult
    LocalFile::Close()
    {
        if (IsOpen())
        {
            m_fstream.clear();
            m_fstream.close();
            if (IsOpen() || m_fstream.fail())
            {
                DJO_FAILED_ASSERTION(false); // to have a breakpoint before the stream clear
                m_fstream.clear();
                return MakeResult(EResult::FAIL, "std::fstream close failed");
            }
        }
        return EResult::OK;
    }

    EResult
    LocalFile::Read_Internal(char* const _dst, const sz _size)
    {
        m_fstream.clear();
        m_fstream.read(_dst, _size);
        if (m_fstream.bad())
        {
            DJO_FAILED_ASSERTION(false); // to have a breakpoint before the stream clear
            m_fstream.clear();
            return MakeResult(EResult::FAIL, "std::fstream read failed");
        }

        return EResult::OK;
    }

    EResult
    LocalFile::Read(std::vector<u8>* _outContent)
    {
        DJO_OK_OR_RETURN(EnsureValidSize());
        _outContent->resize(m_size);
        return Read_Internal(reinterpret_cast<char*>(_outContent->data()), _outContent->size());
    }

    EResult
    LocalFile::Read(std::string* _outContent)
    {
        DJO_OK_OR_RETURN(EnsureValidSize());
        _outContent->resize(m_size);
        return Read_Internal(_outContent->data(), _outContent->size());
    }

    EResult
    LocalFile::Read(const MemView<u8>& _mem, sz* const _outReadSize)
    {
        DJO_OK_OR_RETURN(EnsureValidSize());
        *_outReadSize = Min<sz>(m_size, _mem.GetMemSize());
        return Read_Internal(reinterpret_cast<char*>(_mem.data), *_outReadSize);
    }

    EResult
    LocalFile::Write_Internal(const char* const _src, const sz _size, const DoFlushPendingWritings _flush)
    {
        m_fstream.write(_src, _size);
        if (m_fstream.fail())
        {
            DJO_FAILED_ASSERTION(false); // to have a breakpoint before the stream clear
            m_fstream.clear();
            return MakeResult(EResult::FAIL, "std::fstream write failed");
        }

        if (_flush)
            DJO_OK_OR_RETURN(FlushPendingWritings());

        return EResult::OK;
    }

    EResult
    LocalFile::Write(const std::string& _txt, const DoFlushPendingWritings _flush /*= false*/)
    {
        return Write_Internal(_txt.c_str(), _txt.size(), _flush);
    }

    EResult
    LocalFile::Write(const MemView<const u8>& _mem, const DoFlushPendingWritings _flush /*= false*/)
    {
        return Write_Internal(reinterpret_cast<const char*>(_mem.data), _mem.GetMemSize(), _flush);
    }

    EResult
    LocalFile::FlushPendingWritings()
    {
        m_fstream.flush();
        if (m_fstream.fail())
        {
            DJO_FAILED_ASSERTION(false); // to have a breakpoint before the stream clear
            m_fstream.clear();
            return MakeResult(EResult::FAIL, "std::fstream flush failed");
        }
        return EResult::OK;
    }
}
