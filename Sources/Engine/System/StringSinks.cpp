
#include <System/StringSinks.hpp>

#include <System/Path.hpp>
#include <Platform/WindowsHeader.hpp> // OutputDebugStringA

#include <iostream>


namespace Djo
{
    // StringSinkStdCout

    void
    StringSinkStdCout::Output(const std::string& _str)
    {
        std::cout << _str;
    }


    // StringSinkVisualConsoleOutput

    void
    StringSinkVisualConsoleOutput::Output(const std::string& _str)
    {
        ::OutputDebugStringA(_str.c_str());
    }


    // StringSinkFile

    StringSinkFile::StringSinkFile(const Path& _filePath)
        : m_file{}
    {
        m_file.Open(_filePath, EOpenModeFlag::Write | EOpenModeFlag::Truncate);
    }

    StringSinkFile::~StringSinkFile()
    {
        m_file.Close();
    }

    void
    StringSinkFile::Output(const std::string& _str)
    {
        m_file.Write(_str);
    }
}
