
#include <System/VirtualFileSystem.hpp>

#include <System/Path.hpp>


namespace Djo
{
    VirtualFileSystem::VirtualFileSystem()
        : m_fileSystems{}
    {
    }

    VirtualFileSystem::~VirtualFileSystem()
    {
    }

    EResult
    VirtualFileSystem::MountFileSystem(IFileSystem* const _fileSystem)
    {
        if (_fileSystem == nullptr)
            return MakeResult(EResult::FAIL, "Null file system");

        // append if unique
        if (std::find(m_fileSystems.begin(), m_fileSystems.end(), _fileSystem) == m_fileSystems.end())
            m_fileSystems.emplace_back(_fileSystem);

        return EResult::OK;
    }

    EResult
    VirtualFileSystem::UnmountFileSystem(IFileSystem* const _fileSystem)
    {
        RemoveFromVector(m_fileSystems, _fileSystem);
        return EResult::OK;
    }

    u64
    VirtualFileSystem::GetHash(const Path& _path) const
    {
        const IFileSystem* const fileSystem = GetFileSystemWhereExists(_path);
        if (fileSystem == nullptr)
        {
            DJO_ASSERT_MSG(false, "Path not found in file systems");
            return 0;
        }
        return fileSystem->GetHash(_path);
    }

    IFile*
    VirtualFileSystem::NewFile(const Path& _path, const DoComputeSize _doComputeSize /*= false*/) const
    {
        const IFileSystem* const fileSystem = GetFileSystemWhereExists(_path);
        if (fileSystem == nullptr)
        {
            DJO_ASSERT_MSG(false, "Path not found in file systems");
            return nullptr;
        }
        return fileSystem->NewFile(_path, _doComputeSize);
    }

    bool
    VirtualFileSystem::DoesFileExist(const Path& _path) const
    {
        return (GetFileSystemWhereExists(_path) != nullptr);
    }

    sz
    VirtualFileSystem::GetFileSize(const Path& _path) const
    {
        const IFileSystem* const fileSystem = GetFileSystemWhereExists(_path);
        if (fileSystem == nullptr)
        {
            DJO_ASSERT_MSG(false, "Path not found in file systems");
            return c_invalidFileSize;
        }
        return fileSystem->GetFileSize(_path);
    }

    std::string
    VirtualFileSystem::AsPrettyString(const Path& _path) const
    {
        const IFileSystem* const fileSystem = GetFileSystemWhereExists(_path);
        if (fileSystem == nullptr)
        {
            DJO_ASSERT_MSG(false, "Path not found in file systems");
            return _path.GetFullPath();
        }
        return fileSystem->AsPrettyString(_path);
    }

    IFileSystem*
    VirtualFileSystem::GetFileSystemWhereExists(const Path& _path) const
    {
        const std::vector<IFileSystem*>::const_reverse_iterator ritEnd = m_fileSystems.crend();
        for (std::vector<IFileSystem*>::const_reverse_iterator rit = m_fileSystems.crbegin(); rit != ritEnd; ++rit)
        {
            IFileSystem* const fileSystem = *rit;
            if (fileSystem != nullptr && fileSystem->DoesFileExist(_path))
                return fileSystem;
        }
        return nullptr;
    }
}
