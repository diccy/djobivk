#pragma once

#include <System/IFileSystem.hpp>
#include <System/LocalFile.hpp>


namespace Djo
{
    class LocalFileSystem
        : public IFileSystem
    {
    public:

        LocalFileSystem();
        virtual ~LocalFileSystem() override;

        EResult Init(const Path& _root);
        virtual u64 GetHash(const Path& _path) const override;
        virtual LocalFile* NewFile(const Path& _path, DoComputeSize _doComputeSize = { false }) const override;
        virtual bool DoesFileExist(const Path& _path) const override;
        virtual sz GetFileSize(const Path& _path) const override;
        virtual std::string AsPrettyString(const Path& _path) const override;

        LocalFile GetLocalFile(const Path& _path, DoComputeSize _doComputeSize = { false }) const;

        DJO_INLINE const Path& GetRootPath() const  { return m_root; }
        EResult AbsolutePath(const Path& _localPath, Path* _outAbsolutePath) const;
        Path AbsolutePath(const Path& _localPath) const;

        static bool DoesExist(const char* _path);
        static DJO_INLINE bool DoesExist(const Path& _path)  { return DoesExist(_path.GetFullPath().c_str()); }

        static sz GetSize(const char* _path);
        static DJO_INLINE sz GetSize(const Path& _path)  { return GetSize(_path.GetFullPath().c_str()); }

        static bool IsDirectory(const char* _path);
        static DJO_INLINE bool IsDirectory(const Path& _path)  { return IsDirectory(_path.GetFullPath().c_str()); }

        static EResult Normalize(const char* _inPath, Path* _outPath);
        static DJO_INLINE EResult Normalize(const Path& _inPath, Path* const _outPath)  { return Normalize(_inPath.GetFullPath().c_str(), _outPath); }
        static DJO_INLINE EResult Normalize(Path* const _inOutPath)                     { return Normalize(_inOutPath->GetFullPath().c_str(), _inOutPath); }

    protected:

        Path m_root;
        u64 m_hash;
    };
}
