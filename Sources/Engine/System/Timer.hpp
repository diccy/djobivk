#pragma once

#include <Core/Time.hpp>

#include <chrono>


namespace Djo
{
    class Timer
    {
        DJO_NO_COPY_NO_MOVE(Timer);

    public:

        Timer() noexcept;
        ~Timer() noexcept;

        void Start() noexcept;
        Microseconds64 Get() const noexcept;
        Microseconds64 Restart() noexcept;
        Microseconds64 Stop() noexcept;

    private:

        using TimePoint = std::chrono::time_point<std::chrono::steady_clock, std::chrono::microseconds>;

        TimePoint m_started;
        TimePoint m_stopped;
    };
}
