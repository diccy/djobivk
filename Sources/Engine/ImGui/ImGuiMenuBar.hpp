#pragma once

#include <Core/ClassTraits.hpp>
#include <Core/StdVector.hpp>
#include <Core/StdString.hpp>


namespace Djo
{
    // fw
    class IImGuiMenuItem;

    class ImGuiMenuBar
    {
        DJO_NO_COPY_NO_MOVE(ImGuiMenuBar);

    public:

        ImGuiMenuBar();
        ~ImGuiMenuBar();

        void RegisterMenuBarItem(const char* _menuName, IImGuiMenuItem* _item);
        void UnregisterMenuBarItem(const char* _menuName, IImGuiMenuItem* _item);
        virtual bool UpdateMenuBar();

    protected:

        void UpdateMenus();

    private:

        struct Menu
        {
            std::string name;
            std::vector<IImGuiMenuItem*> items;
        };
        std::vector<Menu> m_menus;
    };

    class ImGuiMainMenuBar
        : public ImGuiMenuBar
    {
    public:

        virtual bool UpdateMenuBar() override;
    };
}
