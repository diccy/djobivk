#pragma once

#include <ImGui/ImGuiMenuBar.hpp>


namespace Djo
{
    // fw
    class IImGuiMenuItem;
    class IImGuiWindow;

    class ImGuiManager
    {
        DJO_NO_COPY_NO_MOVE(ImGuiManager);

    public:

        ImGuiManager(bool _defaultVisibility = false);
        ~ImGuiManager();

        DJO_INLINE ImGuiMainMenuBar& GetMainMenuBar()  { return m_mainMenuBar; }

        DJO_INLINE bool GetVisibility() const                  { return m_isVisible; }
        DJO_INLINE void SetVisibility(const bool _visibility)  { m_isVisible = _visibility; }
        DJO_INLINE void ToggleVisibility()                     { SetVisibility(!m_isVisible); }

        void RegisterMainMenuBarItem(IImGuiMenuItem* _item);
        void UnregisterMainMenuBarItem(IImGuiMenuItem* _item);
        void RegisterWindow(IImGuiWindow* _window);
        void UnregisterWindow(IImGuiWindow* _window);

        void Update();

    private:

        ImGuiMainMenuBar m_mainMenuBar;
        std::vector<IImGuiWindow*> m_windows;
        bool m_isVisible;
    };
}
