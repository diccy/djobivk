#pragma once


namespace Djo
{
    class IImGuiMenuItem
    {
    public:
        virtual const char* GetMenuName() const = 0;
        virtual void UpdateImGuiMenuItem() = 0;
    };

    class IImGuiWindow
    {
    public:
        virtual void UpdateImGuiWindow() = 0;
    };
}
