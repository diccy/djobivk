
#include <ImGui/ImGuiMenuBar.hpp>

#include <ImGui/ImGui.hpp>
#include <ImGui/ImGuiInterfaces.hpp>

#include <algorithm>


namespace Djo
{
    ImGuiMenuBar::ImGuiMenuBar()
        : m_menus{}
    {
    }

    ImGuiMenuBar::~ImGuiMenuBar()
    {
        DJO_ASSERT(m_menus.empty());
    }

    void
    ImGuiMenuBar::RegisterMenuBarItem(const char* _menuName, IImGuiMenuItem* const _item)
    {
        const auto itFindMenu = std::find_if(m_menus.begin(), m_menus.end(),
            [_menuName](const Menu& _m) { return _m.name == _menuName; }
        );
        Menu& menu = (itFindMenu == m_menus.end()) ? m_menus.emplace_back(Menu{ _menuName, {} }) : *itFindMenu;
        menu.items.emplace_back(_item);
    }

    void
    ImGuiMenuBar::UnregisterMenuBarItem(const char* _menuName, IImGuiMenuItem* const _item)
    {
        const auto itFindMenu = std::find_if(m_menus.begin(), m_menus.end(),
            [_menuName](const Menu& _m) { return _m.name == _menuName; }
        );
        if (itFindMenu != m_menus.end())
        {
            auto& menuItems = itFindMenu->items;
            const auto itFindItem = std::find(menuItems.begin(), menuItems.end(), _item);
            if (itFindItem != menuItems.end())
            {
                menuItems.erase(itFindItem);
                if (menuItems.empty())
                    m_menus.erase(itFindMenu);
            }
        }
    }

    void
    ImGuiMenuBar::UpdateMenus()
    {
        for (Menu& menu : m_menus)
        {
            if (::ImGui::BeginMenu(menu.name.c_str()))
            {
                for (IImGuiMenuItem* const item : menu.items)
                {
                    item->UpdateImGuiMenuItem();
                }
                ::ImGui::EndMenu();
            }
        }
    }

    bool
    ImGuiMenuBar::UpdateMenuBar()
    {
        if (::ImGui::BeginMenuBar())
        {
            UpdateMenus();
            ::ImGui::EndMenuBar();
        }
        return true;
    }

    bool
    ImGuiMainMenuBar::UpdateMenuBar()
    {
        bool closeMenu = true;
        if (::ImGui::BeginMainMenuBar())
        {
            closeMenu = ::ImGui::MenuItem("X");
            UpdateMenus();
            ::ImGui::EndMainMenuBar();
        }
        return !closeMenu;
    }
}
