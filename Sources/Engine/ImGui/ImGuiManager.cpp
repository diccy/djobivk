
#include <ImGui/ImGuiManager.hpp>

#include <ImGui/ImGuiInterfaces.hpp>


namespace Djo
{
    ImGuiManager::ImGuiManager(bool _defaultVisibility /*= false*/)
        : m_mainMenuBar{}
        , m_windows{}
        , m_isVisible{ _defaultVisibility }
    {
    }

    ImGuiManager::~ImGuiManager()
    {
    }

    void
    ImGuiManager::RegisterMainMenuBarItem(IImGuiMenuItem* const _item)
    {
        m_mainMenuBar.RegisterMenuBarItem(_item->GetMenuName(), _item);
    }

    void
    ImGuiManager::UnregisterMainMenuBarItem(IImGuiMenuItem* const _item)
    {
        m_mainMenuBar.UnregisterMenuBarItem(_item->GetMenuName(), _item);
    }

    void
    ImGuiManager::RegisterWindow(IImGuiWindow* const _window)
    {
        m_windows.emplace_back(_window);
    }

    void
    ImGuiManager::UnregisterWindow(IImGuiWindow* const _window)
    {
        RemoveFromVector(m_windows, _window);
    }

    void
    ImGuiManager::Update()
    {
        if (m_isVisible)
        {
            m_isVisible = m_mainMenuBar.UpdateMenuBar();
            if (m_isVisible)
            {
                for (IImGuiWindow* const window : m_windows)
                {
                    window->UpdateImGuiWindow();
                }
            }
        }
    }
}
