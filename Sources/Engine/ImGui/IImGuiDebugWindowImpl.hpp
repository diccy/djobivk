#pragma once

#include <ImGui/ImGuiInterfaces.hpp>


namespace Djo
{
    class IImGuiDebugWindowImpl
        : public IImGuiMenuItem
        , public IImGuiWindow
    {
    };
}
