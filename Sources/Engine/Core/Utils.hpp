#pragma once

#include <Core/Core.hpp>
#include <Core/Types.hpp>


namespace Djo
{
    // Type utils

    template <typename T, u32 TSize>
    constexpr
    u32
    ArraySize(const T(&)[TSize]) noexcept
    {
        return TSize;
    }

    // Numeric utils

    template <typename T>
    constexpr
    T
    GetFirstBitIndex(T _n) noexcept
    {
        DJO_STATIC_ASSERT(std::is_integral_v<T>);

        if (_n == 0)
            return c_invalidUint<T>;

        T i = 0;
        while ((_n & 1) == 0)
        {
            _n >>= 1;
            ++i;
        }
        return i;
    }

    template <typename T>
    constexpr
    T
    Min(const T _a, const T _b) noexcept
    {
        return _a <= _b ? _a : _b;
    }

    template <typename T>
    constexpr
    T
    Max(const T _a, const T _b) noexcept
    {
        return _a >= _b ? _a : _b;
    }

    template <typename T>
    constexpr
    T
    Clamp(const T _a, const T _min, const T _max) noexcept
    {
        return Min<T>(Max<T>(_a, _min), _max);
    }

    template <typename T, typename A>
    constexpr
    T
    Align(const T _i, const A _a) noexcept
    {
        DJO_STATIC_ASSERT(std::is_integral_v<T> || std::is_pointer_v<T>);
        DJO_STATIC_ASSERT(std::is_integral_v<A>);

        if constexpr (std::is_pointer_v<T>)
            return reinterpret_cast<T>(Align(reinterpret_cast<uptr>(_i), _a));
        else
            return ((_i + (_a - 1)) / _a) * _a;
    }

    template <typename T>
    constexpr
    T
    AlignPow2(const T _i, const T _a) noexcept
    {
        DJO_STATIC_ASSERT(std::is_integral_v<T>);

        // Only works if _a is a power of 2
        return (_i + _a - 1) & ~(_a - 1);
    }

    template <typename T>
    constexpr
    bool
    IsPow2(const T _i) noexcept
    {
        DJO_STATIC_ASSERT(std::is_integral_v<T>);

        return (_i > 0ull) && ((_i & (_i - 1ull)) == 0ull);
    }

    template <typename T>
    constexpr
    T
    NextPow2(T _i) noexcept
    {
        DJO_STATIC_ASSERT(std::is_integral_v<T>);

        --_i;
        _i |= _i >> 1ull;
        _i |= _i >> 2ull;
        _i |= _i >> 4ull;
        if constexpr (sizeof(T) > 1)
            _i |= _i >> 8ull;
        if constexpr (sizeof(T) > 2)
            _i |= _i >> 16ull;
        if constexpr (sizeof(T) > 4)
            _i |= _i >> 32ull;
        ++_i;
        return _i;
    }

    template <typename T>
    constexpr
    T
    SignOf(const T _n) noexcept
    {
        return (T)(T{ 0 } < _n) - (_n < T{ 0 });
    }

    // C str utils

    constexpr
    sz
    StrLen(const char* _s) noexcept
    {
        sz len{ 0 };
        while (*_s != '\0') { ++_s; ++len; }
        return len;
    }

    constexpr
    s32
    StrNCmp(const char* _s1, const char* _s2, sz _count) noexcept
    {
        for (; 0 < _count; --_count, ++_s1, ++_s2)
            if (*_s1 != *_s2)
                return SignOf<s32>(*_s2 - *_s1);
        return 0;
    }

    constexpr
    const char*
    StrNChr(const char* _s, sz _count, const char _c) noexcept
    {
        for (; 0 < _count; --_count, ++_s)
            if (*_s == _c)
                return _s;
        return nullptr;
    }
    constexpr
    const char*
    StrNRChr(const char* _s, sz _count, const char _c) noexcept
    {
        for (_s = _s + _count - 1; 0 < _count; --_count, --_s)
            if (*_s == _c)
                return _s;
        return nullptr;
    }
    template <typename TStrNChrFn>
    constexpr
    sz
    TStrNChrI(const char* _s, const sz _count, const char _c, TStrNChrFn _fn) noexcept
    {
        const char* const p = _fn(_s, _count, _c);
        return p ? (sz)(p - _s) : c_invalidUint<sz>;
    }
}
