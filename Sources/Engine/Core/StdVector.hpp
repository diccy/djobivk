#pragma once

#include <Core/MemView.hpp>

#include <vector>


namespace Djo
{
    DJO_TO_VIEW_IMPL_T(
        DJO_NOOP(class T, class A),
        DJO_NOOP(std::vector<T, A>), T,
        _o.data(), _o.size());

    // Utils

    template <class T, class A>
    void
    RemoveFromVector(std::vector<T, A>& _v, const T& _value)
    {
        const auto itEnd{ _v.end() };
        _v.erase(std::remove(_v.begin(), itEnd, _value), itEnd);
    }

    template <typename T, typename A, class TPredicate>
    void
    RemoveFromVectorIf(std::vector<T, A>& _v, TPredicate _pred)
    {
        const auto itEnd{ _v.end() };
        _v.erase(std::remove_if(_v.begin(), itEnd, _pred), itEnd);
    }

    template <typename T, typename A>
    T
    Sum(const std::vector<T, A>& _v)
    {
        T sum{ 0 };
        for (T n : _v)
            sum += n;
        return sum;
    }
}
