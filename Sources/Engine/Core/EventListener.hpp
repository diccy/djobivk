#pragma once


namespace Djo
{
    template <typename TEvent>
    class TIEventListener
    {
    public:

        virtual void OnEvent(const TEvent& _event) = 0;
    };
}
