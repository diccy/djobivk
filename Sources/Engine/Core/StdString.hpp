#pragma once

#include <Core/MemView.hpp>

#include <string>
#include <cstdarg>


namespace Djo
{
    DJO_INLINE
    void
    VFormat(std::string* const _str, const char* const _fmt, ::va_list _args)
    {
        const int n = ::vsnprintf(nullptr, 0, _fmt, _args);
        if (n > 0)
        {
            _str->resize(n - 1);
            ::vsnprintf(_str->data(), n, _fmt, _args);
        }
    }

    DJO_INLINE
    void
    VFormatAdd(std::string* const _str, const char* const _fmt, ::va_list _args)
    {
        const int n = ::vsnprintf(nullptr, 0, _fmt, _args);
        if (n > 0)
        {
            const sz strSize = _str->size();
            _str->resize(_str->size() + n - 1);
            ::vsnprintf(_str->data() + strSize, n, _fmt, _args);
        }
    }

    DJO_INLINE
    void
    Format(std::string* const _str, const char* const _fmt, ...)
    {
        ::va_list args;
        va_start(args, _fmt);
        VFormat(_str, _fmt, args);
        va_end(args);
    }

    DJO_INLINE
    void
    FormatAdd(std::string* const _str, const char* const _fmt, ...)
    {
        ::va_list args;
        va_start(args, _fmt);
        VFormatAdd(_str, _fmt, args);
        va_end(args);
    }

    DJO_INLINE
    std::string
    Format(const char* const _fmt, ...)
    {
        std::string str;
        ::va_list args;
        va_start(args, _fmt);
        VFormat(&str, _fmt, args);
        va_end(args);
        return str;
    }


    DJO_TO_VIEW_IMPL_T(
        DJO_NOOP(class C, class A),
        DJO_NOOP(std::basic_string<C, std::char_traits<C>, A>), C,
        _o.data(), _o.size());

    template <typename C, typename A>
    DJO_INLINE
    std::basic_string<C, std::char_traits<C>, A>
    ToString(const MemView<C>& _view) noexcept
    {
        return std::basic_string<C, std::char_traits<C>, A>{ _view.data, _view.End() };
    }


    DJO_HASHER_FUNC_IMPL_T(
        DJO_NOOP(typename C, typename A),
        DJO_NOOP(std::basic_string<C, std::char_traits<C>, A>),
        _Internal::StrFnv1a_64,
        _o.c_str(), _o.size() * sizeof(C));
}
