#pragma once

#include <Core/Logger.hpp>
#include <Core/ResultEnum.hpp>


namespace Djo
{
    namespace GlobalLog
    {
        extern Logger* g_coreLogger;
        extern Logger* g_appLogger;

        EResult Init();
        void Shutdown();
    };
}

// Core log macros
#define DJO_LOGGER_CORE       (*::Djo::GlobalLog::g_coreLogger)
#define DJO_LOG_CORE(level_)  DJO_LOGGER_CORE.StartLogging(level_)
#define DJO_LOG_CORE_TRACE    DJO_LOG_CORE(::Djo::ELogLevel::Trace)
#define DJO_LOG_CORE_INFO     DJO_LOG_CORE(::Djo::ELogLevel::Info)
#define DJO_LOG_CORE_WARN     DJO_LOG_CORE(::Djo::ELogLevel::Warning)
#define DJO_LOG_CORE_ERROR    DJO_LOG_CORE(::Djo::ELogLevel::Error)

// Client log macros
#define DJO_LOGGER       (*::Djo::GlobalLog::g_appLogger)
#define DJO_LOG(level_)  DJO_LOGGER.StartLogging(level_)
#define DJO_LOG_TRACE    DJO_LOG(::Djo::ELogLevel::Trace)
#define DJO_LOG_INFO     DJO_LOG(::Djo::ELogLevel::Info)
#define DJO_LOG_WARN     DJO_LOG(::Djo::ELogLevel::Warning)
#define DJO_LOG_ERROR    DJO_LOG(::Djo::ELogLevel::Error)
