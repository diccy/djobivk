
#include <Core/IntrusiveRefCount.hpp>

#include <Core/Core.hpp>


namespace Djo
{
    IntrusiveRefCount::IntrusiveRefCount() noexcept
        : m_refCount{ 0 }
    {
    }

    IntrusiveRefCount::~IntrusiveRefCount() noexcept
    {
        // Logic issue if refcount is not zero !
        DJO_ASSERT(GetRefCount() == 0);
    }

    void
    IntrusiveRefCount::AddRef() noexcept
    {
        m_refCount.fetch_add(1u);
    }

    void
    IntrusiveRefCount::ReleaseRef() noexcept
    {
        DJO_ASSERT(m_refCount.load() > 0);
        m_refCount.fetch_sub(1);
    }

    u16
    IntrusiveRefCount::GetRefCount() const noexcept
    {
        return m_refCount.load();
    }
}
