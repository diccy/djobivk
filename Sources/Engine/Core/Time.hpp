#pragma once

#include <Core/Types.hpp>
#include <Core/ClassTraits.hpp>


namespace Djo
{
    // fw
    template <typename T, u32 N, u32 D> class Time;

    template <typename T>
    using SecondsT = Time<T, 1u, 1u>;
    template <typename T>
    using MillisecondsT = Time<T, 1u, 1000u>;
    template <typename T>
    using MicrosecondsT = Time<T, 1u, 1000u * 1000u>;

    template <typename T, u32 N, u32 D>
    class Time
    {
        DJO_DEFAULT_COPY_DEFAULT_MOVE(Time);

    public:

        constexpr Time() noexcept : t{ 0 }             {}
        constexpr Time(const T _t) noexcept : t{ _t }  {}
        ~Time() noexcept                               = default;

        constexpr Time& operator = (const T _t) noexcept  { t = _t; return *this; }

        constexpr operator T() const noexcept  { return t; }
        constexpr operator T&() noexcept       { return t; }

        constexpr SecondsT<T> Sec() const noexcept      { return t * T{ 1 } * T{ N } / T{ D }; }
        constexpr MillisecondsT<T> Ms() const noexcept  { return t * T{ 1000 } * T{ N } / T{ D }; }
        constexpr MicrosecondsT<T> Us() const noexcept  { return t * T{ 1000 * 1000 } * T{ N } / T{ D }; }

    private:

        T t;
    };

    using Seconds      = SecondsT<f32>;
    using Milliseconds = MillisecondsT<f32>;
    using Microseconds = MicrosecondsT<f32>;

    using Seconds64      = SecondsT<f64>;
    using Milliseconds64 = MillisecondsT<f64>;
    using Microseconds64 = MicrosecondsT<f64>;
}
