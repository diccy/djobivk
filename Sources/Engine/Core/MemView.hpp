#pragma once

#include <Core/Core.hpp>
#include <Core/ClassTraits.hpp>
#include <Core/Types.hpp>
#include <Core/Hash.hpp>

#include <type_traits>


namespace Djo
{
    struct MemViewIgnoreEndingZeroes {};
    constexpr MemViewIgnoreEndingZeroes c_memViewIgnoreEndingZeroes{};

    template <typename T>
    struct MemView
    {
        DJO_DEFAULT_COPY_DEFAULT_MOVE(MemView<T>);

    public:

        using DataType = T;
        using Iterator = DataType*;
        using ConstIterator = const DataType*;

        constexpr
        MemView<T>() noexcept
            : data{ nullptr }
            , count{ 0 }
        {}

        constexpr
        MemView<T>(DataType* const _data, const sz _count) noexcept
            : data{ _data }
            , count{ _count }
        {}

        template <sz N>
        constexpr
        MemView<T>(DataType (&_carray)[N]) noexcept
            : data{ _carray }
            , count{ N }
        {}

        template <sz N>
        constexpr
        MemView<T>(DataType (&_carray)[N], MemViewIgnoreEndingZeroes) noexcept
            : data{ _carray }
            , count{ N }
        {
            while (count > 0 && *(GetEnd() - 1) == DataType{ 0 })
                --count;
        }

        ~MemView<T>() = default;

        // Implicit cast to MemView<const DataType>
        constexpr operator MemView<const DataType> () const noexcept  { return MemView<const DataType>{ data, count }; }

        constexpr const DataType& operator[](const sz _i) const noexcept  { DJO_ASSERT(_i < this->count); return this->data[_i]; }
        constexpr DataType& operator[](const sz _i) noexcept              { DJO_ASSERT(_i < this->count); return this->data[_i]; }

        constexpr sz GetMemSize() const noexcept           { return sizeof(DataType) * count; }
        constexpr ConstIterator GetStart() const noexcept  { return data; }
        constexpr Iterator GetStart() noexcept             { return data; }
        constexpr ConstIterator GetEnd() const noexcept    { return GetStart() + count; }
        constexpr Iterator GetEnd() noexcept               { return GetStart() + count; }

        // std
        constexpr ConstIterator begin() const noexcept  { return GetStart(); }
        constexpr Iterator begin() noexcept             { return GetStart(); }
        constexpr ConstIterator end() const noexcept    { return GetEnd(); }
        constexpr Iterator end() noexcept               { return GetEnd(); }

        DataType* data;
        sz count;
    };
    //DJO_STATIC_ASSERT(std::is_literal_type_v<MemView<int>>); // deprecated in c++17, still can be used to ensure the usage of MemView in constexpr

#define DJO_INTERNAL_TO_VIEW_IMPL_T(template_, containerType_, elemType_, ptr_, count_)\
    template_\
    DJO_INLINE\
    MemView<const elemType_>\
    ToView(const containerType_& _o) noexcept\
    {\
        return MemView<const elemType_>{ (ptr_), (count_) };\
    }\
    template_\
    DJO_INLINE\
    MemView<elemType_>\
    ToView(containerType_& _o) noexcept\
    {\
        return MemView<elemType_>{ (ptr_), (count_) };\
    }\
    template_\
    DJO_INLINE\
    MemView<const elemType_>\
    ToConstView(const containerType_& _o) noexcept\
    {\
        return MemView<const elemType_>{ (ptr_), (count_) };\
    }

#define DJO_TO_VIEW_IMPL_T(template_, containerType_, elemType_, ptr_, count_)\
    DJO_INTERNAL_TO_VIEW_IMPL_T(template <template_>, containerType_, elemType_, ptr_, count_)

#define DJO_TO_VIEW_IMPL(containerType_, elemType_, ptr_, count_)\
    DJO_INTERNAL_TO_VIEW_IMPL_T(, containerType_, elemType_, ptr_, count_)


    // void specializations

    namespace _Internal
    {
        template <typename T>
        struct TMemViewVoid
        {
        public:

            using VoidType = T;
            using Iterator = VoidType*;
            using ConstIterator = const VoidType*;

            constexpr
            TMemViewVoid<T>() noexcept
                : data{ nullptr }
                , count{ 0 }
            {}

            constexpr
            TMemViewVoid<T>(VoidType* const _data, const sz _size) noexcept
                : data{ _data }
                , count{ _size }
            {}

            template <typename U>
            constexpr
            TMemViewVoid<T>(const MemView<U>& _memView) noexcept
                : data{ static_cast<VoidType*>(_memView.data) }
                , count{ _memView.GetMemSize() }
            {}

            constexpr TMemViewVoid<T>(const TMemViewVoid<T>&) = default;
            constexpr TMemViewVoid<T>(TMemViewVoid<T>&&) noexcept = default;

            ~TMemViewVoid<T>() = default;

            constexpr TMemViewVoid<T>& operator = (const TMemViewVoid<T>&) = default;
            constexpr TMemViewVoid<T>& operator = (TMemViewVoid<T>&&) noexcept = default;

            // Implicit cast to TMemViewVoid<const VoidType>
            constexpr operator TMemViewVoid<const VoidType>() const noexcept { return TMemViewVoid<const VoidType>{ data, count }; }

            constexpr sz GetMemSize() const noexcept           { return count; }
            constexpr ConstIterator GetStart() const noexcept  { return data; }
            constexpr Iterator GetStart() noexcept             { return data; }
            constexpr ConstIterator GetEnd() const noexcept    { return (const char*)GetStart() + count; }
            constexpr Iterator GetEnd() noexcept               { return (Iterator)((const char*)GetStart() + count); }

            VoidType* data;
            sz count;
        };
    }

    template <>
    struct MemView<void>
        : public _Internal::TMemViewVoid<void>
    {};

    template <>
    struct MemView<const void>
        : public _Internal::TMemViewVoid<const void>
    {};


    DJO_HASHER_IMPL_T(
        typename T,
        MemView<T>,
        _o.data, _o.GetMemSize());
}
