#pragma once

// !! Garder ce fichier sans include !!

// https://sourceforge.net/p/predef/wiki/Home/


// Config

// 4201 : nonstandard extension used : nameless struct/union
#pragma warning(disable : 4201)


// Basic macro Tools

#define DJO_NOOP(...)    __VA_ARGS__
#define DJO_DO(M_, ...)  DJO_NOOP(M_(__VA_ARGS__))

#define DJO_STR_(s_)  # s_
#define DJO_STR(s_)   DJO_STR_(s_)

#define DJO_CAT_(a_, b_)  a_ ## b_
#define DJO_CAT(a_, b_)   DJO_CAT_(a_, b_)

#define DJO_UNPAR(...)  DJO_NOOP __VA_ARGS__

#define DJO_UNUSED_VAR(var_) (void)var_

#define DJO_COMMA0 ,
#define DJO_COMMA1 DJO_COMMA0
#define DJO_COMMA2 DJO_COMMA1
#define DJO_COMMA3 DJO_COMMA2
#define DJO_COMMA4 DJO_COMMA3
#define DJO_COMMA5 DJO_COMMA4
#define DJO_COMMA6 DJO_COMMA5
#define DJO_COMMA7 DJO_COMMA6
#define DJO_COMMA8 DJO_COMMA7
#define DJO_COMMA9 DJO_COMMA8
#define DJO_COMMA  DJO_COMMA9

// Todo

#define DJO_TODO(s_)  __pragma(message(__FILE__ "(" DJO_STR(__LINE__) "): TODO: " s_))


// Break

namespace Djo::_Internal
{
    void Break();
}

// Asserts

// Les macros ASSERT sont vides en Release

#if defined(__DJO_DEBUG__)

    #define DJO_IS_ASSERT_AVAILABLE 1

    namespace Djo::_Internal
    {
        extern bool g_areAssertsEnabled;
        void PrintAssertInfos(const char* _expression, const char* _filePath, int _line, const char* _function);
    }

    #define DJO_FAILED_ASSERTION(expr_)\
    {\
        if (::Djo::_Internal::g_areAssertsEnabled)\
        {\
            ::Djo::_Internal::PrintAssertInfos(DJO_STR(expr_), __FILE__, __LINE__, __FUNCTION__);\
            ::Djo::_Internal::Break();\
        }\
    }

    #define DJO_ASSERT(expr_)\
    {\
        if (!(expr_))\
        {\
            DJO_FAILED_ASSERTION(expr_);\
        }\
    }

    #define DJO_ASSERT_MSG(expr_, msg_)\
    {\
        if (!(expr_))\
        {\
            DJO_FAILED_ASSERTION(msg_);\
        }\
    }

    // Utiliser avec parcimonie (pour les tests principalement)
    #define DJO_DISABLE_ASSERT_WHILE(expr_)\
    {\
        const bool wereAssertsEnabled__ = ::Djo::_Internal::g_areAssertsEnabled;\
        ::Djo::_Internal::g_areAssertsEnabled = false;\
        { expr_; }\
        ::Djo::_Internal::g_areAssertsEnabled = wereAssertsEnabled__;\
    }

#else

    #define DJO_FAILED_ASSERTION(expr_)      do {} while (0)
    #define DJO_ASSERT(expr_)                do {} while (0)
    #define DJO_ASSERT_MSG(expr_, msg_)      do {} while (0)
    #define DJO_DISABLE_ASSERT_WHILE(expr_)  expr_

#endif // __DJO_DEBUG__


#define DJO_STATIC_ASSERT_MSG     static_assert
#define DJO_STATIC_ASSERT(expr_)  DJO_STATIC_ASSERT_MSG((expr_), "")


// Information d'implementation manquante

#define DJO_NOT_IMPLEMENTED(mess_)  { DJO_TODO(mess_); DJO_FAILED_ASSERTION(mess_); }


// Flags

namespace Djo
{
    template <typename T>
    constexpr T Flag(const T _v) noexcept                     { return T{ 1u } << _v; }

    template <typename T>
    constexpr T Mask(const T _v) noexcept                     { return Flag(_v) - T{ 1u }; }

    template <typename T>
    constexpr bool TestFlag(const T _v, const T _f) noexcept  { return (_v & _f) == _f; }
}


// Templates

// From std
namespace Djo
{
    template <bool TTest, class T = void>
    struct EnableIf {};
    template <class T>
    struct EnableIf<true, T> { using Type = T; };
    template <bool TTest, class T = void>
    using EnableIf_t = typename EnableIf<TTest, T>::Type;

    template <class>
    constexpr bool c_isConst = false;
    template <class T>
    constexpr bool c_isConst<const T> = true;
}

#define DJO_TARG_ENABLE_IF(test_)            ::Djo::EnableIf_t<(test_), int> = 0

#define DJO_TARG_ENABLE_IF_CONST(type_)      DJO_TARG_ENABLE_IF( ::Djo::c_isConst<type_>)
#define DJO_TARG_ENABLE_IF_NOT_CONST(type_)  DJO_TARG_ENABLE_IF(!::Djo::c_isConst<type_>)


// Verification de casts (debug seulement)

#if defined(__DJO_DEBUG__)

    #define DJO_ENABLE_CAST_CHECK 1

#endif


// Inline strategy

#define DJO_INLINE inline


// Profiling

#define DJO_PROFILING_ENABLED 1


// Utilities

#define DJO_CASE_VALUE_RETURN_STR(enum_) case enum_: { return #enum_; }

namespace Djo
{
    template <typename T> constexpr bool c_isUnsignedInt = false;
    template <>           constexpr bool c_isUnsignedInt<unsigned char>      = true;
    template <>           constexpr bool c_isUnsignedInt<unsigned short>     = true;
    template <>           constexpr bool c_isUnsignedInt<unsigned int>       = true;
    template <>           constexpr bool c_isUnsignedInt<unsigned long long> = true;

    template <typename T, DJO_TARG_ENABLE_IF(c_isUnsignedInt<T>)>
    constexpr T c_invalidUint{ ~(T{ 0 }) };
}


// Version

namespace Djo
{
    struct Version
    {
        unsigned char major;
        unsigned char minor;
        unsigned short patch;

        constexpr
        Version(const unsigned char _major = 0, const unsigned char _minor = 0, const unsigned short _patch = 0)
            : major{ _major }
            , minor{ _minor }
            , patch{ _patch }
        {}

        constexpr operator unsigned int () const { return (unsigned int)(major << 24u) | (unsigned int)(minor << 16u) | (unsigned int)(patch); }
    };
    DJO_STATIC_ASSERT(sizeof(Version) == sizeof(unsigned int));
    DJO_STATIC_ASSERT((Version{ 1, 0, 0 } > Version{ 0, 1, 0 }));
    DJO_STATIC_ASSERT((Version{ 0, 1, 0 } > Version{ 0, 0, 1 }));

    constexpr Version c_djobiEngineVersion{ 0, 1, 0 };
}


// Memory

#if defined(__DJO_DEBUG__)

    #define DJO_CORE_NEW                 ::new (_NORMAL_BLOCK, __FILE__, __LINE__)
    #define DJO_CORE_OPERATOR_NEW(size_) ::operator new (size_, _NORMAL_BLOCK, __FILE__, __LINE__)

#else

    #define DJO_CORE_NEW                 ::new
    #define DJO_CORE_OPERATOR_NEW(size_) ::operator new(size_)

#endif

namespace Djo
{
    void SetupSystemMemoryLeakWatcher();
}


// Macro tools

#define DJO_ARG_COUNT(...)   DJO_DO(DJO_ARG_COUNT_ARGS_, __VA_ARGS__, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
#define DJO_ARG_COUNT_ARGS_(_1, _2, _3, _4, _5, _6, _7, _8, _9,_10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, N, ...) N

#define DJO_APPLY_1(M_, sep_, _1)\
    M_(_1)
#define DJO_APPLY_2(M_, sep_, _1, _2)\
    M_(_1) sep_ M_(_2)
#define DJO_APPLY_3(M_, sep_, _1, _2, _3)\
    M_(_1) sep_ M_(_2) sep_ M_(_3)
#define DJO_APPLY_4(M_, sep_, _1, _2, _3, _4)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4)
#define DJO_APPLY_5(M_, sep_, _1, _2, _3, _4, _5)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5)
#define DJO_APPLY_6(M_, sep_, _1, _2, _3, _4, _5, _6)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6)
#define DJO_APPLY_7(M_, sep_, _1, _2, _3, _4, _5, _6, _7)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7)
#define DJO_APPLY_8(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8)
#define DJO_APPLY_9(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9)
#define DJO_APPLY_10(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9) sep_ M_(_10)
#define DJO_APPLY_11(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9) sep_ M_(_10) sep_ M_(_11)
#define DJO_APPLY_12(M_, sep_, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12)\
    M_(_1) sep_ M_(_2) sep_ M_(_3) sep_ M_(_4) sep_ M_(_5) sep_ M_(_6) sep_ M_(_7) sep_ M_(_8) sep_ M_(_9) sep_ M_(_10) sep_ M_(_11) sep_ M_(_12)

#define DJO_APPLY(M_, ...)  DJO_NOOP(DJO_CAT(DJO_APPLY_, DJO_ARG_COUNT(__VA_ARGS__))(M_, , __VA_ARGS__))
#define DJO_APPLY_SEP(M_, sep_, ...)  DJO_NOOP(DJO_CAT(DJO_APPLY_, DJO_ARG_COUNT(__VA_ARGS__))(M_, DJO_NOOP(sep_), __VA_ARGS__))
