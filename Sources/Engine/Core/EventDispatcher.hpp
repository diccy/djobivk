#pragma once

#include <Core/StdVector.hpp>


namespace Djo
{
    // fw
    template <typename TEvent>
    class TIEventListener;

    class IEventDispatcher
    {
    public:
        virtual ~IEventDispatcher() {}
    };

    template <typename TEvent>
    class TEventDispatcher
        : public IEventDispatcher
    {
        DJO_NO_COPY_DEFAULT_MOVE(TEventDispatcher<TEvent>);

    public:

        TEventDispatcher<TEvent>()
            : m_listeners{}
        {}

        virtual ~TEventDispatcher<TEvent>()
            = default;

        DJO_INLINE
        void
        Register(TIEventListener<TEvent>* const _listener)
        {
            m_listeners.emplace_back(_listener);
        }

        DJO_INLINE
        void
        Unregister(TIEventListener<TEvent>* const _listener)
        {
            RemoveFromVector(m_listeners, _listener);
        }
        
        DJO_INLINE
        void Clear()
        {
            m_listeners.clear();
        }

        void
        SendEvent(const TEvent& _event) const
        {
            for (TIEventListener<TEvent>* const listener : m_listeners)
            {
                listener->OnEvent(_event);
            }
        }

    private:

        std::vector<TIEventListener<TEvent>*> m_listeners;
    };
}
