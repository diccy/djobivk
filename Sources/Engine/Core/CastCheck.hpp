#pragma once

#include <Core/Core.hpp>

#include <type_traits>


namespace Djo
{
    template <class TOut, class TIn>
    TOut*
    CastCheck_Internal(TIn* p)
    {
        TOut* const ret = dynamic_cast<TOut*>(p);
        DJO_ASSERT(ret != nullptr || p == nullptr);
        return ret;
    }

    template <class TOutPtr, class TInPtr, DJO_TARG_ENABLE_IF(std::is_pointer_v<TOutPtr> && std::is_pointer_v<TInPtr>)>
    TOutPtr
    CastCheck(TInPtr p)
    {
        return CastCheck_Internal<std::remove_pointer_t<TOutPtr>, std::remove_pointer_t<TInPtr>>(p);
    }

    template <class TOut, class TIn, DJO_TARG_ENABLE_IF(std::is_reference_v<TOut> && !std::is_pointer_v<TIn>)>
    TOut
    CastCheck(TIn& p)
    {
        return *CastCheck_Internal<std::remove_reference_t<TOut>, std::remove_reference_t<TIn>>(std::adressof(p));
    }

#if defined(DJO_ENABLE_CAST_CHECK)

    #define DJO_CAST   ::Djo::CastCheck

#else

    #define DJO_CAST   static_cast

#endif
}
