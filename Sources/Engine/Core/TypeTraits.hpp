#pragma once

#include <Core/Core.hpp>
#include <Core/Utils.hpp>

#include <type_traits>


namespace Djo
{
    // Metaprogramming utils

    template <class T, class... TTypes>
    constexpr bool c_isAnyOf_v = std::disjunction_v<std::is_same<T, TTypes>...>;

    template <typename T>
    using PlainType_t = typename std::remove_reference_t<std::remove_pointer_t<std::remove_all_extents_t<T>>>;
    template <typename T>
    using PureType_t = typename std::remove_cv_t<PlainType_t<T>>;


    // Example:
    // template <typename T>
    // void DoSomething(CopyOrConstRef_t<T> _data) { blah; }
    template <typename T>
    struct CopyOrConstRef
    {
        using Type = std::conditional_t<std::is_scalar_v<T>, T, const T&>;
    };
    template <typename T>
    using CopyOrConstRef_t = typename CopyOrConstRef<T>::Type;


    // Enums
    // To make enum class less verbose to use,
    // and "standardize" Count and Invalid values
    // Example:
    // enum EMyEnum : u32
    // {
    //      Toto = 0,
    //      Tata,
    //      Tutu,
    //      DJO_ENUM_COUNT_INVALID
    // };
    // static_cast<u32>(value) == EValue(value)
    // static_cast<EMyEnum>(12u) == ECast<EMyEnum>(12u)
    // static_cast<u32>(EMyEnum::Count) == ECount<EMyEnum>
    // static_cast<u32>(EMyEnum::Invalid) == EInvalid<EMyEnum>

    template <typename E>
    constexpr bool c_isClassEnum = std::is_enum_v<E> && !std::is_convertible_v<E, int>;

    #define DJO_TARG_ENUM(E)        typename E, DJO_TARG_ENABLE_IF(std::is_enum_v<E>)
    #define DJO_TARG_CLASS_ENUM(E)  typename E, DJO_TARG_ENABLE_IF(c_isClassEnum<E>)

    template <DJO_TARG_CLASS_ENUM(E)>
    constexpr
    std::underlying_type_t<E>
    EValue(const E _e) noexcept
    {
        return static_cast<std::underlying_type_t<E>>(_e);
    }
    template <DJO_TARG_CLASS_ENUM(E)>
    constexpr
    E
    ECast(const std::underlying_type_t<E> _v) noexcept
    {
        return static_cast<E>(_v);
    }

    #define DJO_ENUM_COUNT_INVALID  Count, Invalid = Count

    template <typename E>
    constexpr std::underlying_type_t<E> ECount = EValue(E::Count);

    template <typename E>
    constexpr std::underlying_type_t<E> EInvalid = EValue(E::Invalid);


    // Grab the flag count in a flag fashioned enum
    // Exemple:
    // enum class EMyEnum : u32
    // {
    //     Tata = Flag(0),
    //     Toto = Flag(1),
    //     Tutu = Flag(2),
    //     Titi = Flag(3),
    //     DJO_ENUM_FLAG_COUNT
    // };
    // There EFlagCount<EMyEnum> will be 4
    // static_cast<u32>(EMyEnum::FlagCount) == EFlagCount<EMyEnum>

    #define DJO_ENUM_FLAG_COUNT  next__, FlagCount = (GetFirstBitIndex(next__ - 1) + 1)

    template <typename E>
    constexpr std::underlying_type_t<E> EFlagCount = EValue(E::FlagCount);


    // Enum class bitwise operators

    #define DJO_IMPL_CLASS_ENUM_BIT_OPERATOR(op_)\
        template <DJO_TARG_CLASS_ENUM(E)>\
        constexpr\
        std::underlying_type_t<E>\
        operator op_ (const E _e1, const E _e2)\
        {\
            return EValue(_e1) op_ EValue(_e2);\
        }\
        template <DJO_TARG_CLASS_ENUM(E)>\
        constexpr\
        std::underlying_type_t<E>\
        operator op_ (const E _e1, const std::underlying_type_t<E> _et2)\
        {\
            return EValue(_e1) op_ _et2;\
        }\
        template <DJO_TARG_CLASS_ENUM(E)>\
        constexpr\
        std::underlying_type_t<E>\
        operator op_ (const std::underlying_type_t<E> _et1, const E _e2)\
        {\
            return _et1 op_ EValue(_e2);\
        }

    DJO_IMPL_CLASS_ENUM_BIT_OPERATOR(|);
    DJO_IMPL_CLASS_ENUM_BIT_OPERATOR(&);
    #undef DJO_IMPL_CLASS_ENUM_BIT_OPERATOR


    // This function is here because is uses std::move and <type_traits> is already included here
    template <typename T>
    constexpr
    void
    Swap(T& _a, T& _b) noexcept
    {
        T tmp{ std::move(_a) };
        _a = std::move(_b);
        _b = std::move(tmp);
    }
}
