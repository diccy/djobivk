#pragma once

#include <Core/Core.hpp> // #pragma warning (disable : 4201) nonstandard extension used : nameless struct/union
#include <Core/Types.hpp>


namespace Djo
{
    // Array wrapper, for return values mostly
    template <typename T, sz N>
    struct ArrayWrapper
    {
        T a[N];
    };


    // Hard type alias
    template <typename T>
    struct HardAlias
    {
                 constexpr operator const T& () const  { return value; }
        explicit constexpr operator       T& ()        { return value; }

        T value;
    };

#pragma pack(push, 1)
    template <typename T, typename U>
    struct Pair { T a; U b; };
#pragma pack(pop)


    // Unsigned int conversion structs

    // ASSUMES BIG ENDIANESS

    struct U8ToU32
    {
        union
        {
            struct { u8 i8_0, i8_1, i8_2, i8_3; };
            u32 i32 = 0u;
        };

        constexpr
        U8ToU32(const u8 _i0, const u8 _i1, const u8 _i2, const u8 _i3) noexcept
            : i8_0(_i0)
            , i8_1(_i1)
            , i8_2(_i2)
            , i8_3(_i3)
        {
        }
    };

    struct U32ToU8
    {
        union
        {
            u32 i32 = 0u;
            struct { u8 i8_0, i8_1, i8_2, i8_3; };
        };

        constexpr
        U32ToU8(const u32 _i) noexcept
            : i32(_i)
        {
        }
    };

    struct U32ToU64
    {
        union
        {
            struct { u32 i32_0, i32_1; };
            u64 i64 = 0ull;
        };

        constexpr
        U32ToU64(const u32 _i0, const u32 _i1) noexcept
            : i32_0(_i0)
            , i32_1(_i1)
        {
        }
    };

    struct U64ToU32
    {
        union
        {
            u64 i64 = 0ull;
            struct { u32 i32_0, i32_1; };
        };

        constexpr
        U64ToU32(const u64 _i) noexcept
            : i64(_i)
        {
        }
    };


    // Lookup table macro definitions for compile time generation

    #define DJO_LUT_START(lutVarName_, count_, inType_, outType_, default_)\
        static const auto lutVarName_ = \
        []() -> auto\
        {\
            ::Djo::ArrayWrapper<outType_, (count_)> wrapper{};\
            for (::Djo::u32 i = 0; i < (count_); ++i)\
            {\
                outType_ out = default_;\
                switch (i)\
                {

    #define DJO_LUT_MATCH(inValue_, outValue_) case inValue_: { out = outValue_; break; }

    #define DJO_LUT_END()\
                }\
                wrapper.a[i] = out;\
            }\
            return wrapper;\
        }()
}
