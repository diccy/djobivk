#pragma once

#include <Core/ClassTraits.hpp>
#include <Core/StdVector.hpp>


namespace Djo
{
    DJO_TODO("Tests Averager");

    template <typename T>
    class Averager
    {
        DJO_DEFAULT_COPY_DEFAULT_MOVE(Averager<T>);

    public:

        Averager<T>();
        Averager<T>(u32 _size, T _defaultValue = T{ 0 });
        ~Averager<T>() = default;

        DJO_INLINE u32 GetSize() const  { return (u32)m_values.size(); }

        void Resize(u32 _size, T _defaultValue = T{ 0 });
        void Push(T _value);
        T GetSum() const;
        T Get() const;

        T GetWithMaxSum(T _maxSum) const;

    private:

        std::vector<T> m_values;
        u32 m_currentCircularId;
    };

    template <typename T>
    Averager<T>::Averager()
        : Averager<T>(1)
    {
    }

    template <typename T>
    Averager<T>::Averager(const u32 _size, const T _defaultValue /*= T{ 0 }*/)
        : m_values{}
        , m_currentCircularId{ 0 }
    {
        Resize(_size, _defaultValue);
    }

    template <typename T>
    void
    Averager<T>::Resize(const u32 _size, const T _defaultValue /*= T{ 0 }*/)
    {
        m_values.resize(_size, _defaultValue);
    }

    template <typename T>
    void
    Averager<T>::Push(const T _value)
    {
        m_values[m_currentCircularId++] = _value;
        m_currentCircularId %= (u32)m_values.size();
    }

    template <typename T>
    T
    Averager<T>::GetSum() const
    {
        T sum{ 0 };
        for (const T val : m_values)
            sum = sum + val;
        return sum;
    }

    template <typename T>
    T
    Averager<T>::Get() const
    {
        return GetSum() / (T)GetSize();
    }

    template <typename T>
    T
    Averager<T>::GetWithMaxSum(const T _maxSum) const
    {
        const u32 size = GetSize();
        if (size == 0)
            return T{ 0 };

        T sum{ 0 };
        u32 count = 0;
        if (m_currentCircularId > 0)
        {
            for (s32 i = (s32)m_currentCircularId - 1; i >= 0 && sum < _maxSum; --i)
            {
                sum = sum + m_values[i];
                ++count;
            }
        }
        for (u32 i = (u32)size - 1; i >= m_currentCircularId && sum < _maxSum; --i)
        {
            sum = sum + m_values[i];
            ++count;
        }
        return sum / (T)count;
    }
}
