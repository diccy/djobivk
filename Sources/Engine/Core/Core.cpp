
#include <Core/Core.hpp>

#if defined(DJO_IS_ASSERT_AVAILABLE)
    #include <Core/GlobalLog.hpp>
#endif

#include <intrin.h> // __debugbreak()
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h> // _CrtSetDbgFlag()


namespace Djo
{
    namespace _Internal
    {
        void
        Break()
        {
            __debugbreak();
        }

    #if defined(DJO_IS_ASSERT_AVAILABLE)

        bool g_areAssertsEnabled{ true };

        void
        PrintAssertInfos(const char* const _expression, const char* const _filePath, const int _line, const char* const _function)
        {
            DJO_LOG_CORE_INFO << "############## ASSERTION ECHOUEE ##############\n";
            DJO_LOG_CORE_INFO << "Fichier:  " << _filePath << " (" << _line << ")\n";
            DJO_LOG_CORE_INFO << "Fonction: " << _function << "\n\n";
            DJO_LOG_CORE_INFO << _expression << "\n\n";
            DJO_LOG_CORE_INFO << "###############################################" << std::endl;
        }

    #endif // DJO_IS_ASSERT_AVAILABLE
    }

    void
    SetupSystemMemoryLeakWatcher()
    {
    #if defined(__DJO_DEBUG__)
        ::_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
        //::_CrtSetBreakAlloc(441); // uncomment to use when memory leak is found
    #endif
    }
}
