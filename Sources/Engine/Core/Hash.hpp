#pragma once

#include <Core/Types.hpp>
#include <Core/Core.hpp>


namespace Djo
{
    // Runtime hash

    namespace _Internal
    {
        // From Zilong Tan's "fasthash"
        // https://github.com/rurban/smhasher

        constexpr
        u64
        mix_hash(u64 _h) noexcept
        {
            _h ^= _h >> 23ull;
            _h *= 0x2127599bf4325c37ull;
            _h ^= _h >> 47ull;
            return _h;
        }

        DJO_INLINE
        u64
        fasthash64(const void* const _buf, const sz _len) noexcept
        {
            constexpr u64 c_m = 0x880355f21e6d1965ull;
            const u64* pos = reinterpret_cast<const u64*>(_buf);
            const u64* end = pos + (_len / 8ull);
            u64 h = 1099511628211ull ^ (_len * c_m);
            u64 v = 0ull;

            while (pos != end)
            {
                h ^= mix_hash(*pos++);
                h *= c_m;
            }

            const u8* const pos2 = reinterpret_cast<const u8*>(pos);
            switch (_len & 7ull)
            {
                case 7ull:
                    v ^= (u64)(pos2[6]) << 48ull;
                    [[fallthrough]];
                case 6ull:
                    v ^= (u64)(pos2[5]) << 40ull;
                    [[fallthrough]];
                case 5ull:
                    v ^= (u64)(pos2[4]) << 32ull;
                    [[fallthrough]];
                case 4ull:
                    v ^= (u64)(pos2[3]) << 24ull;
                    [[fallthrough]];
                case 3ull:
                    v ^= (u64)(pos2[2]) << 16ull;
                    [[fallthrough]];
                case 2ull:
                    v ^= (u64)(pos2[1]) << 8ull;
                    [[fallthrough]];
                case 1ull:
                    v ^= (u64)(pos2[0]);
                    h ^= mix_hash(v);
                    h *= c_m;
            }

            return mix_hash(h);
        }

        DJO_INLINE
        u32
        fasthash32(const void* const _buf, const sz _len) noexcept
        {
            const u64 h = fasthash64(_buf, _len);
            return (u32)(h - (h >> 32ull));
        }
    }

    DJO_INLINE
    u64
    Hash64(const void* const _buf, const sz _len) noexcept
    {
        return _Internal::fasthash64(_buf, _len);
    }

    DJO_INLINE
    u32
    Hash32(const void* const _buf, const sz _len) noexcept
    {
        return _Internal::fasthash32(_buf, _len);
    }

    template <typename T>
    struct Hasher
    {
        DJO_INLINE
        u64
        operator () (const T& _data) const noexcept
        {
            return Hash64(reinterpret_cast<const char*>(std::addressof(_data)), sizeof(_data));
        }
    };


    // Compile time str hash

    namespace _Internal
    {
        // FNV-1a
        // http://isthe.com/chongo/tech/comp/fnv/

        // For constexpr array hash

        constexpr u32 c_fnv1aBaseSeed32 = 2166136261u;
        constexpr u32 c_fnv1aPrime32 = 16777619u;
        constexpr u64 c_fnv1aBaseSeed64 = 14695981039346656037ull;
        constexpr u64 c_fnv1aPrime64 = 1099511628211ull;

        constexpr
        u32
        StrFnv1a_32(char const* const _cstr, sz _size) noexcept
        {
            u32 h = c_fnv1aBaseSeed32;
            while (_size > 0)
            {
                h ^= (u32)_cstr[--_size];
                h *= c_fnv1aPrime32;
            }
            return h;
        }
        constexpr
        u64
        StrFnv1a_64(char const* const _cstr, sz _size) noexcept
        {
            u64 h = c_fnv1aBaseSeed64;
            while (_size > 0)
            {
                h ^= (u64)_cstr[--_size];
                h *= c_fnv1aPrime64;
            }
            return h;
        }
    }

    constexpr
    u32
    operator"" _h32(char const* const _cstr, const sz _size) noexcept
    {
        return _Internal::StrFnv1a_32(_cstr, _size);
    }
    constexpr
    u64
    operator"" _h64(char const* const _cstr, const sz _size) noexcept
    {
        return _Internal::StrFnv1a_64(_cstr, _size);
    }

#define DJO_HASHER_FUNC_IMPL_T(template_, type_, func_, ptr_, size_)\
    template <template_>\
    struct Hasher<type_>\
    {\
        constexpr\
        u64\
        operator () (const type_& _o) const noexcept\
        {\
            return func_(reinterpret_cast<const char*>(ptr_), (size_));\
        }\
    }

#define DJO_HASHER_FUNC_IMPL(type_, func_, ptr_, size_)\
    DJO_HASHER_FUNC_IMPL_T(, type_, func_, ptr_, size_)

#define DJO_HASHER_IMPL_T(template_, type_, ptr_, size_)\
    DJO_HASHER_FUNC_IMPL_T(template_, type_, Hash64, ptr_, size_)

#define DJO_HASHER_IMPL(type_, ptr_, size_)\
    DJO_HASHER_IMPL_T(, type_, ptr_, size_)

}
