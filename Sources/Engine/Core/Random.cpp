
#include <Core/Random.hpp>

#include <Core/Core.hpp> // DJO_TODO


namespace Djo
{
    u64 FastRandom::s_sessionSeed{ c_baseSeed };
    FastRandom FastRandom::s_global{ s_sessionSeed };

    DJO_TODO("Tests of FastRandom");
    DJO_TODO("Add a 'safer' Random?");
}
