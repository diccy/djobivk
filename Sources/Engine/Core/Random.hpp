#pragma once

#include <Core/Types.hpp>
#include <Core/ClassTraits.hpp>


// One of George Marsaglia's algorithms

namespace Djo
{
    class FastRandom
    {
        DJO_DEFAULT_COPY_DEFAULT_MOVE(FastRandom);

        static constexpr u64 c_baseSeed{ 362436069ull };
        static constexpr u64 c_baseMult{ 2083801278ull };
        static constexpr u32 c_baseAdd{ 36931u };

    public:

        explicit
        constexpr
        FastRandom(const u64 _seed = c_baseSeed, const u64 _mult = c_baseMult, const u32 _add = c_baseAdd) noexcept
            : m_n{ _seed }
            , m_mult{ _mult }
            , m_add{ _add }
        {}

        ~FastRandom() = default;

        constexpr
        void
        Reset(const u64 _seed = c_baseSeed, const u64 _mult = c_baseMult, const u32 _add = c_baseAdd) noexcept
        {
            m_n = _seed;
            m_mult = _mult;
            m_add = _add;
        }

        constexpr
        u64
        U64() noexcept
        {
            m_n = m_n * m_mult + (m_n >> 32ull) + m_add;
            return m_n;
        }

        constexpr u32 U32() noexcept                                { return (u32)(U64() >> 32ull); }
        constexpr u32 U32(const u32 _min, const u32 _max) noexcept  { return _min + U32() % (_max - _min); }
        constexpr u16 U16() noexcept                                { return (u16)(U64() >> 48ull); }
        constexpr u8  U8() noexcept                                 { return (u8)(U64() >> 56ull); }
        constexpr f32 F32() noexcept                                { return (1.f / (f32)((1 << 24u) - 1)) * (f32)(U32() >> 8u); }
        constexpr f32 F32(const f32 _min, const f32 _max) noexcept  { return _min + F32() * (_max - _min); }

        static
        FastRandom
        Get() noexcept
        {
            return FastRandom{ s_global.U64() };
        }

        static u64 s_sessionSeed;
        static FastRandom s_global;

    private:

        u64 m_n;
        u64 m_mult;
        u32 m_add;
    };
}
