#pragma once

#include <Core/MemView.hpp>

#include <array>


namespace Djo
{
    DJO_TO_VIEW_IMPL_T(
        DJO_NOOP(class T, sz S),
        DJO_NOOP(std::array<T, S>), T,
        _c.data(), _c.size());
}
