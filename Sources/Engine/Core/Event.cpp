
#include <Core/Event.hpp>


namespace Djo
{
    namespace _Internal
    {
        std::atomic<EventId> s_eventIdCounter{ 0 };

        EventId
        NewEventId()
        {
            return ++s_eventIdCounter;
        }
    }
}
