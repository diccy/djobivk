#pragma once

#include <Core/ClassTraits.hpp>
#include <Core/Types.hpp>

#include <atomic>


namespace Djo
{
    class IntrusiveRefCount
    {
        DJO_NO_COPY_NO_MOVE(IntrusiveRefCount);

    public:

        IntrusiveRefCount() noexcept;
        virtual ~IntrusiveRefCount() noexcept;

        void AddRef() noexcept;
        void ReleaseRef() noexcept;
        u16 GetRefCount() const noexcept;

    private:

        std::atomic<u16> m_refCount;
    };
}
