#pragma once

#include <Core/StdString.hpp>
#include <Core/Utils.hpp>


namespace Djo
{
    struct StringView
        : public MemView<const char>
    {
        DJO_ALL_DEFAULT(StringView);

        static constexpr sz npos{ c_invalidUint<sz> }; // std style

        /*implicit*/
        template <sz N>
        constexpr
        StringView(const char (&_carray)[N]) noexcept
            : MemView<const char>{ _carray, c_memViewIgnoreEndingZeroes }
        {
        }
        constexpr
        StringView(const char* _cstr, const sz _len) noexcept
            : MemView<const char>{ _cstr, _len }
        {
        }
        explicit
        constexpr
        StringView(const char* _cstr) noexcept
            : MemView<const char>{ _cstr, StrLen(_cstr) }
        {
        }
        explicit
        StringView(const std::string& _str) noexcept
            : MemView<const char>{ ToView(_str) }
        {
        }

        /*implicit*/
        constexpr operator std::string_view() const noexcept  { return std::string_view{ data, count }; }

        constexpr bool IsEmpty() const noexcept         { return this->count == 0; }
        constexpr const char& GetLast() const noexcept  { DJO_ASSERT(!IsEmpty()); return operator[](this->count - 1); }

        constexpr
        bool
        IsEqual(const StringView& _other) const noexcept
        {
            return this->count == _other.count
                && (this->data == _other.data || (StrNCmp(this->data, _other.data, this->count) == 0));
        }

        constexpr
        sz
        Find(const char _c) const noexcept
        {
            return IsEmpty() ? npos : TStrNChrI(this->data, this->count, _c, StrNChr);
        }
        constexpr
        sz
        FindLast(const char _c) const noexcept
        {
            return IsEmpty() ? npos : TStrNChrI(this->data, this->count, _c, StrNRChr);
        }

        constexpr
        StringView&
        ShrinkBack(const sz _pos) noexcept
        {
            if (_pos < this->count)
                this->count = _pos;
            return *this;
        }
        constexpr
        StringView&
        ShrinkFront(const sz _pos) noexcept
        {
            if (_pos < this->count)
            {
                this->data += _pos;
                this->count -= _pos;
            }
            else
            {
                this->data += this->count;
                this->count = 0;
            }
            return *this;
        }
        constexpr
        StringView&
        Shrink(const sz _begin, const sz _end) noexcept
        {
            return ShrinkBack(_end).ShrinkFront(_begin);
        }

        constexpr
        StringView
        SubBack(const sz _pos) const noexcept
        {
            return StringView{ *this }.ShrinkFront(_pos);
        }
        constexpr
        StringView
        SubFront(const sz _pos) const noexcept
        {
            return StringView{ *this }.ShrinkBack(_pos);
        }
        constexpr
        StringView
        Sub(const sz _begin, const sz _end) const noexcept
        {
            return StringView{ *this }.Shrink(_begin, _end);
        }
    };
    //DJO_STATIC_ASSERT(std::is_literal_type_v<StringView>); // deprecated in c++17, still can be used to ensure the usage of StringView in constexpr

    constexpr
    bool
    operator == (const StringView& _lhs, const StringView& _rhs) noexcept
    {
        return _lhs.IsEqual(_rhs);
    }
    constexpr
    bool
    operator != (const StringView& _lhs, const StringView& _rhs) noexcept
    {
        return !(operator==(_lhs, _rhs));
    }


    DJO_HASHER_FUNC_IMPL(
        StringView,
        _Internal::StrFnv1a_64,
        _o.data, _o.count);
}
