#pragma once

#include <Core/ClassTraits.hpp>
#include <Core/CastCheck.hpp>
#include <Core/Types.hpp>


namespace Djo
{
    #define DJO_TARG_PTRHANDLER_CONVERTIBLE(T_, U_)\
        class U_,\
        DJO_TARG_ENABLE_IF((std::is_convertible_v<typename PtrHandler<U_>::PtrType*, typename PtrHandler<T_>::PtrType*>))
    
    #define DJO_STATIC_ASSERT_VIRTUAL_DESTRUCTOR(class_)\
        DJO_STATIC_ASSERT_MSG((std::is_scalar_v<class_> || std::has_virtual_destructor_v<class_>), "Calling delete on a parent class without virtual destructor is undefined behaviour")


    template <class T>
    class PtrHandler
    {
        DJO_STATIC_ASSERT_MSG((!std::is_array_v<T>), "Do not use PtrHandler for array type (use std::vector instead)");

        template <class U> friend class PtrHandler;

    public:
        using PtrType = std::remove_extent_t<T>;

        // Default
        constexpr
        PtrHandler<T>() noexcept
            : m_ptr{ nullptr }
        {}

        // By raw pointer

        constexpr
        PtrHandler<T>(nullptr_t) noexcept
            : m_ptr{ nullptr }
        {}

        explicit
        constexpr
        PtrHandler<T>(PtrType* const _p) noexcept
            : m_ptr{ _p }
        {}

        constexpr
        PtrHandler<T>&
        operator = (PtrType* const _p) noexcept
        {
            m_ptr = _p;
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        explicit
        constexpr
        PtrHandler<T>(typename PtrHandler<U>::PtrType* const _p) noexcept
            : PtrHandler<T>{ DJO_CAST<PtrType*>(_p) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        PtrHandler<T>&
        operator = (typename PtrHandler<U>::PtrType* const _p) noexcept
        {
            return operator=(DJO_CAST<PtrType*>(_p));
        }

        // Copy

        constexpr
        PtrHandler<T>(const PtrHandler<T>& _p) noexcept
            : m_ptr{ _p.m_ptr }
        {}

        constexpr
        PtrHandler<T>&
        operator = (const PtrHandler<T>& _p) noexcept
        {
            m_ptr = _p.m_ptr;
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        PtrHandler<T>(const PtrHandler<U>& _p) noexcept
            : PtrHandler<T>{ DJO_CAST<PtrType*>(_p.m_ptr) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        PtrHandler<T>&
        operator = (const PtrHandler<U>& _p) noexcept
        {
            return operator=(DJO_CAST<PtrType*>(_p.m_ptr));
        }

        // Move

        constexpr
        PtrHandler<T>(PtrHandler<T>&& _p) noexcept
            : PtrHandler<T>{}
        {
            Swap(_p);
        }

        constexpr
        PtrHandler<T>&
        operator = (PtrHandler<T>&& _p) noexcept
        {
            Swap(_p);
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        PtrHandler<T>(PtrHandler<U>&& _p) noexcept
            : PtrHandler<T>{}
        {
            Swap(_p);
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        PtrHandler<T>&
        operator = (PtrHandler<U>&& _p) noexcept
        {
            Swap(_p);
            return *this;
        }

        // Destructor

        ~PtrHandler<T>()
        {
            m_ptr = nullptr;
        }

        // Functions

        constexpr const PtrType* Get() const noexcept                 { return m_ptr; }
        constexpr PtrType* Get() noexcept                             { return m_ptr; }
        constexpr explicit operator const PtrType* () const noexcept  { return m_ptr; }
        constexpr explicit operator PtrType* () noexcept              { return m_ptr; }
        constexpr const PtrType& operator * () const noexcept         { return *m_ptr; }
        constexpr PtrType& operator * () noexcept                     { return *m_ptr; }
        constexpr const PtrType* operator -> () const noexcept        { return m_ptr; }
        constexpr PtrType* operator -> () noexcept                    { return m_ptr; }

        constexpr explicit operator bool() const noexcept  { return static_cast<bool>(m_ptr); }

        // Implicit cast to PtrHandler<const T>
        constexpr operator PtrHandler<const T> () const noexcept  { return PtrHandler<const T>{ *this }; }
        
        constexpr
        bool
        IsNull() const noexcept
        {
            return (m_ptr == nullptr);
        }

        constexpr
        PtrType*
        Pop() noexcept
        {
            PtrType* const p = m_ptr;
            m_ptr = nullptr;
            return p;
        }

        constexpr
        void
        Swap(PtrHandler<T>& _p) noexcept
        {
            PtrType* const pTmp = _p.m_ptr;
            _p.m_ptr = m_ptr;
            m_ptr = pTmp;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        void
        Swap(PtrHandler<U>& _p) noexcept
        {
            PtrType* const pTmp = DJO_CAST<PtrType*>(_p.m_ptr);
            _p.m_ptr = DJO_CAST<PtrHandler<U>::PtrType*>(m_ptr);
            m_ptr = pTmp;
        }

    protected:
        PtrType* m_ptr;
    };

    template <class T, class U = T>
    constexpr
    bool
    operator == (const PtrHandler<T>& _p1, const PtrHandler<U>& _p2) noexcept
    {
        return (_p1.Get() == _p2.Get());
    }

    template <class T, class U = T>
    constexpr
    bool
    operator == (const PtrHandler<T>& _p1, const U* const _p2) noexcept
    {
        return (_p1.Get() == _p2);
    }

    template <class T, class U = T>
    constexpr
    bool
    operator == (const U* const _p1, const PtrHandler<T>& _p2) noexcept
    {
        return (_p1 == _p2.Get());
    }


    // Unique pointer, delete data when destructed

    template <class T>
    class UPtr
        : public PtrHandler<T>
    {
        template <class U> friend class UPtr;

    public:
        // Default

        constexpr
        UPtr<T>() noexcept
            : PtrHandler<T>{}
        {}

        // By raw pointer

        constexpr
        UPtr<T>(nullptr_t) noexcept
            : PtrHandler<T>{ nullptr }
        {}

        explicit
        constexpr
        UPtr<T>(PtrType* const _p) noexcept
            : PtrHandler<T>{ _p }
        {}

        constexpr
        UPtr<T>&
        operator = (PtrType* const _p)
        {
            Reset(_p);
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        explicit
        constexpr
        UPtr<T>(typename UPtr<U>::PtrType* const _p) noexcept
            : UPtr<T>{ DJO_CAST<PtrType*>(_p) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        UPtr<T>&
        operator = (typename UPtr<U>::PtrType* const _p)
        {
            return operator=(DJO_CAST<PtrType*>(_p));
        }

        // Copy

        DJO_NO_COPY(UPtr<T>);

    private:
        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        UPtr<T>(const UPtr<U>& _p)
            = delete;

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        UPtr<T>&
        operator = (const UPtr<U>& _p)
            = delete;

        // Move

    public:
        constexpr
        UPtr<T>(UPtr<T>&& _p) noexcept
            : PtrHandler<T>{ std::forward<PtrHandler<T>>(_p) }
        {}

        constexpr
        UPtr<T>&
        operator = (UPtr<T>&& _p) noexcept
        {
            Swap(_p);
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        UPtr<T>(UPtr<U>&& _p) noexcept
            : PtrHandler<T>{ std::forward<PtrHandler<U>>(_p) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        UPtr<T>&
        operator = (UPtr<U>&& _p) noexcept
        {
            Swap(_p);
            return *this;
        }

        // Destructor

        ~UPtr<T>()
        {
            //DJO_STATIC_ASSERT_VIRTUAL_DESTRUCTOR(PtrType);
            Release();
        }

        // Functions

        // Implicit cast reference to UPtr<const T>& (undefined behaviour?)
        constexpr operator const UPtr<const T>& () const noexcept  { return reinterpret_cast<const UPtr<const T>&>(*this); }
        constexpr operator       UPtr<const T>& () noexcept        { return reinterpret_cast<      UPtr<const T>&>(*this); }

        void
        Reset(PtrType* const _p)
        {
            if (_p != m_ptr)
            {
                Release();
                m_ptr = _p;
            }
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        constexpr
        void
        Reset(typename UPtr<U>::PtrType* const _p)
        {
            Reset(DJO_CAST<PtrType*>(_p));
        }

        void
        Release()
        {
            if (!IsNull())
            {
                delete m_ptr;
                m_ptr = nullptr;
            }
        }

    private:
        PtrType* Pop() = delete;
    };


    // Reference counter pointer, based on "intrusive" implementation

    class IntrusiveRefCount;

    template <class T>
    class ShPtr
        : public PtrHandler<T>
    {
        template <class U> friend class ShPtr;

    public:
        // Default

        constexpr
        ShPtr<T>() noexcept
            : PtrHandler<T>{}
        {}

        // By raw pointer

        constexpr
        ShPtr<T>(nullptr_t) noexcept
            : PtrHandler<T>{ nullptr }
        {}

        explicit
        ShPtr<T>(PtrType* const _p) noexcept
            : PtrHandler<T>{ _p }
        {
            if (!IsNull())
                m_ptr->AddRef();
        }

        ShPtr<T>&
        operator = (PtrType* const _p)
        {
            Reset(_p);
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        explicit
        ShPtr<T>(typename ShPtr<U>::PtrType* const _p)
            : ShPtr<T>{ DJO_CAST<PtrType*>(_p) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        ShPtr<T>&
        operator = (typename ShPtr<U>::PtrType* const _p)
        {
            return operator=(DJO_CAST<PtrType*>(_p));
        }

        // Copy

        ShPtr<T>(const ShPtr<T>& _p) noexcept
            : ShPtr<T>{ _p.m_ptr }
        {}

        ShPtr<T>&
        operator = (const ShPtr<T>& _p)
        {
            operator=(_p.m_ptr);
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        ShPtr<T>(const ShPtr<U>& _p) noexcept
            : ShPtr<T>{ DJO_CAST<PtrType*>(_p.m_ptr) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        ShPtr<T>&
        operator = (const ShPtr<U>& _p)
        {
            operator=(DJO_CAST<PtrType*>(_p.m_ptr));
            return *this;
        }

        // Move

        ShPtr<T>(ShPtr<T>&& _p) noexcept
            : PtrHandler<T>{ std::forward<PtrHandler<T>>(_p) }
        {}

        ShPtr<T>&
        operator = (ShPtr<T>&& _p) noexcept
        {
            PtrHandler<T>::operator=(std::forward<PtrHandler<T>>(_p));
            return *this;
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        ShPtr<T>(ShPtr<U>&& _p) noexcept
            : PtrHandler<T>{ std::forward<PtrHandler<U>>(_p) }
        {}

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        ShPtr<T>&
        operator = (ShPtr<U>&& _p) noexcept
        {
            PtrHandler<T>::operator=(std::forward<PtrHandler<U>>(_p));
            return *this;
        }

        // Destructor

        ~ShPtr<T>()
        {
            DJO_STATIC_ASSERT_VIRTUAL_DESTRUCTOR(PtrType);
            DJO_STATIC_ASSERT_MSG(std::is_base_of_v<IntrusiveRefCount, PtrType>, "Must derive from Djo::IntrusiveRefCount");
            Release();
        }

        // Functions

        // Implicit cast reference to ShPtr<const T>& (undefined behaviour?)
        constexpr operator const ShPtr<const T>& () const noexcept  { return reinterpret_cast<const ShPtr<const T>&>(*this); }
        constexpr operator       ShPtr<const T>& () noexcept        { return reinterpret_cast<      ShPtr<const T>&>(*this); }

        void
        Reset(PtrType* const _p)
        {
            if (_p != m_ptr)
            {
                Release();
                m_ptr = _p;
                if (!IsNull())
                    m_ptr->AddRef();
            }
        }

        template <DJO_TARG_PTRHANDLER_CONVERTIBLE(T, U)>
        void
        Reset(typename ShPtr<U>::PtrType* const _p)
        {
            Reset(DJO_CAST<PtrType*>(_p));
        }

        void
        Release()
        {
            if (!IsNull())
            {
                m_ptr->ReleaseRef();
                if (m_ptr->GetRefCount() == 0)
                    delete m_ptr;

                m_ptr = nullptr;
            }
        }

    private:
        PtrType* Pop() = delete;
    };

    #undef DJO_STATIC_ASSERT_VIRTUAL_DESTRUCTOR
    #undef DJO_TARG_PTRHANDLER_CONVERTIBLE
}
