
#include <Core/GlobalLog.hpp>


namespace Djo
{
    namespace GlobalLog
    {
        Logger* g_coreLogger{ nullptr };
        Logger* g_appLogger{ nullptr };

        EResult
        Init()
        {
            g_coreLogger = DJO_CORE_NEW Logger{ "DjoCore" };
            g_appLogger = DJO_CORE_NEW Logger{ "App" };
            return EResult::OK;
        }

        void
        Shutdown()
        {
            delete g_appLogger;
            delete g_coreLogger;
            g_appLogger = nullptr;
            g_coreLogger = nullptr;
        }
    }
}
