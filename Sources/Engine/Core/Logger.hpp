#pragma once

#include <Core/StdStringStream.hpp>
#include <Core/TypeTraits.hpp>
#include <Core/Pointers.hpp>
#include <Core/StdVector.hpp>
#include <Core/IStringSink.hpp>


namespace Djo
{
    class LogOutputBuffer
        : public std::stringbuf
    {
        DJO_DEFAULT_CTOR_DTOR(LogOutputBuffer);
        DJO_NO_COPY_NO_MOVE(LogOutputBuffer);

    public:

        void AddSink(ShPtr<IStringSink> _sink);

    protected:

        virtual int sync() override;

    private:

        std::vector<ShPtr<IStringSink>> m_sinks{};
    };

    enum class ELogLevel
    {
        Trace = 0,
        Info,
        Warning,
        Error,

        DJO_ENUM_COUNT_INVALID
    };

    class Logger
        : public std::ostream
    {
        DJO_NO_DEFAULT_CTOR(Logger);
        DJO_NO_COPY_NO_MOVE(Logger);

    public:

        explicit Logger(const char* _name);
        ~Logger() = default;

        DJO_INLINE void AddSink(ShPtr<IStringSink> _sink)  { m_buffer.AddSink(std::move(_sink)); }

        Logger& StartLogging(ELogLevel _level);

    private:

        std::string m_name;
        LogOutputBuffer m_buffer;
    };
}
