#pragma once

#include <Core/IntrusiveRefCount.hpp>
#include <Core/StdString.hpp>


namespace Djo
{
    class IStringSink
        : public IntrusiveRefCount
    {
    public:

        virtual void Output(const std::string& _str) = 0;
    };
}
