
#include <Core/Logger.hpp>

#include <Core/TypeTraits.hpp>


namespace Djo
{
    // LogOutputBuffer

    void
    LogOutputBuffer::AddSink(ShPtr<IStringSink> _sink)
    {
        m_sinks.emplace_back(std::move(_sink));
    }

    int
    LogOutputBuffer::sync()
    {
        for (ShPtr<IStringSink>& sink : m_sinks)
        {
            sink->Output(str());
        }
        str(std::string{});
        return 0;
    }


    // Logger

    Logger::Logger(const char* _name)
        : std::ostream{ &m_buffer }
        , m_name{ _name }
        , m_buffer{}
    {}

    Logger&
    Logger::StartLogging(const ELogLevel _level)
    {
        DJO_TODO("Log timestamp");

        static const char* s_str[ECount<ELogLevel>]
        {
            "Trace",
            "Info",
            "Warn",
            "Error",
        };

        *this << '[' << m_name << "][" << s_str[EValue(_level)] << "] ";
        return *this;
    }
}
