#pragma once

#include <Core/TypeTraits.hpp>

#include <atomic>
#include <type_traits>


namespace Djo
{
    using EventId = u16;

    namespace _Internal
    {
        extern std::atomic<EventId> s_eventIdCounter;

        EventId NewEventId();

        template <typename TEvent>
        EventId
        GetEventId()
        {
            static const EventId s_id{ NewEventId() };
            return s_id;
        }
    }

    template <typename TEvent>
    EventId
    GetEventId()
    {
        return _Internal::GetEventId<PureType_t<TEvent>>();
    }
}
