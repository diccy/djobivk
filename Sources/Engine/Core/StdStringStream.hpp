#pragma once

#include <Core/StringView.hpp>

#include <sstream>


namespace Djo
{
    template <class TTraits>
    std::basic_ostream<char, TTraits>&
    operator << (std::basic_ostream<char, TTraits>& _os, const StringView& _sv)
    {
        _os << std::string_view{ _sv };
        return _os;
    }
}
