#pragma once

#include <Core/Hash.hpp>
#include <Core/MemView.hpp>

#include <type_traits>


namespace Djo
{
    // Minimal growing array with compile time known fixed max capacity
    // Not safe at all
    // WARNING: destructors not called, do not use with non-trivialy constructible/destructible elements

    template <typename T, u32 TSize>
    class MaxArray
    {
        DJO_STATIC_ASSERT_MSG(TSize > 0, "MaxArray size must be grater than 0");
        DJO_STATIC_ASSERT_MSG(TSize > 1, "Use a straight class instance instead");

    public:

        using DataType = T;
        using Iterator = DataType*;
        using ConstIterator = const DataType*;

        constexpr
        MaxArray<T, TSize>() noexcept
            : m_data{}
            , m_size{ 0 }
        {}

        explicit
        constexpr
        MaxArray<T, TSize>(const u32 _size) noexcept
            : MaxArray<T, TSize>{}
        {
            Resize(_size);
        }

        MaxArray<T, TSize>(std::initializer_list<DataType> _list) noexcept
            : MaxArray<T, TSize>{}
        {
            Resize(static_cast<u32>(_list.size()));
            std::initializer_list<DataType>::const_iterator cit = _list.begin();
            for (u32 i = 0; i < m_size; ++i)
            {
                m_data[i] = *cit++;
            }
        }

        MaxArray<T, TSize>(const MaxArray<T, TSize>& _other) noexcept
            : MaxArray<T, TSize>{}
        {
            Resize(_other.Size());
            for (u32 i = 0; i < m_size; ++i)
            {
                m_data[i] = _other[i];
            }
        }

        template <u32 USize>
        explicit
        MaxArray<T, TSize>(const MaxArray<T, USize>& _other) noexcept
            : MaxArray<T, TSize>{}
        {
            Resize(_other.GetSize());
            for (u32 i = 0; i < m_size; ++i)
            {
                m_data[i] = _other[i];
            }
        }

        MaxArray<T, TSize>(MaxArray<T, TSize>&& _other) noexcept
            : MaxArray<T, TSize>{}
        {
            Swap(std::forward<MaxArray<T, TSize>>(_other));
        }

        ~MaxArray<T, TSize>() noexcept
        {
            DJO_STATIC_ASSERT_MSG(
                (std::is_trivially_constructible_v<DataType>
                && std::is_nothrow_constructible_v<DataType>
                && std::is_trivially_destructible_v<DataType>
                && std::is_nothrow_destructible_v<DataType>),
                "MaxArray does NOT call destructors. "
                "Do NOT use on class with side effect destructor."
            );
        }

        MaxArray<T, TSize>&
        operator = (const MaxArray<T, TSize>& _other) noexcept
        {
            Resize(_other.GetSize());
            for (u32 i = 0; i < m_size; ++i)
            {
                m_data[i] = _other[i];
            }
            return *this;
        }

        template <u32 USize>
        MaxArray<T, TSize>&
        operator = (const MaxArray<T, USize>& _other) noexcept
        {
            Resize(_other.GetSize());
            for (u32 i = 0; i < m_size; ++i)
            {
                m_data[i] = _other[i];
            }
            return *this;
        }

        MaxArray<T, TSize>&
        operator = (MaxArray<T, TSize>&& _other) noexcept
        {
            Swap(std::forward<MaxArray<T, TSize>>(_other));
            return *this;
        }

        DJO_INLINE
        void
        Swap(MaxArray<T, TSize>&& _other) noexcept
        {
            std::swap(m_data, _other.m_data);
            std::swap(m_size, _other.m_size);
        }

        static constexpr u32 GetCapacity()  { return TSize; }

        DJO_INLINE const DataType* GetData() const noexcept  { return &m_data[0]; }
        DJO_INLINE DataType* GetData() noexcept              { return &m_data[0]; }
        DJO_INLINE u32 GetSize() const noexcept              { return m_size; }

        DJO_INLINE void Resize(const u32 _size)       { DJO_ASSERT(_size <= TSize); m_size = _size < TSize ? _size : TSize; }
        DJO_INLINE void PushBack(const DataType& _a)  { DJO_ASSERT(m_size < TSize); m_data[m_size++] = _a; }
        DJO_INLINE void PushBack(DataType&& _a)       { DJO_ASSERT(m_size < TSize); m_data[m_size++] = std::forward<DataType>(_a); }
        DJO_INLINE const DataType& PopBack()          { DJO_ASSERT(m_size > 0); return m_data[--m_size]; }

        DJO_INLINE const DataType& GetFront() const  { DJO_ASSERT(m_size > 0); return m_data[0]; }
        DJO_INLINE DataType& GetFront()              { DJO_ASSERT(m_size > 0); return m_data[0]; }
        DJO_INLINE const DataType& GetBack() const   { DJO_ASSERT(m_size > 0); return m_data[m_size - 1]; }
        DJO_INLINE DataType& GetBack()               { DJO_ASSERT(m_size > 0); return m_data[m_size - 1]; }

        DJO_INLINE const DataType& operator[] (const u32 _i) const  { DJO_ASSERT(_i < m_size); return m_data[_i]; }
        DJO_INLINE DataType& operator[] (const u32 _i)              { DJO_ASSERT(_i < m_size); return m_data[_i]; }

        template <typename TIt>
        bool
        Assign(TIt _begin, const TIt _end)
        {
            const u32 size = static_cast<u32>(_end - _begin);
            if (size > TSize)
            {
                DJO_ASSERT_MSG(false, "Too many data to assign");
                return false;
            }
            m_size = 0;
            for (; _begin != _end; ++_begin)
            {
                PushBack(*_begin);
            }
            return true;
        }

        // std
        DJO_INLINE Iterator begin() noexcept             { return &m_data[0]; }
        DJO_INLINE ConstIterator begin() const noexcept  { return &m_data[0]; }
        DJO_INLINE Iterator end() noexcept               { return (DataType*)(m_data) + m_size; }
        DJO_INLINE ConstIterator end() const noexcept    { return (const DataType*)(m_data) + m_size; }

    private:

        u32 m_size;
        T m_data[TSize];
    };


    DJO_TO_VIEW_IMPL_T(
        DJO_NOOP(class T, u32 TSize),
        DJO_NOOP(MaxArray<T, TSize>), T,
        _c.Data(), _c.Size());

    DJO_HASHER_IMPL_T(
        DJO_NOOP(class T, u32 TSize),
        DJO_NOOP(MaxArray<T, TSize>),
        _o.Data(), _o.Size() * sizeof(T));
}
