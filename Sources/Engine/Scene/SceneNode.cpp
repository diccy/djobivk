
#include <Scene/SceneNode.hpp>

#include <Math/Matrix33.hpp>


namespace Djo
{
    SceneNode::SceneNode()
        : ParentNodeType{}
        , IntrusiveRefCount{}
        , m_worldTrans{ 1.f }
        , m_localTrans{ 1.f }
        , m_worldRot{ 0.f, 0.f, 0.f, 1.f }
        , m_localRot{ 0.f, 0.f, 0.f, 1.f }
    {
    }

    SceneNode::~SceneNode()
    {
    }

    void
    SceneNode::AddChild(SceneNode* const _newChild)
    {
        ParentNodeType::AddChild(_newChild);
    }

    void
    SceneNode::Unlink()
    {
        ParentNodeType::Unlink();
    }

    void
    SceneNode::SetWorldTransform(const Mat44f& _m)
    {
        m_worldTrans = _m;
        UpdateLocalTransformFromWorld();
    }

    void
    SceneNode::SetLocalTransform(const Mat44f& _m)
    {
        m_localTrans = _m;
        UpdateWorldTransformFromLocal();
    }

    void
    SceneNode::SetWorldPosition(const Vec3f& _pos)
    {
        Mat44Pos(m_worldTrans) = _pos;
        UpdateLocalTransformFromWorld();
    }

    void
    SceneNode::SetLocalPosition(const Vec3f& _pos)
    {
        Mat44Pos(m_localTrans) = _pos;
        UpdateWorldTransformFromLocal();
    }

    void
    SceneNode::SetWorldRotation(const Quatf& _q)
    {
        const Mat33f rotMatrix{ glm::mat3_cast(_q) };
        reinterpret_cast<Vec3f&>(m_worldTrans[0]) = rotMatrix[0];
        reinterpret_cast<Vec3f&>(m_worldTrans[1]) = rotMatrix[1];
        reinterpret_cast<Vec3f&>(m_worldTrans[2]) = rotMatrix[2];
        m_worldRot = _q;
        UpdateLocalTransformFromWorld();
    }

    void
    SceneNode::SetLocalRotation(const Quatf& _q)
    {
        const Mat33f rotMatrix{ glm::mat3_cast(_q) };
        reinterpret_cast<Vec3f&>(m_localTrans[0]) = rotMatrix[0];
        reinterpret_cast<Vec3f&>(m_localTrans[1]) = rotMatrix[1];
        reinterpret_cast<Vec3f&>(m_localTrans[2]) = rotMatrix[2];
        m_localRot = _q;
        UpdateWorldTransformFromLocal();
    }

    void
    SceneNode::UpdateWorldTransformFromLocal()
    {
        if (GetParent() != nullptr)
        {
            m_worldTrans = GetParent()->GetWorldTransform() * m_localTrans;
            m_worldRot = glm::quat_cast(m_worldTrans);
        }
        else
        {
            m_worldTrans = m_localTrans;
            m_worldRot = m_localRot;
        }

        if (GetChild() != nullptr)
            UpdateChildrenTransform();
    }

    void
    SceneNode::UpdateLocalTransformFromWorld()
    {
        if (GetParent() != nullptr)
        {
            m_localTrans = glm::inverse(GetParent()->GetWorldTransform()) * m_worldTrans;
            m_localRot = glm::quat_cast(m_localTrans);
        }
        else
        {
            m_localTrans = m_worldTrans;
            m_localRot = m_worldRot;
        }

        if (GetChild() != nullptr)
            UpdateChildrenTransform();
    }

    void
    SceneNode::UpdateChildrenTransform()
    {
        for (Iterator it{ GetChild() }; it.Get() != nullptr; it.Next())
            it.Get()->UpdateWorldTransformFromLocal();
    }
}
