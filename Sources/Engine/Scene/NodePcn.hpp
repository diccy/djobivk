#pragma once

#include <Core/Core.hpp>
#include <Core/ClassTraits.hpp>
#include <Core/CastCheck.hpp>


namespace Djo
{
    // Templated hierarchy node with Parent, Child, Next (pcn)
    // Usage
    // class MyNode : public NodePCN<MyNode> {}

    template <class TNodeImpl>
    class NodePcnT
    {
        DJO_NO_COPY_DEFAULT_MOVE(NodePcnT<TNodeImpl>);

    public:

        using NodeType = TNodeImpl;

        NodePcnT<TNodeImpl>()
            : m_parent{ nullptr }
            , m_child{ nullptr }
            , m_nextSibling{ nullptr }
        {
        }

        virtual
        ~NodePcnT<TNodeImpl>()
        {
            DJO_STATIC_ASSERT_MSG((std::is_base_of_v<NodePcnT<TNodeImpl>, NodeType>),
                                  "Usage: class MyNode : public NodePcnT<MyNode>");
            m_parent = nullptr;
            m_child = nullptr;
            m_nextSibling = nullptr;
        }

        DJO_INLINE const NodeType* GetParent() const           { return m_parent; }
        DJO_INLINE       NodeType* GetParent()                 { return m_parent; }
        DJO_INLINE const NodeType* GetChild() const            { return m_child; }
        DJO_INLINE       NodeType* GetChild()                  { return m_child; }
        DJO_INLINE const NodeType* GetNextSibling() const      { return m_nextSibling; }
        DJO_INLINE       NodeType* GetNextSibling()            { return m_nextSibling; }
        DJO_INLINE const NodeType* GetPreviousSibling() const  { return GetPreviousSibling_Internal(); }
        DJO_INLINE       NodeType* GetPreviousSibling()        { return GetPreviousSibling_Internal(); }

        virtual
        void
        AddChild(NodeType* const _newChild)
        {
            if (GetChild() == nullptr)
            {
                m_child = _newChild;
            }
            else
            {
                NodeType* lastChild = GetChild();
                while (lastChild->GetNextSibling() != nullptr)
                    lastChild = lastChild->GetNextSibling();

                lastChild->m_nextSibling = _newChild;
            }
            _newChild->m_parent = DJO_CAST<NodeType*>(this);
            _newChild->m_nextSibling = nullptr;
        }

        virtual
        void
        Unlink()
        {
            NodeType* const parent = GetParent();
            if (parent != nullptr)
            {
                if (parent->GetChild() == static_cast<void*>(this))
                {
                    parent->m_child = GetNextSibling();
                }
                else
                {
                    NodeType* const previous = GetPreviousSibling();
                    DJO_ASSERT(previous->GetNextSibling() == static_cast<void*>(this));
                    previous->m_nextSibling = GetNextSibling();
                }
                m_nextSibling = nullptr;
                m_parent = nullptr;
            }
        }

        u32
        GetChildCount() const
        {
            u32 childCount = 0;
            const NodeType* child = GetChild();
            while (child != nullptr)
            {
                ++childCount;
                child = child->GetNextSibling();
            }
            return childCount;
        }

    private:

        NodeType*
        GetPreviousSibling_Internal() const
        {
            const NodeType* const parent = GetParent();
            if (parent == nullptr)
                return nullptr;

            const NodeType* previous = parent->GetChild();
            if (previous == static_cast<const void*>(this))
                return nullptr;

            while (previous->GetNextSibling() != static_cast<const void*>(this))
                previous = previous->GetNextSibling();

            return const_cast<NodeType*>(previous);
        }

        NodeType* m_parent;
        NodeType* m_child;
        NodeType* m_nextSibling;
    };


    template <class TNodeImpl, bool TIsConst>
    class NodePcnIteratorT
    {
        DJO_NO_DEFAULT_CTOR(DJO_UNPAR((NodePcnIteratorT<TNodeImpl, TIsConst>)))

    public:

        DJO_STATIC_ASSERT_MSG(
            (std::is_base_of_v<NodePcnT<TNodeImpl>, TNodeImpl>),
            "TNodeImpl must inherit from NodePcnT<TNodeImpl>"
        );

        using IteratorType = NodePcnIteratorT<TNodeImpl, TIsConst>;
        using NodeType = std::conditional_t<TIsConst, const TNodeImpl, TNodeImpl>;

        DJO_DEFAULT_COPY_DEFAULT_MOVE(DJO_UNPAR((NodePcnIteratorT<TNodeImpl, TIsConst>)));

        NodePcnIteratorT<TNodeImpl, TIsConst>(NodeType* const _node)
            : m_start{ nullptr }
            , m_current{ nullptr }
        {
            Reset(_node);
        }

        ~NodePcnIteratorT<TNodeImpl, TIsConst>()
        {
            m_start = nullptr;
            m_current = nullptr;
        }

        DJO_INLINE operator NodeType* () const     { return Get(); }
        DJO_INLINE NodeType* operator * () const   { return Get(); }
        DJO_INLINE NodeType* Get() const           { return m_current; }

        DJO_INLINE void Reset(NodeType* const _node)  { m_start = _node; m_current = _node; }
        DJO_INLINE void Restart()                     { m_current = m_start; }

        void Next();
        void Previous();

    private:

        NodeType* m_start;
        NodeType* m_current;
    };

    template <class THNodeImpl> using NodePcnConstIt = NodePcnIteratorT<THNodeImpl, true>;
    template <class THNodeImpl> using NodePcnIt      = NodePcnIteratorT<THNodeImpl, false>;


    /*
        Visiting order:

                1
             /  |  \
            2   5   8
           /|  /|  /| \
          3 4 6 7 9 10 11
    */

    template <class THNodeImpl, bool TIsConst>
    void
    NodePcnIteratorT<THNodeImpl, TIsConst>::Next()
    {
        if (m_current != nullptr)
        {
            // Go deep
            NodeType* next = m_current->GetChild();
            if (next == nullptr)
            {
                if (m_current == m_start)
                {
                    // End
                    m_current = nullptr;
                }
                else
                {
                    // Go horizontal
                    next = m_current->GetNextSibling();
                    if (next == nullptr)
                    {
                        // Go back up
                        NodeType* parent = m_current->GetParent();
                        while (parent != nullptr && parent != m_start)
                        {
                            if (parent->GetNextSibling() != nullptr)
                            {
                                next = parent->GetNextSibling();
                                break;
                            }
                            parent = parent->GetParent();
                        }
                    }
                }
            }
            m_current = next;
        }
    }

    template <class THNodeImpl, bool TIsConst>
    void
    NodePcnIteratorT<THNodeImpl, TIsConst>::Previous()
    {
        if (m_current != nullptr)
        {
            if (m_current == m_start)
            {
                // End
                m_current = nullptr;
            }
            else
            {
                NodeType* previous = m_current->GetPreviousSibling();
                while (previous != nullptr && previous->GetChild() != nullptr)
                {
                    // Go deep
                    previous = previous->GetChild();
                    while (previous->GetNextSibling() != nullptr)
                        previous = previous->GetNextSibling();
                }
                if (previous != nullptr)
                {
                    m_current = previous;
                }
                else
                {
                    // Go back up
                    m_current = m_current->GetParent();
                }
            }
        }
    }
}
