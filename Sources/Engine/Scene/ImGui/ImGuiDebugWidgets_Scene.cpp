
#include <Scene/ImGui/ImGuiDebugWidgets_Scene.hpp>

#include <Scene/SceneNode.hpp>
#include <ImGui/ImGui.hpp>
#include <Math/Math.hpp>


namespace Djo
{
    void
    ImGuiDebugWidget(SceneNode& _node)
    {
        ::ImGui::Text("Local");
        {
            const Vec3f& localPos = _node.GetLocalPosition();
            f32 localPosValues[3]{ localPos.x, localPos.y, localPos.z };
            if (::ImGui::InputFloat3("Local Pos", localPosValues, "%.2f"))
            {
                _node.SetLocalPosition(Vec3f{ localPosValues[0], localPosValues[1], localPosValues[2] });
            }
        }
        {
            Vec3f localEulerAngles = glm::eulerAngles(_node.GetLocalRotation());
            localEulerAngles.z = Abs(localEulerAngles.z);
            if (::ImGui::InputFloat3("Local Rot", &localEulerAngles[0], "%.2f"))
            {
                _node.SetLocalRotation(Quatf{ localEulerAngles });
            }
        }
        ::ImGui::Text("World");
        {
            const Vec3f& worldPos = _node.GetWorldPosition();
            f32 worldPosValues[3]{ worldPos.x, worldPos.y, worldPos.z };
            if (::ImGui::InputFloat3("World Pos", worldPosValues, "%.2f"))
            {
                _node.SetWorldPosition(Vec3f{ worldPosValues[0], worldPosValues[1], worldPosValues[2] });
            }
        }
        {
            Vec3f worldEulerAngles = glm::eulerAngles(_node.GetWorldRotation());
            worldEulerAngles.z = Abs(worldEulerAngles.z);
            if (::ImGui::InputFloat3("World Rot", &worldEulerAngles[0], "%.2f"))
            {
                _node.SetWorldRotation(Quatf{ worldEulerAngles });
            }
        }
    }
}
