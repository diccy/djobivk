#pragma once

#include <Core/IntrusiveRefCount.hpp>
#include <Core/StructUtils.hpp>
#include <Math/Vector3.hpp>
#include <Math/Quaternion.hpp>
#include <Math/Matrix44.hpp>
#include <Scene/NodePcn.hpp>


namespace Djo
{
    class SceneNode
        : public NodePcnT<SceneNode>
        , public IntrusiveRefCount
    {
    public:

        using ParentNodeType = NodePcnT<SceneNode>;
        using ConstIterator =  NodePcnConstIt<SceneNode>;
        using Iterator =       NodePcnIt<SceneNode>;

        SceneNode();
        virtual ~SceneNode() override;

        virtual void AddChild(SceneNode* _newChild) override;
        virtual void Unlink() override;

        DJO_INLINE const Mat44f& GetWorldTransform() const { return m_worldTrans; }
        DJO_INLINE const Mat44f& GetLocalTransform() const { return m_worldTrans; }
        void SetWorldTransform(const Mat44f& _m);
        void SetLocalTransform(const Mat44f& _m);

        DJO_INLINE const Vec3f& GetWorldPosition() const { return Mat44Pos(m_worldTrans); }
        DJO_INLINE const Vec3f& GetLocalPosition() const { return Mat44Pos(m_localTrans); }
        void SetWorldPosition(const Vec3f& _v);
        void SetLocalPosition(const Vec3f& _v);

        DJO_INLINE const Quatf& GetWorldRotation() const { return m_worldRot; }
        DJO_INLINE const Quatf& GetLocalRotation() const { return m_localRot; }
        void SetWorldRotation(const Quatf& _q);
        void SetLocalRotation(const Quatf& _q);

        void UpdateWorldTransformFromLocal();
        void UpdateLocalTransformFromWorld();

    private:

        friend void ImGuiDebugWidget(SceneNode&);

        void UpdateChildrenTransform();

        Mat44f m_worldTrans;
        Mat44f m_localTrans;
        Quatf m_worldRot;
        Quatf m_localRot;
    };
}
