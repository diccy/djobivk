
#include "Core/TestsCore.hpp"

#include <Core/MaxArray.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        MaxArray_All()
        {
            DJO_UTESTS_INIT();

            {
                MaxArray<u32, 12> ma_u32_12;
                DJO_UTEST(ma_u32_12.GetCapacity() == 12);
                DJO_UTEST(ma_u32_12.GetSize() == 0);
                ma_u32_12.PushBack(10);
                DJO_UTEST(ma_u32_12.GetSize() == 1);
                DJO_UTEST(ma_u32_12[0] == 10);
                u32 n = ma_u32_12.PopBack();
                DJO_UTEST(ma_u32_12.GetSize() == 0);
                DJO_UTEST(n == 10);
                ma_u32_12.Resize(5);
                DJO_UTEST(ma_u32_12.GetSize() == 5);
                DJO_UTEST(ma_u32_12[0] == n); // no erasure during the process
            }
            {
                MaxArray<u32, 12> ma_u32_12{ 5 };
                DJO_UTEST(ma_u32_12.GetSize() == 1);
                DJO_UTEST(ma_u32_12[0] == 5);
            }
            {
                MaxArray<u32, 12> ma_u32_12{ { 0, 1, 2, 3 } };
                DJO_UTEST(ma_u32_12.GetSize() == 4);
                DJO_UTEST(ma_u32_12[0] == 0);
                DJO_UTEST(ma_u32_12[1] == 1);
                DJO_UTEST(ma_u32_12[2] == 2);
                DJO_UTEST(ma_u32_12[3] == 3);
                MaxArray<u32, 6> ma_u32_6{ ma_u32_12 };
                DJO_UTEST(ma_u32_6.GetCapacity() == 6);
                DJO_UTEST(ma_u32_6.GetSize() == 4);
                DJO_UTEST(ma_u32_6[0] == 0);
                DJO_UTEST(ma_u32_6[1] == 1);
                DJO_UTEST(ma_u32_6[2] == 2);
                DJO_UTEST(ma_u32_6[3] == 3);
                MaxArray<u32, 10> ma_u32_10{ { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 } };
                DJO_UTEST(ma_u32_10.GetCapacity() == 10);
                DJO_UTEST(ma_u32_10.GetSize() == 10);
                DJO_DISABLE_ASSERT_WHILE(ma_u32_6 = ma_u32_10);
                DJO_UTEST(ma_u32_6.GetCapacity() == 6);
                DJO_UTEST(ma_u32_6.GetSize() == 6);
                DJO_UTEST(ma_u32_6[0] == 10);
                DJO_UTEST(ma_u32_6[1] == 11);
                DJO_UTEST(ma_u32_6[2] == 12);
                DJO_UTEST(ma_u32_6[3] == 13);
                DJO_UTEST(ma_u32_6[4] == 14);
                DJO_UTEST(ma_u32_6[5] == 15);
            }
            DJO_DISABLE_ASSERT_WHILE(DJO_UNPAR((
                const MaxArray<u32, 2> ma_u32_1{ { 1, 2, 3, 4 } };
                DJO_UTEST(ma_u32_1.GetCapacity() == 2);
                DJO_UTEST(ma_u32_1[0] == 1);
            )))
            DJO_UTESTS_END();
        }
    }
}
