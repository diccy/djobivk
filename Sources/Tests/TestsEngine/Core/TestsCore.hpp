#pragma once

#include "Tests.hpp"


namespace Djo
{
    namespace Tests
    {
        TestsResult TestsCore();

        TestsResult Event_All();
        TestsResult EventDispatcher_All();
        TestsResult MaxArray_All();
        TestsResult Pointers_All();
        TestsResult StringView_All();
        TestsResult Time_All();
        TestsResult Utils_All();
    }
}
