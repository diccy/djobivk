
#include "Core/TestsCore.hpp"

#include <Core/EventDispatcher.hpp>
#include <Core/EventListener.hpp>
#include <Core/Event.hpp>

#include <unordered_map>


namespace Djo
{
    namespace Tests
    {
        struct Event1 {};
        struct Event2 {};
        struct EventN { int n; };

        struct AddOnEvent
            : public TIEventListener<Event1>
            , public TIEventListener<Event2>
            , public TIEventListener<EventN>
        {
        public:
            AddOnEvent(int* _i) : m_i{ *_i } {}
            void OnEvent(const Event1&) override { m_i += 1; }
            void OnEvent(const Event2&) override { m_i += 2; }
            void OnEvent(const EventN& _event) override { m_i += _event.n; }
        private:
            int& m_i;
        };

        TestsResult
        EventDispatcher_Basics()
        {
            DJO_UTESTS_INIT();

            Event1 event1{};
            Event2 event2{};
            EventN eventN{ 10 };

            int i = 0;
            AddOnEvent addOnEvent{ &i };
            TEventDispatcher<Event1> event1Dispatcher;
            TEventDispatcher<Event2> event2Dispatcher;
            TEventDispatcher<EventN> eventNDispatcher;

            i = 0;
            event1Dispatcher.SendEvent(event1);
            event2Dispatcher.SendEvent(event2);
            eventNDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 0);

            i = 0;
            event1Dispatcher.Register(&addOnEvent);
            event2Dispatcher.Register(&addOnEvent);
            eventNDispatcher.Register(&addOnEvent);
            event1Dispatcher.SendEvent(event1);
            event2Dispatcher.SendEvent(event2);
            eventNDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 13);

            i = 0;
            event1Dispatcher.SendEvent(event1);
            event2Dispatcher.SendEvent(event2);
            eventNDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 13);

            i = 0;
            event1Dispatcher.Unregister(&addOnEvent);
            event2Dispatcher.Unregister(&addOnEvent);
            eventNDispatcher.Unregister(&addOnEvent);
            event1Dispatcher.SendEvent(event1);
            event2Dispatcher.SendEvent(event2);
            eventNDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 0);

            i = 0;
            event1Dispatcher.Register(&addOnEvent);
            event1Dispatcher.Register(&addOnEvent);
            event1Dispatcher.Register(&addOnEvent);
            event1Dispatcher.Register(&addOnEvent);
            event1Dispatcher.Register(&addOnEvent);
            event1Dispatcher.SendEvent(event1);
            DJO_UTEST(i == 5);

            i = 0;
            event1Dispatcher.Unregister(&addOnEvent);
            event1Dispatcher.SendEvent(event1);
            DJO_UTEST(i == 0);

            AddOnEvent addOnEvent2{ &i };
            i = 0;
            event1Dispatcher.Register(&addOnEvent);
            event1Dispatcher.Register(&addOnEvent2);
            event1Dispatcher.SendEvent(event1);
            DJO_UTEST(i == 2);

            i = 0;
            event1Dispatcher.Unregister(&addOnEvent2);
            event1Dispatcher.SendEvent(event1);
            DJO_UTEST(i == 1);

            i = 0;
            event1Dispatcher.Unregister(&addOnEvent);
            event1Dispatcher.SendEvent(event1);
            DJO_UTEST(i == 0);

            DJO_UTESTS_END();
        }

        class GenericEventDispatcher
        {
            DJO_NO_COPY_NO_MOVE(GenericEventDispatcher);

        public:

            GenericEventDispatcher()
                : m_perEventDispatchers{}
            {}

            ~GenericEventDispatcher()
            {
                Clear();
            }

            template <typename TEvent>
            TEventDispatcher<TEvent>* GetDispatcher()
            {
                const EventId id = GetEventId<TEvent>();
                if (m_perEventDispatchers.count(id) == 0)
                    return nullptr;
                return static_cast<TEventDispatcher<TEvent>*>(m_perEventDispatchers.at(id).Get());
            }

            template <typename TEvent>
            TEventDispatcher<TEvent>* GetOrCreateDispatcher()
            {
                const EventId id = GetEventId<TEvent>();
                if (m_perEventDispatchers.count(id) == 0)
                {
                    TEventDispatcher<TEvent>* const dispatcher = new TEventDispatcher<TEvent>{};
                    const auto&& [it, inserted] = m_perEventDispatchers.emplace(id, dispatcher);
                    return inserted ? dispatcher : nullptr;
                }
                return static_cast<TEventDispatcher<TEvent>*>(m_perEventDispatchers.at(id).Get());
            }

            template <typename TEvent>
            void Register(TIEventListener<TEvent>* const _listener)
            {
                if (TEventDispatcher<TEvent>* const dispatcher = GetOrCreateDispatcher<TEvent>())
                    dispatcher->Register(_listener);
            }

            template <typename TEvent>
            void Unregister(TIEventListener<TEvent>* const _listener)
            {
                if (TEventDispatcher<TEvent>* const dispatcher = GetOrCreateDispatcher<TEvent>())
                    dispatcher->Unregister(_listener);
            }

            template <typename TEvent>
            void SendEvent(const TEvent& _event)
            {
                if (TEventDispatcher<TEvent>* const dispatcher = GetDispatcher<TEvent>())
                    dispatcher->SendEvent(_event);
            }

            void
            Clear()
            {
                m_perEventDispatchers.clear();
            }

        private:

            std::unordered_map<EventId, UPtr<IEventDispatcher>> m_perEventDispatchers;
        };

        TestsResult
        EventDispatcher_GenericEventDispatcher()
        {
            DJO_UTESTS_INIT();

            Event1 event1{};
            Event2 event2{};
            EventN eventN{ 10 };

            int i = 0;
            AddOnEvent addOnEvent{ &i };
            GenericEventDispatcher genDispatcher;

            i = 0;
            genDispatcher.SendEvent(event1);
            genDispatcher.SendEvent(event2);
            genDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 0);

            i = 0;
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.Register<Event2>(&addOnEvent);
            genDispatcher.Register<EventN>(&addOnEvent);
            genDispatcher.SendEvent(event1);
            genDispatcher.SendEvent(event2);
            genDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 13);

            i = 0;
            genDispatcher.SendEvent(event1);
            genDispatcher.SendEvent(event2);
            genDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 13);

            i = 0;
            genDispatcher.Unregister<Event1>(&addOnEvent);
            genDispatcher.Unregister<Event2>(&addOnEvent);
            genDispatcher.Unregister<EventN>(&addOnEvent);
            genDispatcher.SendEvent(event1);
            genDispatcher.SendEvent(event2);
            genDispatcher.SendEvent(eventN);
            DJO_UTEST(i == 0);

            i = 0;
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.SendEvent(event1);
            DJO_UTEST(i == 5);

            i = 0;
            genDispatcher.Unregister<Event1>(&addOnEvent);
            genDispatcher.SendEvent(event1);
            DJO_UTEST(i == 0);

            AddOnEvent addOnEvent2{ &i };
            i = 0;
            genDispatcher.Register<Event1>(&addOnEvent);
            genDispatcher.Register<Event1>(&addOnEvent2);
            genDispatcher.SendEvent(event1);
            DJO_UTEST(i == 2);

            i = 0;
            genDispatcher.Unregister<Event1>(&addOnEvent2);
            genDispatcher.SendEvent(event1);
            DJO_UTEST(i == 1);

            i = 0;
            genDispatcher.Unregister<Event1>(&addOnEvent);
            genDispatcher.SendEvent(event1);
            DJO_UTEST(i == 0);

            DJO_UTESTS_END();
        }

        TestsResult
        EventDispatcher_All()
        {
            TestsResult ret;
            ret += EventDispatcher_Basics();
            ret += EventDispatcher_GenericEventDispatcher();

            return ret;
        }
    }
}
