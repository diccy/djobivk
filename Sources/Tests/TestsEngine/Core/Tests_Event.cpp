#pragma once

#include "Core/TestsCore.hpp"

#include <Core/Event.hpp>

#include <set>


namespace Djo
{
    namespace Tests
    {
        struct Event0 { int n; };
        struct Event1 { int n; };

        template <typename T>
        struct TEvent { T t; };

        struct EmptyEvent0 {};
        struct EmptyEvent1 {};

        template <typename T>
        struct TEmptyEvent {};

        TestsResult
        Event_All()
        {
            DJO_UTESTS_INIT();

            std::set<EventId> ids;
            unsigned int addCallCount;
            unsigned int insertedCount;
            const auto Add = [&ids, &addCallCount, &insertedCount](const EventId _id)
            {
                ++addCallCount;
                insertedCount += (ids.emplace(_id).second) ? 1 : 0;
            };

            const auto AddAllEvents = [&Add]()
            {
                Add(GetEventId<Event0>());
                Add(GetEventId<Event1>());
                Add(GetEventId<TEvent<int>>());
                Add(GetEventId<TEvent<float>>());
                Add(GetEventId<TEvent<Event0>>());
                Add(GetEventId<TEvent<Event1>>());
                Add(GetEventId<TEvent<EmptyEvent0>>());
                Add(GetEventId<TEvent<EmptyEvent1>>());
                Add(GetEventId<EmptyEvent0>());
                Add(GetEventId<EmptyEvent1>());
                Add(GetEventId<TEmptyEvent<int>>());
                Add(GetEventId<TEmptyEvent<float>>());
                Add(GetEventId<TEmptyEvent<Event0>>());
                Add(GetEventId<TEmptyEvent<Event1>>());
                Add(GetEventId<TEmptyEvent<EmptyEvent0>>());
                Add(GetEventId<TEmptyEvent<EmptyEvent1>>());
                Add(GetEventId<TEmptyEvent<TEmptyEvent<EmptyEvent0>>>());
                Add(GetEventId<TEmptyEvent<TEmptyEvent<TEmptyEvent<EmptyEvent0>>>>());
            };

            // Test if all event ids are unique
            addCallCount = 0;
            insertedCount = 0;
            AddAllEvents();
            DJO_UTEST(addCallCount > 0);
            DJO_UTEST(insertedCount == ids.size());
            DJO_UTEST(addCallCount == insertedCount);

            // Test if event ids are consistent
            insertedCount = 0;
            AddAllEvents();
            AddAllEvents();
            AddAllEvents();
            DJO_UTEST(addCallCount > 0);
            DJO_UTEST(insertedCount == 0);

            DJO_TODO("Test thread safety of EventId generation");

            DJO_UTESTS_END();
        }
    }
}
