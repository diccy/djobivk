
#include "Core/TestsCore.hpp"

#include <Core/StringView.hpp>


namespace Djo
{
    namespace Tests
    {
        #define DJO_TEST_LITTERAL_S1 "abcd"
        #define DJO_TEST_LITTERAL_S2 "abcdefgh"
        #define DJO_TEST_LITTERAL_SEMPTY ""

        static const char s_s1[]{ DJO_TEST_LITTERAL_S1 };
        static const sz s_s1Len{ ArraySize(s_s1) - 1 };
        static const char s_s2[]{ DJO_TEST_LITTERAL_S2 };
        static const sz s_s2Len{ ArraySize(s_s2) - 1 };
        static const char s_sEmpty[]{ DJO_TEST_LITTERAL_SEMPTY };
        static const sz s_sEmptyLen{ ArraySize(s_sEmpty) - 1 };

        TestsResult
        StringView_Constructors()
        {
            DJO_UTESTS_INIT();

            StringView sv1;
            StringView sv2;
            StringView svEmpty;
            const auto TestSvs = [&]()
            {
                DJO_UTEST(sv1.count == s_s1Len);
                DJO_UTEST(sv1[0] == s_s1[0]);
                DJO_UTEST(sv2.count == s_s2Len);
                DJO_UTEST(sv2[0] == s_s2[0]);
                DJO_UTEST(svEmpty.count == 0);
                DJO_UTEST(svEmpty.data != nullptr);
            };

            sv1 = StringView{ DJO_TEST_LITTERAL_S1 };
            sv2 = StringView{ DJO_TEST_LITTERAL_S2 };
            svEmpty = StringView{ DJO_TEST_LITTERAL_SEMPTY };
            TestSvs();

            sv1 = StringView{ (const char*)s_s1 };
            sv2 = StringView{ (const char*)s_s2 };
            svEmpty = StringView{ (const char*)s_sEmpty };
            TestSvs();

            sv1 = StringView{ (const char*)s_s1, s_s1Len };
            sv2 = StringView{ (const char*)s_s2, s_s2Len };
            svEmpty = StringView{ (const char*)s_sEmpty, s_sEmptyLen };
            TestSvs();

            std::string str1{ s_s1 };
            std::string str2{ s_s2 };
            std::string strEmpty{ s_sEmpty };
            sv1 = StringView{ str1 };
            sv2 = StringView{ str2 };
            svEmpty = StringView{ strEmpty };
            TestSvs();

            DJO_UTESTS_END();
        }

        TestsResult
        StringView_Basics()
        {
            DJO_UTESTS_INIT();

            StringView sv1{ DJO_TEST_LITTERAL_S1 };
            StringView sv2{ DJO_TEST_LITTERAL_S2 };
            StringView svEmpty{ DJO_TEST_LITTERAL_SEMPTY };

            DJO_UTEST(sv1.IsEmpty() == false);
            DJO_UTEST(sv2.IsEmpty() == false);
            DJO_UTEST(svEmpty.IsEmpty() == true);

            DJO_UTEST(sv1[0] == 'a');
            DJO_UTEST(sv1[2] == 'c');
            DJO_UTEST(sv1.GetLast() == 'd');
            DJO_UTEST(sv2[0] == 'a');
            DJO_UTEST(sv2[2] == 'c');
            DJO_UTEST(sv2.GetLast() == 'h');

            DJO_UTEST(sv1.IsEqual(sv1) == true);
            DJO_UTEST(sv1.IsEqual(sv2) == false);
            DJO_UTEST(sv1.IsEqual(svEmpty) == false);
            DJO_UTEST(sv2.IsEqual(sv1) == false);
            DJO_UTEST(sv2.IsEqual(sv2) == true);
            DJO_UTEST(sv2.IsEqual(svEmpty) == false);
            DJO_UTEST(svEmpty.IsEqual(sv1) == false);
            DJO_UTEST(svEmpty.IsEqual(sv2) == false);
            DJO_UTEST(svEmpty.IsEqual(svEmpty) == true);
            DJO_UTEST((sv1 == sv1) == true);
            DJO_UTEST((sv1 == sv2) == false);
            DJO_UTEST((sv1 == svEmpty) == false);

            DJO_UTEST(sv1.Find('a') == 0);
            DJO_UTEST(sv1.Find('c') == 2);
            DJO_UTEST(sv1.Find('z') == StringView::npos);
            DJO_UTEST(sv1.FindLast('a') == sv1.Find('a'));
            DJO_UTEST(sv1.FindLast('c') == sv1.Find('c'));
            DJO_UTEST(sv1.FindLast('z') == sv1.Find('z'));
            DJO_UTEST(sv2.Find('a') == 0);
            DJO_UTEST(sv2.Find('c') == 2);
            DJO_UTEST(sv2.Find('g') == 6);
            DJO_UTEST(sv2.Find('z') == StringView::npos);
            DJO_UTEST(sv2.FindLast('a') == sv2.Find('a'));
            DJO_UTEST(sv2.FindLast('c') == sv2.Find('c'));
            DJO_UTEST(sv2.FindLast('g') == sv2.Find('g'));
            DJO_UTEST(sv2.FindLast('z') == sv2.Find('z'));
            DJO_UTEST(svEmpty.Find('a') == StringView::npos);
            DJO_UTEST(svEmpty.Find('z') == StringView::npos);
            DJO_UTEST(svEmpty.FindLast('a') == svEmpty.Find('a'));
            DJO_UTEST(svEmpty.FindLast('z') == svEmpty.Find('z'));

            DJO_UTESTS_END();
        }

        TestsResult
        StringView_Sub()
        {
            DJO_UTESTS_INIT();

            StringView sv1{ DJO_TEST_LITTERAL_S1 };
            StringView sv2{ DJO_TEST_LITTERAL_S2 };
            StringView svEmpty{ DJO_TEST_LITTERAL_SEMPTY };
            StringView sub;

            DJO_UTEST(sv1.SubBack(StringView::npos) == svEmpty);
            DJO_UTEST(sv1.SubBack(123456) == svEmpty);
            DJO_UTEST(sv1.SubBack(2) != sv1);
            DJO_UTEST(sv2.SubBack(StringView::npos) == svEmpty);
            DJO_UTEST(sv2.SubBack(123456) == svEmpty);
            DJO_UTEST(sv2.SubBack(2) != sv2);
            DJO_UTEST(svEmpty.SubBack(StringView::npos) == svEmpty);
            DJO_UTEST(svEmpty.SubBack(123456) == svEmpty);
            DJO_UTEST(svEmpty.SubBack(2) == svEmpty);
            DJO_UTEST(sv1.SubFront(StringView::npos) == sv1);
            DJO_UTEST(sv1.SubFront(123456) == sv1);
            DJO_UTEST(sv1.SubFront(2) != sv1);
            DJO_UTEST(sv2.SubFront(StringView::npos) == sv2);
            DJO_UTEST(sv2.SubFront(123456) == sv2);
            DJO_UTEST(sv2.SubFront(2) != sv2);
            DJO_UTEST(svEmpty.SubFront(StringView::npos) == svEmpty);
            DJO_UTEST(svEmpty.SubFront(123456) == svEmpty);
            DJO_UTEST(svEmpty.SubFront(2) == svEmpty);

            DJO_UTEST(sv1.Sub(0, StringView::npos) == sv1);
            DJO_UTEST(sv1.Sub(1, 3) == "bc");
            DJO_UTEST(sv1.Sub(1, 1) == svEmpty);
            DJO_UTEST(sv1.Sub(8, 10) == svEmpty);

            DJO_UTEST(sv2.Sub(0, StringView::npos) == sv2);
            DJO_UTEST(sv2.Sub(1, 3) == "bc");
            DJO_UTEST(sv2.Sub(1, 1) == svEmpty);
            DJO_UTEST(sv2.Sub(8, 10) == svEmpty);

            DJO_UTEST(svEmpty.Sub(0, StringView::npos) == svEmpty);
            DJO_UTEST(svEmpty.Sub(1, 3) == svEmpty);
            DJO_UTEST(svEmpty.Sub(0, 0) == svEmpty);

            DJO_UTESTS_END();
        }

        #undef DJO_TEST_LITTERAL_S1
        #undef DJO_TEST_LITTERAL_S2
        #undef DJO_TEST_LITTERAL_SEMPTY

        TestsResult
        StringView_All()
        {
            TestsResult ret;
            ret += StringView_Constructors();
            ret += StringView_Basics();
            ret += StringView_Sub();

            return ret;
        }
    }
}
