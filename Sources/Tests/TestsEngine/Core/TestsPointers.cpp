
#include "Core/TestsCore.hpp"

#include <Core/IntrusiveRefCount.hpp>
#include <Core/Pointers.hpp>


namespace Djo
{
    namespace Tests
    {
        class ZeroWhenDestroyed
        {
        public:
            ZeroWhenDestroyed(u32& _ref) : ref(_ref) { }
            virtual ~ZeroWhenDestroyed() { ref = 0; }
        private:
            u32& ref;
        };

        struct Base
        {
            virtual ~Base() {}
            s32 i = 0;
        };
        struct Derived
            : public Base
        {
            s32 j = 0;
        };
        struct NotDerived
        {
            virtual ~NotDerived() {}
            s32 j = 0;
        };

        TestsResult
        Pointers_UniquePointers()
        {
            DJO_UTESTS_INIT();

            u32 i, j;

            i = 12;
            {
                UPtr<ZeroWhenDestroyed> uptr{ new ZeroWhenDestroyed{ i } };  // by raw pointer constructor
                //uptr.Pop(); // private function, must NOT compile if uncommented
                uptr.Release();
                DJO_UTEST(i == 0);
            }

            i = 12;
            {
                UPtr<ZeroWhenDestroyed> uptr{};  // by raw pointer constructor
                uptr = new ZeroWhenDestroyed{ i };
                uptr.Release();
                DJO_UTEST(i == 0);
            }

            i = 12;
            {
                UPtr<ZeroWhenDestroyed> uptr{ new ZeroWhenDestroyed{ i } };
                DJO_UTEST(i == 12);
            }
            DJO_UTEST(i == 0);

            i = 12;
            {
                UPtr<ZeroWhenDestroyed> uptr01{ new ZeroWhenDestroyed{ i } };
                DJO_UTEST(uptr01.Get() != nullptr);
                {
                    //UPtr<ZeroWhenDestroyed> uptr02{ uptr01 }; // copy constructor, must NOT compile if uncommented
                    //uptr01 = UPtr<ZeroWhenDestroyed>{};       // copy assignement, must NOT compile if uncommented
                }
                {
                    UPtr<ZeroWhenDestroyed> uptr02{ std::move(uptr01) }; // move constructor
                    DJO_UTEST(uptr01.Get() == nullptr);
                    DJO_UTEST(uptr02.Get() != nullptr);
                    DJO_UTEST(i == 12);
                }
                DJO_UTEST(i == 0);
            }

            i = 12;
            j = 12;
            {
                UPtr<ZeroWhenDestroyed> uptr01{ new ZeroWhenDestroyed{ i } };
                {
                    UPtr<ZeroWhenDestroyed> uptr02{ new ZeroWhenDestroyed{ j } };
                    DJO_UTEST(uptr01.Get() != nullptr);
                    DJO_UTEST(uptr02.Get() != nullptr);
                    uptr01 = std::move(uptr02); // move assignement
                }
                DJO_UTEST(i == 0);
                DJO_UTEST(j == 12);
            }
            DJO_UTEST(i == 0);
            DJO_UTEST(j == 0);

            {
                UPtr<Derived> uptrDerived{ new Derived() };
                UPtr<Base> uptrBase{ std::move(uptrDerived) }; // move constructor
                DJO_UTEST(uptrDerived.Get() == nullptr);
                DJO_UTEST(uptrBase.Get() != nullptr);
            }

            {
                UPtr<Base> uptrBase{ new Base() };
                //UPtr<Derived> uptrDerived{ std::move(uptrBase) }; // must NOT compile if uncommented
            }

            {
                UPtr<NotDerived> uptrNotDerived{ new NotDerived() };
                //UPtr<Base> uptrBase{ std::move(uptrNotDerived) }; // must NOT compile if uncommented
            }

            DJO_UTESTS_END();
        }

        class IntrusiveZeroWhenDestroyed
            : public ZeroWhenDestroyed
            , public IntrusiveRefCount
        {
        public:
            IntrusiveZeroWhenDestroyed(u32& _ref) : ZeroWhenDestroyed{ _ref } {}
            virtual ~IntrusiveZeroWhenDestroyed() {}
        };

        class NotIntrusive
        {
            int dummy = 0;
        };

        class IntrusiveNotBase
            : public IntrusiveRefCount
        {
        };

        TestsResult
        Pointers_SharedPointers()
        {
            DJO_UTESTS_INIT();

            u32 i, j;

            //ShPtr<NotIntrusive> pNope{ new NotIntrusive }; // Must NOT compile if uncommented

            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptr;
                //sptr.Pop(); // private function, must NOT compile if uncommented
                DJO_UTEST(sptr.Get() == nullptr);
            }

            i = 12;
            {
                IntrusiveZeroWhenDestroyed* pIntrusive = new IntrusiveZeroWhenDestroyed{ i };
                DJO_UTEST(pIntrusive->GetRefCount() == 0);
                pIntrusive->AddRef();
                DJO_UTEST(pIntrusive->GetRefCount() == 1);
                pIntrusive->ReleaseRef();
                DJO_UTEST(pIntrusive->GetRefCount() == 0);
                DJO_UTEST(i == 12);
                delete pIntrusive;
                DJO_UTEST(i == 0);
            }

            i = 12;
            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptr01;
                sptr01 = new IntrusiveZeroWhenDestroyed{ i }; // raw pointer assignement
                DJO_UTEST(sptr01->GetRefCount() == 1);
                {
                    ShPtr<IntrusiveZeroWhenDestroyed> sptr02{ sptr01 }; // copy constructor
                    DJO_UTEST(sptr01.Get() == sptr02.Get());
                    DJO_UTEST(sptr01->GetRefCount() == 2);
                    DJO_UTEST(sptr02->GetRefCount() == 2);
                    sptr02.Release();
                    DJO_UTEST(sptr01->GetRefCount() == 1);
                    DJO_UTEST(sptr02.Get() == nullptr);
                }
                DJO_UTEST(i == 12);
                {
                    ShPtr<IntrusiveZeroWhenDestroyed> sptr02{ sptr01.Get() }; // by raw pointer constructor
                    DJO_UTEST(sptr01.Get() == sptr02.Get());
                    DJO_UTEST(sptr01->GetRefCount() == 2);
                    DJO_UTEST(sptr02->GetRefCount() == 2);
                }
                DJO_UTEST(i == 12);
                DJO_UTEST(sptr01->GetRefCount() == 1);
                sptr01.Release();
                DJO_UTEST(sptr01.Get() == nullptr);
                DJO_UTEST(i == 0);
            }

            i = 12;
            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptr01;
                sptr01 = new IntrusiveZeroWhenDestroyed{ i };
                DJO_UTEST(sptr01->GetRefCount() == 1);
                {
                    ShPtr<IntrusiveZeroWhenDestroyed> sptr02{ std::move(sptr01) }; // move constructor
                    DJO_UTEST(sptr01.Get() == nullptr);
                    DJO_UTEST(sptr02.Get() != nullptr);
                    DJO_UTEST(sptr02->GetRefCount() == 1);
                    DJO_UTEST(i == 12);
                }
                DJO_UTEST(i == 0);
            }

            i = 12;
            j = 12;
            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptr01{ new IntrusiveZeroWhenDestroyed{ i } };
                DJO_UTEST(sptr01->GetRefCount() == 1);
                {
                    ShPtr<IntrusiveZeroWhenDestroyed> sptr02{ new IntrusiveZeroWhenDestroyed{ j } };
                    sptr01 = sptr02; // copy assignment
                    DJO_UTEST(sptr01.Get() != nullptr);
                    DJO_UTEST(sptr01.Get() == sptr02.Get());
                    DJO_UTEST(sptr02->GetRefCount() == 2);
                    DJO_UTEST(i == 0);
                    DJO_UTEST(j == 12);
                    sptr02.Release();
                    DJO_UTEST(j == 12);
                }
                DJO_UTEST(j == 12);
                sptr01.Release();
                DJO_UTEST(j == 0);
            }

            i = 12;
            j = 12;
            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptr01{ new IntrusiveZeroWhenDestroyed{ i } };
                DJO_UTEST(sptr01->GetRefCount() == 1);
                {
                    ShPtr<IntrusiveZeroWhenDestroyed> sptr02{ new IntrusiveZeroWhenDestroyed{ j } };
                    sptr01 = std::move(sptr02); // move assignment
                    DJO_UTEST(sptr01.Get() != nullptr);
                    DJO_UTEST(sptr02.Get() != nullptr);
                    DJO_UTEST(sptr01->GetRefCount() == 1);
                    DJO_UTEST(sptr02->GetRefCount() == 1);
                    DJO_UTEST(i == 12);
                    DJO_UTEST(j == 12);
                    sptr02.Release();
                    DJO_UTEST(i == 0);
                    DJO_UTEST(j == 12);
                }
                DJO_UTEST(j == 12);
                sptr01.Release();
                DJO_UTEST(j == 0);
            }

            // Derived classes

            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptrDerived{ new IntrusiveZeroWhenDestroyed{ i } };
                ShPtr<IntrusiveRefCount> sptrBase{ sptrDerived }; // copy constructor
                DJO_UTEST(sptrDerived.Get() == sptrBase.Get());
                DJO_UTEST(sptrDerived->GetRefCount() == 2);
                DJO_UTEST(sptrBase->GetRefCount() == 2);
            }
            {
                ShPtr<IntrusiveZeroWhenDestroyed> sptrDerived{ new IntrusiveZeroWhenDestroyed{ i } };
                ShPtr<IntrusiveRefCount> sptrBase{ std::move(sptrDerived) }; // move constructor
                DJO_UTEST(sptrDerived.Get() == nullptr);
                DJO_UTEST(sptrBase.Get() != nullptr);
                DJO_UTEST(sptrBase->GetRefCount() == 1);
            }

            {
                ShPtr<IntrusiveRefCount> sptrBase{ new IntrusiveRefCount() };
                //ShPtr<IntrusiveZeroWhenDestroyed> sptrDerived{ std::move(sptrBase) }; // must NOT compile if uncommented
            }

            {
                ShPtr<IntrusiveNotBase> sptrNotDerived{ new IntrusiveNotBase() };
                //ShPtr<IntrusiveZeroWhenDestroyed> sptrBase{ std::move(sptrNotDerived) }; // must NOT compile if uncommented
            }

            DJO_UTESTS_END();
        }

        TestsResult
        Pointers_All()
        {
            TestsResult ret;
            ret += Pointers_UniquePointers();
            ret += Pointers_SharedPointers();

            return ret;
        }
    }
}
