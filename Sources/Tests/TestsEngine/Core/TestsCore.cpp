
#include "Core/TestsCore.hpp"


namespace Djo
{
    namespace Tests
    {
        TestsResult
        TestsCore_Macros()
        {
            DJO_UTESTS_INIT();

            StringView s = DJO_STR(coucou);
            DJO_UTEST(s == "coucou");
            s = DJO_STR(cou   cou);
            DJO_UTEST(s == "cou cou");
            s = DJO_STR(   coucou);
            DJO_UTEST(s == "coucou");
            s = DJO_STR(coucou   );
            DJO_UTEST(s == "coucou");

            DJO_CAT(String, View) sc0;
            DJO_CAT(DJO_CAT(S, DJO_CAT(tr, ing)), DJO_CAT(Vi, ew)) sc1;
            DJO_UNUSED_VAR(sc0);
            DJO_UNUSED_VAR(sc1);

            u32 n = DJO_ARG_COUNT(); // does not work
            DJO_UTEST(n == 0);

            n = DJO_ARG_COUNT(n);
            DJO_UTEST(n == 1);

            n = DJO_ARG_COUNT(a, b);
            DJO_UTEST(n == 2);

            n = DJO_ARG_COUNT(a, b, c, d, e, f);
            DJO_UTEST(n == 6);

            #define DJO_PLUS(a_) a_ +
            n = DJO_APPLY(DJO_PLUS, 1, 2, 3, 4) 5;
            DJO_UTEST(n == 1 + 2 + 3 + 4 + 5);
            #undef DJO_PLUS

            DJO_UTESTS_END();
        }

        TestsResult
        TestsCore()
        {
            TestsResult ret;
            ret += TestsCore_Macros();
            ret += Utils_All();
            ret += Time_All();
            ret += Pointers_All();
            ret += MaxArray_All();
            ret += StringView_All();
            ret += Event_All();
            ret += EventDispatcher_All();

            return ret;
        }
    }
}
