
#include "Core/TestsCore.hpp"

#include <Core/Utils.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        Utils_Numeric()
        {
            DJO_UTESTS_INIT();

            for (u32 i = 0; i < 8; ++i)
            {
                DJO_UTEST(GetFirstBitIndex(Flag(i)) == i);
            }
            for (u32 i = 0; i < 8; ++i)
            {
                DJO_UTEST(GetFirstBitIndex(Flag(i) | Flag(i + 2) | Flag(i + 5)) == i);
            }

            for (u32 a = 15; a <= 17; ++a)
            {
                for (u32 i = 0; i < 50; i += 7)
                {
                    const u32 aligned = Align(i, a);
                    DJO_UTEST(i <= aligned && aligned < i + a);
                    DJO_UTEST(aligned % a == 0u);
                }
            }

            for (u32 a = 2; a <= 16; a <<= 1)
            {
                for (u32 i = 0; i < 30; i += 3)
                {
                    DJO_UTEST(AlignPow2(i, a) == Align(i, a));
                }
            }

            DJO_UTEST(SignOf<s32>(-1) == -1);
            DJO_UTEST(SignOf<s32>(0) == 0);
            DJO_UTEST(SignOf<s32>(1) == 1);
            DJO_UTEST(SignOf<s32>(123) == 1);
            DJO_UTEST(SignOf<s32>(168465) == 1);
            DJO_UTEST(SignOf<s32>(-54641) == -1);
            DJO_UTEST(SignOf<s32>(-3261) == -1);

            DJO_UTEST(SignOf<f32>(-1.f) == -1.f);
            DJO_UTEST(SignOf<f32>(0.f) == 0.f);
            DJO_UTEST(SignOf<f32>(1.f) == 1.f);
            DJO_UTEST(SignOf<f32>(0.00654684f) == 1.f);
            DJO_UTEST(SignOf<f32>(123.654684f) == 1.f);
            DJO_UTEST(SignOf<f32>(168465.6354684f) == 1.f);
            DJO_UTEST(SignOf<f32>(-0.003454f) == -1.f);
            DJO_UTEST(SignOf<f32>(-54641.3454f) == -1.f);
            DJO_UTEST(SignOf<f32>(-3261.2484f) == -1.f);

            DJO_UTESTS_END();
        }

        TestsResult
        Utils_CStr()
        {
            DJO_UTESTS_INIT();

            DJO_UTEST(StrLen("") == 0);
            DJO_UTEST(StrLen("a") == 1);
            DJO_UTEST(StrLen("abcdef") == 6);

            DJO_UTEST(StrNCmp("", "", 0) == 0);
            DJO_UTEST(StrNCmp("a", "a", 1) == 0);
            DJO_UTEST(StrNCmp("abb", "a", 1) == 0);
            DJO_UTEST(StrNCmp("a", "abb", 1) == 0);
            DJO_UTEST(StrNCmp("aaa", "bbb", 3) == 1);
            DJO_UTEST(StrNCmp("bbb", "aaa", 3) == -1);
            DJO_UTEST(StrNCmp("abcdez", "abcdef", 3) == 0);
            DJO_UTEST(StrNCmp("abcdez", "abcdef", 6) == -1);

            const char str[]{ "abcdaefab" };
            const sz len{ ArraySize(str) - 1 };
            DJO_UTEST(TStrNChrI(str, len, 'a', StrNChr) == 0);
            DJO_UTEST(TStrNChrI(str, len, 'b', StrNChr) == 1);
            DJO_UTEST(TStrNChrI(str, len, 'f', StrNChr) == 6);
            DJO_UTEST(TStrNChrI(str,   3, 'f', StrNChr) == c_invalidUint<sz>);
            DJO_UTEST(TStrNChrI(str, len, 'z', StrNChr) == c_invalidUint<sz>);

            DJO_UTEST(TStrNChrI(str, len, 'a', StrNRChr) == 7);
            DJO_UTEST(TStrNChrI(str, len, 'b', StrNRChr) == 8);
            DJO_UTEST(TStrNChrI(str, len, 'f', StrNRChr) == 6);
            DJO_UTEST(TStrNChrI(str,   3, 'f', StrNRChr) == c_invalidUint<sz>);
            DJO_UTEST(TStrNChrI(str, len, 'z', StrNRChr) == c_invalidUint<sz>);

            DJO_UTESTS_END();
        }

        TestsResult
        Utils_All()
        {
            TestsResult ret;
            ret += Utils_Numeric();
            ret += Utils_CStr();

            return ret;
        }
    }
}
