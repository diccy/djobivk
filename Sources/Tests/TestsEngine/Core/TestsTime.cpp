
#include "Core/TestsCore.hpp"

#include <Core/Time.hpp>
#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        Time_All()
        {
            DJO_UTESTS_INIT();

            Seconds s;
            Milliseconds ms;
            Microseconds us;

            DJO_UTEST(s == 0.f);
            DJO_UTEST(s == ms);
            DJO_UTEST(s == us);

            s = 1.f;
            ms = 1000.f;
            us = 1000000.f;

            DJO_UTEST(Equals(s.Sec(), 1.f));
            DJO_UTEST(Equals(ms.Sec(), 1.f));
            DJO_UTEST(Equals(us.Sec(), 1.f));
            DJO_UTEST(Equals(s.Ms(), 1000.f));
            DJO_UTEST(Equals(ms.Ms(), 1000.f));
            DJO_UTEST(Equals(us.Ms(), 1000.f));
            DJO_UTEST(Equals(s.Us(), 1000000.f));
            DJO_UTEST(Equals(ms.Us(), 1000000.f));
            DJO_UTEST(Equals(us.Us(), 1000000.f));

            s = 0.003f;
            ms = 3.f;
            us = 3000.f;

            DJO_UTEST(Equals(s.Sec(), 0.003f));
            DJO_UTEST(Equals(ms.Sec(), 0.003f));
            DJO_UTEST(Equals(us.Sec(), 0.003f));
            DJO_UTEST(Equals(s.Ms(), 3.f));
            DJO_UTEST(Equals(ms.Ms(), 3.f));
            DJO_UTEST(Equals(us.Ms(), 3.f));
            DJO_UTEST(Equals(s.Us(), 3000.f));
            DJO_UTEST(Equals(ms.Us(), 3000.f));
            DJO_UTEST(Equals(us.Us(), 3000.f));

            s = 0.000005f;
            ms = 0.005f;
            us = 5.f;

            DJO_UTEST(Equals(s.Sec(), 0.000005f));
            DJO_UTEST(Equals(ms.Sec(), 0.000005f));
            DJO_UTEST(Equals(us.Sec(), 0.000005f));
            DJO_UTEST(Equals(s.Ms(), 0.005f));
            DJO_UTEST(Equals(ms.Ms(), 0.005f));
            DJO_UTEST(Equals(us.Ms(), 0.005f));
            DJO_UTEST(Equals(s.Us(), 5.f));
            DJO_UTEST(Equals(ms.Us(), 5.f));
            DJO_UTEST(Equals(us.Us(), 5.f));

            s = 50000.f;

            DJO_UTEST(Equals(s.Sec(), 50000.f));
            DJO_UTEST(Equals(s.Ms(), 50000000.f));
            DJO_UTEST(Equals(s.Us(), 50000000000.f));

            DJO_UTESTS_END();
        }
    }
}
