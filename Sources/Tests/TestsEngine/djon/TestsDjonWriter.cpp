
#include "djon/TestsDjon.hpp"

#include <djon/DjonParser.hpp>
#include <djon/DjonSerialization.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        DjonWriter_Basics()
        {
            DJO_UTESTS_INIT();

            djon::ReadableWriter writer;

            int i10{ 10 };
            writer.Write(i10);
            DJO_UTEST(writer.Get() == "10");
            writer.Clear();

            writer.Write(StringView{ "10" });
            DJO_UTEST(writer.Get() == "\"10\"");
            writer.Clear();

            std::vector<u32> numbers{ 2, 6, 10, 15, 23 };
            writer.Write(numbers);
            DJO_UTEST(writer.Get() == "[ 2 6 10 15 23 ]"); // array of number is one line
            writer.Clear();
            writer.SetAttr(djon::ReadableWriter::EAttribute::Oneline);
            writer.Write(numbers);
            DJO_UTEST(writer.Get() == "[ 2 6 10 15 23 ]");
            writer.Clear();

            std::vector<std::string> strs{ "2", "6", "10", "15", "23" };
            writer.Write(strs);
            DJO_UTEST(writer.Get() == "[\n    \"2\"\n    \"6\"\n    \"10\"\n    \"15\"\n    \"23\"\n]");
            writer.Clear();
            writer.SetAttr(djon::ReadableWriter::EAttribute::Oneline);
            writer.Write(strs);
            DJO_UTEST(writer.Get() == "[ \"2\" \"6\" \"10\" \"15\" \"23\" ]");
            writer.Clear();

            SimpleObj simpleObj;
            simpleObj.num = 2;
            simpleObj.numArray = std::vector<f32>{ 0.2f, 1.2f, -3.f };
            simpleObj.str = "tonton";
            simpleObj.strArray = std::vector<std::string>{ "hola", "les", "amis", "" };
            simpleObj.falseBool = false;
            simpleObj.trueBool = true;
            simpleObj.alsoTrue = true;
            writer.Write("SimpleObj", simpleObj);
            DJO_UTEST(writer.Get() == s_simpleObjAllWrite);
            writer.Clear();

            {
                djon::Parser parser;
                djon::Reader reader;
                EResult result;
                result = parser.Parse(s_fullFeatureObjWrite);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    reader.SetElem(*parser.GetRoot());
                    FullFeatured fullFeatured;
                    reader.Read(&fullFeatured);
                    writer.Write(fullFeatured);
                    DJO_UTEST(writer.Get() == s_fullFeatureObjWrite);
                    {
                        parser.Clear();
                        const std::string writerContent = writer.Get();
                        result = parser.Parse(StringView{ writerContent });
                        DJO_UTEST(result == EResult::OK);
                        if (result == EResult::OK)
                        {
                            reader.SetElem(*parser.GetRoot());
                            FullFeatured fullFeatured2;
                            reader.Read(&fullFeatured2);
                            DJO_UTEST(IsSame(fullFeatured, fullFeatured2));
                        }
                    }
                    writer.Clear();

                    writer.SetAttr(djon::ReadableWriter::EAttribute::Oneline);
                    writer.Write(fullFeatured);
                    DJO_UTEST(writer.Get() == s_fullFeatureObjWriteOneLine);
                    {
                        parser.Clear();
                        const std::string writerContent = writer.Get();
                        result = parser.Parse(StringView{ writerContent });
                        DJO_UTEST(result == EResult::OK);
                        if (result == EResult::OK)
                        {
                            reader.SetElem(*parser.GetRoot());
                            FullFeatured fullFeatured2;
                            reader.Read(&fullFeatured2);
                            DJO_UTEST(IsSame(fullFeatured, fullFeatured2));
                        }
                    }
                    writer.Clear();
                }
            }

            DJO_UTESTS_END();
        }

        TestsResult
        DjonWriter_All(LocalFileSystem&)
        {
            TestsResult ret;
            ret += DjonWriter_Basics();

            return ret;
        }
    }
}
