#pragma once

#include "Tests.hpp"

#include <djon/DjonUtils.hpp>
#include <Serialization/Serialization.hpp>
#include <Core/StdString.hpp>


namespace Djo
{
    // fw
    class LocalFileSystem;

    namespace Tests
    {
        TestsResult TestsDjon();

        TestsResult ParsingUtils_All(LocalFileSystem& _srlFs);
        TestsResult DjonTokenizer_All(LocalFileSystem& _srlFs);
        TestsResult DjonParser_All(LocalFileSystem& _srlFs);
        TestsResult DjonReader_All(LocalFileSystem& _srlFs);
        TestsResult DjonWriter_All(LocalFileSystem& _srlFs);

        void OpenReadClose(LocalFileSystem& _srl, const char* const _filePath, std::string* const _outFileContent);

        const char s_simpleObj[]
        {
            "SimpleObj\n"
            "{\n"
            "    num 2\n"
            "    numArray [ 0.2 1.2 -3 ]\n"
            "    str \"tonton\"\n"
            "    strArray[ \"hola\" \"les\" \"amis\" \"\" ]\n"
            "}"
        };
        const char s_numVersionnedNo[]
        {
            "num 2\n"
        };
        const char s_numVersionned010[]
        {
            "#version 0.1.0\n"
            "num 2\n"
        };
        const char s_numVersionned00123[]
        {
            "#version 0.0.123\n"
            "num 2\n"
        };
        const char s_numVersionned0[]
        {
            "#version 0\n"
            "num 2\n"
        };
        const char s_numVersionned01[]
        {
            "#version 0.1\n"
            "num 2\n"
        };
        const char s_simpleObjAllWrite[]
        {
            "SimpleObj\n"
            "{\n"
            "    num 2\n"
            "    numArray [ 0.2 1.2 -3 ]\n"
            "    str \"tonton\"\n"
            "    strArray\n"
            "    [\n"
            "        \"hola\"\n"
            "        \"les\"\n"
            "        \"amis\"\n"
            "        \"\"\n"
            "    ]\n"
            "    falseBool 0\n"
            "    trueBool 1\n"
            "    alsoTrue 1\n"
            "}"
        };

        const char s_fullFeatureObj[]
        {
            "# object\n"
            "SimpleObj\n"
            "{\n"
            "    # value scalar\n"
            "    num 2\n"
            "    # array of scalars\n"
            "    numArray [ 0.2 1.2 -3 ]\n"
            "    # value string\n"
            "    str \"tonton\"\n"
            "    # array of strings\n"
            "    strArray [ \"hola\" \"les\" \"amis\" \"\" ]\n"
            "    # bools from scalar\n"
            "    falseBool 0\n"
            "    trueBool 1\n"
            "    alsoTrue -0.1\n"
            "}\n"
            "# array of objects\n"
            "ArrayObj\n"
            "[\n"
            "    { allo \"misleading strings\" }\n"
            "    { allo \"#\" }\n"
            "    { allo \"][\" }\n"
            "    { allo \"}={\" }\n"
            "]\n"
            "\n"
            "# misleading comments,\n"
            "# ]\n"
            "# \"lol[\n"
            "# = }{\n"
            "\n"
            "# nested objects\n"
            "GrootObj\n"
            "{\n"
            "    name \"Groot!\"\n"
            "    Trunk\n"
            "    {\n"
            "        width 15\n"
            "        height 135.2\n"
            "        Branches\n"
            "        [\n"
            "            {\n"
            "                width 1.6\n"
            "                length 12\n"
            "                leafCount 125\n"
            "            }\n"
            "            {\n"
            "                width 2.1\n"
            "                length 15\n"
            "                leafCount 156\n"
            "            }\n"
            "            {\n"
            "                width 1.4\n"
            "                length 9\n"
            "                leafCount 79\n"
            "            }\n"
            "        ]\n"
            "    }\n"
            "}"
        };
        const char s_fullFeatureObjWrite[]
        {
            "SimpleObj\n"
            "{\n"
            "    num 2\n"
            "    numArray [ 0.2 1.2 -3 ]\n"
            "    str \"tonton\"\n"
            "    strArray\n"
            "    [\n"
            "        \"hola\"\n"
            "        \"les\"\n"
            "        \"amis\"\n"
            "        \"\"\n"
            "    ]\n"
            "    falseBool 0\n"
            "    trueBool 1\n"
            "    alsoTrue 1\n"
            "}\n"
            "ArrayObj\n"
            "[\n"
            "    {\n"
            "        allo \"misleading strings\"\n"
            "    }\n"
            "    {\n"
            "        allo \"#\"\n"
            "    }\n"
            "    {\n"
            "        allo \"][\"\n"
            "    }\n"
            "    {\n"
            "        allo \"}={\"\n"
            "    }\n"
            "]\n"
            "GrootObj\n"
            "{\n"
            "    name \"Groot!\"\n"
            "    Trunk\n"
            "    {\n"
            "        width 15\n"
            "        height 135.2\n"
            "        Branches\n"
            "        [\n"
            "            {\n"
            "                width 1.6\n"
            "                length 12\n"
            "                leafCount 125\n"
            "            }\n"
            "            {\n"
            "                width 2.1\n"
            "                length 15\n"
            "                leafCount 156\n"
            "            }\n"
            "            {\n"
            "                width 1.4\n"
            "                length 9\n"
            "                leafCount 79\n"
            "            }\n"
            "        ]\n"
            "    }\n"
            "}"
        };
        const char s_fullFeatureObjWriteOneLine[]
        {
            "SimpleObj { num 2 numArray [ 0.2 1.2 -3 ] str \"tonton\" strArray [ \"hola\" \"les\" \"amis\" \"\" ] falseBool 0 trueBool 1 alsoTrue 1 } "
            "ArrayObj [ { allo \"misleading strings\" } { allo \"#\" } { allo \"][\" } { allo \"}={\" } ] "
            "GrootObj { name \"Groot!\" Trunk { width 15 height 135.2 Branches [ { width 1.6 length 12 leafCount 125 } { width 2.1 length 15 leafCount 156 } { width 1.4 length 9 leafCount 79 } ] } }"
        };
        const char s_missLastObjClose[]
        {
            "SimpleObj\n"
            "{\n"
            "    num 2\n"
            "    numArray [0.2 1.2 -3]\n"
            "    str \"tonton\"\n"
            "    strArray[\"hola\" \"les\" \"amis\" \"\"]\n"
            ""
        };
        const char s_missArrayClose[]
        {
            "SimpleObj\n"
            "{\n"
            "    num 2\n"
            "    numArray [0.2 1.2 -3\n"
            "    str \"tonton\"\n"
            "    strArray[\"hola\" \"les\" \"amis\" \"\"]\n"
            "}"
        };
        const char s_missStrClose[]
        {
            "SimpleObj\n"
            "{\n"
            "    num 2\n"
            "    numArray [0.2 1.2 -3]\n"
            "    str \"tonton\n"
            "    strArray[\"hola\" \"les\" \"amis\" \"\"]\n"
            "}"
        };

        struct SimpleObj
        {
            u32 num;
            std::vector<f32> numArray;
            std::string str;
            std::vector<std::string> strArray;
            bool falseBool, trueBool, alsoTrue;

            DJO_SRL_MEMBERS_DEC_IMPL(
                ReadWrite, DJO_SRL_NHDP_SRL,
                num, numArray, str, strArray,
                falseBool, trueBool, alsoTrue);
        };
        bool IsSame(const SimpleObj&, const SimpleObj&);

        struct SimpleObjStringView
        {
            u32 num;
            std::vector<f32> numArray;
            StringView str;
            std::vector<StringView> strArray;
            bool falseBool, trueBool, alsoTrue;

            DJO_SRL_MEMBERS_DEC_IMPL(
                ReadWrite, DJO_SRL_NHDP_SRL,
                num, numArray, str, strArray,
                falseBool, trueBool, alsoTrue);
        };

        struct ArrayObjElem
        {
            std::string allo;

            DJO_SRL_MEMBERS_DEC_IMPL(ReadWrite, DJO_SRL_NHDP_SRL, allo);
        };
        bool IsSame(const ArrayObjElem&, const ArrayObjElem&);

        struct GrootBranch
        {
            f32 width, length;
            u32 leafCount;

            DJO_SRL_MEMBERS_DEC_IMPL(
                ReadWrite, DJO_SRL_NHDP_SRL,
                width, length, leafCount);
        };
        bool IsSame(const GrootBranch&, const GrootBranch&);

        struct GrootTrunk
        {
            f32 width, height;
            std::vector<GrootBranch> branches;

            DJO_SRL_MEMBERS_IMPL(
                ReadWrite,
                DJO_SRL_NHDP_SRL(width), DJO_SRL_NHDP_SRL(height),
                DJO_SRL_NHDP_NAME_SRL("Branches", branches));
        };
        bool IsSame(const GrootTrunk&, const GrootTrunk&);

        struct GrootObj
        {
            StringView name;
            GrootTrunk trunk;

            DJO_SRL_MEMBERS_IMPL(
                ReadWrite,
                DJO_SRL_NHDP_SRL(name),
                DJO_SRL_NHDP_NAME_SRL("Trunk", trunk));
        };
        bool IsSame(const GrootObj&, const GrootObj&);

        struct FullFeatured
        {
            SimpleObj simpleObj;
            std::vector<ArrayObjElem> arrayObj;
            GrootObj groot;

            DJO_SRL_MEMBERS_IMPL(
                ReadWrite,
                DJO_SRL_NHDP_NAME_SRL("SimpleObj", simpleObj),
                DJO_SRL_NHDP_NAME_SRL("ArrayObj", arrayObj),
                DJO_SRL_NHDP_NAME_SRL("GrootObj", groot));
        };
        bool IsSame(const FullFeatured&, const FullFeatured&);
    }
}
