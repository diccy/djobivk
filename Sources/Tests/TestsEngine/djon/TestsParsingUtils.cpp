
#include "djon/TestsDjon.hpp"

#include <djon/ParsingUtils.hpp>


namespace Djo
{
    namespace Tests
    {
        template <sz N>
        constexpr
        ArrayWrapper<const char, N>
        RNToSN(const char (&_str)[N]) noexcept
        {
            ArrayWrapper<const char, N> r{};
            char* p = const_cast<char*>(r.a);
            for (sz i = 0; i < N; ++i)
            {
                const char c = _str[i];
                p[i] = (c == '\r') ? ' ' : c;
            }
            return r;
        }
        template <typename TTestFunc, sz N>
        void
        TestSNAndRN(TTestFunc _func, const char (&_strRN)[N]) noexcept
        {
            _func(_strRN);
            _func(RNToSN(_strRN).a);
        }

        TestsResult
        ParsingUtils_GetNextLine()
        {
            DJO_UTESTS_INIT();

            const char s_basic1RN[]{
                "bjr\r\n"
                "  coucou  \r\n"
                "\t\r\n"
                "\r\n"
                "\r\n"
            };
            const char s_basic2RN[]{
                "\t\r\n"
                "      bjr   \r\n"
                "coucou"
            };
            const auto BasicTest = [DJO_UTESTS_LAMBDA_CAPTURE](auto& _str)
            {
                const char* it = _str;
                const char* const itEnd = it + ArraySize(_str) - 1;
                StringView line;
                bool gotLine;

                gotLine = GetNextLine(it, itEnd, &line, &it);
                DJO_UTEST(gotLine == true);
                DJO_UTEST(line == "bjr");
                gotLine = GetNextLine(it, itEnd, &line, &it);
                DJO_UTEST(gotLine == true);
                DJO_UTEST(line == "coucou");
                gotLine = GetNextLine(it, itEnd, &line, &it);
                DJO_UTEST(gotLine == false);
            };
            TestSNAndRN(BasicTest, s_basic1RN);
            TestSNAndRN(BasicTest, s_basic2RN);

            const char s_whiteRN[]{ "\r\n    \t\r\n    \r\n\r\n\r\n" };
            const auto EmptyTest = [DJO_UTESTS_LAMBDA_CAPTURE](auto& _str)
            {
                StringView line;
                DJO_UTEST(GetNextLine(_str, _str + ArraySize(_str) - 1, &line) == false);
            };
            TestSNAndRN(EmptyTest, s_whiteRN);
            TestSNAndRN(EmptyTest, "");

            DJO_UTESTS_END();
        }

        TestsResult
        ParsingUtils_All(LocalFileSystem&)
        {
            TestsResult ret;
            ret += ParsingUtils_GetNextLine();

            return ret;
        }
    }
}
