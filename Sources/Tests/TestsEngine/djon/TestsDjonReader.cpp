
#include "djon/TestsDjon.hpp"

#include <djon/DjonParser.hpp>
#include <djon/DjonSerialization.hpp>
#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        DjonReader_Basics()
        {
            DJO_UTESTS_INIT();

            djon::Parser parser;
            djon::Reader reader;
            EResult result;

            result = parser.Parse(s_simpleObj);
            DJO_UTEST(result == EResult::OK);
            if (result == EResult::OK)
            {
                const djon::Elem& parserRoot = *parser.GetRoot();
                reader.SetElem(parserRoot);

                SimpleObj simpleObj;
                result = reader.Read("SimpleObj"_h64, &simpleObj);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    DJO_UTEST(simpleObj.num == 2);
                    DJO_UTEST(simpleObj.numArray.size() == 3);
                    DJO_UTEST(Equals(simpleObj.numArray[0], 0.2f));
                    DJO_UTEST(Equals(simpleObj.numArray[1], 1.2f));
                    DJO_UTEST(Equals(simpleObj.numArray[2], -3.f));
                    DJO_UTEST(simpleObj.str == "tonton");
                    DJO_UTEST(simpleObj.strArray.size() == 4);
                    DJO_UTEST(simpleObj.strArray[0] == "hola");
                    DJO_UTEST(simpleObj.strArray[1] == "les");
                    DJO_UTEST(simpleObj.strArray[2] == "amis");
                    DJO_UTEST(simpleObj.strArray[3] == "");
                }

                SimpleObjStringView simpleObjStrView;
                result = reader.Read("SimpleObj"_h64, &simpleObjStrView);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    DJO_UTEST(simpleObjStrView.num == 2);
                    DJO_UTEST(simpleObjStrView.numArray.size() == 3);
                    DJO_UTEST(Equals(simpleObjStrView.numArray[0], 0.2f));
                    DJO_UTEST(Equals(simpleObjStrView.numArray[1], 1.2f));
                    DJO_UTEST(Equals(simpleObjStrView.numArray[2], -3.f));
                    DJO_UTEST(simpleObjStrView.str == "tonton");
                    DJO_UTEST(simpleObjStrView.strArray.size() == 4);
                    DJO_UTEST(simpleObjStrView.strArray[0] == "hola");
                    DJO_UTEST(simpleObjStrView.strArray[1] == "les");
                    DJO_UTEST(simpleObjStrView.strArray[2] == "amis");
                    DJO_UTEST(simpleObjStrView.strArray[3] == "");
                }
            }

            result = parser.Parse(s_fullFeatureObj);
            DJO_UTEST(result == EResult::OK);
            if (result == EResult::OK)
            {
                const djon::Elem& parserRoot = *parser.GetRoot();
                reader.SetElem(parserRoot);

                FullFeatured fullFeatured;
                reader.Read(&fullFeatured);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    DJO_UTEST(fullFeatured.simpleObj.num == 2);
                    DJO_UTEST(fullFeatured.simpleObj.numArray.size() == 3);
                    DJO_UTEST(Equals(fullFeatured.simpleObj.numArray[0], 0.2f));
                    DJO_UTEST(Equals(fullFeatured.simpleObj.numArray[1], 1.2f));
                    DJO_UTEST(Equals(fullFeatured.simpleObj.numArray[2], -3.f));
                    DJO_UTEST(fullFeatured.simpleObj.str == "tonton");
                    DJO_UTEST(fullFeatured.simpleObj.strArray.size() == 4);
                    DJO_UTEST(fullFeatured.simpleObj.strArray[0] == "hola");
                    DJO_UTEST(fullFeatured.simpleObj.strArray[1] == "les");
                    DJO_UTEST(fullFeatured.simpleObj.strArray[2] == "amis");
                    DJO_UTEST(fullFeatured.simpleObj.strArray[3] == "");
                    DJO_UTEST(fullFeatured.arrayObj.size() == 4);
                    DJO_UTEST(fullFeatured.arrayObj[0].allo == "misleading strings");
                    DJO_UTEST(fullFeatured.arrayObj[1].allo == "#");
                    DJO_UTEST(fullFeatured.arrayObj[2].allo == "][");
                    DJO_UTEST(fullFeatured.arrayObj[3].allo == "}={");
                    DJO_UTEST(fullFeatured.groot.name == "Groot!");
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.width, 15.f));
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.height, 135.2f));
                    DJO_UTEST(fullFeatured.groot.trunk.branches.size() == 3);
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.branches[0].width, 1.6f));
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.branches[0].length, 12.f));
                    DJO_UTEST(fullFeatured.groot.trunk.branches[0].leafCount == 125);
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.branches[1].width, 2.1f));
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.branches[1].length, 15.f));
                    DJO_UTEST(fullFeatured.groot.trunk.branches[1].leafCount == 156);
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.branches[2].width, 1.4f));
                    DJO_UTEST(Equals(fullFeatured.groot.trunk.branches[2].length, 9.f));
                    DJO_UTEST(fullFeatured.groot.trunk.branches[2].leafCount == 79);
                }
            }

            DJO_UTESTS_END();
        }

        TestsResult
        DjonReader_Extended()
        {
            DJO_UTESTS_INIT();

            djon::Parser parser;
            djon::Reader reader;
            EResult result;

            result = parser.Parse(s_simpleObj);
            DJO_UTEST(result == EResult::OK);
            if (result == EResult::OK)
            {
                const djon::Elem& parserRoot = *parser.GetRoot();

                const djon::Elem* numArrayElem = djon::GetChild(parserRoot, "SimpleObj/numArray");
                DJO_UTEST(numArrayElem != nullptr);
                if (numArrayElem != nullptr)
                {
                    reader.SetElem(*numArrayElem);
                    {
                        Vec2f v2{ 0.f, 0.f };
                        reader.Read(&v2);
                        DJO_UTEST(Equals(v2.x, 0.2f));
                        DJO_UTEST(Equals(v2.y, 1.2f));
                    }
                    {
                        Vec3f v3{ 0.f, 0.f, 0.f };
                        reader.Read(&v3);
                        DJO_UTEST(Equals(v3.x, 0.2f));
                        DJO_UTEST(Equals(v3.y, 1.2f));
                        DJO_UTEST(Equals(v3.z, -3.f));
                    }
                    {
                        Vec4f v4{ 0.f, 0.f, 0.f, 0.f };
                        reader.Read(&v4);
                        DJO_UTEST(Equals(v4.x, 0.2f));
                        DJO_UTEST(Equals(v4.y, 1.2f));
                        DJO_UTEST(Equals(v4.z, -3.f));
                        DJO_UTEST(Equals(v4.w, 0.f));
                    }
                }
            }

            DJO_UTESTS_END();
        }

        TestsResult
        DjonReader_All(LocalFileSystem&)
        {
            TestsResult ret;
            ret += DjonReader_Basics();
            ret += DjonReader_Extended();

            return ret;
        }
    }
}
