
#include "djon/TestsDjon.hpp"

#include <djon/DjonParser.hpp>
#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        DjonParser_All(LocalFileSystem&)
        {
            DJO_UTESTS_INIT();

            djon::Parser parser;
            const djon::Elem* currentElem;
            EResult result;

            {
                result = parser.Parse(s_simpleObj);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    currentElem = parser.GetRoot();

                    DJO_UTEST(djon::GetChild(*currentElem, "lol") == nullptr);

                    const djon::Elem* simpleObj = djon::GetChild(*currentElem, "SimpleObj");
                    DJO_UTEST(simpleObj != nullptr);
                    DJO_UTEST(djon::GetChild(*currentElem, "SimpleObj") == djon::GetChild(*currentElem, "SimpleObj"_h64));
                    if (simpleObj != nullptr)
                    {
                        DJO_UTEST(simpleObj->name == "SimpleObj");
                        DJO_UTEST(simpleObj->nameHash == "SimpleObj"_h64);
                        DJO_UTEST(simpleObj->type == EValue(djon::EValueType::Object));
                        DJO_UTEST(simpleObj->value->o.count == 4);
                    }

                    DJO_UTEST(djon::GetChild(*currentElem, "SimpleObj/") == nullptr);
                    DJO_UTEST(djon::GetChild(*currentElem, "SimpleObj/lol") == nullptr);

                    const djon::Elem* num = djon::GetChild(*currentElem, "SimpleObj/num");
                    DJO_UTEST(num != nullptr);
                    if (num != nullptr)
                    {
                        DJO_UTEST(num->name == "num");
                        DJO_UTEST(num->nameHash == "num"_h64);
                        DJO_UTEST(num->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(num->value->n == 2.);
                    }
                    const djon::Elem* numArray = djon::GetChild(*currentElem, "SimpleObj/numArray");
                    DJO_UTEST(numArray != nullptr);
                    if (numArray != nullptr)
                    {
                        DJO_UTEST(numArray->name == "numArray");
                        DJO_UTEST(numArray->nameHash == "numArray"_h64);
                        DJO_UTEST(numArray->type == (EValue(djon::EValueType::Number) | Flag(EValue(djon::EValueType::Array))));
                        DJO_UTEST(numArray->value->a.count == 3);
                        DJO_UTEST(Equals(numArray->value->a[0].n, 0.2));
                        DJO_UTEST(Equals(numArray->value->a[1].n, 1.2));
                        DJO_UTEST(Equals(numArray->value->a[2].n, -3.));
                    }
                    const djon::Elem* str = djon::GetChild(*currentElem, "SimpleObj/str");
                    DJO_UTEST(str != nullptr);
                    if (str != nullptr)
                    {
                        DJO_UTEST(str->name == "str");
                        DJO_UTEST(str->nameHash == "str"_h64);
                        DJO_UTEST(str->type == EValue(djon::EValueType::String));
                        DJO_UTEST(str->value->s == "tonton");
                    }
                    const djon::Elem* strArray = djon::GetChild(*currentElem, "SimpleObj/strArray");
                    DJO_UTEST(strArray != nullptr);
                    if (strArray != nullptr)
                    {
                        DJO_UTEST(strArray->name == "strArray");
                        DJO_UTEST(strArray->nameHash == "strArray"_h64);
                        DJO_UTEST(strArray->type == (EValue(djon::EValueType::String) | Flag(EValue(djon::EValueType::Array))));
                        DJO_UTEST(strArray->value->a.count == 4);
                        DJO_UTEST(strArray->value->a[0].s == "hola");
                        DJO_UTEST(strArray->value->a[1].s == "les");
                        DJO_UTEST(strArray->value->a[2].s == "amis");
                        DJO_UTEST(strArray->value->a[3].s == "");
                    }
                }
            }
            {
                result = parser.Parse(s_fullFeatureObj);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    currentElem = parser.GetRoot();

                    const djon::Elem* simpleObj = djon::GetChild(*currentElem, "SimpleObj");
                    DJO_UTEST(simpleObj != nullptr);
                    DJO_UTEST(djon::GetChild(*currentElem, "SimpleObj") == djon::GetChild(*currentElem, "SimpleObj"_h64));
                    if (simpleObj != nullptr)
                    {
                        DJO_UTEST(simpleObj->name == "SimpleObj");
                        DJO_UTEST(simpleObj->nameHash == "SimpleObj"_h64);
                        DJO_UTEST(simpleObj->type == EValue(djon::EValueType::Object));
                        DJO_UTEST(simpleObj->value->o.count == 7);
                    }
                    const djon::Elem* num = djon::GetChild(*currentElem, "SimpleObj/num");
                    DJO_UTEST(num != nullptr);
                    if (num != nullptr)
                    {
                        DJO_UTEST(num->name == "num");
                        DJO_UTEST(num->nameHash == "num"_h64);
                        DJO_UTEST(num->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(num->value->n == 2.);
                    }
                    const djon::Elem* numArray = djon::GetChild(*currentElem, "SimpleObj/numArray");
                    DJO_UTEST(numArray != nullptr);
                    if (numArray != nullptr)
                    {
                        DJO_UTEST(numArray->name == "numArray");
                        DJO_UTEST(numArray->nameHash == "numArray"_h64);
                        DJO_UTEST(numArray->type == (EValue(djon::EValueType::Number) | Flag(EValue(djon::EValueType::Array))));
                        DJO_UTEST(Equals(numArray->value->a[0].n, 0.2));
                        DJO_UTEST(Equals(numArray->value->a[1].n, 1.2));
                        DJO_UTEST(Equals(numArray->value->a[2].n, -3.));
                    }
                    const djon::Elem* str = djon::GetChild(*currentElem, "SimpleObj/str");
                    DJO_UTEST(str != nullptr);
                    if (str != nullptr)
                    {
                        DJO_UTEST(str->name == "str");
                        DJO_UTEST(str->nameHash == "str"_h64);
                        DJO_UTEST(str->type == EValue(djon::EValueType::String));
                        DJO_UTEST(str->value->s == "tonton");
                    }
                    const djon::Elem* strArray = djon::GetChild(*currentElem, "SimpleObj/strArray");
                    DJO_UTEST(strArray != nullptr);
                    if (strArray != nullptr)
                    {
                        DJO_UTEST(strArray->name == "strArray");
                        DJO_UTEST(strArray->nameHash == "strArray"_h64);
                        DJO_UTEST(strArray->type == (EValue(djon::EValueType::String) | Flag(EValue(djon::EValueType::Array))));
                        DJO_UTEST(strArray->value->a.count == 4);
                        DJO_UTEST(strArray->value->a[0].s == "hola");
                        DJO_UTEST(strArray->value->a[1].s == "les");
                        DJO_UTEST(strArray->value->a[2].s == "amis");
                        DJO_UTEST(strArray->value->a[3].s == "");
                    }
                    const djon::Elem* trueBool = djon::GetChild(*currentElem, "SimpleObj/trueBool");
                    DJO_UTEST(trueBool != nullptr);
                    if (trueBool != nullptr)
                    {
                        DJO_UTEST(trueBool->name == "trueBool");
                        DJO_UTEST(trueBool->nameHash == "trueBool"_h64);
                        DJO_UTEST(trueBool->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(((bool)trueBool->value->n) == true);
                    }
                    const djon::Elem* falseBool = djon::GetChild(*currentElem, "SimpleObj/falseBool");
                    DJO_UTEST(falseBool != nullptr);
                    if (falseBool != nullptr)
                    {
                        DJO_UTEST(falseBool->name == "falseBool");
                        DJO_UTEST(falseBool->nameHash == "falseBool"_h64);
                        DJO_UTEST(falseBool->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(((bool)falseBool->value->n) == false);
                    }
                    const djon::Elem* alsoTrue = djon::GetChild(*currentElem, "SimpleObj/alsoTrue");
                    DJO_UTEST(alsoTrue != nullptr);
                    if (alsoTrue != nullptr)
                    {
                        DJO_UTEST(alsoTrue->name == "alsoTrue");
                        DJO_UTEST(alsoTrue->nameHash == "alsoTrue"_h64);
                        DJO_UTEST(alsoTrue->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(((bool)alsoTrue->value->n) == true);
                    }

                    const djon::Elem* arrayObj = djon::GetChild(*currentElem, "ArrayObj");
                    DJO_UTEST(arrayObj != nullptr);
                    if (arrayObj != nullptr)
                    {
                        DJO_UTEST(arrayObj->name == "ArrayObj");
                        DJO_UTEST(arrayObj->nameHash == "ArrayObj"_h64);
                        DJO_UTEST(arrayObj->type == (EValue(djon::EValueType::Object) | Flag(EValue(djon::EValueType::Array))));
                        DJO_UTEST(arrayObj->value->a.count == 4);
                        const djon::Value* v;
                        v = &arrayObj->value->a[0];
                        DJO_UTEST(v->o.count == 1);
                        DJO_UTEST(v->o[0].name == "allo");
                        DJO_UTEST(v->o[0].nameHash == "allo"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::String));
                        DJO_UTEST(v->o[0].value->s == "misleading strings");
                        v = &arrayObj->value->a[1];
                        DJO_UTEST(v->o.count == 1);
                        DJO_UTEST(v->o[0].name == "allo");
                        DJO_UTEST(v->o[0].nameHash == "allo"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::String));
                        DJO_UTEST(v->o[0].value->s == "#");
                        v = &arrayObj->value->a[2];
                        DJO_UTEST(v->o.count == 1);
                        DJO_UTEST(v->o[0].name == "allo");
                        DJO_UTEST(v->o[0].nameHash == "allo"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::String));
                        DJO_UTEST(v->o[0].value->s == "][");
                        v = &arrayObj->value->a[3];
                        DJO_UTEST(v->o.count == 1);
                        DJO_UTEST(v->o[0].name == "allo");
                        DJO_UTEST(v->o[0].nameHash == "allo"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::String));
                        DJO_UTEST(v->o[0].value->s == "}={");
                    }

                    const djon::Elem* grootObj = djon::GetChild(*currentElem, "GrootObj");
                    DJO_UTEST(grootObj != nullptr);
                    if (grootObj != nullptr)
                    {
                        DJO_UTEST(grootObj->name == "GrootObj");
                        DJO_UTEST(grootObj->nameHash == "GrootObj"_h64);
                        DJO_UTEST(grootObj->type == EValue(djon::EValueType::Object));
                        DJO_UTEST(grootObj->value->o.count == 2);
                    }

                    currentElem = djon::GetChild(*currentElem, "GrootObj"_h64);
                    const djon::Elem* grootName = djon::GetChild(*currentElem, "name");
                    DJO_UTEST(grootName != nullptr);
                    if (grootName != nullptr)
                    {
                        DJO_UTEST(grootName->name == "name");
                        DJO_UTEST(grootName->nameHash == "name"_h64);
                        DJO_UTEST(grootName->type == EValue(djon::EValueType::String));
                        DJO_UTEST(grootName->value->s == "Groot!");
                    }
                    const djon::Elem* trunk = djon::GetChild(*currentElem, "Trunk");
                    DJO_UTEST(trunk != nullptr);
                    if (trunk != nullptr)
                    {
                        DJO_UTEST(trunk->name == "Trunk");
                        DJO_UTEST(trunk->nameHash == "Trunk"_h64);
                        DJO_UTEST(trunk->type == EValue(djon::EValueType::Object));
                        DJO_UTEST(trunk->value->o.count == 3);
                    }

                    currentElem = djon::GetChild(*currentElem, "Trunk"_h64);
                    const djon::Elem* trunkWidth = djon::GetChild(*currentElem, "width");
                    DJO_UTEST(trunkWidth != nullptr);
                    if (trunkWidth != nullptr)
                    {
                        DJO_UTEST(trunkWidth->name == "width");
                        DJO_UTEST(trunkWidth->nameHash == "width"_h64);
                        DJO_UTEST(trunkWidth->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(trunkWidth->value->n, 15.));
                    }
                    const djon::Elem* trunkHeight = djon::GetChild(*currentElem, "height");
                    DJO_UTEST(trunkHeight != nullptr);
                    if (trunkHeight != nullptr)
                    {
                        DJO_UTEST(trunkHeight->name == "height");
                        DJO_UTEST(trunkHeight->nameHash == "height"_h64);
                        DJO_UTEST(trunkHeight->type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(trunkHeight->value->n, 135.2));
                    }
                    const djon::Elem* trunkBranches = djon::GetChild(*currentElem, "Branches");
                    DJO_UTEST(trunkBranches != nullptr);
                    if (trunkBranches != nullptr)
                    {
                        DJO_UTEST(trunkBranches->name == "Branches");
                        DJO_UTEST(trunkBranches->nameHash == "Branches"_h64);
                        DJO_UTEST(trunkBranches->type == (EValue(djon::EValueType::Object) | Flag(EValue(djon::EValueType::Array))));
                        DJO_UTEST(trunkBranches->value->a.count == 3);
                        const djon::Value* v;
                        v = &trunkBranches->value->a[0];
                        DJO_UTEST(v->o.count == 3);
                        DJO_UTEST(v->o[0].name == "width");
                        DJO_UTEST(v->o[0].nameHash == "width"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[0].value->n, 1.6));
                        DJO_UTEST(v->o[1].name == "length");
                        DJO_UTEST(v->o[1].nameHash == "length"_h64);
                        DJO_UTEST(v->o[1].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[1].value->n, 12.));
                        DJO_UTEST(v->o[2].name == "leafCount");
                        DJO_UTEST(v->o[2].nameHash == "leafCount"_h64);
                        DJO_UTEST(v->o[2].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[2].value->n, 125.));
                        v = &trunkBranches->value->a[1];
                        DJO_UTEST(v->o.count == 3);
                        DJO_UTEST(v->o[0].name == "width");
                        DJO_UTEST(v->o[0].nameHash == "width"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[0].value->n, 2.1));
                        DJO_UTEST(v->o[1].name == "length");
                        DJO_UTEST(v->o[1].nameHash == "length"_h64);
                        DJO_UTEST(v->o[1].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[1].value->n, 15.));
                        DJO_UTEST(v->o[2].name == "leafCount");
                        DJO_UTEST(v->o[2].nameHash == "leafCount"_h64);
                        DJO_UTEST(v->o[2].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[2].value->n, 156.));
                        v = &trunkBranches->value->a[2];
                        DJO_UTEST(v->o.count == 3);
                        DJO_UTEST(v->o[0].name == "width");
                        DJO_UTEST(v->o[0].nameHash == "width"_h64);
                        DJO_UTEST(v->o[0].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[0].value->n, 1.4));
                        DJO_UTEST(v->o[1].name == "length");
                        DJO_UTEST(v->o[1].nameHash == "length"_h64);
                        DJO_UTEST(v->o[1].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[1].value->n, 9.));
                        DJO_UTEST(v->o[2].name == "leafCount");
                        DJO_UTEST(v->o[2].nameHash == "leafCount"_h64);
                        DJO_UTEST(v->o[2].type == EValue(djon::EValueType::Number));
                        DJO_UTEST(Equals(v->o[2].value->n, 79.));
                    }

                    currentElem = parser.GetRoot();
                    const djon::Elem* branches1 = djon::GetChild(*currentElem, "GrootObj/Trunk/Branches");
                    DJO_UTEST(branches1 != nullptr);
                    currentElem = djon::GetChild(*currentElem, "GrootObj"_h64);
                    const djon::Elem* branches2 = djon::GetChild(*currentElem, "Trunk/Branches");
                    DJO_UTEST(branches1 == branches2);
                    currentElem = djon::GetChild(*currentElem, "Trunk"_h64);
                    const djon::Elem* branches3 = djon::GetChild(*currentElem, "Branches"_h64);
                    DJO_UTEST(branches3 == branches1);
                }
            }
            {
                const auto TestVersion = [&parser, DJO_UTESTS_LAMBDA_CAPTURE]
                    (const StringView& _content, const Version _test)
                    -> void
                {
                    const EResult result = parser.Parse(_content);
                    DJO_UTEST(result == EResult::OK);
                    DJO_UTEST((parser.GetContentVersion() == _test));
                    if (result == EResult::OK)
                        DJO_UTEST((djon::GetChild(*parser.GetRoot(), "num")->value->n == 2.));
                };
                TestVersion(s_numVersionned010, Version{ 0, 1, 0 });
                TestVersion(s_numVersionned00123, Version{ 0, 0, 123 });
                TestVersion(s_numVersionned0, Version{ 0 });
                TestVersion(s_numVersionned01, Version{ 0, 1 });
                TestVersion(s_numVersionnedNo, djon::c_version);
            }

            DJO_UTESTS_END();
        }
    }
}
