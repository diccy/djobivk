
#include "djon/TestsDjon.hpp"

#include <djon/DjonTokenizer.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        DjonTokenizer_All(LocalFileSystem&)
        {
            DJO_UTESTS_INIT();

            #define DJO_UTEST_TOKEN_IT(type_, token_, count_)\
                DJO_UTEST(it->type == type_ && it->token == token_ && it->contentCount == count_);\
                ++it

            std::string errorMsg;
            djon::Tokenizer tokenizer;
            EResult result;

            result = tokenizer.Tokenize(s_simpleObj, &errorMsg);
            DJO_UTEST(result == EResult::OK);
            if (result == EResult::OK)
            {
                result = tokenizer.VerifyTokens(&errorMsg);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    result = tokenizer.CountContent();
                    DJO_UTEST(result == EResult::OK);
                    if (result == EResult::OK)
                    {
                        DJO_UTEST(tokenizer.GetElemCount() == 6);
                        DJO_UTEST(tokenizer.GetValueCount() == 12);
                        {
                            const std::vector<djon::Token>& tokens = tokenizer.GetTokens();
                            DJO_UTEST(tokens.size() == 21);
                            std::vector<djon::Token>::const_iterator it = tokens.begin();

                            DJO_UTEST_TOKEN_IT(djon::ETokenType::None, StringView{}, 1);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "SimpleObj", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 4);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "num", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "numArray", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayStart, "[", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "0.2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "1.2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "-3", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayEnd, "]", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "str", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"tonton", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "strArray", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayStart, "[", 4);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"hola", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"les", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"amis", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayEnd, "]", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                        }
                    }
                }
            }

            result = tokenizer.Tokenize(s_fullFeatureObj, &errorMsg);
            DJO_UTEST(result == EResult::OK);
            if (result == EResult::OK)
            {
                result = tokenizer.VerifyTokens(&errorMsg);
                DJO_UTEST(result == EResult::OK);
                if (result == EResult::OK)
                {
                    result = tokenizer.CountContent();
                    DJO_UTEST(result == EResult::OK);
                    if (result == EResult::OK)
                    {
                        DJO_UTEST(tokenizer.GetElemCount() == 29);
                        DJO_UTEST(tokenizer.GetValueCount() == 42);
                        {
                            const std::vector<djon::Token>& tokens = tokenizer.GetTokens();
                            DJO_UTEST(tokens.size() == 85);
                            std::vector<djon::Token>::const_iterator it = tokens.begin();

                            DJO_UTEST_TOKEN_IT(djon::ETokenType::None, StringView{}, 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "SimpleObj", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 7);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "num", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "numArray", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayStart, "[", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "0.2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "1.2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "-3", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayEnd, "]", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "str", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"tonton", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "strArray", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayStart, "[", 4);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"hola", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"les", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"amis", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayEnd, "]", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "falseBool", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "0", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "trueBool", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "1", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "alsoTrue", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "-0.1", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);

                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "ArrayObj", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayStart, "[", 4);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 1);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "allo", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"misleading strings", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 1);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "allo", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"#", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 1);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "allo", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"][", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 1);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "allo", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"}={", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayEnd, "]", 0);

                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "GrootObj", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 2);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "name", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "\"Groot!", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "Trunk", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "width", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "15", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "height", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "135.2", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "Branches", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayStart, "[", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "width", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "1.6", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "length", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "12", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "leafCount", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "125", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "width", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "2.1", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "length", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "15", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "leafCount", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "156", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjStart, "{", 3);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "width", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "1.4", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "length", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "9", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ElemName, "leafCount", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::Value, "79", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ArrayEnd, "]", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                            DJO_UTEST_TOKEN_IT(djon::ETokenType::ObjEnd, "}", 0);
                        }
                    }
                }
            }

            DJO_DISABLE_ASSERT_WHILE(
                tokenizer.Tokenize(s_missLastObjClose, &errorMsg);
                result = tokenizer.VerifyTokens(&errorMsg);
                DJO_UTEST(result != EResult::OK);
                tokenizer.Tokenize(s_missArrayClose, &errorMsg);
                result = tokenizer.VerifyTokens(&errorMsg);
                DJO_UTEST(result != EResult::OK);
                tokenizer.Tokenize(s_missStrClose, &errorMsg);
                result = tokenizer.VerifyTokens(&errorMsg);
                DJO_UTEST(result != EResult::OK);
            );

            #undef DJO_UTEST_TOKEN_IT

            DJO_UTESTS_END();
        }
    }
}
