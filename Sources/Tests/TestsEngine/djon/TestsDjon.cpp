
#include "djon/TestsDjon.hpp"

#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        TestsDjon()
        {
            TestsResult ret;

            LocalFileSystem srlFs;
            const EResult result = srlFs.Init(Path{ DJO_TESTS_FS->GetRootPath() }.Add("Serialization/"));
            DJO_ASSERT(result == EResult::OK);
            if (result == EResult::OK)
            {
                ret += ParsingUtils_All(srlFs);
                ret += DjonTokenizer_All(srlFs);
                ret += DjonParser_All(srlFs);
                ret += DjonReader_All(srlFs);
                ret += DjonWriter_All(srlFs);
            }

            return ret;
        }

        void
        OpenReadClose(LocalFileSystem& _srl, const char* const _filePath, std::string* const _outFileContent)
        {
            LocalFile f;
            f.Open(_srl.AbsolutePath(_filePath), EValue(EOpenModeFlag::Read));
            f.Read(_outFileContent);
            f.Close();
        }


        bool IsSame(const SimpleObj& _o1, const SimpleObj& _o2)
        {
            const bool equals =
                (_o1.num == _o2.num) &&
                (_o1.numArray.size() == _o2.numArray.size()) &&
                (_o1.str == _o2.str) &&
                (_o1.strArray.size() == _o2.strArray.size()) &&
                (_o1.falseBool == _o2.falseBool) &&
                (_o1.trueBool == _o2.trueBool) &&
                (_o1.alsoTrue == _o2.alsoTrue);
            if (!equals)
                return false;
            for (u32 i = 0; i < _o1.numArray.size(); ++i)
                if (!Equals(_o1.numArray[i], _o2.numArray[i]))
                    return false;
            for (u32 i = 0; i < _o1.strArray.size(); ++i)
                if (_o1.strArray[i] != _o2.strArray[i])
                    return false;
            return true;
        }
        bool IsSame(const ArrayObjElem& _o1, const ArrayObjElem& _o2)
        {
            return (_o1.allo == _o2.allo);
        }
        bool IsSame(const GrootBranch& _o1, const GrootBranch& _o2)
        {
            return Equals(_o1.width, _o2.width) && Equals(_o1.length, _o2.length) && (_o1.leafCount == _o2.leafCount);
        }
        bool IsSame(const GrootTrunk& _o1, const GrootTrunk& _o2)
        {
            const bool equals =
                Equals(_o1.width, _o2.width) &&
                Equals(_o1.height, _o2.height) &&
                (_o1.branches.size() == _o2.branches.size());
            if (!equals)
                return false;
            for (u32 i = 0; i < _o1.branches.size(); ++i)
                if (!IsSame(_o1.branches[i], _o2.branches[i]))
                    return false;
            return true;
        }
        bool IsSame(const GrootObj& _o1, const GrootObj& _o2)
        {
            return (_o1.name == _o2.name) && IsSame(_o1.trunk, _o2.trunk);
        }
        bool IsSame(const FullFeatured& _o1, const FullFeatured& _o2)
        {
            const bool equals =
                IsSame(_o1.simpleObj, _o2.simpleObj) &&
                (_o1.arrayObj.size() == _o2.arrayObj.size()) &&
                IsSame(_o1.groot, _o2.groot);
            if (!equals)
                return false;
            for (u32 i = 0; i < _o1.arrayObj.size(); ++i)
                if (!IsSame(_o1.arrayObj[i], _o2.arrayObj[i]))
                    return false;
            return true;
        }
    }
}
