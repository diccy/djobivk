
#include "Serialization/TestsSerialization.hpp"

#include <Serialization/Serialization.hpp>
#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        struct SimpleInt
        {
            int i;

            DJO_SRL_MEMBERS_IMPL(ReadWrite, i);
        };

        struct SimpleIntPrivate
        {
            int i;

        private:
            friend struct Srl::Access;
            DJO_SRL_MEMBERS_IMPL(ReadWrite, i);
        };

        struct SixNumbers
        {
            f64 d; f32 f; sz s; s32 i; u16 sh; char c;

            DJO_SRL_MEMBERS_IMPL(ReadWrite,
                d, f, s, i, sh, c);
        };

        struct SixNumbersNDP
        {
            f64 d; f32 f; sz s; s32 i; u16 sh; char c;

            DJO_SRL_MEMBERS_DEC_IMPL(ReadWrite,
                DJO_SRL_NDP,
                d, f, s, i, sh, c);
        };

        struct Versionned
        {
            int i;

            DJO_SRL_MEMBERS_FUNC(ReadWrite)
            {
                if (_srl.GetVersion().major == 1)
                {
                    int j = 0;
                    _srl.Apply(i, j);
                }
                else if (_srl.GetVersion().major == 2)
                {
                    int j = 0, k = 0;
                    _srl.Apply(i, j, k);
                }
                else
                {
                    _srl.Apply(i);
                }
            }
        };

        class TestSerializer
            : public Srl::Serializer<TestSerializer>
        {
        public:
            TestSerializer() : Srl::Serializer<TestSerializer>(*this) { Reset(); }

            void Reset()
            {
                numberCount = 0;
                numberAcc = 0.;
                strCount = 0;
                strAcc.clear();
            }

            void
            DoNumber(const f64 _n)
            {
                ++this->numberCount;
                this->numberAcc += _n;
            }

            void
            DoString(const StringView& _s)
            {
                ++this->strCount;
                this->strAcc.append(_s.data, _s.count);
            }

            u32 numberCount;
            f64 numberAcc;
            u32 strCount;
            std::string strAcc;
        };

        template <class TData, DJO_TARG_ENABLE_IF(std::is_arithmetic_v<TData>)>
        DJO_INLINE
        void
        ReadWriteData(TestSerializer& _srl, TData& _n)
        {
            _srl.DoNumber(static_cast<f64>(_n));
        }

        DJO_INLINE
        void
        ReadWriteData(TestSerializer& _srl, StringView& _s)
        {
            _srl.DoString(_s);
        }
        DJO_INLINE
        void
        ReadWriteData(TestSerializer& _srl, std::string& _s)
        {
            _srl.DoString(StringView{ _s });
        }

        template <u32 N>
        DJO_INLINE
        void
        ReadWriteData(TestSerializer& _srl, const char (&_s)[N])
        {
            _srl.DoString(StringView{ _s });
        }

        template <class TData>
        DJO_INLINE
        void
        ReadWriteData(TestSerializer& _srl, Srl::NameDataPair<TData>& _ndp)
        {
            _srl.DoString(_ndp.name);
            _srl.Apply(_ndp.data);
        }

        template <class TData, class A>
        DJO_INLINE
        void
        ReadWriteData(TestSerializer& _srl, std::vector<TData, A>& _v)
        {
            _srl.DoNumber((f64)_v.size());
            for (TData& data : _v)
                _srl.Apply(data);
        }

        TestsResult
        Serialization_SerializeData()
        {
            DJO_UTESTS_INIT();

            TestSerializer sz;

            sz.Reset();
            sz.Apply(1);
            DJO_UTEST(sz.numberCount == 1);
            DJO_UTEST(Equals(sz.numberAcc, 1));

            sz.Reset();
            sz.Apply(1, 2, 3, 4, 5);
            DJO_UTEST(sz.numberCount == 5);
            DJO_UTEST(Equals(sz.numberAcc, 15));

            sz.Reset();
            sz.Apply("");
            DJO_UTEST(sz.strCount == 1);
            DJO_UTEST(sz.strAcc == "");

            sz.Reset();
            sz.Apply("coucou");
            DJO_UTEST(sz.strCount == 1);
            DJO_UTEST(sz.strAcc == "coucou");

            sz.Reset();
            sz.Apply("", "coucou", " les", " potes", "!!");
            DJO_UTEST(sz.strCount == 5);
            DJO_UTEST(sz.strAcc == "coucou les potes!!");

            sz.Reset();
            sz.Apply(StringView{ "" }, 1u, std::string{ "coucou" }, 2, StringView{ " les" }, 3.f, StringView{ " potes" }, 4., StringView{ "!!" }, (char)5);
            DJO_UTEST(sz.numberCount == 5);
            DJO_UTEST(Equals(sz.numberAcc, 15));
            DJO_UTEST(sz.strCount == 5);
            DJO_UTEST(sz.strAcc == "coucou les potes!!");

            DJO_UTESTS_END();
        }

        TestsResult
        Serialization_SerializeMember()
        {
            DJO_UTESTS_INIT();

            TestSerializer sz;

            sz.Reset();
            sz.Apply(SimpleInt{ 1 });
            DJO_UTEST(sz.numberCount == 1);
            DJO_UTEST(Equals(sz.numberAcc, 1));

            sz.Reset();
            sz.Apply(SimpleIntPrivate{ 1 });
            DJO_UTEST(sz.numberCount == 1);
            DJO_UTEST(Equals(sz.numberAcc, 1));

            sz.Reset();
            sz.Apply(SimpleInt{ 1 }, SimpleInt{ 2 }, SimpleInt{ 3 }, SimpleInt{ 4 }, SimpleInt{ 5 });
            DJO_UTEST(sz.numberCount == 5);
            DJO_UTEST(Equals(sz.numberAcc, 15));

            sz.Reset();
            sz.Apply(SixNumbers{ 1, 2, 3, 4, 5, 6 });
            DJO_UTEST(sz.numberCount == 6);
            DJO_UTEST(Equals(sz.numberAcc, 21));

            sz.Reset();
            sz.Apply(SixNumbersNDP{ 1, 2, 3, 4, 5, 6 });
            DJO_UTEST(sz.numberCount == 6);
            DJO_UTEST(Equals(sz.numberAcc, 21));
            DJO_UTEST(sz.strCount == 6);
            DJO_UTEST(sz.strAcc == "dfsishc");

            DJO_UTESTS_END();
        }

        TestsResult
        Serialization_SerializeVector()
        {
            DJO_UTESTS_INIT();

            TestSerializer sz;

            {
                std::vector<int> vi;

                sz.Reset();
                sz.Apply(vi);
                DJO_UTEST(sz.numberCount == 1);
                DJO_UTEST(Equals(sz.numberAcc, 0));

                vi.push_back(1); vi.push_back(2); vi.push_back(3);
                sz.Reset();
                sz.Apply(vi);
                DJO_UTEST(sz.numberCount == 4);
                DJO_UTEST(Equals(sz.numberAcc, 6 + 3));
            }
            {
                std::vector<std::string> vs;

                sz.Reset();
                sz.Apply(vs);
                DJO_UTEST(sz.numberCount == 1);
                DJO_UTEST(Equals(sz.numberAcc, 0));
                DJO_UTEST(sz.strCount == 0);
                DJO_UTEST(sz.strAcc == "");

                vs.push_back(""); vs.push_back("coucou"); vs.push_back(" les"); vs.push_back(" potes"); vs.push_back("!!");
                sz.Reset();
                sz.Apply(vs);
                DJO_UTEST(sz.numberCount == 1);
                DJO_UTEST(Equals(sz.numberAcc, 5));
                DJO_UTEST(sz.strCount == 5);
                DJO_UTEST(sz.strAcc == "coucou les potes!!");
            }
            {
                std::vector<SixNumbersNDP> v6ndp;

                sz.Reset();
                sz.Apply(v6ndp);
                DJO_UTEST(sz.numberCount == 1);
                DJO_UTEST(Equals(sz.numberAcc, 0));
                DJO_UTEST(sz.strCount == 0);
                DJO_UTEST(sz.strAcc == "");

                v6ndp.push_back(SixNumbersNDP{ 1, 2, 3, 4, 5, 6 });
                v6ndp.push_back(SixNumbersNDP{ 1, 2, 3, 4, 5, 6 });
                v6ndp.push_back(SixNumbersNDP{ 1, 2, 3, 4, 5, 6 });
                sz.Reset();
                sz.Apply(v6ndp);
                DJO_UTEST(sz.numberCount == 19);
                DJO_UTEST(Equals(sz.numberAcc, 66));
                DJO_UTEST(sz.strCount == 18);
                DJO_UTEST(sz.strAcc == "dfsishcdfsishcdfsishc");
            }
            {
                std::vector<std::vector<std::string>> vvs;

                sz.Reset();
                sz.Apply(vvs);
                DJO_UTEST(sz.numberCount == 1);
                DJO_UTEST(Equals(sz.numberAcc, 0));
                DJO_UTEST(sz.strCount == 0);
                DJO_UTEST(sz.strAcc == "");

                vvs.push_back(std::vector<std::string>{ "", "coucou", " les" });
                vvs.push_back(std::vector<std::string>{ "", " potes", "" });
                vvs.push_back(std::vector<std::string>{ "!", "", "!" });
                sz.Reset();
                sz.Apply(vvs);
                DJO_UTEST(sz.numberCount == 4);
                DJO_UTEST(Equals(sz.numberAcc, 12));
                DJO_UTEST(sz.strCount == 9);
                DJO_UTEST(sz.strAcc == "coucou les potes!!");
            }

            DJO_UTESTS_END();
        }

        TestsResult
        Serialization_SerializeVersionned()
        {
            DJO_UTESTS_INIT();

            TestSerializer sz;

            {
                Versionned v{ 10 };

                sz.Reset();
                sz.SetVersion({ 1, 0, 0 });
                sz.Apply(v);
                DJO_UTEST(sz.numberCount == 2);
                DJO_UTEST(Equals(sz.numberAcc, 10));

                sz.Reset();
                sz.SetVersion({ 2, 0, 0 });
                sz.Apply(v);
                DJO_UTEST(sz.numberCount == 3);
                DJO_UTEST(Equals(sz.numberAcc, 10));

                sz.Reset();
                sz.SetVersion({ 3, 0, 0 });
                sz.Apply(v);
                DJO_UTEST(sz.numberCount == 1);
                DJO_UTEST(Equals(sz.numberAcc, 10));
            }

            DJO_UTESTS_END();
        }

        TestsResult
        TestsSerialization()
        {
            TestsResult ret;

            ret += Serialization_SerializeData();
            ret += Serialization_SerializeMember();
            ret += Serialization_SerializeVector();
            ret += Serialization_SerializeVersionned();

            return ret;
        }
    }
}
