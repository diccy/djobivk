
#include "Math/TestsMath.hpp"

#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        Float_All()
        {
            DJO_UTESTS_INIT();

            const u8  i_1{ 0x10 };
            const f32 f_1{ (f32)i_1 / 255.f };
            const u8  i_2{ (u8)(f_1 * 255.f) };
            DJO_UTEST(i_1 == i_2);

            // example from https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
            const f64 point2 = 0.2;
            const f64 sqrt5 = 1.0 / std::sqrt(5.0) / std::sqrt(5.0);
            DJO_UTEST(point2 != sqrt5);
            DJO_UTEST(Equals(point2, sqrt5));

            DJO_UTESTS_END();
        }
    }
}
