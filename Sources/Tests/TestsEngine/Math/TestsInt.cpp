
#include "Math/TestsMath.hpp"

#include <Math/Math.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        Int_All()
        {
            DJO_UTESTS_INIT();

            DJO_UTEST(NextPow2(0u) == 0);
            DJO_UTEST(NextPow2(1u) == 1);
            DJO_UTEST(NextPow2(2u) == 2);
            DJO_UTEST(NextPow2(13u) == 16);
            DJO_UTEST(NextPow2(64u) == 64);
            DJO_UTEST(NextPow2(256u) == 256);
            DJO_UTEST(NextPow2(257u) == 512);
            DJO_UTEST(NextPow2(5245u) == NextPow2(NextPow2(5245u)));

            DJO_UTESTS_END();
        }
    }
}
