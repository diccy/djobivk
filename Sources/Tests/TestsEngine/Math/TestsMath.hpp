#pragma once

#include "Tests.hpp"


namespace Djo
{
    namespace Tests
    {
        TestsResult TestsMath();

        TestsResult Int_All();
        TestsResult Float_All();
    }
}
