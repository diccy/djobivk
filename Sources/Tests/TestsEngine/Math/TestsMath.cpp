
#include "Math/TestsMath.hpp"


namespace Djo
{
    namespace Tests
    {
        TestsResult
        TestsMath()
        {
            TestsResult ret;
            ret += Int_All();
            ret += Float_All();

            return ret;
        }
    }
}
