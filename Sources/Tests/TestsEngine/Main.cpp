
#include "Core/TestsCore.hpp"
#include "Math/TestsMath.hpp"
#include "System/TestsSystem.hpp"
#include "Serialization/TestsSerialization.hpp"
#include "djon/TestsDjon.hpp"


int
main()
{
    using namespace Djo;

    SetupSystemMemoryLeakWatcher();

    if (Tests::Init() != EResult::OK)
        return -1;

    DJO_TESTS_LOG_INFO << "Tests start ----------------------------------------------------------------------------------" << std::endl;

    Tests::TestsResult ret;
    ret += Tests::TestsCore();
    ret += Tests::TestsMath();
    ret += Tests::TestsSystem();
    ret += Tests::TestsSerialization();
    ret += Tests::TestsDjon();

    DJO_TESTS_LOG_INFO << "Tests end ------------------------------------------------------------------------------------" << std::endl;
    DJO_TESTS_LOG_INFO << "Overall tests result: (" << ret.success << '/' << ret.count << ')' << std::endl;
    DJO_TESTS_LOG_INFO << "----------------------------------------------------------------------------------------------" << std::endl;

    Tests::Shutdown();

    return (ret.success == ret.count) ? 0 : -1;
}
