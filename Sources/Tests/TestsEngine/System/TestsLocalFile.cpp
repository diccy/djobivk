
#include "System/TestsSystem.hpp"

#include <System/LocalFileSystem.hpp>
#include <System/LocalFile.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        LocalFileSystem_Static()
        {
            DJO_UTESTS_INIT();

            DJO_UTEST(LocalFileSystem::DoesExist("Assets\\Tests\\TestsEngine\\System") == true);
            DJO_UTEST(LocalFileSystem::IsDirectory("Assets\\Tests\\TestsEngine\\System") == true);
            DJO_UTEST(LocalFileSystem::DoesExist("Assets\\Tests\\TestsEngine\\System\\") == true);
            DJO_UTEST(LocalFileSystem::IsDirectory("Assets\\Tests\\TestsEngine\\System\\") == true);
            DJO_UTEST(LocalFileSystem::DoesExist("Assets\\Tests\\TestsEngine\\System\\Fichier0.txt") == true);
            DJO_UTEST(LocalFileSystem::IsDirectory("Assets\\Tests\\TestsEngine\\System\\Fichier0.txt") == false);
            DJO_UTEST(LocalFileSystem::DoesExist("Assets\\Tests\\TestsEngine\\System\\existepas.txt") == false);
            DJO_UTEST(LocalFileSystem::IsDirectory("Assets\\Tests\\TestsEngine\\System\\existepas.txt") == false);
            DJO_UTEST(LocalFileSystem::DoesExist("Tests\\TestsEngine\\System\\Fichier0.txt") == false);
            DJO_UTEST(LocalFileSystem::IsDirectory("Tests\\TestsEngine\\System\\System") == false);
            DJO_UTEST(LocalFileSystem::IsDirectory("Tests\\TestsEngine\\System\\System\\") == false);

            Path p1{ "Assets\\Tests\\TestsEngine\\System\\Fichier0.txt" };
            Path p11;
            Path p2{ "Assets\\Tests\\..\\Tests\\TestsEngine\\.\\System\\Fichier0.txt" };
            Path p3{ p1 };
            LocalFileSystem::Normalize(&p1);
            LocalFileSystem::Normalize(p1, &p11);
            LocalFileSystem::Normalize(p2, &p2);
            LocalFileSystem::Normalize(p3.GetFullPath().c_str(), &p3);
            DJO_UTEST(p1.GetFullPath() == p11.GetFullPath());
            DJO_UTEST(p1.GetFullPath() == p2.GetFullPath());
            DJO_UTEST(p1.GetFullPath() == p3.GetFullPath());

            DJO_UTESTS_END();
        }

        TestsResult
        LocalFileSystem_Instance()
        {
            DJO_UTESTS_INIT();

            EResult result;

            LocalFileSystem fs1;
            result = fs1.Init(DJO_TESTS_FS->GetRootPath());
            DJO_UTEST(result == EResult::OK);

            Path p1 = fs1.GetRootPath();
            p1.Add("System\\Fichier0.txt");
            LocalFileSystem::Normalize(&p1);
            Path p2{ "System\\Fichier0.txt" };
            result = fs1.AbsolutePath(p2, &p2);
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(p1.GetFullPath() == p2.GetFullPath());

            LocalFileSystem fs2;
            result = fs2.Init(Path{ fs1.GetRootPath() }.Add("System\\"));
            DJO_UTEST(result == EResult::OK);

            result = fs1.AbsolutePath("System\\Fichier0.txt", &p1);
            DJO_UTEST(result == EResult::OK);
            result = fs2.AbsolutePath("Fichier0.txt", &p2);
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(p1.GetFullPath() == p2.GetFullPath());

            DJO_UTEST(fs1.AsPrettyString("System\\Fichier0.txt") == fs2.AsPrettyString("Fichier0.txt"));

            DJO_UTEST(fs1.DoesFileExist("System\\Fichier0.txt") == true);
            DJO_UTEST(fs1.DoesFileExist("System\\existepas.txt") == false);
            DJO_UTEST(fs1.DoesFileExist("System\\Dossier/Fichier0.txt") == true);
            DJO_UTEST(fs2.DoesFileExist("Fichier0.txt") == true);
            DJO_UTEST(fs2.DoesFileExist("toto.txt") == false);
            DJO_UTEST(fs2.DoesFileExist("Dossier\\Fichier0.txt") == true);

            DJO_UTESTS_END();
        }

        TestsResult
        LocalFile_Basics()
        {
            DJO_UTESTS_INIT();

            EResult result;
            sz size;

            LocalFile f0;
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);
            DJO_UTEST(f0.IsOpen() == false);
            DJO_UTEST(f0.ComputeSize() == c_invalidFileSize);
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);

            DJO_DISABLE_ASSERT_WHILE(
                result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/existepas.txt"), EValue(EOpenModeFlag::Read));
            );
            DJO_UTEST(result == EResult::FAIL);
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);
            DJO_UTEST(f0.IsOpen() == false);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Fichier0.txt"), EValue(EOpenModeFlag::Read));
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);
            DJO_UTEST(f0.IsOpen() == true);
            size = f0.ComputeSize();
            DJO_UTEST(size == 0);
            DJO_UTEST(f0.GetSize() == size);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Fichier0.txt"), EValue(EOpenModeFlag::Read));
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);
            DJO_UTEST(f0.IsOpen() == true);
            size = f0.ComputeSize();
            DJO_UTEST(size == 0);
            DJO_UTEST(f0.GetSize() == size);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Fichier_az.txt"), EValue(EOpenModeFlag::Read));
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);
            DJO_UTEST(f0.IsOpen() == true);
            size = f0.ComputeSize();
            DJO_UTEST(size == 26);
            DJO_UTEST(f0.GetSize() == size);

            std::string str;
            result = f0.Read(&str);
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(str == "abcdefghijklmnopqrstuvwxyz");

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Dossier/Fichier0.txt"), EOpenModeFlag::Write | EOpenModeFlag::Binary | EOpenModeFlag::Truncate);
            DJO_UTEST(result == EResult::OK);
            DJO_UTEST(f0.GetSize() == c_invalidFileSize);
            DJO_UTEST(f0.IsOpen() == true);
            size = f0.ComputeSize();
            DJO_UTEST(size == 0);
            DJO_UTEST(f0.GetSize() == size);

            result = f0.Write("lol", DoFlushPendingWritings{ true }); // TEST ERROR : flushing not sufficient to have ComputeSize() call returning 3
            DJO_UTEST(result == EResult::OK);
            size = f0.ComputeSize();
            DJO_UTEST(size == 3);
            DJO_UTEST(f0.GetSize() == size);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Dossier/Fichier0.txt"), EOpenModeFlag::Read | EOpenModeFlag::Binary);
            DJO_UTEST(result == EResult::OK);
            size = f0.ComputeSize();
            DJO_UTEST(size == 3);
            DJO_UTEST(f0.GetSize() == size);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Dossier/Fichier0.txt"), EValue(EOpenModeFlag::Write));
            DJO_UTEST(result == EResult::OK);
            DJO_DISABLE_ASSERT_WHILE(
                result = f0.Read(&str); // TEST ERROR : fstream can read files opened in write mode?
            );
            DJO_UTEST(result == EResult::FAIL);
            result = f0.Close();
            DJO_UTEST(result == EResult::OK);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Dossier/Fichier0.txt"), EValue(EOpenModeFlag::Read));
            DJO_UTEST(result == EResult::OK);
            DJO_DISABLE_ASSERT_WHILE(
                result = f0.Write("mdr");
            );
            DJO_UTEST(result == EResult::FAIL);
            result = f0.Close();
            DJO_UTEST(result == EResult::OK);

            result = f0.Open(DJO_TESTS_FS->AbsolutePath("System/Dossier/Fichier0.txt"), EOpenModeFlag::Write | EOpenModeFlag::Truncate);
            DJO_UTEST(result == EResult::OK);
            size = f0.ComputeSize();
            DJO_UTEST(size == 0);
            DJO_UTEST(f0.GetSize() == size);

            DJO_UTESTS_END();
        }

        TestsResult
        LocalFile_All()
        {
            TestsResult ret;
            ret += LocalFileSystem_Static();
            ret += LocalFileSystem_Instance();
            ret += LocalFile_Basics();

            return ret;
        }
    }
}
