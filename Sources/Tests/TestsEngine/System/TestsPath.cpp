
#include "System/TestsSystem.hpp"

#include <System/Path.hpp>


namespace Djo
{
    namespace Tests
    {
        TestsResult
        Path_All()
        {
            DJO_UTESTS_INIT();

            Djo::Path path;
            DJO_UTEST(path.IsEmpty());
            DJO_UTEST(path.GetLength() == 0);
            DJO_UTEST(path.GetFullPath() == "");
            DJO_UTEST(path.GetParentDirPath() == "");
            DJO_UTEST(path.GetFullName() == "");
            DJO_UTEST(path.GetName() == "");
            DJO_UTEST(path.GetExtension() == "");
            DJO_UTEST(path.IsDirectory() == false);

            path.Set("toto");
            DJO_UTEST(path.IsEmpty() == false);
            DJO_UTEST(path.GetLength() == 4);
            DJO_UTEST(path.GetFullPath() == "toto");
            DJO_UTEST(path.GetParentDirPath() == "");
            DJO_UTEST(path.GetFullName() == "toto");
            DJO_UTEST(path.GetName() == "toto");
            DJO_UTEST(path.GetExtension() == "");
            DJO_UTEST(path.IsDirectory() == false);

            path.Set("toto.txt");
            DJO_UTEST(path.IsEmpty() == false);
            DJO_UTEST(path.GetLength() == 8);
            DJO_UTEST(path.GetFullPath() == "toto.txt");
            DJO_UTEST(path.GetParentDirPath() == "");
            DJO_UTEST(path.GetFullName() == "toto.txt");
            DJO_UTEST(path.GetName() == "toto");
            DJO_UTEST(path.GetExtension() == ".txt");
            DJO_UTEST(path.IsDirectory() == false);

            path.Set("tata/");
            DJO_UTEST(path.IsEmpty() == false);
            DJO_UTEST(path.GetLength() == 5);
            DJO_UTEST(path.GetFullPath() == DJO_PATH_SEP("tata/"));
            DJO_UTEST(path.GetParentDirPath() == DJO_PATH_SEP(""));
            DJO_UTEST(path.GetFullName() == DJO_PATH_SEP("tata/"));
            DJO_UTEST(path.GetName() == "tata");
            DJO_UTEST(path.GetExtension() == DJO_PATH_SEP("/"));
            DJO_UTEST(path.IsDirectory() == true);

            path.Set("tata").Add("toto.txt");
            DJO_UTEST(path.IsEmpty() == false);
            DJO_UTEST(path.GetLength() == 13);
            DJO_UTEST(path.GetFullPath() == DJO_PATH_SEP("tata/toto.txt"));
            DJO_UTEST(path.GetParentDirPath() == DJO_PATH_SEP("tata/"));
            DJO_UTEST(path.GetFullName() == "toto.txt");
            DJO_UTEST(path.GetName() == "toto");
            DJO_UTEST(path.GetExtension() == ".txt");
            DJO_UTEST(path.IsDirectory() == false);

            path.Set("tata").Add("titi.titi").Add("tutu/");
            DJO_UTEST(path.IsEmpty() == false);
            DJO_UTEST(path.GetLength() == 20);
            DJO_UTEST(path.GetFullPath() == DJO_PATH_SEP("tata/titi.titi/tutu/"));
            DJO_UTEST(path.GetParentDirPath() == DJO_PATH_SEP("tata/titi.titi/"));
            DJO_UTEST(path.GetFullName() == DJO_PATH_SEP("tutu/"));
            DJO_UTEST(path.GetName() == "tutu");
            DJO_UTEST(path.GetExtension() == DJO_PATH_SEP("/"));
            DJO_UTEST(path.IsDirectory() == true);

            path.Add("toto.txt");
            DJO_UTEST(path.IsEmpty() == false);
            DJO_UTEST(path.GetLength() == 28);
            DJO_UTEST(path.GetFullPath() == DJO_PATH_SEP("tata/titi.titi/tutu/toto.txt"));
            DJO_UTEST(path.GetParentDirPath() == DJO_PATH_SEP("tata/titi.titi/tutu/"));
            DJO_UTEST(path.GetFullName() == "toto.txt");
            DJO_UTEST(path.GetName() == "toto");
            DJO_UTEST(path.GetExtension() == ".txt");
            DJO_UTEST(path.IsDirectory() == false);

            struct TestAndResult { const char* t; const char* r; }; // storing DJO_PATH_SEP result in const char* is an issue in Release
            const TestAndResult testsAndResults[]
            {
                { ".",                                   "" },
                { "..",                                  "" },
                { "./",                                  "" },
                { "../",                                 "" },
                { "../././.././../toto/./././../.././",  "" },
                { "toto/............../tutu/",           DJO_PATH_SEP("toto/............../tutu/") }, // nonsense? what to do with this? windows forbids it
                { "tata////titi\\\\tutu///",             DJO_PATH_SEP("tata/titi/tutu/") },
                { "./tata/titi\\./tutu/../",             DJO_PATH_SEP("tata/titi/") },
                { "titi/tata./toto",                     DJO_PATH_SEP("titi/tata./toto") }, // is it even possible on windows/*nix? what to do with this? windows ignores and trashes them
                { "titi/tata../toto",                    DJO_PATH_SEP("titi/tata../toto") }, // is it even possible on windows/*nix? what to do with this? windows ignores and trashes them
            };

            std::string s1, s2;
            for (const TestAndResult& testAndResult : testsAndResults)
            {
                path.Set(testAndResult.t);
                Path::Normalize(testAndResult.t, &s1);
                Path::Normalize(StringView{ testAndResult.t }, &s2);
                DJO_UTEST(path.GetFullPath() == testAndResult.r);
                DJO_UTEST(path.GetFullPath() == s1);
                DJO_UTEST(path.GetFullPath() == s2);
            }

            path.Set("..").Add("tata").Add("titi").Add(".").Add("tutu").Add("..").Add("toto.txt");
            DJO_UTEST(path.GetFullPath() == DJO_PATH_SEP("tata/titi/toto.txt"));

            DJO_UTESTS_END();


            // toto/tata/./tutu/../lol
            // toto/tata/lol
        }
    }
}
