#pragma once

#include "Tests.hpp"


namespace Djo
{
    namespace Tests
    {
        TestsResult TestsSystem();

        TestsResult Path_All();
        TestsResult LocalFile_All();
        TestsResult VirtualFileSystem_All();
    }
}
