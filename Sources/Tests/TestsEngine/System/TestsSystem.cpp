
#include "System/TestsSystem.hpp"


namespace Djo
{
    namespace Tests
    {
        TestsResult
        TestsSystem()
        {
            TestsResult ret;
            ret += Path_All();
            ret += LocalFile_All();
            ret += VirtualFileSystem_All();

            return ret;
        }
    }
}
