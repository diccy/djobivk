
#include "Tests.hpp"

#include <Core/GlobalLog.hpp>
#include <System/StringSinks.hpp>


namespace Djo
{
    namespace Tests
    {
        Logger* g_testsLogger{ nullptr };
        LocalFileSystem* g_testsFileSystem{ nullptr };

        EResult
        Init()
        {
            DJO_OK_OR_RETURN(GlobalLog::Init());

            g_testsLogger = new Logger{ "Tests" };

            const ShPtr<IStringSink> stdCoutSink{ new StringSinkStdCout{} };
            const ShPtr<IStringSink> vsConsoleOutputSink{ new StringSinkVisualConsoleOutput{} };
            const ShPtr<IStringSink> coreLogFileSink{ new StringSinkFile{ "Log_Tests.txt" } };
            if (!((const StringSinkFile*)coreLogFileSink.Get())->GetFile().IsOpen())
                return MakeResult(EResult::FAIL, "Can't init Tests' log sink file");

            g_testsLogger->AddSink(stdCoutSink);
            g_testsLogger->AddSink(vsConsoleOutputSink);
            g_testsLogger->AddSink(coreLogFileSink);

            g_testsFileSystem = new LocalFileSystem{};
            DJO_OK_OR_RETURN(g_testsFileSystem->Init("Assets/Tests/TestsEngine/"));

            return EResult::OK;
        }

        void
        Shutdown()
        {
            delete g_testsFileSystem;
            delete g_testsLogger;
            g_testsFileSystem = nullptr;
            g_testsLogger = nullptr;

            GlobalLog::Shutdown();
        }

        void
        FailLog(const char* const _filePath, const char* const _expr, const s32 _line, const char* const _function)
        {
            DJO_TESTS_LOG_ERROR << "Test: Failed: " << _filePath << ", " << _line << ", " << _function << '\n';
            DJO_TESTS_LOG_ERROR << "    " << _expr << std::endl;
        }

        void
        EndLog(const char* const _function, const TestsResult& _res)
        {
            if (_res.count == _res.success)
            {
                DJO_TESTS_LOG_INFO <<  "Tests: Success: " << _function << " (" << _res.success << '/' << _res.count << ')' << std::endl;
            }
            else
            {
                DJO_TESTS_LOG_ERROR << "Tests: Failed:  " << _function << " | " << _res.count - _res.success << " failed tests (" << _res.success << '/' << _res.count << ')' << std::endl;
            }
        }
    }
}
