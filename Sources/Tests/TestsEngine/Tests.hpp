#pragma once

#include <Core/Core.hpp>
#include <Core/Types.hpp>
#include <Core/Logger.hpp>
#include <System/LocalFileSystem.hpp>


//#define DJO_ENABLE_BREAK_AT_ASSERT // uncomment this to enable break on failed DJO_UTEST
#if defined(DJO_ENABLE_BREAK_AT_ASSERT)

    #define DJO_TESTS_FAILED_ASSERTION     DJO_FAILED_ASSERTION

#else

    #define DJO_TESTS_FAILED_ASSERTION

#endif

namespace Djo
{
    namespace Tests
    {
        struct TestsResult
        {
            u32 success{ 0 };
            u32 count{ 0 };

            DJO_INLINE TestsResult operator + (const TestsResult& _rhs) const  { return TestsResult{ success + _rhs.success, count + _rhs.count }; }
            DJO_INLINE TestsResult& operator += (const TestsResult& _rhs)      { success += _rhs.success; count += _rhs.count; return *this; }
        };

        extern Logger* g_testsLogger;
        extern LocalFileSystem* g_testsFileSystem;

        EResult Init();
        void Shutdown();
        void FailLog(const char* _filePath, const char* _expr, s32 _line, const char* _function);
        void EndLog(const char* _function, const TestsResult& _res);
    }
}

// Tests main macros

#define DJO_UTESTS_INIT()  ::Djo::Tests::TestsResult res__{}

#define DJO_UTEST(expr_)\
{\
    const bool utest__ = (expr_);\
    ++res__.count;\
    if (utest__)\
    {\
        ++res__.success;\
    }\
    else\
    {\
        ::Djo::Tests::FailLog(__FILE__, DJO_STR(expr_), __LINE__, __FUNCTION__);\
        DJO_TESTS_FAILED_ASSERTION(false);\
    }\
}

#define DJO_UTESTS_LAMBDA_CAPTURE  &res__

#define DJO_UTESTS_END()\
{\
    ::Djo::Tests::EndLog(__FUNCTION__, res__);\
    return res__;\
}

// Tests log macros

#define DJO_TESTS_LOG_TRACE  ::Djo::Tests::g_testsLogger->StartLogging(::Djo::ELogLevel::Trace)
#define DJO_TESTS_LOG_INFO   ::Djo::Tests::g_testsLogger->StartLogging(::Djo::ELogLevel::Info)
#define DJO_TESTS_LOG_WARN   ::Djo::Tests::g_testsLogger->StartLogging(::Djo::ELogLevel::Warning)
#define DJO_TESTS_LOG_ERROR  ::Djo::Tests::g_testsLogger->StartLogging(::Djo::ELogLevel::Error)

// Tests file system macros

#define DJO_TESTS_FS  ::Djo::Tests::g_testsFileSystem
