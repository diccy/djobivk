
#include <Engine/EngineApplication.hpp>
#include <Engine/Engine.hpp>
#include <Game/CameraController.hpp>
#include <Graphics/Renderer.hpp>
#include <Graphics/Camera.hpp>
#include <Graphics/ObjLoader.hpp>
#include <ImGui/IImGuiDebugWindowImpl.hpp>
#include <ImGui/ImGuiManager.hpp>
#include <ImGui/ImGui.hpp>
#include <Window/Window.hpp>
#include <System/GlobalFS.hpp>


using namespace Djo;

// fw
class VulkanTutoApplication;

class ImGuiDebugWindow_VulkanTutoApplication
    : public IImGuiDebugWindowImpl
{
    DJO_NO_DEFAULT_CTOR(ImGuiDebugWindow_VulkanTutoApplication);
    DJO_NO_COPY_NO_MOVE(ImGuiDebugWindow_VulkanTutoApplication);

public:

    static const char* s_menuName;
    static const char* s_windowName;

    ImGuiDebugWindow_VulkanTutoApplication(VulkanTutoApplication& _app);
    ~ImGuiDebugWindow_VulkanTutoApplication();

    virtual const char* GetMenuName() const override;
    virtual void UpdateImGuiMenuItem() override;
    virtual void UpdateImGuiWindow() override;

private:

    VulkanTutoApplication& m_app;
    bool m_isWindowOpen;
};

class VulkanTutoApplication
    : public EngineApplication
{
public:

    VulkanTutoApplication()
        : EngineApplication{}
        , m_scene{ nullptr }
        , m_camera{ nullptr }
        , m_camController{}
        , m_imGuiDebugWindow{ nullptr }
        , m_showImGuiApp{ true }
    {}

    virtual
    EResult
    OnAfterInit() override
    {
        m_imGuiDebugWindow.Reset(new ImGuiDebugWindow_VulkanTutoApplication{ *this });
        GetImGuiManager().RegisterMainMenuBarItem(m_imGuiDebugWindow.Get());
        GetImGuiManager().RegisterWindow(m_imGuiDebugWindow.Get());

        m_scene.Reset(new SceneNode{});

        m_camera.Reset(new Camera{});
        m_camera->SetFieldOfView(glm::radians(60.f));
        m_camera->SetNear(0.1f);
        m_camera->SetFar(1000.f);
        m_camera->SetRatio(GetWindow().GetFrameBufferRatio());
        m_camController.SetCamera(m_camera.Get());
        m_camController.SetCameraWorldPos({ 5.f, 5.f, 0.5f });
        m_camController.SetTargetWorldPos({ 0.f, 3.f, 0.f });
        m_camController.SetMoveSpeed(3.f);

        /*
        ObjLoader objLoader;
        ObjLoader::Params objLoaderParams;
        objLoaderParams.fsp = FSPath{ "Models/sponza/sponza.obj", DJO_APP_FS };
        EResult result = objLoader.Load(objLoaderParams);
        if (result != EResult::OK || objLoader.GetMeshes().empty())
        {
            return result;
        }

        for (const ObjLoader::Mesh& objLoaderMesh : objLoader.GetMeshes())
        {
            Mesh loadedMesh{};
            const PropertySemanticsArray& loadedLayoutSemantics = objLoaderMesh.layoutSemantics;
            const MeshData& loadedMeshData = objLoaderMesh.data;
            const BufferLayout& loadedBufferLayout = loadedMeshData.vertexLayout;

            MaxArray<sz, BufferLayout::c_maxPropertyCount> offsets{};
            for (const ELayoutPropertySemantic outSemantic : {
                ELayoutPropertySemantic::Position,
                    ELayoutPropertySemantic::Normal,
                    ELayoutPropertySemantic::Normal
            })
            {
                for (u32 i = 0; i < loadedLayoutSemantics.GetSize(); ++i)
                {
                    const ELayoutPropertySemantic inSemantic = loadedLayoutSemantics[i];
                    if (inSemantic == outSemantic)
                    {
                        offsets.PushBack(loadedBufferLayout.GetProperties()[i].offset / sizeof(f32));
                        DJO_ASSERT(loadedBufferLayout.GetProperties()[i].size == (3 * sizeof(f32)));
                        break;
                    }
                }
            }

            const u32 vertCount = (u32)loadedMeshData.indices.size();
            loadedMesh.vertices.resize(vertCount);
            const u32 loadedVertexFloatStride = loadedBufferLayout.GetStride() / sizeof(f32);
            for (u32 i = 0; i < vertCount; ++i)
            {
                Vertex& vert = loadedMesh.vertices[i];
                const f32* v = &loadedMeshData.vertices[loadedMeshData.indices[i] * loadedVertexFloatStride];
                vert.pos = *reinterpret_cast<const Vec3f*>(v + offsets[0]);
                vert.normal = *reinterpret_cast<const Vec3f*>(v + offsets[1]);
                vert.color = *reinterpret_cast<const Vec3f*>(v + offsets[2]);
            }
        }
        */

        return EResult::OK;
    }

    virtual
    void
    OnBeforeDestroy() override
    {
        m_camController.SetCamera(nullptr);
        m_camera.Release();

        m_scene.Release();

        GetImGuiManager().UnregisterWindow(m_imGuiDebugWindow.Get());
        GetImGuiManager().UnregisterMainMenuBarItem(m_imGuiDebugWindow.Get());
        m_imGuiDebugWindow.Release();
    }

    virtual
    void
    OnWindowEvent(const WindowEvent& _event) override
    {
        switch (_event.type)
        {
            case WindowEvent::EType::KeyPressed:
            {
                if (_event.key.code == EKeyboardKey::Escape)
                {
                    Shutdown();
                }
                else if (_event.key.code == EKeyboardKey::A)
                {
                    m_showImGuiApp = true;
                }
                break;
            }
            case WindowEvent::EType::FrameBufferResized:
            {
                break;
            }
        }
    }

    virtual
    void
    OnUpdate(const Milliseconds64 _dt) override
    {
        m_camController.Update(_dt);

        GetRenderer().SetCamera(m_camera.Get());
    }

    virtual
    void
    OnImGuiUpdate()
    {
        if (m_showImGuiApp)
        {
            if (::ImGui::Begin("Vulkan Tuto Application", &m_showImGuiApp))
            {
            }
            ::ImGui::End();
        }
    }

private:

    friend class ImGuiDebugWindow_VulkanTutoApplication;

    ShPtr<SceneNode> m_scene;
    ShPtr<Camera> m_camera;
    CameraController m_camController;
    UPtr<ImGuiDebugWindow_VulkanTutoApplication> m_imGuiDebugWindow;
    bool m_showImGuiApp;
};


const char* ImGuiDebugWindow_VulkanTutoApplication::s_menuName{ "Application" };
const char* ImGuiDebugWindow_VulkanTutoApplication::s_windowName{ "Debug VulkanTuto Application" };

ImGuiDebugWindow_VulkanTutoApplication::ImGuiDebugWindow_VulkanTutoApplication(VulkanTutoApplication& _app)
    : m_app{ _app }
    , m_isWindowOpen{ false }
{
}

ImGuiDebugWindow_VulkanTutoApplication::~ImGuiDebugWindow_VulkanTutoApplication()
{
}

const char*
ImGuiDebugWindow_VulkanTutoApplication::GetMenuName() const
{
    return s_menuName;
}

void
ImGuiDebugWindow_VulkanTutoApplication::UpdateImGuiMenuItem()
{
    ::ImGui::MenuItem(s_windowName, nullptr, &m_isWindowOpen);
}

void
ImGuiDebugWindow_VulkanTutoApplication::UpdateImGuiWindow()
{
    if (m_isWindowOpen)
    {
        if (::ImGui::Begin(s_windowName, &m_isWindowOpen))
        {
            if (::ImGui::TreeNode("CameraController"))
            {
                ImGuiDebugWidget(m_app.m_camController);
                ::ImGui::TreePop();
            }
        }
        ::ImGui::End();
    }
}


int
main()
{
    SetupSystemMemoryLeakWatcher();

    {
        EngineApplication::InitDesc appInitDesc;
        appInitDesc.appAssetsPath = "Games/VulkanTuto/";
        appInitDesc.windowName = "Vulkan tuto";
        appInitDesc.windowInitSize = Vec2i{ 1080, 720 };
        Engine::Run<VulkanTutoApplication>(appInitDesc);
    }

    return 0;
}
