#version 460
#pragma shader_stage(fragment)

layout (location = 0) in vec3 i_color;
layout (location = 1) in vec3 i_normal;

layout (set = 0, binding = 1) uniform SceneData
{
    vec4 fogColor; // w for exponent
    vec4 fogDistances; // x min, y max, zw unused
    vec4 ambientColor;
    vec4 sunlightDirection; // w sun power
    vec4 sunlightColor;

} u_sceneData;

layout (location = 0) out vec4 o_fragColor;


void main()
{
	o_fragColor = vec4(i_normal, 1.0);
}
