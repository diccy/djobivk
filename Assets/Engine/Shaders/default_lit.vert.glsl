#version 460
#pragma shader_stage(vertex)

layout (location = 0) in vec3 i_position;
layout (location = 1) in vec3 i_normal;
layout (location = 2) in vec3 i_color;

layout (set = 0, binding = 0) uniform ViewContext
{
    mat4 view;
    mat4 proj;
    mat4 viewProj;

} u_viewContext;

struct ObjectData
{
    mat4 transform;
};
layout (std140, set = 1, binding = 0) readonly buffer ObjectsBuffer
{
    ObjectData objects[];

} u_objectsBuffer;

layout (location = 0) out vec3 o_color;
layout (location = 1) out vec3 o_normal;

void main()
{
    mat4 objectTransform = u_objectsBuffer.objects[gl_BaseInstance].transform;
    mat4 mvp = u_viewContext.viewProj * objectTransform;
	gl_Position = mvp * vec4(i_position, 1.0f);
	o_color = i_color;
	o_normal = i_normal;
}
