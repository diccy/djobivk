#version 450
#pragma shader_stage(fragment)

layout (location = 0) in vec3 i_color;

layout (location = 0) out vec4 o_fragColor;


void main()
{
	o_fragColor = vec4(i_color, 1.0);
}
