#version 450
#pragma shader_stage(vertex)

layout (location = 0) in vec3 i_position;
layout (location = 1) in vec3 i_normal;
layout (location = 2) in vec3 i_color;

layout (set = 0, binding = 0) uniform ViewContext
{
    mat4 view;
    mat4 proj;
    mat4 viewProj;

} u_viewContext;

layout( push_constant ) uniform Constants
{
	mat4 model;

} c_const;

layout (location = 0) out vec3 o_color;

void main()
{
    mat4 mvp = u_viewContext.viewProj * c_const.model;
	gl_Position = mvp * vec4(i_position, 1.0);
	o_color = i_color;
}
