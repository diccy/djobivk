
import PythonUtils as Djo
import subprocess
from pathlib import Path
import glob
from time import time
import sys
import os

def GetGlslc():
    vkSdkPath = os.environ.get('VK_SDK_PATH')
    if not vkSdkPath:
        Djo.PrintError('Vulkan SDK not installed')
        return None
    Djo.PrintInfo(f'VK_SDK_PATH : {vkSdkPath}'.format())

    glslc = vkSdkPath + '/Bin/glslc.exe'
    if not os.path.exists(glslc):
        Djo.PrintError(f'Can\'t find glslc.exe (tried {glslc})'.format())
        return None
    Djo.PrintInfo(f'glslc : {glslc}'.format())

    return glslc

def CompileShadersDir(_dir):
    Djo.PrintInfo('Compile shaders process')

    if not os.path.isdir(_dir):
        Djo.PrintError(f'Compiling directory does not exist : {_dir}'.format())
        return False
    Djo.PrintInfo(f'Directory : {_dir}'.format())

    glslc = GetGlslc()

    Djo.PrintInfo('Compiling...')

    startTime = time()

    compilationCount = 0
    successfulCompilationCount = 0
    searchBase = _dir + '**/*'
    for file in glob.iglob(searchBase + '.glsl', recursive = True):
        compilationCount += 1
        if Djo.GlslcCommand(glslc, file):
            successfulCompilationCount += 1

    endTime = time()

    resultStr = f'Successful compilations : {successfulCompilationCount}/{compilationCount}'.format()
    if successfulCompilationCount == compilationCount:
        Djo.PrintStyle(resultStr, Djo.TXTSTYLE_GREEN)
    else:
        Djo.PrintError(resultStr)

    elapsedTime = endTime - startTime
    Djo.PrintInfo(f'Compilation time: {elapsedTime:.3f}s')

    return True

def CompileShaders(*argv): #unused argv for now
    rootDir = str(Path(__file__).parent.absolute().as_posix()) + '/Assets/'
    return CompileShadersDir(rootDir)

# #############################################################################

if __name__ == '__main__':
    CompileShaders(sys.argv)
    input('\nPress Enter to close...')
