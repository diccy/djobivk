
import PythonUtils as Djo
from pathlib import Path
import os

Djo.CONFIG_VERBOSE = True

GLOBAL_INI_FILE_NAME = 'Setup_Global.ini'
EXTERNAL_INI_FILE_NAME = 'Setup_External.ini'

ROOT_DIR = str(Path(__file__).parent.absolute().as_posix()) + '/'
EXTERNAL_DIR = ROOT_DIR + 'External/'
TMP_DIR = EXTERNAL_DIR + '_tmp/'
PREBUILD_DIR = ROOT_DIR + 'Prebuild/'
PREBUILD_EXTERNAL_DIR = PREBUILD_DIR + 'ExternalLibs/'
PREMAKE = PREBUILD_DIR + 'premake5.exe'

GLOBAL_INI = Djo.ReadMandatoryIniFile(ROOT_DIR + GLOBAL_INI_FILE_NAME)
EXTERNAL_INI = Djo.ReadMandatoryIniFile(ROOT_DIR + EXTERNAL_INI_FILE_NAME)

PREMAKE_TARGET = Djo.GetMandatoryIniValue(GLOBAL_INI, 'Premake', 'target')
MSBUILD = Djo.GetMandatoryIniValue(GLOBAL_INI, 'MSBuild', 'path')

# Setup directories
Djo.CreateFolder(EXTERNAL_DIR)
Djo.CreateFolder(TMP_DIR)

# Utils #######################################################################

def DownloadExternalInTmp(_url, _name):
    return Djo.DownloadUrl(_url, TMP_DIR + _name)

def UnzipExternalInTmp(_name):
    return Djo.UnzipFile(TMP_DIR + _name + '.zip')

def DownloadAndUnzipInTmp(_section, _baseName):
    url = EXTERNAL_INI.GetValue(_section, 'url')
    if not url or \
       not DownloadExternalInTmp(url, _baseName + '.zip') or \
       not UnzipExternalInTmp(_baseName):
        return False
    return True

def PrebuildExternal(_section, _rootDir, _args = ''):
    file = PREBUILD_EXTERNAL_DIR + _section + 'Prebuild.lua'
    return Djo.PremakeCommand(PREMAKE, PREMAKE_TARGET, file, f'--rootDir="{_rootDir}" '.format() + _args)

def BuildExternal(_rootDir, _baseName):
    buildDir = _rootDir + 'build/'
    sln = buildDir + _baseName + '.sln'
    proj = buildDir + _baseName + '.vcxproj'
    if not Djo.MsBuildCommand(MSBUILD, sln, '-t:clean') or \
       not Djo.MsBuildCommand(MSBUILD, proj, '-t:build -p:Configuration="Release";Platform="x64"') or \
       not Djo.MsBuildCommand(MSBUILD, proj, '-t:build -p:Configuration="Debug";Platform="x64"'):
        return False
    return True

ROBOCOPY_ARGS_FORCE_COPY = ' /IS /IT /IM'
ROBOCOPY_HEADER_FILES = '*.h *.hpp *.inl'
ROBOCOPY_LIB_FILES = '*.pdb *.exp *.lib'

def CopyFiles(_sourceDir, _destDir, _files, _args = ''):
    return Djo.RobocopyCommand(_sourceDir, _destDir, _files, _args + ' /purge' + ROBOCOPY_ARGS_FORCE_COPY)

def CopyIncludes(_sourceDir, _destDir, _args = ''):
    return CopyFiles(_sourceDir, _destDir + 'include/', ROBOCOPY_HEADER_FILES, _args)

def CopyLibs(_sourceDir, _destDir, _args = ''):
    return CopyFiles(_sourceDir, _destDir + 'lib/', ROBOCOPY_LIB_FILES, _args)


# GLFW ########################################################################

def ManageGLFW():
    section = 'GLFW'
    baseName = 'glfw'
    baseDir = baseName + '/'

    if not DownloadAndUnzipInTmp(section, baseName):
        return False

    rootDir = TMP_DIR + baseDir
    rootDir = next(os.scandir(rootDir)).path + '/'
    if not PrebuildExternal(section, rootDir) or \
       not BuildExternal(rootDir, baseName):
        return False

    includeDir = rootDir + 'include/'
    libsDir = rootDir + 'build/lib/'
    destDir = EXTERNAL_DIR + baseDir
    Djo.DeleteFolder(destDir)
    if not CopyIncludes(includeDir, destDir, '/s') or \
       not CopyLibs(libsDir, destDir):
        return False

    return True


# GLM #########################################################################

def ManageGLM():
    section = 'GLM'
    baseName = 'glm'
    baseDir = baseName + '/'

    if not DownloadAndUnzipInTmp(section, baseName):
        return False

    rootDir = TMP_DIR + baseDir
    rootDir = next(os.scandir(rootDir)).path + '/'
    includeDir = rootDir
    destDir = EXTERNAL_DIR + baseDir
    Djo.DeleteFolder(destDir)
    if not CopyIncludes(includeDir, destDir, '/s'):
        return False

    return True


# Dear ImGui #################################################################

def ManageDearImGui():
    section = 'DearImGui'
    baseName = 'imgui'
    baseDir = baseName + '/'

    if not DownloadAndUnzipInTmp(section, baseName):
        return False

    rootDir = TMP_DIR + baseDir
    rootDir = next(os.scandir(rootDir)).path + '/'
    if not PrebuildExternal(section, rootDir) or \
       not BuildExternal(rootDir, baseName):
        return False

    includeDir = rootDir
    libsDir = rootDir + 'build/lib/'
    destDir = EXTERNAL_DIR + baseDir
    Djo.DeleteFolder(destDir)
    backendsDir = rootDir + 'backends/'
    backendsHeaders = 'imgui_impl_glfw.h imgui_impl_vulkan.h'
    backendsImpl = 'imgui_impl_glfw.cpp imgui_impl_vulkan.cpp'
    if not CopyIncludes(includeDir, destDir) or \
       not CopyFiles(backendsDir, destDir + 'include/', backendsHeaders) or \
       not CopyFiles(backendsDir, destDir + 'src/', backendsImpl) or \
       not CopyLibs(libsDir, destDir):
        return False

    return True


# Stb image ###################################################################

def ManageStb():
    section = 'Stb'
    baseName = 'stb'
    baseDir = baseName + '/'

    rootDir = TMP_DIR + baseDir
    Djo.CreateFolder(rootDir)

    url = EXTERNAL_INI.GetValue(section, 'url')
    if not url or \
       not DownloadExternalInTmp(url, 'stb_image.h') or \
       not CopyFiles(TMP_DIR, rootDir, 'stb_image.h'):
        return False

    if not CopyFiles(PREBUILD_EXTERNAL_DIR, rootDir, 'stb_image.c') or \
       not PrebuildExternal(section, rootDir) or \
       not BuildExternal(rootDir, baseName):
        return False

    includeDir = rootDir
    libsDir = rootDir + 'build/lib/'
    destDir = EXTERNAL_DIR + baseDir
    Djo.DeleteFolder(destDir)
    if not CopyIncludes(includeDir, destDir) or \
       not CopyLibs(libsDir, destDir):
        return False

    return True


# Vulkan ######################################################################

def ManageVulkan():

    def _CheckVulkanSDK():
        vkSdkPath = os.environ.get('VK_SDK_PATH')
        vulkanSdk = os.environ.get('VULKAN_SDK')
        Djo.PrintInfo(f'VK_SDK_PATH = {vkSdkPath}'.format())
        Djo.PrintInfo(f'VULKAN_SDK = {vulkanSdk}'.format())
        if not vkSdkPath and not vulkanSdk:
            Djo.PrintError('Vulkan SDK not installed')
            return False
        return True

    def _ManageVulkanMemoryAllocator():
        section = 'VulkanMemoryAllocator'
        baseName = 'vma'
        baseDir = baseName + '/'
        
        if not DownloadAndUnzipInTmp(section, baseName):
            return False
            
        rootDir = TMP_DIR + baseDir
        rootDir = next(os.scandir(rootDir)).path + '/'
        includeDir = rootDir
        destDir = EXTERNAL_DIR + baseDir
        Djo.DeleteFolder(destDir)
        if not CopyFiles(includeDir + 'include/', destDir + 'include/vma/', 'vk_mem_alloc.h'):
            return False
        return True

    if not _CheckVulkanSDK() or \
       not _ManageVulkanMemoryAllocator():
        return False

    return True


# Delete tmp folder ###########################################################

def DeleteTmpFolder():
    Djo.DeleteFolder(TMP_DIR)
    return True


# #############################################################################

def ManageExternal():
    menu = Djo.Menu('External dependencies management')
    menu.AddAction(Djo.Action('GLFW', ManageGLFW))
    menu.AddAction(Djo.Action('GLM', ManageGLM))
    menu.AddAction(Djo.Action('ImGui', ManageDearImGui))
    menu.AddAction(Djo.Action('Stb', ManageStb))
    menu.AddAction(Djo.Action('Vulkan', ManageVulkan))
    menu.AddAction(Djo.Action('Delete temporary folder', DeleteTmpFolder))
    menu.DisplayLoop()


# #############################################################################

if __name__ == '__main__':
    ManageExternal()
