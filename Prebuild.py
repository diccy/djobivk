
import PythonUtils as Djo
from pathlib import Path

Djo.CONFIG_VERBOSE = True

GLOBAL_INI_FILE_NAME = 'Setup_Global.ini'

ROOT_DIR = str(Path(__file__).parent.absolute().as_posix()) + '/'
PREBUILD_DIR = ROOT_DIR + 'Prebuild/'
PREMAKE = PREBUILD_DIR + 'premake5.exe'

GLOBAL_INI = Djo.ReadMandatoryIniFile(ROOT_DIR + GLOBAL_INI_FILE_NAME)
PREMAKE_TARGET = Djo.GetMandatoryIniValue(GLOBAL_INI, 'Premake', 'target')

if __name__ == '__main__':
    Djo.PrintInfo('Prebuilding Djobi Engine:\n')
    Djo.PremakeCommand(PREMAKE, PREMAKE_TARGET, PREBUILD_DIR + 'All.lua')
    input('\nPress Enter to finish...')
